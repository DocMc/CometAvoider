﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BackgroundScroll
struct BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519;
// Block
struct Block_t613E03B6725E5824DD83D0B230AC258E08338D24;
// BuyButton
struct BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B;
// BuyingLives
struct BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5;
// DeathMenu
struct DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124;
// Engine
struct Engine_t62C213B195B780D1183D68751B5F0118D702848D;
// Explosion
struct Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA;
// GameManager
struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89;
// IAPDemo
struct IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92;
// IAPDemo/UnityChannelLoginHandler
struct UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E;
// IAPDemo/UnityChannelPurchaseInfo
struct UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9;
// IAPManager
struct IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A;
// LevelFader
struct LevelFader_t80A930EAED182822655C69B819748C49C59A31A5;
// Player
struct Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873;
// Sound[]
struct SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B;
// Spawner
struct Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_tCF5AFC4513076B0E4A471BEF3A11BFBBDF57F857;
// System.Action`1<UnityEngine.Store.UserInfo>
struct Action_1_t3A20718901FDC69E02C6ED48CF5A04A428B89027;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI>
struct Dictionary_2_t86FD27876E6B6061449915AF4C4EE31BE83977FA;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t6AF508DE18DA398DBB91330BEEB14B0CFBD4A8ED;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>
struct IDictionary_2_t028F1617B4A810A549A995B8BE3281124D2E7FBA;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct IDictionary_2_t23497D69F526B4556DC679CBA0F91D0F85872FB0;
// System.Collections.Generic.IDictionary`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t1EF905ACAD3F0F545FE06225B68811E0166BC26D;
// System.Collections.Generic.Queue`1<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>>
struct Queue_1_tBE60FA9EC7CEAC6A2101CA6BF8E09035C8DA2F3E;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs>
struct EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927;
// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>
struct EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226;
// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs>
struct EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A;
// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs>
struct EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Net.WebRequest
struct WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.Advertisements.Advertisement/<Show>c__AnonStorey0
struct U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4;
// UnityEngine.Advertisements.CallbackExecutor
struct CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78;
// UnityEngine.Advertisements.Editor.Configuration
struct Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B;
// UnityEngine.Advertisements.Editor.Placeholder
struct Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244;
// UnityEngine.Advertisements.Editor.Platform
struct Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740;
// UnityEngine.Advertisements.IPlatform
struct IPlatform_t641848859ED10CAE8664D5746AABD9243D5DEA45;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t6637C67D4907C169839DA10D810B696A19941904;
// UnityEngine.Advertisements.SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_tE452A33F7FCE60A3E60819983B00BF48EF1E5DC0;
// UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E;
// UnityEngine.Advertisements.iOS.Platform/<UnityAdsDidFinish>c__AnonStorey3
struct U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1;
// UnityEngine.Advertisements.iOS.Platform/unityAdsDidError
struct unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61;
// UnityEngine.Advertisements.iOS.Platform/unityAdsDidFinish
struct unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045;
// UnityEngine.Advertisements.iOS.Platform/unityAdsDidStart
struct unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA;
// UnityEngine.Advertisements.iOS.Platform/unityAdsReady
struct unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.ConstantForce2D
struct ConstantForce2D_t7EF90AB9059BC8CDD84C1699757F3AD9F8FB8C0B;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE;
// UnityEngine.Purchasing.IAppleExtensions
struct IAppleExtensions_t2CA069FC8325FC7D40686B4B5A97F4C59F04862D;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t4AEE9698E20BA2DCA32EC4729DCAA60D09DA95CA;
// UnityEngine.Purchasing.IGooglePlayStoreExtensions
struct IGooglePlayStoreExtensions_t63389521830FA70C30406A8F87D21C40052225D7;
// UnityEngine.Purchasing.IMicrosoftExtensions
struct IMicrosoftExtensions_t1A1F62E0DC5DEA1F53AB3D188AF9881664EA33C7;
// UnityEngine.Purchasing.IMoolahExtension
struct IMoolahExtension_t25225BB8DAC584877D68380CF25F4CDE11780ED6;
// UnityEngine.Purchasing.ISamsungAppsExtensions
struct ISamsungAppsExtensions_tCEF8B12CB0B195903BE3010FB69E6FBB1D73B999;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t6A30FF7F81804FCC0082AB9DF2CE86BCFB91DF67;
// UnityEngine.Purchasing.ITransactionHistoryExtensions
struct ITransactionHistoryExtensions_t0E1959DD322B49FB17BA98929B58B163D47C5C98;
// UnityEngine.Purchasing.IUnityChannelExtensions
struct IUnityChannelExtensions_tB969072882C86BBD517B829999B6B1715CE0FF04;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#define U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_TF54F3CFD46E387BF9588D963503C85101F5C63CD_H
#define U3CU3EC__DISPLAYCLASS3_0_TF54F3CFD46E387BF9588D963503C85101F5C63CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_tF54F3CFD46E387BF9588D963503C85101F5C63CD  : public RuntimeObject
{
public:
	// System.String AudioManager_<>c__DisplayClass3_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tF54F3CFD46E387BF9588D963503C85101F5C63CD, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_TF54F3CFD46E387BF9588D963503C85101F5C63CD_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A_H
#define U3CU3EC__DISPLAYCLASS4_0_TDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A  : public RuntimeObject
{
public:
	// System.String AudioManager_<>c__DisplayClass4_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A_H
#ifndef U3CSPEEDUPU3ED__4_TD90F6D88C9B4C7242CFA6C631475692619BA0660_H
#define U3CSPEEDUPU3ED__4_TD90F6D88C9B4C7242CFA6C631475692619BA0660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScroll_<Speedup>d__4
struct  U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660  : public RuntimeObject
{
public:
	// System.Int32 BackgroundScroll_<Speedup>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BackgroundScroll_<Speedup>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BackgroundScroll BackgroundScroll_<Speedup>d__4::<>4__this
	BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660, ___U3CU3E4__this_2)); }
	inline BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPEEDUPU3ED__4_TD90F6D88C9B4C7242CFA6C631475692619BA0660_H
#ifndef U3CLOADPRICEROUTINEU3ED__6_T3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF_H
#define U3CLOADPRICEROUTINEU3ED__6_T3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyButton_<LoadPriceRoutine>d__6
struct  U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF  : public RuntimeObject
{
public:
	// System.Int32 BuyButton_<LoadPriceRoutine>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BuyButton_<LoadPriceRoutine>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BuyButton BuyButton_<LoadPriceRoutine>d__6::<>4__this
	BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF, ___U3CU3E4__this_2)); }
	inline BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPRICEROUTINEU3ED__6_T3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF_H
#ifndef U3CREPLAYU3ED__12_T3EF73B603B18AC38C3C53C07D1D1D9291A0250A5_H
#define U3CREPLAYU3ED__12_T3EF73B603B18AC38C3C53C07D1D1D9291A0250A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathMenu_<Replay>d__12
struct  U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5  : public RuntimeObject
{
public:
	// System.Int32 DeathMenu_<Replay>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DeathMenu_<Replay>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DeathMenu DeathMenu_<Replay>d__12::<>4__this
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5, ___U3CU3E4__this_2)); }
	inline DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREPLAYU3ED__12_T3EF73B603B18AC38C3C53C07D1D1D9291A0250A5_H
#ifndef U3CSTARTUPU3ED__7_TF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4_H
#define U3CSTARTUPU3ED__7_TF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Engine_<StartUp>d__7
struct  U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4  : public RuntimeObject
{
public:
	// System.Int32 Engine_<StartUp>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Engine_<StartUp>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Engine Engine_<StartUp>d__7::<>4__this
	Engine_t62C213B195B780D1183D68751B5F0118D702848D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4, ___U3CU3E4__this_2)); }
	inline Engine_t62C213B195B780D1183D68751B5F0118D702848D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Engine_t62C213B195B780D1183D68751B5F0118D702848D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Engine_t62C213B195B780D1183D68751B5F0118D702848D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTUPU3ED__7_TF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4_H
#ifndef U3CBOOMU3ED__3_T4B42E446A6E29CEC63E3FE3902A053401747194C_H
#define U3CBOOMU3ED__3_T4B42E446A6E29CEC63E3FE3902A053401747194C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Explosion_<Boom>d__3
struct  U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C  : public RuntimeObject
{
public:
	// System.Int32 Explosion_<Boom>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Explosion_<Boom>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Explosion Explosion_<Boom>d__3::<>4__this
	Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C, ___U3CU3E4__this_2)); }
	inline Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBOOMU3ED__3_T4B42E446A6E29CEC63E3FE3902A053401747194C_H
#ifndef U3CRESTARTU3ED__1_TDEEC48D65C587F41A028A3F6C7F157CC873E65FA_H
#define U3CRESTARTU3ED__1_TDEEC48D65C587F41A028A3F6C7F157CC873E65FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameRestart_<Restart>d__1
struct  U3CRestartU3Ed__1_tDEEC48D65C587F41A028A3F6C7F157CC873E65FA  : public RuntimeObject
{
public:
	// System.Int32 GameRestart_<Restart>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object GameRestart_<Restart>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestartU3Ed__1_tDEEC48D65C587F41A028A3F6C7F157CC873E65FA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestartU3Ed__1_tDEEC48D65C587F41A028A3F6C7F157CC873E65FA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTARTU3ED__1_TDEEC48D65C587F41A028A3F6C7F157CC873E65FA_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A_H
#define U3CU3EC__DISPLAYCLASS30_0_T4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A  : public RuntimeObject
{
public:
	// IAPDemo IAPDemo_<>c__DisplayClass30_0::<>4__this
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92 * ___U3CU3E4__this_0;
	// UnityEngine.Purchasing.ConfigurationBuilder IAPDemo_<>c__DisplayClass30_0::builder
	ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * ___builder_1;
	// System.Action IAPDemo_<>c__DisplayClass30_0::initializeUnityIap
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___initializeUnityIap_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A, ___U3CU3E4__this_0)); }
	inline IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_builder_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A, ___builder_1)); }
	inline ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * get_builder_1() const { return ___builder_1; }
	inline ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE ** get_address_of_builder_1() { return &___builder_1; }
	inline void set_builder_1(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE * value)
	{
		___builder_1 = value;
		Il2CppCodeGenWriteBarrier((&___builder_1), value);
	}

	inline static int32_t get_offset_of_initializeUnityIap_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A, ___initializeUnityIap_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_initializeUnityIap_2() const { return ___initializeUnityIap_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_initializeUnityIap_2() { return &___initializeUnityIap_2; }
	inline void set_initializeUnityIap_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___initializeUnityIap_2 = value;
		Il2CppCodeGenWriteBarrier((&___initializeUnityIap_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A_H
#ifndef UNITYCHANNELLOGINHANDLER_T716B2D821D678D13F93E406A33996C6F12611A1E_H
#define UNITYCHANNELLOGINHANDLER_T716B2D821D678D13F93E406A33996C6F12611A1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo_UnityChannelLoginHandler
struct  UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E  : public RuntimeObject
{
public:
	// System.Action IAPDemo_UnityChannelLoginHandler::initializeSucceededAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___initializeSucceededAction_0;
	// System.Action`1<System.String> IAPDemo_UnityChannelLoginHandler::initializeFailedAction
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___initializeFailedAction_1;
	// System.Action`1<UnityEngine.Store.UserInfo> IAPDemo_UnityChannelLoginHandler::loginSucceededAction
	Action_1_t3A20718901FDC69E02C6ED48CF5A04A428B89027 * ___loginSucceededAction_2;
	// System.Action`1<System.String> IAPDemo_UnityChannelLoginHandler::loginFailedAction
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___loginFailedAction_3;

public:
	inline static int32_t get_offset_of_initializeSucceededAction_0() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E, ___initializeSucceededAction_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_initializeSucceededAction_0() const { return ___initializeSucceededAction_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_initializeSucceededAction_0() { return &___initializeSucceededAction_0; }
	inline void set_initializeSucceededAction_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___initializeSucceededAction_0 = value;
		Il2CppCodeGenWriteBarrier((&___initializeSucceededAction_0), value);
	}

	inline static int32_t get_offset_of_initializeFailedAction_1() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E, ___initializeFailedAction_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_initializeFailedAction_1() const { return ___initializeFailedAction_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_initializeFailedAction_1() { return &___initializeFailedAction_1; }
	inline void set_initializeFailedAction_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___initializeFailedAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___initializeFailedAction_1), value);
	}

	inline static int32_t get_offset_of_loginSucceededAction_2() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E, ___loginSucceededAction_2)); }
	inline Action_1_t3A20718901FDC69E02C6ED48CF5A04A428B89027 * get_loginSucceededAction_2() const { return ___loginSucceededAction_2; }
	inline Action_1_t3A20718901FDC69E02C6ED48CF5A04A428B89027 ** get_address_of_loginSucceededAction_2() { return &___loginSucceededAction_2; }
	inline void set_loginSucceededAction_2(Action_1_t3A20718901FDC69E02C6ED48CF5A04A428B89027 * value)
	{
		___loginSucceededAction_2 = value;
		Il2CppCodeGenWriteBarrier((&___loginSucceededAction_2), value);
	}

	inline static int32_t get_offset_of_loginFailedAction_3() { return static_cast<int32_t>(offsetof(UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E, ___loginFailedAction_3)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_loginFailedAction_3() const { return ___loginFailedAction_3; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_loginFailedAction_3() { return &___loginFailedAction_3; }
	inline void set_loginFailedAction_3(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___loginFailedAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___loginFailedAction_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELLOGINHANDLER_T716B2D821D678D13F93E406A33996C6F12611A1E_H
#ifndef UNITYCHANNELPURCHASEERROR_TFF027995D57BFD5C96AC83869F58682E5BF7AC7D_H
#define UNITYCHANNELPURCHASEERROR_TFF027995D57BFD5C96AC83869F58682E5BF7AC7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo_UnityChannelPurchaseError
struct  UnityChannelPurchaseError_tFF027995D57BFD5C96AC83869F58682E5BF7AC7D  : public RuntimeObject
{
public:
	// System.String IAPDemo_UnityChannelPurchaseError::error
	String_t* ___error_0;
	// IAPDemo_UnityChannelPurchaseInfo IAPDemo_UnityChannelPurchaseError::purchaseInfo
	UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9 * ___purchaseInfo_1;

public:
	inline static int32_t get_offset_of_error_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_tFF027995D57BFD5C96AC83869F58682E5BF7AC7D, ___error_0)); }
	inline String_t* get_error_0() const { return ___error_0; }
	inline String_t** get_address_of_error_0() { return &___error_0; }
	inline void set_error_0(String_t* value)
	{
		___error_0 = value;
		Il2CppCodeGenWriteBarrier((&___error_0), value);
	}

	inline static int32_t get_offset_of_purchaseInfo_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseError_tFF027995D57BFD5C96AC83869F58682E5BF7AC7D, ___purchaseInfo_1)); }
	inline UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9 * get_purchaseInfo_1() const { return ___purchaseInfo_1; }
	inline UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9 ** get_address_of_purchaseInfo_1() { return &___purchaseInfo_1; }
	inline void set_purchaseInfo_1(UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9 * value)
	{
		___purchaseInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASEERROR_TFF027995D57BFD5C96AC83869F58682E5BF7AC7D_H
#ifndef UNITYCHANNELPURCHASEINFO_T166CAF917D0E17C3E53B135D8652B9DE6D02F4F9_H
#define UNITYCHANNELPURCHASEINFO_T166CAF917D0E17C3E53B135D8652B9DE6D02F4F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo_UnityChannelPurchaseInfo
struct  UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9  : public RuntimeObject
{
public:
	// System.String IAPDemo_UnityChannelPurchaseInfo::productCode
	String_t* ___productCode_0;
	// System.String IAPDemo_UnityChannelPurchaseInfo::gameOrderId
	String_t* ___gameOrderId_1;
	// System.String IAPDemo_UnityChannelPurchaseInfo::orderQueryToken
	String_t* ___orderQueryToken_2;

public:
	inline static int32_t get_offset_of_productCode_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9, ___productCode_0)); }
	inline String_t* get_productCode_0() const { return ___productCode_0; }
	inline String_t** get_address_of_productCode_0() { return &___productCode_0; }
	inline void set_productCode_0(String_t* value)
	{
		___productCode_0 = value;
		Il2CppCodeGenWriteBarrier((&___productCode_0), value);
	}

	inline static int32_t get_offset_of_gameOrderId_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9, ___gameOrderId_1)); }
	inline String_t* get_gameOrderId_1() const { return ___gameOrderId_1; }
	inline String_t** get_address_of_gameOrderId_1() { return &___gameOrderId_1; }
	inline void set_gameOrderId_1(String_t* value)
	{
		___gameOrderId_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameOrderId_1), value);
	}

	inline static int32_t get_offset_of_orderQueryToken_2() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9, ___orderQueryToken_2)); }
	inline String_t* get_orderQueryToken_2() const { return ___orderQueryToken_2; }
	inline String_t** get_address_of_orderQueryToken_2() { return &___orderQueryToken_2; }
	inline void set_orderQueryToken_2(String_t* value)
	{
		___orderQueryToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___orderQueryToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASEINFO_T166CAF917D0E17C3E53B135D8652B9DE6D02F4F9_H
#ifndef U3CADWAITU3ED__18_TADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670_H
#define U3CADWAITU3ED__18_TADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelFader_<AdWait>d__18
struct  U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670  : public RuntimeObject
{
public:
	// System.Int32 LevelFader_<AdWait>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LevelFader_<AdWait>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LevelFader LevelFader_<AdWait>d__18::<>4__this
	LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670, ___U3CU3E4__this_2)); }
	inline LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADWAITU3ED__18_TADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670_H
#ifndef U3CFADEOUTU3ED__13_TEAC76A815C6E244A3B46A863EC2EBC6438C53929_H
#define U3CFADEOUTU3ED__13_TEAC76A815C6E244A3B46A863EC2EBC6438C53929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelFader_<Fadeout>d__13
struct  U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929  : public RuntimeObject
{
public:
	// System.Int32 LevelFader_<Fadeout>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LevelFader_<Fadeout>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LevelFader LevelFader_<Fadeout>d__13::<>4__this
	LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929, ___U3CU3E4__this_2)); }
	inline LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LevelFader_t80A930EAED182822655C69B819748C49C59A31A5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEOUTU3ED__13_TEAC76A815C6E244A3B46A863EC2EBC6438C53929_H
#ifndef U3CINVINCIBLEU3ED__51_T8A52115D5FFD1C2623A766358A2800F0CE98E142_H
#define U3CINVINCIBLEU3ED__51_T8A52115D5FFD1C2623A766358A2800F0CE98E142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player_<Invincible>d__51
struct  U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142  : public RuntimeObject
{
public:
	// System.Int32 Player_<Invincible>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Player_<Invincible>d__51::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Player Player_<Invincible>d__51::<>4__this
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142, ___U3CU3E4__this_2)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVINCIBLEU3ED__51_T8A52115D5FFD1C2623A766358A2800F0CE98E142_H
#ifndef U3CSHRINKEFFECTU3ED__50_T4C3270177B1B02F4EBF74A27ABF3D642632FB888_H
#define U3CSHRINKEFFECTU3ED__50_T4C3270177B1B02F4EBF74A27ABF3D642632FB888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player_<ShrinkEffect>d__50
struct  U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888  : public RuntimeObject
{
public:
	// System.Int32 Player_<ShrinkEffect>d__50::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Player_<ShrinkEffect>d__50::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Player Player_<ShrinkEffect>d__50::<>4__this
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888, ___U3CU3E4__this_2)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHRINKEFFECTU3ED__50_T4C3270177B1B02F4EBF74A27ABF3D642632FB888_H
#ifndef PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#define PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043  : public RuntimeObject
{
public:
	// System.Int32 PlayerData::lives
	int32_t ___lives_0;
	// System.Int32 PlayerData::hiScore
	int32_t ___hiScore_1;

public:
	inline static int32_t get_offset_of_lives_0() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___lives_0)); }
	inline int32_t get_lives_0() const { return ___lives_0; }
	inline int32_t* get_address_of_lives_0() { return &___lives_0; }
	inline void set_lives_0(int32_t value)
	{
		___lives_0 = value;
	}

	inline static int32_t get_offset_of_hiScore_1() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___hiScore_1)); }
	inline int32_t get_hiScore_1() const { return ___hiScore_1; }
	inline int32_t* get_address_of_hiScore_1() { return &___hiScore_1; }
	inline void set_hiScore_1(int32_t value)
	{
		___hiScore_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#ifndef SAVESYSTEM_T9373337803DA692D0868900C223D80B89CF75FD6_H
#define SAVESYSTEM_T9373337803DA692D0868900C223D80B89CF75FD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveSystem
struct  SaveSystem_t9373337803DA692D0868900C223D80B89CF75FD6  : public RuntimeObject
{
public:

public:
};

struct SaveSystem_t9373337803DA692D0868900C223D80B89CF75FD6_StaticFields
{
public:
	// System.Int32 SaveSystem::FileLoadAttemps
	int32_t ___FileLoadAttemps_0;

public:
	inline static int32_t get_offset_of_FileLoadAttemps_0() { return static_cast<int32_t>(offsetof(SaveSystem_t9373337803DA692D0868900C223D80B89CF75FD6_StaticFields, ___FileLoadAttemps_0)); }
	inline int32_t get_FileLoadAttemps_0() const { return ___FileLoadAttemps_0; }
	inline int32_t* get_address_of_FileLoadAttemps_0() { return &___FileLoadAttemps_0; }
	inline void set_FileLoadAttemps_0(int32_t value)
	{
		___FileLoadAttemps_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVESYSTEM_T9373337803DA692D0868900C223D80B89CF75FD6_H
#ifndef SOUND_TE1867C28E2BC13723E294746060427A1487DF1F1_H
#define SOUND_TE1867C28E2BC13723E294746060427A1487DF1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sound
struct  Sound_tE1867C28E2BC13723E294746060427A1487DF1F1  : public RuntimeObject
{
public:
	// System.String Sound::name
	String_t* ___name_0;
	// UnityEngine.AudioClip Sound::clip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___clip_1;
	// System.Single Sound::volume
	float ___volume_2;
	// System.Single Sound::pitch
	float ___pitch_3;
	// UnityEngine.AudioSource Sound::source
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___source_4;
	// System.Boolean Sound::loop
	bool ___loop_5;
	// System.Boolean Sound::mute
	bool ___mute_6;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_clip_1() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___clip_1)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_clip_1() const { return ___clip_1; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_clip_1() { return &___clip_1; }
	inline void set_clip_1(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___clip_1), value);
	}

	inline static int32_t get_offset_of_volume_2() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___volume_2)); }
	inline float get_volume_2() const { return ___volume_2; }
	inline float* get_address_of_volume_2() { return &___volume_2; }
	inline void set_volume_2(float value)
	{
		___volume_2 = value;
	}

	inline static int32_t get_offset_of_pitch_3() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___pitch_3)); }
	inline float get_pitch_3() const { return ___pitch_3; }
	inline float* get_address_of_pitch_3() { return &___pitch_3; }
	inline void set_pitch_3(float value)
	{
		___pitch_3 = value;
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___source_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_source_4() const { return ___source_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_loop_5() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___loop_5)); }
	inline bool get_loop_5() const { return ___loop_5; }
	inline bool* get_address_of_loop_5() { return &___loop_5; }
	inline void set_loop_5(bool value)
	{
		___loop_5 = value;
	}

	inline static int32_t get_offset_of_mute_6() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___mute_6)); }
	inline bool get_mute_6() const { return ___mute_6; }
	inline bool* get_address_of_mute_6() { return &___mute_6; }
	inline void set_mute_6(bool value)
	{
		___mute_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUND_TE1867C28E2BC13723E294746060427A1487DF1F1_H
#ifndef U3CSPAWNBLOCKSU3ED__25_TB573CF44EF21F3877E971E08C705FD52830CE120_H
#define U3CSPAWNBLOCKSU3ED__25_TB573CF44EF21F3877E971E08C705FD52830CE120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawner_<SpawnBlocks>d__25
struct  U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120  : public RuntimeObject
{
public:
	// System.Int32 Spawner_<SpawnBlocks>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spawner_<SpawnBlocks>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spawner Spawner_<SpawnBlocks>d__25::<>4__this
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120, ___U3CU3E4__this_2)); }
	inline Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNBLOCKSU3ED__25_TB573CF44EF21F3877E971E08C705FD52830CE120_H
#ifndef U3CSPAWNPOWERUPSU3ED__26_TB25473D6B5134BEBA04939363DEB8B42CF807102_H
#define U3CSPAWNPOWERUPSU3ED__26_TB25473D6B5134BEBA04939363DEB8B42CF807102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawner_<SpawnPowerUps>d__26
struct  U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102  : public RuntimeObject
{
public:
	// System.Int32 Spawner_<SpawnPowerUps>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Spawner_<SpawnPowerUps>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Spawner Spawner_<SpawnPowerUps>d__26::<>4__this
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator Spawner_<SpawnPowerUps>d__26::<spawner>5__2
	RuntimeObject* ___U3CspawnerU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102, ___U3CU3E4__this_2)); }
	inline Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CspawnerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102, ___U3CspawnerU3E5__2_3)); }
	inline RuntimeObject* get_U3CspawnerU3E5__2_3() const { return ___U3CspawnerU3E5__2_3; }
	inline RuntimeObject** get_address_of_U3CspawnerU3E5__2_3() { return &___U3CspawnerU3E5__2_3; }
	inline void set_U3CspawnerU3E5__2_3(RuntimeObject* value)
	{
		___U3CspawnerU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnerU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNPOWERUPSU3ED__26_TB25473D6B5134BEBA04939363DEB8B42CF807102_H
#ifndef U3CPLAYU3ED__4_TF0E541E4FF96BA814812B41638B704D1B0C01204_H
#define U3CPLAYU3ED__4_TF0E541E4FF96BA814812B41638B704D1B0C01204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartMenu_<Play>d__4
struct  U3CPlayU3Ed__4_tF0E541E4FF96BA814812B41638B704D1B0C01204  : public RuntimeObject
{
public:
	// System.Int32 StartMenu_<Play>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object StartMenu_<Play>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ed__4_tF0E541E4FF96BA814812B41638B704D1B0C01204, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlayU3Ed__4_tF0E541E4FF96BA814812B41638B704D1B0C01204, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYU3ED__4_TF0E541E4FF96BA814812B41638B704D1B0C01204_H
#ifndef LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#define LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CSHOWU3EC__ANONSTOREY0_T3F78313FF5E87445044283B31B3DB8DF4BBCA6B4_H
#define U3CSHOWU3EC__ANONSTOREY0_T3F78313FF5E87445044283B31B3DB8DF4BBCA6B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement_<Show>c__AnonStorey0
struct  U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowOptions UnityEngine.Advertisements.Advertisement_<Show>c__AnonStorey0::showOptions
	ShowOptions_t6637C67D4907C169839DA10D810B696A19941904 * ___showOptions_0;

public:
	inline static int32_t get_offset_of_showOptions_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4, ___showOptions_0)); }
	inline ShowOptions_t6637C67D4907C169839DA10D810B696A19941904 * get_showOptions_0() const { return ___showOptions_0; }
	inline ShowOptions_t6637C67D4907C169839DA10D810B696A19941904 ** get_address_of_showOptions_0() { return &___showOptions_0; }
	inline void set_showOptions_0(ShowOptions_t6637C67D4907C169839DA10D810B696A19941904 * value)
	{
		___showOptions_0 = value;
		Il2CppCodeGenWriteBarrier((&___showOptions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3EC__ANONSTOREY0_T3F78313FF5E87445044283B31B3DB8DF4BBCA6B4_H
#ifndef U3CSHOWU3EC__ANONSTOREY1_T7C410F6A2B71DAAC4DAEB5851994E08082C63A80_H
#define U3CSHOWU3EC__ANONSTOREY1_T7C410F6A2B71DAAC4DAEB5851994E08082C63A80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement_<Show>c__AnonStorey1
struct  U3CShowU3Ec__AnonStorey1_t7C410F6A2B71DAAC4DAEB5851994E08082C63A80  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Advertisement_<Show>c__AnonStorey1::finishHandler
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___finishHandler_0;
	// UnityEngine.Advertisements.Advertisement_<Show>c__AnonStorey0 UnityEngine.Advertisements.Advertisement_<Show>c__AnonStorey1::<>f__refU240
	U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_finishHandler_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey1_t7C410F6A2B71DAAC4DAEB5851994E08082C63A80, ___finishHandler_0)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_finishHandler_0() const { return ___finishHandler_0; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_finishHandler_0() { return &___finishHandler_0; }
	inline void set_finishHandler_0(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___finishHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___finishHandler_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey1_t7C410F6A2B71DAAC4DAEB5851994E08082C63A80, ___U3CU3Ef__refU240_1)); }
	inline U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3EC__ANONSTOREY1_T7C410F6A2B71DAAC4DAEB5851994E08082C63A80_H
#ifndef CONFIGURATION_T7CFA129695A07F478DA33F95E224C457CFE8193B_H
#define CONFIGURATION_T7CFA129695A07F478DA33F95E224C457CFE8193B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Configuration
struct  Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Advertisements.Editor.Configuration::<enabled>k__BackingField
	bool ___U3CenabledU3Ek__BackingField_0;
	// System.String UnityEngine.Advertisements.Editor.Configuration::<defaultPlacement>k__BackingField
	String_t* ___U3CdefaultPlacementU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Editor.Configuration::<placements>k__BackingField
	Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 * ___U3CplacementsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CenabledU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B, ___U3CenabledU3Ek__BackingField_0)); }
	inline bool get_U3CenabledU3Ek__BackingField_0() const { return ___U3CenabledU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CenabledU3Ek__BackingField_0() { return &___U3CenabledU3Ek__BackingField_0; }
	inline void set_U3CenabledU3Ek__BackingField_0(bool value)
	{
		___U3CenabledU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultPlacementU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B, ___U3CdefaultPlacementU3Ek__BackingField_1)); }
	inline String_t* get_U3CdefaultPlacementU3Ek__BackingField_1() const { return ___U3CdefaultPlacementU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdefaultPlacementU3Ek__BackingField_1() { return &___U3CdefaultPlacementU3Ek__BackingField_1; }
	inline void set_U3CdefaultPlacementU3Ek__BackingField_1(String_t* value)
	{
		___U3CdefaultPlacementU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdefaultPlacementU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CplacementsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B, ___U3CplacementsU3Ek__BackingField_2)); }
	inline Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 * get_U3CplacementsU3Ek__BackingField_2() const { return ___U3CplacementsU3Ek__BackingField_2; }
	inline Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 ** get_address_of_U3CplacementsU3Ek__BackingField_2() { return &___U3CplacementsU3Ek__BackingField_2; }
	inline void set_U3CplacementsU3Ek__BackingField_2(Dictionary_2_t7C989490AC01A6A5F45395A5CA72FAF4B7D59FF2 * value)
	{
		___U3CplacementsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementsU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATION_T7CFA129695A07F478DA33F95E224C457CFE8193B_H
#ifndef PLATFORM_T8B216EBF236DAD4583B1BB43FD792439EC1BE740_H
#define PLATFORM_T8B216EBF236DAD4583B1BB43FD792439EC1BE740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Platform
struct  Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Advertisements.Editor.Platform::m_DebugMode
	bool ___m_DebugMode_1;
	// UnityEngine.Advertisements.Editor.Configuration UnityEngine.Advertisements.Editor.Platform::m_Configuration
	Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B * ___m_Configuration_2;
	// UnityEngine.Advertisements.Editor.Placeholder UnityEngine.Advertisements.Editor.Platform::m_Placeholder
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244 * ___m_Placeholder_3;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.Editor.Platform::OnStart
	EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * ___OnStart_4;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Editor.Platform::OnFinish
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___OnFinish_5;

public:
	inline static int32_t get_offset_of_m_DebugMode_1() { return static_cast<int32_t>(offsetof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740, ___m_DebugMode_1)); }
	inline bool get_m_DebugMode_1() const { return ___m_DebugMode_1; }
	inline bool* get_address_of_m_DebugMode_1() { return &___m_DebugMode_1; }
	inline void set_m_DebugMode_1(bool value)
	{
		___m_DebugMode_1 = value;
	}

	inline static int32_t get_offset_of_m_Configuration_2() { return static_cast<int32_t>(offsetof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740, ___m_Configuration_2)); }
	inline Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B * get_m_Configuration_2() const { return ___m_Configuration_2; }
	inline Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B ** get_address_of_m_Configuration_2() { return &___m_Configuration_2; }
	inline void set_m_Configuration_2(Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B * value)
	{
		___m_Configuration_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Configuration_2), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_3() { return static_cast<int32_t>(offsetof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740, ___m_Placeholder_3)); }
	inline Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244 * get_m_Placeholder_3() const { return ___m_Placeholder_3; }
	inline Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244 ** get_address_of_m_Placeholder_3() { return &___m_Placeholder_3; }
	inline void set_m_Placeholder_3(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244 * value)
	{
		___m_Placeholder_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_3), value);
	}

	inline static int32_t get_offset_of_OnStart_4() { return static_cast<int32_t>(offsetof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740, ___OnStart_4)); }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * get_OnStart_4() const { return ___OnStart_4; }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 ** get_address_of_OnStart_4() { return &___OnStart_4; }
	inline void set_OnStart_4(EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * value)
	{
		___OnStart_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_4), value);
	}

	inline static int32_t get_offset_of_OnFinish_5() { return static_cast<int32_t>(offsetof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740, ___OnFinish_5)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_OnFinish_5() const { return ___OnFinish_5; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_OnFinish_5() { return &___OnFinish_5; }
	inline void set_OnFinish_5(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___OnFinish_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_5), value);
	}
};

struct Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740_StaticFields
{
public:
	// System.String UnityEngine.Advertisements.Editor.Platform::s_BaseUrl
	String_t* ___s_BaseUrl_0;

public:
	inline static int32_t get_offset_of_s_BaseUrl_0() { return static_cast<int32_t>(offsetof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740_StaticFields, ___s_BaseUrl_0)); }
	inline String_t* get_s_BaseUrl_0() const { return ___s_BaseUrl_0; }
	inline String_t** get_address_of_s_BaseUrl_0() { return &___s_BaseUrl_0; }
	inline void set_s_BaseUrl_0(String_t* value)
	{
		___s_BaseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_BaseUrl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T8B216EBF236DAD4583B1BB43FD792439EC1BE740_H
#ifndef U3CINITIALIZEU3EC__ANONSTOREY0_T01695F5A9E779590A7DB912A981E37FC10911AF8_H
#define U3CINITIALIZEU3EC__ANONSTOREY0_T01695F5A9E779590A7DB912A981E37FC10911AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Platform_<Initialize>c__AnonStorey0
struct  U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8  : public RuntimeObject
{
public:
	// System.Net.WebRequest UnityEngine.Advertisements.Editor.Platform_<Initialize>c__AnonStorey0::request
	WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * ___request_0;
	// System.String UnityEngine.Advertisements.Editor.Platform_<Initialize>c__AnonStorey0::gameId
	String_t* ___gameId_1;
	// UnityEngine.Advertisements.Editor.Platform UnityEngine.Advertisements.Editor.Platform_<Initialize>c__AnonStorey0::U24this
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740 * ___U24this_2;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8, ___request_0)); }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * get_request_0() const { return ___request_0; }
	inline WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(WebRequest_t5668DA48802E9FE2F1DE5F5A770B218608B918C8 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_gameId_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8, ___gameId_1)); }
	inline String_t* get_gameId_1() const { return ___gameId_1; }
	inline String_t** get_address_of_gameId_1() { return &___gameId_1; }
	inline void set_gameId_1(String_t* value)
	{
		___gameId_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameId_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8, ___U24this_2)); }
	inline Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740 * get_U24this_2() const { return ___U24this_2; }
	inline Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3EC__ANONSTOREY0_T01695F5A9E779590A7DB912A981E37FC10911AF8_H
#ifndef METADATA_T5CBC93B0A4A7377BB006186A40340BA51A9D6A03_H
#define METADATA_T5CBC93B0A4A7377BB006186A40340BA51A9D6A03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.MetaData
struct  MetaData_t5CBC93B0A4A7377BB006186A40340BA51A9D6A03  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::m_MetaData
	RuntimeObject* ___m_MetaData_0;
	// System.String UnityEngine.Advertisements.MetaData::<category>k__BackingField
	String_t* ___U3CcategoryU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_m_MetaData_0() { return static_cast<int32_t>(offsetof(MetaData_t5CBC93B0A4A7377BB006186A40340BA51A9D6A03, ___m_MetaData_0)); }
	inline RuntimeObject* get_m_MetaData_0() const { return ___m_MetaData_0; }
	inline RuntimeObject** get_address_of_m_MetaData_0() { return &___m_MetaData_0; }
	inline void set_m_MetaData_0(RuntimeObject* value)
	{
		___m_MetaData_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MetaData_0), value);
	}

	inline static int32_t get_offset_of_U3CcategoryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetaData_t5CBC93B0A4A7377BB006186A40340BA51A9D6A03, ___U3CcategoryU3Ek__BackingField_1)); }
	inline String_t* get_U3CcategoryU3Ek__BackingField_1() const { return ___U3CcategoryU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CcategoryU3Ek__BackingField_1() { return &___U3CcategoryU3Ek__BackingField_1; }
	inline void set_U3CcategoryU3Ek__BackingField_1(String_t* value)
	{
		___U3CcategoryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcategoryU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATA_T5CBC93B0A4A7377BB006186A40340BA51A9D6A03_H
#ifndef SHOWOPTIONS_T6637C67D4907C169839DA10D810B696A19941904_H
#define SHOWOPTIONS_T6637C67D4907C169839DA10D810B696A19941904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowOptions
struct  ShowOptions_t6637C67D4907C169839DA10D810B696A19941904  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::<resultCallback>k__BackingField
	Action_1_tCF5AFC4513076B0E4A471BEF3A11BFBBDF57F857 * ___U3CresultCallbackU3Ek__BackingField_0;
	// System.String UnityEngine.Advertisements.ShowOptions::<gamerSid>k__BackingField
	String_t* ___U3CgamerSidU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CresultCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowOptions_t6637C67D4907C169839DA10D810B696A19941904, ___U3CresultCallbackU3Ek__BackingField_0)); }
	inline Action_1_tCF5AFC4513076B0E4A471BEF3A11BFBBDF57F857 * get_U3CresultCallbackU3Ek__BackingField_0() const { return ___U3CresultCallbackU3Ek__BackingField_0; }
	inline Action_1_tCF5AFC4513076B0E4A471BEF3A11BFBBDF57F857 ** get_address_of_U3CresultCallbackU3Ek__BackingField_0() { return &___U3CresultCallbackU3Ek__BackingField_0; }
	inline void set_U3CresultCallbackU3Ek__BackingField_0(Action_1_tCF5AFC4513076B0E4A471BEF3A11BFBBDF57F857 * value)
	{
		___U3CresultCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CgamerSidU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowOptions_t6637C67D4907C169839DA10D810B696A19941904, ___U3CgamerSidU3Ek__BackingField_1)); }
	inline String_t* get_U3CgamerSidU3Ek__BackingField_1() const { return ___U3CgamerSidU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgamerSidU3Ek__BackingField_1() { return &___U3CgamerSidU3Ek__BackingField_1; }
	inline void set_U3CgamerSidU3Ek__BackingField_1(String_t* value)
	{
		___U3CgamerSidU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgamerSidU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWOPTIONS_T6637C67D4907C169839DA10D810B696A19941904_H
#ifndef JSONOBJECT_T5430387D9A9215A629850E012C366DBE5AD5A2B5_H
#define JSONOBJECT_T5430387D9A9215A629850E012C366DBE5AD5A2B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.JsonObject
struct  JsonObject_t5430387D9A9215A629850E012C366DBE5AD5A2B5  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.SimpleJson.JsonObject::_members
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____members_0;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(JsonObject_t5430387D9A9215A629850E012C366DBE5AD5A2B5, ____members_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__members_0() const { return ____members_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T5430387D9A9215A629850E012C366DBE5AD5A2B5_H
#ifndef POCOJSONSERIALIZERSTRATEGY_T40E49A3237BC56F62F5DE6F9034160AD2660BB0E_H
#define POCOJSONSERIALIZERSTRATEGY_T40E49A3237BC56F62F5DE6F9034160AD2660BB0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy
struct  PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_ConstructorDelegate> UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::ConstructorCache
	RuntimeObject* ___ConstructorCache_0;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_GetDelegate>> UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::GetCache
	RuntimeObject* ___GetCache_1;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_SetDelegate>>> UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::SetCache
	RuntimeObject* ___SetCache_2;

public:
	inline static int32_t get_offset_of_ConstructorCache_0() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E, ___ConstructorCache_0)); }
	inline RuntimeObject* get_ConstructorCache_0() const { return ___ConstructorCache_0; }
	inline RuntimeObject** get_address_of_ConstructorCache_0() { return &___ConstructorCache_0; }
	inline void set_ConstructorCache_0(RuntimeObject* value)
	{
		___ConstructorCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorCache_0), value);
	}

	inline static int32_t get_offset_of_GetCache_1() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E, ___GetCache_1)); }
	inline RuntimeObject* get_GetCache_1() const { return ___GetCache_1; }
	inline RuntimeObject** get_address_of_GetCache_1() { return &___GetCache_1; }
	inline void set_GetCache_1(RuntimeObject* value)
	{
		___GetCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___GetCache_1), value);
	}

	inline static int32_t get_offset_of_SetCache_2() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E, ___SetCache_2)); }
	inline RuntimeObject* get_SetCache_2() const { return ___SetCache_2; }
	inline RuntimeObject** get_address_of_SetCache_2() { return &___SetCache_2; }
	inline void set_SetCache_2(RuntimeObject* value)
	{
		___SetCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___SetCache_2), value);
	}
};

struct PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields
{
public:
	// System.Type[] UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_3;
	// System.Type[] UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::ArrayConstructorParameterTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___ArrayConstructorParameterTypes_4;
	// System.String[] UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy::Iso8601Format
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___Iso8601Format_5;

public:
	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_ArrayConstructorParameterTypes_4() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields, ___ArrayConstructorParameterTypes_4)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_ArrayConstructorParameterTypes_4() const { return ___ArrayConstructorParameterTypes_4; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_ArrayConstructorParameterTypes_4() { return &___ArrayConstructorParameterTypes_4; }
	inline void set_ArrayConstructorParameterTypes_4(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___ArrayConstructorParameterTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayConstructorParameterTypes_4), value);
	}

	inline static int32_t get_offset_of_Iso8601Format_5() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields, ___Iso8601Format_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_Iso8601Format_5() const { return ___Iso8601Format_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_Iso8601Format_5() { return &___Iso8601Format_5; }
	inline void set_Iso8601Format_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___Iso8601Format_5 = value;
		Il2CppCodeGenWriteBarrier((&___Iso8601Format_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POCOJSONSERIALIZERSTRATEGY_T40E49A3237BC56F62F5DE6F9034160AD2660BB0E_H
#ifndef REFLECTIONUTILS_T716A5426EE76AD7B91D2D6989AB62AA4BB23A534_H
#define REFLECTIONUTILS_T716A5426EE76AD7B91D2D6989AB62AA4BB23A534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t716A5426EE76AD7B91D2D6989AB62AA4BB23A534  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t716A5426EE76AD7B91D2D6989AB62AA4BB23A534_StaticFields
{
public:
	// System.Object[] UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___EmptyObjects_0;

public:
	inline static int32_t get_offset_of_EmptyObjects_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t716A5426EE76AD7B91D2D6989AB62AA4BB23A534_StaticFields, ___EmptyObjects_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_EmptyObjects_0() const { return ___EmptyObjects_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_EmptyObjects_0() { return &___EmptyObjects_0; }
	inline void set_EmptyObjects_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___EmptyObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T716A5426EE76AD7B91D2D6989AB62AA4BB23A534_H
#ifndef U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T4266D02561276A50905B1F44DEDE646F4845D634_H
#define U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T4266D02561276A50905B1F44DEDE646F4845D634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetConstructorByReflection>c__AnonStorey0
struct  U3CGetConstructorByReflectionU3Ec__AnonStorey0_t4266D02561276A50905B1F44DEDE646F4845D634  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetConstructorByReflection>c__AnonStorey0::constructorInfo
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___constructorInfo_0;

public:
	inline static int32_t get_offset_of_constructorInfo_0() { return static_cast<int32_t>(offsetof(U3CGetConstructorByReflectionU3Ec__AnonStorey0_t4266D02561276A50905B1F44DEDE646F4845D634, ___constructorInfo_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_constructorInfo_0() const { return ___constructorInfo_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_constructorInfo_0() { return &___constructorInfo_0; }
	inline void set_constructorInfo_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___constructorInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___constructorInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONSTRUCTORBYREFLECTIONU3EC__ANONSTOREY0_T4266D02561276A50905B1F44DEDE646F4845D634_H
#ifndef U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T5304BC457840A263778B61647A6214450139CE2F_H
#define U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T5304BC457840A263778B61647A6214450139CE2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetGetMethodByReflection>c__AnonStorey1
struct  U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t5304BC457840A263778B61647A6214450139CE2F  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetGetMethodByReflection>c__AnonStorey1::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t5304BC457840A263778B61647A6214450139CE2F, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY1_T5304BC457840A263778B61647A6214450139CE2F_H
#ifndef U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14_H
#define U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetGetMethodByReflection>c__AnonStorey2
struct  U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetGetMethodByReflection>c__AnonStorey2::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETGETMETHODBYREFLECTIONU3EC__ANONSTOREY2_T5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14_H
#ifndef U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_TFBA12729E943700B178CA5477723857345AB686B_H
#define U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_TFBA12729E943700B178CA5477723857345AB686B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetSetMethodByReflection>c__AnonStorey3
struct  U3CGetSetMethodByReflectionU3Ec__AnonStorey3_tFBA12729E943700B178CA5477723857345AB686B  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetSetMethodByReflection>c__AnonStorey3::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CGetSetMethodByReflectionU3Ec__AnonStorey3_tFBA12729E943700B178CA5477723857345AB686B, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY3_TFBA12729E943700B178CA5477723857345AB686B_H
#ifndef U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T43269ED7005D484DB0429A87155BB781825B5E47_H
#define U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T43269ED7005D484DB0429A87155BB781825B5E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetSetMethodByReflection>c__AnonStorey4
struct  U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t43269ED7005D484DB0429A87155BB781825B5E47  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_<GetSetMethodByReflection>c__AnonStorey4::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t43269ED7005D484DB0429A87155BB781825B5E47, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSETMETHODBYREFLECTIONU3EC__ANONSTOREY4_T43269ED7005D484DB0429A87155BB781825B5E47_H
#ifndef SIMPLEJSON_TD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_H
#define SIMPLEJSON_TD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.SimpleJson
struct  SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D  : public RuntimeObject
{
public:

public:
};

struct SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_StaticFields
{
public:
	// UnityEngine.Advertisements.SimpleJson.IJsonSerializerStrategy UnityEngine.Advertisements.SimpleJson.SimpleJson::_currentJsonSerializerStrategy
	RuntimeObject* ____currentJsonSerializerStrategy_13;
	// UnityEngine.Advertisements.SimpleJson.PocoJsonSerializerStrategy UnityEngine.Advertisements.SimpleJson.SimpleJson::_pocoJsonSerializerStrategy
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E * ____pocoJsonSerializerStrategy_14;

public:
	inline static int32_t get_offset_of__currentJsonSerializerStrategy_13() { return static_cast<int32_t>(offsetof(SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_StaticFields, ____currentJsonSerializerStrategy_13)); }
	inline RuntimeObject* get__currentJsonSerializerStrategy_13() const { return ____currentJsonSerializerStrategy_13; }
	inline RuntimeObject** get_address_of__currentJsonSerializerStrategy_13() { return &____currentJsonSerializerStrategy_13; }
	inline void set__currentJsonSerializerStrategy_13(RuntimeObject* value)
	{
		____currentJsonSerializerStrategy_13 = value;
		Il2CppCodeGenWriteBarrier((&____currentJsonSerializerStrategy_13), value);
	}

	inline static int32_t get_offset_of__pocoJsonSerializerStrategy_14() { return static_cast<int32_t>(offsetof(SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_StaticFields, ____pocoJsonSerializerStrategy_14)); }
	inline PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E * get__pocoJsonSerializerStrategy_14() const { return ____pocoJsonSerializerStrategy_14; }
	inline PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E ** get_address_of__pocoJsonSerializerStrategy_14() { return &____pocoJsonSerializerStrategy_14; }
	inline void set__pocoJsonSerializerStrategy_14(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E * value)
	{
		____pocoJsonSerializerStrategy_14 = value;
		Il2CppCodeGenWriteBarrier((&____pocoJsonSerializerStrategy_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEJSON_TD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_H
#ifndef UNSUPPORTEDPLATFORM_T5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3_H
#define UNSUPPORTEDPLATFORM_T5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnsupportedPlatform
struct  UnsupportedPlatform_t5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.UnsupportedPlatform::OnFinish
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___OnFinish_0;

public:
	inline static int32_t get_offset_of_OnFinish_0() { return static_cast<int32_t>(offsetof(UnsupportedPlatform_t5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3, ___OnFinish_0)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_OnFinish_0() const { return ___OnFinish_0; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_OnFinish_0() { return &___OnFinish_0; }
	inline void set_OnFinish_0(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___OnFinish_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDPLATFORM_T5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3_H
#ifndef PLATFORM_T7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_H
#define PLATFORM_T7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform
struct  Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs> UnityEngine.Advertisements.iOS.Platform::OnReady
	EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A * ___OnReady_2;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.iOS.Platform::OnStart
	EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * ___OnStart_3;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.iOS.Platform::OnFinish
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___OnFinish_4;
	// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs> UnityEngine.Advertisements.iOS.Platform::OnError
	EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 * ___OnError_5;

public:
	inline static int32_t get_offset_of_OnReady_2() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D, ___OnReady_2)); }
	inline EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A * get_OnReady_2() const { return ___OnReady_2; }
	inline EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A ** get_address_of_OnReady_2() { return &___OnReady_2; }
	inline void set_OnReady_2(EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A * value)
	{
		___OnReady_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnReady_2), value);
	}

	inline static int32_t get_offset_of_OnStart_3() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D, ___OnStart_3)); }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * get_OnStart_3() const { return ___OnStart_3; }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 ** get_address_of_OnStart_3() { return &___OnStart_3; }
	inline void set_OnStart_3(EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * value)
	{
		___OnStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnStart_3), value);
	}

	inline static int32_t get_offset_of_OnFinish_4() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D, ___OnFinish_4)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_OnFinish_4() const { return ___OnFinish_4; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_OnFinish_4() { return &___OnFinish_4; }
	inline void set_OnFinish_4(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___OnFinish_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_4), value);
	}

	inline static int32_t get_offset_of_OnError_5() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D, ___OnError_5)); }
	inline EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 * get_OnError_5() const { return ___OnError_5; }
	inline EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 ** get_address_of_OnError_5() { return &___OnError_5; }
	inline void set_OnError_5(EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 * value)
	{
		___OnError_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_5), value);
	}
};

struct Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields
{
public:
	// UnityEngine.Advertisements.iOS.Platform UnityEngine.Advertisements.iOS.Platform::s_Instance
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D * ___s_Instance_0;
	// UnityEngine.Advertisements.CallbackExecutor UnityEngine.Advertisements.iOS.Platform::s_CallbackExecutor
	CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78 * ___s_CallbackExecutor_1;
	// UnityEngine.Advertisements.iOS.Platform_unityAdsReady UnityEngine.Advertisements.iOS.Platform::<>f__mgU24cache0
	unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67 * ___U3CU3Ef__mgU24cache0_6;
	// UnityEngine.Advertisements.iOS.Platform_unityAdsDidError UnityEngine.Advertisements.iOS.Platform::<>f__mgU24cache1
	unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61 * ___U3CU3Ef__mgU24cache1_7;
	// UnityEngine.Advertisements.iOS.Platform_unityAdsDidStart UnityEngine.Advertisements.iOS.Platform::<>f__mgU24cache2
	unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA * ___U3CU3Ef__mgU24cache2_8;
	// UnityEngine.Advertisements.iOS.Platform_unityAdsDidFinish UnityEngine.Advertisements.iOS.Platform::<>f__mgU24cache3
	unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045 * ___U3CU3Ef__mgU24cache3_9;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields, ___s_Instance_0)); }
	inline Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D * get_s_Instance_0() const { return ___s_Instance_0; }
	inline Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_s_CallbackExecutor_1() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields, ___s_CallbackExecutor_1)); }
	inline CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78 * get_s_CallbackExecutor_1() const { return ___s_CallbackExecutor_1; }
	inline CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78 ** get_address_of_s_CallbackExecutor_1() { return &___s_CallbackExecutor_1; }
	inline void set_s_CallbackExecutor_1(CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78 * value)
	{
		___s_CallbackExecutor_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CallbackExecutor_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_7() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields, ___U3CU3Ef__mgU24cache1_7)); }
	inline unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61 * get_U3CU3Ef__mgU24cache1_7() const { return ___U3CU3Ef__mgU24cache1_7; }
	inline unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61 ** get_address_of_U3CU3Ef__mgU24cache1_7() { return &___U3CU3Ef__mgU24cache1_7; }
	inline void set_U3CU3Ef__mgU24cache1_7(unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61 * value)
	{
		___U3CU3Ef__mgU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_8() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields, ___U3CU3Ef__mgU24cache2_8)); }
	inline unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA * get_U3CU3Ef__mgU24cache2_8() const { return ___U3CU3Ef__mgU24cache2_8; }
	inline unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA ** get_address_of_U3CU3Ef__mgU24cache2_8() { return &___U3CU3Ef__mgU24cache2_8; }
	inline void set_U3CU3Ef__mgU24cache2_8(unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA * value)
	{
		___U3CU3Ef__mgU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_9() { return static_cast<int32_t>(offsetof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields, ___U3CU3Ef__mgU24cache3_9)); }
	inline unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045 * get_U3CU3Ef__mgU24cache3_9() const { return ___U3CU3Ef__mgU24cache3_9; }
	inline unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045 ** get_address_of_U3CU3Ef__mgU24cache3_9() { return &___U3CU3Ef__mgU24cache3_9; }
	inline void set_U3CU3Ef__mgU24cache3_9(unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045 * value)
	{
		___U3CU3Ef__mgU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_H
#ifndef U3CUNITYADSDIDERRORU3EC__ANONSTOREY1_T6A5E888349986FCAC117F1F71F73587DAE2AFFEA_H
#define U3CUNITYADSDIDERRORU3EC__ANONSTOREY1_T6A5E888349986FCAC117F1F71F73587DAE2AFFEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidError>c__AnonStorey1
struct  U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ErrorEventArgs> UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidError>c__AnonStorey1::handler
	EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 * ___handler_0;
	// System.Int64 UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidError>c__AnonStorey1::rawError
	int64_t ___rawError_1;
	// System.String UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidError>c__AnonStorey1::message
	String_t* ___message_2;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA, ___handler_0)); }
	inline EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t674DACB444A256903A17FB5F7644CEB641C01927 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_rawError_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA, ___rawError_1)); }
	inline int64_t get_rawError_1() const { return ___rawError_1; }
	inline int64_t* get_address_of_rawError_1() { return &___rawError_1; }
	inline void set_rawError_1(int64_t value)
	{
		___rawError_1 = value;
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDERRORU3EC__ANONSTOREY1_T6A5E888349986FCAC117F1F71F73587DAE2AFFEA_H
#ifndef U3CUNITYADSDIDFINISHU3EC__ANONSTOREY3_TFF312E3E401419649E7A80CB5F522EFBD8C7A1E1_H
#define U3CUNITYADSDIDFINISHU3EC__ANONSTOREY3_TFF312E3E401419649E7A80CB5F522EFBD8C7A1E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey3
struct  U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey3::handler
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___handler_0;
	// System.String UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey3::placementId
	String_t* ___placementId_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1, ___handler_0)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDFINISHU3EC__ANONSTOREY3_TFF312E3E401419649E7A80CB5F522EFBD8C7A1E1_H
#ifndef U3CUNITYADSDIDSTARTU3EC__ANONSTOREY2_T69450E13B9A35C35663BBFDA85A317661053E4F6_H
#define U3CUNITYADSDIDSTARTU3EC__ANONSTOREY2_T69450E13B9A35C35663BBFDA85A317661053E4F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidStart>c__AnonStorey2
struct  U3CUnityAdsDidStartU3Ec__AnonStorey2_t69450E13B9A35C35663BBFDA85A317661053E4F6  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidStart>c__AnonStorey2::handler
	EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * ___handler_0;
	// System.String UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidStart>c__AnonStorey2::placementId
	String_t* ___placementId_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidStartU3Ec__AnonStorey2_t69450E13B9A35C35663BBFDA85A317661053E4F6, ___handler_0)); }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidStartU3Ec__AnonStorey2_t69450E13B9A35C35663BBFDA85A317661053E4F6, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDSTARTU3EC__ANONSTOREY2_T69450E13B9A35C35663BBFDA85A317661053E4F6_H
#ifndef U3CUNITYADSREADYU3EC__ANONSTOREY0_TB27C3EFC407540F32BE16EEEECF2C7B88BF43A79_H
#define U3CUNITYADSREADYU3EC__ANONSTOREY0_TB27C3EFC407540F32BE16EEEECF2C7B88BF43A79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_<UnityAdsReady>c__AnonStorey0
struct  U3CUnityAdsReadyU3Ec__AnonStorey0_tB27C3EFC407540F32BE16EEEECF2C7B88BF43A79  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.ReadyEventArgs> UnityEngine.Advertisements.iOS.Platform_<UnityAdsReady>c__AnonStorey0::handler
	EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A * ___handler_0;
	// System.String UnityEngine.Advertisements.iOS.Platform_<UnityAdsReady>c__AnonStorey0::placementId
	String_t* ___placementId_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsReadyU3Ec__AnonStorey0_tB27C3EFC407540F32BE16EEEECF2C7B88BF43A79, ___handler_0)); }
	inline EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A * get_handler_0() const { return ___handler_0; }
	inline EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(EventHandler_1_tB57D8AF0A9FA3BFE6ABEC77DE046E4A20526A09A * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_placementId_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsReadyU3Ec__AnonStorey0_tB27C3EFC407540F32BE16EEEECF2C7B88BF43A79, ___placementId_1)); }
	inline String_t* get_placementId_1() const { return ___placementId_1; }
	inline String_t** get_address_of_placementId_1() { return &___placementId_1; }
	inline void set_placementId_1(String_t* value)
	{
		___placementId_1 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSREADYU3EC__ANONSTOREY0_TB27C3EFC407540F32BE16EEEECF2C7B88BF43A79_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ERROREVENTARGS_T77C07CCB4554162961BBE00E3B7E52DDA8648409_H
#define ERROREVENTARGS_T77C07CCB4554162961BBE00E3B7E52DDA8648409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ErrorEventArgs
struct  ErrorEventArgs_t77C07CCB4554162961BBE00E3B7E52DDA8648409  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Int64 UnityEngine.Advertisements.ErrorEventArgs::<error>k__BackingField
	int64_t ___U3CerrorU3Ek__BackingField_1;
	// System.String UnityEngine.Advertisements.ErrorEventArgs::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t77C07CCB4554162961BBE00E3B7E52DDA8648409, ___U3CerrorU3Ek__BackingField_1)); }
	inline int64_t get_U3CerrorU3Ek__BackingField_1() const { return ___U3CerrorU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CerrorU3Ek__BackingField_1() { return &___U3CerrorU3Ek__BackingField_1; }
	inline void set_U3CerrorU3Ek__BackingField_1(int64_t value)
	{
		___U3CerrorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t77C07CCB4554162961BBE00E3B7E52DDA8648409, ___U3CmessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_2() const { return ___U3CmessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_2() { return &___U3CmessageU3Ek__BackingField_2; }
	inline void set_U3CmessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T77C07CCB4554162961BBE00E3B7E52DDA8648409_H
#ifndef READYEVENTARGS_TA3E33B18240D91F48C92CC62CB5515388449E250_H
#define READYEVENTARGS_TA3E33B18240D91F48C92CC62CB5515388449E250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ReadyEventArgs
struct  ReadyEventArgs_tA3E33B18240D91F48C92CC62CB5515388449E250  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String UnityEngine.Advertisements.ReadyEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReadyEventArgs_tA3E33B18240D91F48C92CC62CB5515388449E250, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READYEVENTARGS_TA3E33B18240D91F48C92CC62CB5515388449E250_H
#ifndef JSONARRAY_TEC81164ADEC87B1B3F3D28FEA89F9AF6458479A6_H
#define JSONARRAY_TEC81164ADEC87B1B3F3D28FEA89F9AF6458479A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.JsonArray
struct  JsonArray_tEC81164ADEC87B1B3F3D28FEA89F9AF6458479A6  : public List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_TEC81164ADEC87B1B3F3D28FEA89F9AF6458479A6_H
#ifndef STARTEVENTARGS_T44F8C8482BBCC37963CFBC1C4D2F6473040717E2_H
#define STARTEVENTARGS_T44F8C8482BBCC37963CFBC1C4D2F6473040717E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.StartEventArgs
struct  StartEventArgs_t44F8C8482BBCC37963CFBC1C4D2F6473040717E2  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String UnityEngine.Advertisements.StartEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StartEventArgs_t44F8C8482BBCC37963CFBC1C4D2F6473040717E2, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTEVENTARGS_T44F8C8482BBCC37963CFBC1C4D2F6473040717E2_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ITEMTYPE_TCF5610CD6C681493E68253D10F26EFC6E5F1CC61_H
#define ITEMTYPE_TCF5610CD6C681493E68253D10F26EFC6E5F1CC61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyButton_ItemType
struct  ItemType_tCF5610CD6C681493E68253D10F26EFC6E5F1CC61 
{
public:
	// System.Int32 BuyButton_ItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemType_tCF5610CD6C681493E68253D10F26EFC6E5F1CC61, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTYPE_TCF5610CD6C681493E68253D10F26EFC6E5F1CC61_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef DEBUGLEVEL_T72A7B4FC98B1A5E19A789475341FD35F5CAB0537_H
#define DEBUGLEVEL_T72A7B4FC98B1A5E19A789475341FD35F5CAB0537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement_DebugLevel
struct  DebugLevel_t72A7B4FC98B1A5E19A789475341FD35F5CAB0537 
{
public:
	// System.Int32 UnityEngine.Advertisements.Advertisement_DebugLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLevel_t72A7B4FC98B1A5E19A789475341FD35F5CAB0537, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVEL_T72A7B4FC98B1A5E19A789475341FD35F5CAB0537_H
#ifndef DEBUGLEVELINTERNAL_T2FF63CDE387CA44C2B4C4715A40FAEC807F23E22_H
#define DEBUGLEVELINTERNAL_T2FF63CDE387CA44C2B4C4715A40FAEC807F23E22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement_DebugLevelInternal
struct  DebugLevelInternal_t2FF63CDE387CA44C2B4C4715A40FAEC807F23E22 
{
public:
	// System.Int32 UnityEngine.Advertisements.Advertisement_DebugLevelInternal::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebugLevelInternal_t2FF63CDE387CA44C2B4C4715A40FAEC807F23E22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLEVELINTERNAL_T2FF63CDE387CA44C2B4C4715A40FAEC807F23E22_H
#ifndef PLACEMENTSTATE_T19D760B7BB28598800BD374D7ECBC43FC9D48333_H
#define PLACEMENTSTATE_T19D760B7BB28598800BD374D7ECBC43FC9D48333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.PlacementState
struct  PlacementState_t19D760B7BB28598800BD374D7ECBC43FC9D48333 
{
public:
	// System.Int32 UnityEngine.Advertisements.PlacementState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlacementState_t19D760B7BB28598800BD374D7ECBC43FC9D48333, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTSTATE_T19D760B7BB28598800BD374D7ECBC43FC9D48333_H
#ifndef SHOWRESULT_TE352F124530178145C4D05ACA8F66AD11DEECDA8_H
#define SHOWRESULT_TE352F124530178145C4D05ACA8F66AD11DEECDA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowResult
struct  ShowResult_tE352F124530178145C4D05ACA8F66AD11DEECDA8 
{
public:
	// System.Int32 UnityEngine.Advertisements.ShowResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShowResult_tE352F124530178145C4D05ACA8F66AD11DEECDA8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWRESULT_TE352F124530178145C4D05ACA8F66AD11DEECDA8_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#define TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ADVERTISEMENT_T40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_H
#define ADVERTISEMENT_T40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Advertisement
struct  Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139  : public RuntimeObject
{
public:

public:
};

struct Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields
{
public:
	// System.Boolean UnityEngine.Advertisements.Advertisement::s_Initialized
	bool ___s_Initialized_0;
	// UnityEngine.Advertisements.IPlatform UnityEngine.Advertisements.Advertisement::s_Platform
	RuntimeObject* ___s_Platform_1;
	// System.Boolean UnityEngine.Advertisements.Advertisement::s_EditorSupportedPlatform
	bool ___s_EditorSupportedPlatform_2;
	// System.Boolean UnityEngine.Advertisements.Advertisement::s_Showing
	bool ___s_Showing_3;
	// UnityEngine.Advertisements.Advertisement_DebugLevelInternal UnityEngine.Advertisements.Advertisement::s_DebugLevel
	int32_t ___s_DebugLevel_4;
	// System.EventHandler`1<UnityEngine.Advertisements.StartEventArgs> UnityEngine.Advertisements.Advertisement::<>f__amU24cache0
	EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * ___U3CU3Ef__amU24cache0_5;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Advertisement::<>f__amU24cache1
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_s_Initialized_0() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___s_Initialized_0)); }
	inline bool get_s_Initialized_0() const { return ___s_Initialized_0; }
	inline bool* get_address_of_s_Initialized_0() { return &___s_Initialized_0; }
	inline void set_s_Initialized_0(bool value)
	{
		___s_Initialized_0 = value;
	}

	inline static int32_t get_offset_of_s_Platform_1() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___s_Platform_1)); }
	inline RuntimeObject* get_s_Platform_1() const { return ___s_Platform_1; }
	inline RuntimeObject** get_address_of_s_Platform_1() { return &___s_Platform_1; }
	inline void set_s_Platform_1(RuntimeObject* value)
	{
		___s_Platform_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Platform_1), value);
	}

	inline static int32_t get_offset_of_s_EditorSupportedPlatform_2() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___s_EditorSupportedPlatform_2)); }
	inline bool get_s_EditorSupportedPlatform_2() const { return ___s_EditorSupportedPlatform_2; }
	inline bool* get_address_of_s_EditorSupportedPlatform_2() { return &___s_EditorSupportedPlatform_2; }
	inline void set_s_EditorSupportedPlatform_2(bool value)
	{
		___s_EditorSupportedPlatform_2 = value;
	}

	inline static int32_t get_offset_of_s_Showing_3() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___s_Showing_3)); }
	inline bool get_s_Showing_3() const { return ___s_Showing_3; }
	inline bool* get_address_of_s_Showing_3() { return &___s_Showing_3; }
	inline void set_s_Showing_3(bool value)
	{
		___s_Showing_3 = value;
	}

	inline static int32_t get_offset_of_s_DebugLevel_4() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___s_DebugLevel_4)); }
	inline int32_t get_s_DebugLevel_4() const { return ___s_DebugLevel_4; }
	inline int32_t* get_address_of_s_DebugLevel_4() { return &___s_DebugLevel_4; }
	inline void set_s_DebugLevel_4(int32_t value)
	{
		___s_DebugLevel_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(EventHandler_1_t888C12D387DFA77DBE176EE733AD5DD59E8E0E95 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVERTISEMENT_T40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_H
#ifndef FINISHEVENTARGS_TBC627E351CBF3F2563ACD7F5D765110318D7F49D_H
#define FINISHEVENTARGS_TBC627E351CBF3F2563ACD7F5D765110318D7F49D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.FinishEventArgs
struct  FinishEventArgs_tBC627E351CBF3F2563ACD7F5D765110318D7F49D  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String UnityEngine.Advertisements.FinishEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;
	// UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.FinishEventArgs::<showResult>k__BackingField
	int32_t ___U3CshowResultU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FinishEventArgs_tBC627E351CBF3F2563ACD7F5D765110318D7F49D, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CshowResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FinishEventArgs_tBC627E351CBF3F2563ACD7F5D765110318D7F49D, ___U3CshowResultU3Ek__BackingField_2)); }
	inline int32_t get_U3CshowResultU3Ek__BackingField_2() const { return ___U3CshowResultU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CshowResultU3Ek__BackingField_2() { return &___U3CshowResultU3Ek__BackingField_2; }
	inline void set_U3CshowResultU3Ek__BackingField_2(int32_t value)
	{
		___U3CshowResultU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINISHEVENTARGS_TBC627E351CBF3F2563ACD7F5D765110318D7F49D_H
#ifndef U3CUNITYADSDIDFINISHU3EC__ANONSTOREY4_TF3998F44D42BAA24F6FC8B71E5E2443B74010AB7_H
#define U3CUNITYADSDIDFINISHU3EC__ANONSTOREY4_TF3998F44D42BAA24F6FC8B71E5E2443B74010AB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey4
struct  U3CUnityAdsDidFinishU3Ec__AnonStorey4_tF3998F44D42BAA24F6FC8B71E5E2443B74010AB7  : public RuntimeObject
{
public:
	// UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey4::showResult
	int32_t ___showResult_0;
	// UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey3 UnityEngine.Advertisements.iOS.Platform_<UnityAdsDidFinish>c__AnonStorey4::<>f__refU243
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1 * ___U3CU3Ef__refU243_1;

public:
	inline static int32_t get_offset_of_showResult_0() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey4_tF3998F44D42BAA24F6FC8B71E5E2443B74010AB7, ___showResult_0)); }
	inline int32_t get_showResult_0() const { return ___showResult_0; }
	inline int32_t* get_address_of_showResult_0() { return &___showResult_0; }
	inline void set_showResult_0(int32_t value)
	{
		___showResult_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_1() { return static_cast<int32_t>(offsetof(U3CUnityAdsDidFinishU3Ec__AnonStorey4_tF3998F44D42BAA24F6FC8B71E5E2443B74010AB7, ___U3CU3Ef__refU243_1)); }
	inline U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1 * get_U3CU3Ef__refU243_1() const { return ___U3CU3Ef__refU243_1; }
	inline U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1 ** get_address_of_U3CU3Ef__refU243_1() { return &___U3CU3Ef__refU243_1; }
	inline void set_U3CU3Ef__refU243_1(U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1 * value)
	{
		___U3CU3Ef__refU243_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUNITYADSDIDFINISHU3EC__ANONSTOREY4_TF3998F44D42BAA24F6FC8B71E5E2443B74010AB7_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#define TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t806752C775BA713A91B6588A07CA98417CABC003 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifndef CONSTRUCTORDELEGATE_T758C58A6B467BA586E2C6B57A535729A620BFCEC_H
#define CONSTRUCTORDELEGATE_T758C58A6B467BA586E2C6B57A535729A620BFCEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_ConstructorDelegate
struct  ConstructorDelegate_t758C58A6B467BA586E2C6B57A535729A620BFCEC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORDELEGATE_T758C58A6B467BA586E2C6B57A535729A620BFCEC_H
#ifndef GETDELEGATE_T5384142A3E4A970422627B6C02B4EB4319934A8F_H
#define GETDELEGATE_T5384142A3E4A970422627B6C02B4EB4319934A8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_GetDelegate
struct  GetDelegate_t5384142A3E4A970422627B6C02B4EB4319934A8F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDELEGATE_T5384142A3E4A970422627B6C02B4EB4319934A8F_H
#ifndef SETDELEGATE_TB85E10DD9F713CAE7ED82A18744F73AED4EA6D04_H
#define SETDELEGATE_TB85E10DD9F713CAE7ED82A18744F73AED4EA6D04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.SimpleJson.Reflection.ReflectionUtils_SetDelegate
struct  SetDelegate_tB85E10DD9F713CAE7ED82A18744F73AED4EA6D04  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETDELEGATE_TB85E10DD9F713CAE7ED82A18744F73AED4EA6D04_H
#ifndef UNITYADSDIDERROR_T0AEE1AF310020AF7C9F6A7881F50B21996A32C61_H
#define UNITYADSDIDERROR_T0AEE1AF310020AF7C9F6A7881F50B21996A32C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_unityAdsDidError
struct  unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSDIDERROR_T0AEE1AF310020AF7C9F6A7881F50B21996A32C61_H
#ifndef UNITYADSDIDFINISH_T9EE8992F28B4D62991C98D7D85A9E93A6835E045_H
#define UNITYADSDIDFINISH_T9EE8992F28B4D62991C98D7D85A9E93A6835E045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_unityAdsDidFinish
struct  unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSDIDFINISH_T9EE8992F28B4D62991C98D7D85A9E93A6835E045_H
#ifndef UNITYADSDIDSTART_TD78BF20784F7416774E14ADE453F6F4B4428D7FA_H
#define UNITYADSDIDSTART_TD78BF20784F7416774E14ADE453F6F4B4428D7FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_unityAdsDidStart
struct  unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSDIDSTART_TD78BF20784F7416774E14ADE453F6F4B4428D7FA_H
#ifndef UNITYADSREADY_T716CB3207A4D591AFD38E610CC8DC5EA4DE62A67_H
#define UNITYADSREADY_T716CB3207A4D591AFD38E610CC8DC5EA4DE62A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.iOS.Platform_unityAdsReady
struct  unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSREADY_T716CB3207A4D591AFD38E610CC8DC5EA4DE62A67_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#define AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Sound[] AudioManager::sounds
	SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* ___sounds_4;

public:
	inline static int32_t get_offset_of_sounds_4() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___sounds_4)); }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* get_sounds_4() const { return ___sounds_4; }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B** get_address_of_sounds_4() { return &___sounds_4; }
	inline void set_sounds_4(SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* value)
	{
		___sounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_4), value);
	}
};

struct AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields
{
public:
	// AudioManager AudioManager::instance
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields, ___instance_5)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_instance_5() const { return ___instance_5; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifndef BACKGROUNDSCROLL_TF3B9D70F56D79BB113311F4DA718F61CAB9F2519_H
#define BACKGROUNDSCROLL_TF3B9D70F56D79BB113311F4DA718F61CAB9F2519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScroll
struct  BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single BackgroundScroll::speed
	float ___speed_4;
	// UnityEngine.GameObject BackgroundScroll::background
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___background_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_background_5() { return static_cast<int32_t>(offsetof(BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519, ___background_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_background_5() const { return ___background_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_background_5() { return &___background_5; }
	inline void set_background_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___background_5 = value;
		Il2CppCodeGenWriteBarrier((&___background_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDSCROLL_TF3B9D70F56D79BB113311F4DA718F61CAB9F2519_H
#ifndef BLOCK_T613E03B6725E5824DD83D0B230AC258E08338D24_H
#define BLOCK_T613E03B6725E5824DD83D0B230AC258E08338D24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Block
struct  Block_t613E03B6725E5824DD83D0B230AC258E08338D24  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ConstantForce2D Block::cF
	ConstantForce2D_t7EF90AB9059BC8CDD84C1699757F3AD9F8FB8C0B * ___cF_4;
	// Player Block::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_5;
	// System.Int32 Block::value
	int32_t ___value_6;
	// System.Boolean Block::getPoints
	bool ___getPoints_7;
	// System.Int32 Block::rand
	int32_t ___rand_8;
	// System.Boolean Block::canBeSuperSized
	bool ___canBeSuperSized_9;
	// System.Int32 Block::damage
	int32_t ___damage_10;
	// System.Boolean Block::isSuperSized
	bool ___isSuperSized_11;
	// UnityEngine.GameObject[] Block::otherComets
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___otherComets_12;
	// UnityEngine.Vector3 Block::monsterSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___monsterSize_13;
	// UnityEngine.ParticleSystem Block::trail
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___trail_14;
	// UnityEngine.Animator Block::explode
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___explode_15;

public:
	inline static int32_t get_offset_of_cF_4() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___cF_4)); }
	inline ConstantForce2D_t7EF90AB9059BC8CDD84C1699757F3AD9F8FB8C0B * get_cF_4() const { return ___cF_4; }
	inline ConstantForce2D_t7EF90AB9059BC8CDD84C1699757F3AD9F8FB8C0B ** get_address_of_cF_4() { return &___cF_4; }
	inline void set_cF_4(ConstantForce2D_t7EF90AB9059BC8CDD84C1699757F3AD9F8FB8C0B * value)
	{
		___cF_4 = value;
		Il2CppCodeGenWriteBarrier((&___cF_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___player_5)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_5() const { return ___player_5; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___value_6)); }
	inline int32_t get_value_6() const { return ___value_6; }
	inline int32_t* get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(int32_t value)
	{
		___value_6 = value;
	}

	inline static int32_t get_offset_of_getPoints_7() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___getPoints_7)); }
	inline bool get_getPoints_7() const { return ___getPoints_7; }
	inline bool* get_address_of_getPoints_7() { return &___getPoints_7; }
	inline void set_getPoints_7(bool value)
	{
		___getPoints_7 = value;
	}

	inline static int32_t get_offset_of_rand_8() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___rand_8)); }
	inline int32_t get_rand_8() const { return ___rand_8; }
	inline int32_t* get_address_of_rand_8() { return &___rand_8; }
	inline void set_rand_8(int32_t value)
	{
		___rand_8 = value;
	}

	inline static int32_t get_offset_of_canBeSuperSized_9() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___canBeSuperSized_9)); }
	inline bool get_canBeSuperSized_9() const { return ___canBeSuperSized_9; }
	inline bool* get_address_of_canBeSuperSized_9() { return &___canBeSuperSized_9; }
	inline void set_canBeSuperSized_9(bool value)
	{
		___canBeSuperSized_9 = value;
	}

	inline static int32_t get_offset_of_damage_10() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___damage_10)); }
	inline int32_t get_damage_10() const { return ___damage_10; }
	inline int32_t* get_address_of_damage_10() { return &___damage_10; }
	inline void set_damage_10(int32_t value)
	{
		___damage_10 = value;
	}

	inline static int32_t get_offset_of_isSuperSized_11() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___isSuperSized_11)); }
	inline bool get_isSuperSized_11() const { return ___isSuperSized_11; }
	inline bool* get_address_of_isSuperSized_11() { return &___isSuperSized_11; }
	inline void set_isSuperSized_11(bool value)
	{
		___isSuperSized_11 = value;
	}

	inline static int32_t get_offset_of_otherComets_12() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___otherComets_12)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_otherComets_12() const { return ___otherComets_12; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_otherComets_12() { return &___otherComets_12; }
	inline void set_otherComets_12(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___otherComets_12 = value;
		Il2CppCodeGenWriteBarrier((&___otherComets_12), value);
	}

	inline static int32_t get_offset_of_monsterSize_13() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___monsterSize_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_monsterSize_13() const { return ___monsterSize_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_monsterSize_13() { return &___monsterSize_13; }
	inline void set_monsterSize_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___monsterSize_13 = value;
	}

	inline static int32_t get_offset_of_trail_14() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___trail_14)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_trail_14() const { return ___trail_14; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_trail_14() { return &___trail_14; }
	inline void set_trail_14(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___trail_14 = value;
		Il2CppCodeGenWriteBarrier((&___trail_14), value);
	}

	inline static int32_t get_offset_of_explode_15() { return static_cast<int32_t>(offsetof(Block_t613E03B6725E5824DD83D0B230AC258E08338D24, ___explode_15)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_explode_15() const { return ___explode_15; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_explode_15() { return &___explode_15; }
	inline void set_explode_15(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___explode_15 = value;
		Il2CppCodeGenWriteBarrier((&___explode_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK_T613E03B6725E5824DD83D0B230AC258E08338D24_H
#ifndef BUYBUTTON_TED0F7B2CEC81E44C83395BF3C51745C21F675E2B_H
#define BUYBUTTON_TED0F7B2CEC81E44C83395BF3C51745C21F675E2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyButton
struct  BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// BuyButton_ItemType BuyButton::itemType
	int32_t ___itemType_4;
	// TMPro.TextMeshProUGUI BuyButton::priceText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___priceText_5;
	// System.String BuyButton::defaultText
	String_t* ___defaultText_6;

public:
	inline static int32_t get_offset_of_itemType_4() { return static_cast<int32_t>(offsetof(BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B, ___itemType_4)); }
	inline int32_t get_itemType_4() const { return ___itemType_4; }
	inline int32_t* get_address_of_itemType_4() { return &___itemType_4; }
	inline void set_itemType_4(int32_t value)
	{
		___itemType_4 = value;
	}

	inline static int32_t get_offset_of_priceText_5() { return static_cast<int32_t>(offsetof(BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B, ___priceText_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_priceText_5() const { return ___priceText_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_priceText_5() { return &___priceText_5; }
	inline void set_priceText_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___priceText_5 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_5), value);
	}

	inline static int32_t get_offset_of_defaultText_6() { return static_cast<int32_t>(offsetof(BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B, ___defaultText_6)); }
	inline String_t* get_defaultText_6() const { return ___defaultText_6; }
	inline String_t** get_address_of_defaultText_6() { return &___defaultText_6; }
	inline void set_defaultText_6(String_t* value)
	{
		___defaultText_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYBUTTON_TED0F7B2CEC81E44C83395BF3C51745C21F675E2B_H
#ifndef CURSER_T60015E78331BAC502803E3D3F29AEB516654948E_H
#define CURSER_T60015E78331BAC502803E3D3F29AEB516654948E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Curser
struct  Curser_t60015E78331BAC502803E3D3F29AEB516654948E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Touch Curser::touch
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  ___touch_4;

public:
	inline static int32_t get_offset_of_touch_4() { return static_cast<int32_t>(offsetof(Curser_t60015E78331BAC502803E3D3F29AEB516654948E, ___touch_4)); }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003  get_touch_4() const { return ___touch_4; }
	inline Touch_t806752C775BA713A91B6588A07CA98417CABC003 * get_address_of_touch_4() { return &___touch_4; }
	inline void set_touch_4(Touch_t806752C775BA713A91B6588A07CA98417CABC003  value)
	{
		___touch_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSER_T60015E78331BAC502803E3D3F29AEB516654948E_H
#ifndef DEATHMENU_T0CB407645FA349343DECE47FD54C29699A6B3124_H
#define DEATHMENU_T0CB407645FA349343DECE47FD54C29699A6B3124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathMenu
struct  DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator DeathMenu::fadeout
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___fadeout_5;
	// UnityEngine.GameObject DeathMenu::deathMenuUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___deathMenuUI_6;
	// UnityEngine.UI.Button DeathMenu::continueButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___continueButton_7;
	// TMPro.TextMeshProUGUI DeathMenu::continueText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___continueText_8;
	// UnityEngine.GameObject DeathMenu::pauseButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pauseButton_9;
	// System.Int32 DeathMenu::lives
	int32_t ___lives_10;
	// Player DeathMenu::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_11;

public:
	inline static int32_t get_offset_of_fadeout_5() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___fadeout_5)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_fadeout_5() const { return ___fadeout_5; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_fadeout_5() { return &___fadeout_5; }
	inline void set_fadeout_5(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___fadeout_5 = value;
		Il2CppCodeGenWriteBarrier((&___fadeout_5), value);
	}

	inline static int32_t get_offset_of_deathMenuUI_6() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___deathMenuUI_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_deathMenuUI_6() const { return ___deathMenuUI_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_deathMenuUI_6() { return &___deathMenuUI_6; }
	inline void set_deathMenuUI_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___deathMenuUI_6 = value;
		Il2CppCodeGenWriteBarrier((&___deathMenuUI_6), value);
	}

	inline static int32_t get_offset_of_continueButton_7() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___continueButton_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_continueButton_7() const { return ___continueButton_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_continueButton_7() { return &___continueButton_7; }
	inline void set_continueButton_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___continueButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___continueButton_7), value);
	}

	inline static int32_t get_offset_of_continueText_8() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___continueText_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_continueText_8() const { return ___continueText_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_continueText_8() { return &___continueText_8; }
	inline void set_continueText_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___continueText_8 = value;
		Il2CppCodeGenWriteBarrier((&___continueText_8), value);
	}

	inline static int32_t get_offset_of_pauseButton_9() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___pauseButton_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pauseButton_9() const { return ___pauseButton_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pauseButton_9() { return &___pauseButton_9; }
	inline void set_pauseButton_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pauseButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButton_9), value);
	}

	inline static int32_t get_offset_of_lives_10() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___lives_10)); }
	inline int32_t get_lives_10() const { return ___lives_10; }
	inline int32_t* get_address_of_lives_10() { return &___lives_10; }
	inline void set_lives_10(int32_t value)
	{
		___lives_10 = value;
	}

	inline static int32_t get_offset_of_player_11() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124, ___player_11)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_11() const { return ___player_11; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_11() { return &___player_11; }
	inline void set_player_11(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_11 = value;
		Il2CppCodeGenWriteBarrier((&___player_11), value);
	}
};

struct DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124_StaticFields
{
public:
	// System.Boolean DeathMenu::continued
	bool ___continued_4;

public:
	inline static int32_t get_offset_of_continued_4() { return static_cast<int32_t>(offsetof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124_StaticFields, ___continued_4)); }
	inline bool get_continued_4() const { return ___continued_4; }
	inline bool* get_address_of_continued_4() { return &___continued_4; }
	inline void set_continued_4(bool value)
	{
		___continued_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHMENU_T0CB407645FA349343DECE47FD54C29699A6B3124_H
#ifndef DESTROYATBOTTOM_T5D5F03C616513070775056625ADCE00243A22C28_H
#define DESTROYATBOTTOM_T5D5F03C616513070775056625ADCE00243A22C28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyAtBottom
struct  DestroyAtBottom_t5D5F03C616513070775056625ADCE00243A22C28  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Player DestroyAtBottom::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_4;
	// Block DestroyAtBottom::block
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24 * ___block_5;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(DestroyAtBottom_t5D5F03C616513070775056625ADCE00243A22C28, ___player_4)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_4() const { return ___player_4; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_block_5() { return static_cast<int32_t>(offsetof(DestroyAtBottom_t5D5F03C616513070775056625ADCE00243A22C28, ___block_5)); }
	inline Block_t613E03B6725E5824DD83D0B230AC258E08338D24 * get_block_5() const { return ___block_5; }
	inline Block_t613E03B6725E5824DD83D0B230AC258E08338D24 ** get_address_of_block_5() { return &___block_5; }
	inline void set_block_5(Block_t613E03B6725E5824DD83D0B230AC258E08338D24 * value)
	{
		___block_5 = value;
		Il2CppCodeGenWriteBarrier((&___block_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYATBOTTOM_T5D5F03C616513070775056625ADCE00243A22C28_H
#ifndef DESTROYATBOTTOMLARGE_T858E6FD2D9935E296877163DD64389352C2D74D8_H
#define DESTROYATBOTTOMLARGE_T858E6FD2D9935E296877163DD64389352C2D74D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyAtBottomLarge
struct  DestroyAtBottomLarge_t858E6FD2D9935E296877163DD64389352C2D74D8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYATBOTTOMLARGE_T858E6FD2D9935E296877163DD64389352C2D74D8_H
#ifndef ENGINE_T62C213B195B780D1183D68751B5F0118D702848D_H
#define ENGINE_T62C213B195B780D1183D68751B5F0118D702848D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Engine
struct  Engine_t62C213B195B780D1183D68751B5F0118D702848D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem Engine::engine
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___engine_4;
	// Player Engine::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_5;
	// System.Single Engine::flashtime
	float ___flashtime_6;
	// System.Boolean Engine::go
	bool ___go_7;
	// UnityEngine.Color Engine::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_8;

public:
	inline static int32_t get_offset_of_engine_4() { return static_cast<int32_t>(offsetof(Engine_t62C213B195B780D1183D68751B5F0118D702848D, ___engine_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_engine_4() const { return ___engine_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_engine_4() { return &___engine_4; }
	inline void set_engine_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___engine_4 = value;
		Il2CppCodeGenWriteBarrier((&___engine_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(Engine_t62C213B195B780D1183D68751B5F0118D702848D, ___player_5)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_5() const { return ___player_5; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_flashtime_6() { return static_cast<int32_t>(offsetof(Engine_t62C213B195B780D1183D68751B5F0118D702848D, ___flashtime_6)); }
	inline float get_flashtime_6() const { return ___flashtime_6; }
	inline float* get_address_of_flashtime_6() { return &___flashtime_6; }
	inline void set_flashtime_6(float value)
	{
		___flashtime_6 = value;
	}

	inline static int32_t get_offset_of_go_7() { return static_cast<int32_t>(offsetof(Engine_t62C213B195B780D1183D68751B5F0118D702848D, ___go_7)); }
	inline bool get_go_7() const { return ___go_7; }
	inline bool* get_address_of_go_7() { return &___go_7; }
	inline void set_go_7(bool value)
	{
		___go_7 = value;
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(Engine_t62C213B195B780D1183D68751B5F0118D702848D, ___color_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_8() const { return ___color_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENGINE_T62C213B195B780D1183D68751B5F0118D702848D_H
#ifndef EXPLOSION_T02AD52B12A8E210C86E08E90E69809A4F2DD23AA_H
#define EXPLOSION_T02AD52B12A8E210C86E08E90E69809A4F2DD23AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Explosion
struct  Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem Explosion::boom
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___boom_4;

public:
	inline static int32_t get_offset_of_boom_4() { return static_cast<int32_t>(offsetof(Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA, ___boom_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_boom_4() const { return ___boom_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_boom_4() { return &___boom_4; }
	inline void set_boom_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___boom_4 = value;
		Il2CppCodeGenWriteBarrier((&___boom_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSION_T02AD52B12A8E210C86E08E90E69809A4F2DD23AA_H
#ifndef GAMEBEGIN_T534AB32FFB887AF031E737A7CAD5EB4E8C809405_H
#define GAMEBEGIN_T534AB32FFB887AF031E737A7CAD5EB4E8C809405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameBegin
struct  GameBegin_t534AB32FFB887AF031E737A7CAD5EB4E8C809405  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator GameBegin::player
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___player_4;
	// UnityEngine.GameObject GameBegin::canvas
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___canvas_5;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(GameBegin_t534AB32FFB887AF031E737A7CAD5EB4E8C809405, ___player_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_player_4() const { return ___player_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(GameBegin_t534AB32FFB887AF031E737A7CAD5EB4E8C809405, ___canvas_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_canvas_5() const { return ___canvas_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEBEGIN_T534AB32FFB887AF031E737A7CAD5EB4E8C809405_H
#ifndef GAMERESTART_TB1422A29C71CC4EA28057CE03A9D579DA2EFFF7C_H
#define GAMERESTART_TB1422A29C71CC4EA28057CE03A9D579DA2EFFF7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameRestart
struct  GameRestart_tB1422A29C71CC4EA28057CE03A9D579DA2EFFF7C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMERESTART_TB1422A29C71CC4EA28057CE03A9D579DA2EFFF7C_H
#ifndef HEALTHCOUNTER_TEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E_H
#define HEALTHCOUNTER_TEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthCounter
struct  HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 HealthCounter::health
	int32_t ___health_4;
	// Player HealthCounter::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_5;
	// UnityEngine.UI.Image[] HealthCounter::healthCounter
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___healthCounter_6;
	// UnityEngine.Sprite HealthCounter::shipsHas
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___shipsHas_7;
	// UnityEngine.Sprite HealthCounter::shipslost
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___shipslost_8;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E, ___health_4)); }
	inline int32_t get_health_4() const { return ___health_4; }
	inline int32_t* get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(int32_t value)
	{
		___health_4 = value;
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E, ___player_5)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_5() const { return ___player_5; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_healthCounter_6() { return static_cast<int32_t>(offsetof(HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E, ___healthCounter_6)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_healthCounter_6() const { return ___healthCounter_6; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_healthCounter_6() { return &___healthCounter_6; }
	inline void set_healthCounter_6(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___healthCounter_6 = value;
		Il2CppCodeGenWriteBarrier((&___healthCounter_6), value);
	}

	inline static int32_t get_offset_of_shipsHas_7() { return static_cast<int32_t>(offsetof(HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E, ___shipsHas_7)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_shipsHas_7() const { return ___shipsHas_7; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_shipsHas_7() { return &___shipsHas_7; }
	inline void set_shipsHas_7(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___shipsHas_7 = value;
		Il2CppCodeGenWriteBarrier((&___shipsHas_7), value);
	}

	inline static int32_t get_offset_of_shipslost_8() { return static_cast<int32_t>(offsetof(HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E, ___shipslost_8)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_shipslost_8() const { return ___shipslost_8; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_shipslost_8() { return &___shipslost_8; }
	inline void set_shipslost_8(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___shipslost_8 = value;
		Il2CppCodeGenWriteBarrier((&___shipslost_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHCOUNTER_TEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E_H
#ifndef HEARTTOKKEN_T170763CE258ADD2C5F33A17D44655474031CC6FC_H
#define HEARTTOKKEN_T170763CE258ADD2C5F33A17D44655474031CC6FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeartTokken
struct  HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Player HeartTokken::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_4;
	// UnityEngine.Rigidbody2D HeartTokken::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_5;
	// UnityEngine.Vector3 HeartTokken::move
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move_6;
	// System.Boolean HeartTokken::needsToMove
	bool ___needsToMove_7;
	// System.Single HeartTokken::screenLeft
	float ___screenLeft_8;
	// System.Single HeartTokken::screenRight
	float ___screenRight_9;
	// System.Single HeartTokken::yValue
	float ___yValue_10;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___player_4)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_4() const { return ___player_4; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_rb_5() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___rb_5)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_5() const { return ___rb_5; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_5() { return &___rb_5; }
	inline void set_rb_5(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_5 = value;
		Il2CppCodeGenWriteBarrier((&___rb_5), value);
	}

	inline static int32_t get_offset_of_move_6() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___move_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_move_6() const { return ___move_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_move_6() { return &___move_6; }
	inline void set_move_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___move_6 = value;
	}

	inline static int32_t get_offset_of_needsToMove_7() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___needsToMove_7)); }
	inline bool get_needsToMove_7() const { return ___needsToMove_7; }
	inline bool* get_address_of_needsToMove_7() { return &___needsToMove_7; }
	inline void set_needsToMove_7(bool value)
	{
		___needsToMove_7 = value;
	}

	inline static int32_t get_offset_of_screenLeft_8() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___screenLeft_8)); }
	inline float get_screenLeft_8() const { return ___screenLeft_8; }
	inline float* get_address_of_screenLeft_8() { return &___screenLeft_8; }
	inline void set_screenLeft_8(float value)
	{
		___screenLeft_8 = value;
	}

	inline static int32_t get_offset_of_screenRight_9() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___screenRight_9)); }
	inline float get_screenRight_9() const { return ___screenRight_9; }
	inline float* get_address_of_screenRight_9() { return &___screenRight_9; }
	inline void set_screenRight_9(float value)
	{
		___screenRight_9 = value;
	}

	inline static int32_t get_offset_of_yValue_10() { return static_cast<int32_t>(offsetof(HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC, ___yValue_10)); }
	inline float get_yValue_10() const { return ___yValue_10; }
	inline float* get_address_of_yValue_10() { return &___yValue_10; }
	inline void set_yValue_10(float value)
	{
		___yValue_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEARTTOKKEN_T170763CE258ADD2C5F33A17D44655474031CC6FC_H
#ifndef HISCOREMANAGER_TECD89E9A4293A0AC2D4261C014A572E0DD26BCFF_H
#define HISCOREMANAGER_TECD89E9A4293A0AC2D4261C014A572E0DD26BCFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HiScoreManager
struct  HiScoreManager_tECD89E9A4293A0AC2D4261C014A572E0DD26BCFF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text HiScoreManager::highScoreText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___highScoreText_4;
	// System.Int32 HiScoreManager::highScore
	int32_t ___highScore_5;

public:
	inline static int32_t get_offset_of_highScoreText_4() { return static_cast<int32_t>(offsetof(HiScoreManager_tECD89E9A4293A0AC2D4261C014A572E0DD26BCFF, ___highScoreText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_highScoreText_4() const { return ___highScoreText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_highScoreText_4() { return &___highScoreText_4; }
	inline void set_highScoreText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___highScoreText_4 = value;
		Il2CppCodeGenWriteBarrier((&___highScoreText_4), value);
	}

	inline static int32_t get_offset_of_highScore_5() { return static_cast<int32_t>(offsetof(HiScoreManager_tECD89E9A4293A0AC2D4261C014A572E0DD26BCFF, ___highScore_5)); }
	inline int32_t get_highScore_5() const { return ___highScore_5; }
	inline int32_t* get_address_of_highScore_5() { return &___highScore_5; }
	inline void set_highScore_5(int32_t value)
	{
		___highScore_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HISCOREMANAGER_TECD89E9A4293A0AC2D4261C014A572E0DD26BCFF_H
#ifndef IAPDEMO_TF4701A448092D00074EDBAB42C7EA3D673B71C92_H
#define IAPDEMO_TF4701A448092D00074EDBAB42C7EA3D673B71C92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo
struct  IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Purchasing.IStoreController IAPDemo::m_Controller
	RuntimeObject* ___m_Controller_4;
	// UnityEngine.Purchasing.IAppleExtensions IAPDemo::m_AppleExtensions
	RuntimeObject* ___m_AppleExtensions_5;
	// UnityEngine.Purchasing.IMoolahExtension IAPDemo::m_MoolahExtensions
	RuntimeObject* ___m_MoolahExtensions_6;
	// UnityEngine.Purchasing.ISamsungAppsExtensions IAPDemo::m_SamsungExtensions
	RuntimeObject* ___m_SamsungExtensions_7;
	// UnityEngine.Purchasing.IMicrosoftExtensions IAPDemo::m_MicrosoftExtensions
	RuntimeObject* ___m_MicrosoftExtensions_8;
	// UnityEngine.Purchasing.IUnityChannelExtensions IAPDemo::m_UnityChannelExtensions
	RuntimeObject* ___m_UnityChannelExtensions_9;
	// UnityEngine.Purchasing.ITransactionHistoryExtensions IAPDemo::m_TransactionHistoryExtensions
	RuntimeObject* ___m_TransactionHistoryExtensions_10;
	// UnityEngine.Purchasing.IGooglePlayStoreExtensions IAPDemo::m_GooglePlayStoreExtensions
	RuntimeObject* ___m_GooglePlayStoreExtensions_11;
	// System.Boolean IAPDemo::m_IsGooglePlayStoreSelected
	bool ___m_IsGooglePlayStoreSelected_12;
	// System.Boolean IAPDemo::m_IsSamsungAppsStoreSelected
	bool ___m_IsSamsungAppsStoreSelected_13;
	// System.Boolean IAPDemo::m_IsCloudMoolahStoreSelected
	bool ___m_IsCloudMoolahStoreSelected_14;
	// System.Boolean IAPDemo::m_IsUnityChannelSelected
	bool ___m_IsUnityChannelSelected_15;
	// System.String IAPDemo::m_LastTransactionID
	String_t* ___m_LastTransactionID_16;
	// System.Boolean IAPDemo::m_IsLoggedIn
	bool ___m_IsLoggedIn_17;
	// IAPDemo_UnityChannelLoginHandler IAPDemo::unityChannelLoginHandler
	UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E * ___unityChannelLoginHandler_18;
	// System.Boolean IAPDemo::m_FetchReceiptPayloadOnPurchase
	bool ___m_FetchReceiptPayloadOnPurchase_19;
	// System.Boolean IAPDemo::m_PurchaseInProgress
	bool ___m_PurchaseInProgress_20;
	// System.Collections.Generic.Dictionary`2<System.String,IAPDemoProductUI> IAPDemo::m_ProductUIs
	Dictionary_2_t86FD27876E6B6061449915AF4C4EE31BE83977FA * ___m_ProductUIs_21;
	// UnityEngine.GameObject IAPDemo::productUITemplate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___productUITemplate_22;
	// UnityEngine.RectTransform IAPDemo::contentRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___contentRect_23;
	// UnityEngine.UI.Button IAPDemo::restoreButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___restoreButton_24;
	// UnityEngine.UI.Button IAPDemo::loginButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___loginButton_25;
	// UnityEngine.UI.Button IAPDemo::validateButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___validateButton_26;
	// UnityEngine.UI.Text IAPDemo::versionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___versionText_27;

public:
	inline static int32_t get_offset_of_m_Controller_4() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_Controller_4)); }
	inline RuntimeObject* get_m_Controller_4() const { return ___m_Controller_4; }
	inline RuntimeObject** get_address_of_m_Controller_4() { return &___m_Controller_4; }
	inline void set_m_Controller_4(RuntimeObject* value)
	{
		___m_Controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_4), value);
	}

	inline static int32_t get_offset_of_m_AppleExtensions_5() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_AppleExtensions_5)); }
	inline RuntimeObject* get_m_AppleExtensions_5() const { return ___m_AppleExtensions_5; }
	inline RuntimeObject** get_address_of_m_AppleExtensions_5() { return &___m_AppleExtensions_5; }
	inline void set_m_AppleExtensions_5(RuntimeObject* value)
	{
		___m_AppleExtensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppleExtensions_5), value);
	}

	inline static int32_t get_offset_of_m_MoolahExtensions_6() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_MoolahExtensions_6)); }
	inline RuntimeObject* get_m_MoolahExtensions_6() const { return ___m_MoolahExtensions_6; }
	inline RuntimeObject** get_address_of_m_MoolahExtensions_6() { return &___m_MoolahExtensions_6; }
	inline void set_m_MoolahExtensions_6(RuntimeObject* value)
	{
		___m_MoolahExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoolahExtensions_6), value);
	}

	inline static int32_t get_offset_of_m_SamsungExtensions_7() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_SamsungExtensions_7)); }
	inline RuntimeObject* get_m_SamsungExtensions_7() const { return ___m_SamsungExtensions_7; }
	inline RuntimeObject** get_address_of_m_SamsungExtensions_7() { return &___m_SamsungExtensions_7; }
	inline void set_m_SamsungExtensions_7(RuntimeObject* value)
	{
		___m_SamsungExtensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SamsungExtensions_7), value);
	}

	inline static int32_t get_offset_of_m_MicrosoftExtensions_8() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_MicrosoftExtensions_8)); }
	inline RuntimeObject* get_m_MicrosoftExtensions_8() const { return ___m_MicrosoftExtensions_8; }
	inline RuntimeObject** get_address_of_m_MicrosoftExtensions_8() { return &___m_MicrosoftExtensions_8; }
	inline void set_m_MicrosoftExtensions_8(RuntimeObject* value)
	{
		___m_MicrosoftExtensions_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MicrosoftExtensions_8), value);
	}

	inline static int32_t get_offset_of_m_UnityChannelExtensions_9() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_UnityChannelExtensions_9)); }
	inline RuntimeObject* get_m_UnityChannelExtensions_9() const { return ___m_UnityChannelExtensions_9; }
	inline RuntimeObject** get_address_of_m_UnityChannelExtensions_9() { return &___m_UnityChannelExtensions_9; }
	inline void set_m_UnityChannelExtensions_9(RuntimeObject* value)
	{
		___m_UnityChannelExtensions_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnityChannelExtensions_9), value);
	}

	inline static int32_t get_offset_of_m_TransactionHistoryExtensions_10() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_TransactionHistoryExtensions_10)); }
	inline RuntimeObject* get_m_TransactionHistoryExtensions_10() const { return ___m_TransactionHistoryExtensions_10; }
	inline RuntimeObject** get_address_of_m_TransactionHistoryExtensions_10() { return &___m_TransactionHistoryExtensions_10; }
	inline void set_m_TransactionHistoryExtensions_10(RuntimeObject* value)
	{
		___m_TransactionHistoryExtensions_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TransactionHistoryExtensions_10), value);
	}

	inline static int32_t get_offset_of_m_GooglePlayStoreExtensions_11() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_GooglePlayStoreExtensions_11)); }
	inline RuntimeObject* get_m_GooglePlayStoreExtensions_11() const { return ___m_GooglePlayStoreExtensions_11; }
	inline RuntimeObject** get_address_of_m_GooglePlayStoreExtensions_11() { return &___m_GooglePlayStoreExtensions_11; }
	inline void set_m_GooglePlayStoreExtensions_11(RuntimeObject* value)
	{
		___m_GooglePlayStoreExtensions_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_GooglePlayStoreExtensions_11), value);
	}

	inline static int32_t get_offset_of_m_IsGooglePlayStoreSelected_12() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_IsGooglePlayStoreSelected_12)); }
	inline bool get_m_IsGooglePlayStoreSelected_12() const { return ___m_IsGooglePlayStoreSelected_12; }
	inline bool* get_address_of_m_IsGooglePlayStoreSelected_12() { return &___m_IsGooglePlayStoreSelected_12; }
	inline void set_m_IsGooglePlayStoreSelected_12(bool value)
	{
		___m_IsGooglePlayStoreSelected_12 = value;
	}

	inline static int32_t get_offset_of_m_IsSamsungAppsStoreSelected_13() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_IsSamsungAppsStoreSelected_13)); }
	inline bool get_m_IsSamsungAppsStoreSelected_13() const { return ___m_IsSamsungAppsStoreSelected_13; }
	inline bool* get_address_of_m_IsSamsungAppsStoreSelected_13() { return &___m_IsSamsungAppsStoreSelected_13; }
	inline void set_m_IsSamsungAppsStoreSelected_13(bool value)
	{
		___m_IsSamsungAppsStoreSelected_13 = value;
	}

	inline static int32_t get_offset_of_m_IsCloudMoolahStoreSelected_14() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_IsCloudMoolahStoreSelected_14)); }
	inline bool get_m_IsCloudMoolahStoreSelected_14() const { return ___m_IsCloudMoolahStoreSelected_14; }
	inline bool* get_address_of_m_IsCloudMoolahStoreSelected_14() { return &___m_IsCloudMoolahStoreSelected_14; }
	inline void set_m_IsCloudMoolahStoreSelected_14(bool value)
	{
		___m_IsCloudMoolahStoreSelected_14 = value;
	}

	inline static int32_t get_offset_of_m_IsUnityChannelSelected_15() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_IsUnityChannelSelected_15)); }
	inline bool get_m_IsUnityChannelSelected_15() const { return ___m_IsUnityChannelSelected_15; }
	inline bool* get_address_of_m_IsUnityChannelSelected_15() { return &___m_IsUnityChannelSelected_15; }
	inline void set_m_IsUnityChannelSelected_15(bool value)
	{
		___m_IsUnityChannelSelected_15 = value;
	}

	inline static int32_t get_offset_of_m_LastTransactionID_16() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_LastTransactionID_16)); }
	inline String_t* get_m_LastTransactionID_16() const { return ___m_LastTransactionID_16; }
	inline String_t** get_address_of_m_LastTransactionID_16() { return &___m_LastTransactionID_16; }
	inline void set_m_LastTransactionID_16(String_t* value)
	{
		___m_LastTransactionID_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastTransactionID_16), value);
	}

	inline static int32_t get_offset_of_m_IsLoggedIn_17() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_IsLoggedIn_17)); }
	inline bool get_m_IsLoggedIn_17() const { return ___m_IsLoggedIn_17; }
	inline bool* get_address_of_m_IsLoggedIn_17() { return &___m_IsLoggedIn_17; }
	inline void set_m_IsLoggedIn_17(bool value)
	{
		___m_IsLoggedIn_17 = value;
	}

	inline static int32_t get_offset_of_unityChannelLoginHandler_18() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___unityChannelLoginHandler_18)); }
	inline UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E * get_unityChannelLoginHandler_18() const { return ___unityChannelLoginHandler_18; }
	inline UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E ** get_address_of_unityChannelLoginHandler_18() { return &___unityChannelLoginHandler_18; }
	inline void set_unityChannelLoginHandler_18(UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E * value)
	{
		___unityChannelLoginHandler_18 = value;
		Il2CppCodeGenWriteBarrier((&___unityChannelLoginHandler_18), value);
	}

	inline static int32_t get_offset_of_m_FetchReceiptPayloadOnPurchase_19() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_FetchReceiptPayloadOnPurchase_19)); }
	inline bool get_m_FetchReceiptPayloadOnPurchase_19() const { return ___m_FetchReceiptPayloadOnPurchase_19; }
	inline bool* get_address_of_m_FetchReceiptPayloadOnPurchase_19() { return &___m_FetchReceiptPayloadOnPurchase_19; }
	inline void set_m_FetchReceiptPayloadOnPurchase_19(bool value)
	{
		___m_FetchReceiptPayloadOnPurchase_19 = value;
	}

	inline static int32_t get_offset_of_m_PurchaseInProgress_20() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_PurchaseInProgress_20)); }
	inline bool get_m_PurchaseInProgress_20() const { return ___m_PurchaseInProgress_20; }
	inline bool* get_address_of_m_PurchaseInProgress_20() { return &___m_PurchaseInProgress_20; }
	inline void set_m_PurchaseInProgress_20(bool value)
	{
		___m_PurchaseInProgress_20 = value;
	}

	inline static int32_t get_offset_of_m_ProductUIs_21() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___m_ProductUIs_21)); }
	inline Dictionary_2_t86FD27876E6B6061449915AF4C4EE31BE83977FA * get_m_ProductUIs_21() const { return ___m_ProductUIs_21; }
	inline Dictionary_2_t86FD27876E6B6061449915AF4C4EE31BE83977FA ** get_address_of_m_ProductUIs_21() { return &___m_ProductUIs_21; }
	inline void set_m_ProductUIs_21(Dictionary_2_t86FD27876E6B6061449915AF4C4EE31BE83977FA * value)
	{
		___m_ProductUIs_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductUIs_21), value);
	}

	inline static int32_t get_offset_of_productUITemplate_22() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___productUITemplate_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_productUITemplate_22() const { return ___productUITemplate_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_productUITemplate_22() { return &___productUITemplate_22; }
	inline void set_productUITemplate_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___productUITemplate_22 = value;
		Il2CppCodeGenWriteBarrier((&___productUITemplate_22), value);
	}

	inline static int32_t get_offset_of_contentRect_23() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___contentRect_23)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_contentRect_23() const { return ___contentRect_23; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_contentRect_23() { return &___contentRect_23; }
	inline void set_contentRect_23(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___contentRect_23 = value;
		Il2CppCodeGenWriteBarrier((&___contentRect_23), value);
	}

	inline static int32_t get_offset_of_restoreButton_24() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___restoreButton_24)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_restoreButton_24() const { return ___restoreButton_24; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_restoreButton_24() { return &___restoreButton_24; }
	inline void set_restoreButton_24(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___restoreButton_24 = value;
		Il2CppCodeGenWriteBarrier((&___restoreButton_24), value);
	}

	inline static int32_t get_offset_of_loginButton_25() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___loginButton_25)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_loginButton_25() const { return ___loginButton_25; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_loginButton_25() { return &___loginButton_25; }
	inline void set_loginButton_25(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___loginButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___loginButton_25), value);
	}

	inline static int32_t get_offset_of_validateButton_26() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___validateButton_26)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_validateButton_26() const { return ___validateButton_26; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_validateButton_26() { return &___validateButton_26; }
	inline void set_validateButton_26(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___validateButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___validateButton_26), value);
	}

	inline static int32_t get_offset_of_versionText_27() { return static_cast<int32_t>(offsetof(IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92, ___versionText_27)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_versionText_27() const { return ___versionText_27; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_versionText_27() { return &___versionText_27; }
	inline void set_versionText_27(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___versionText_27 = value;
		Il2CppCodeGenWriteBarrier((&___versionText_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPDEMO_TF4701A448092D00074EDBAB42C7EA3D673B71C92_H
#ifndef LEVELFADER_T80A930EAED182822655C69B819748C49C59A31A5_H
#define LEVELFADER_T80A930EAED182822655C69B819748C49C59A31A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelFader
struct  LevelFader_t80A930EAED182822655C69B819748C49C59A31A5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator LevelFader::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_4;
	// System.Int32 LevelFader::skipAd
	int32_t ___skipAd_6;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(LevelFader_t80A930EAED182822655C69B819748C49C59A31A5, ___animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_4() const { return ___animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}

	inline static int32_t get_offset_of_skipAd_6() { return static_cast<int32_t>(offsetof(LevelFader_t80A930EAED182822655C69B819748C49C59A31A5, ___skipAd_6)); }
	inline int32_t get_skipAd_6() const { return ___skipAd_6; }
	inline int32_t* get_address_of_skipAd_6() { return &___skipAd_6; }
	inline void set_skipAd_6(int32_t value)
	{
		___skipAd_6 = value;
	}
};

struct LevelFader_t80A930EAED182822655C69B819748C49C59A31A5_StaticFields
{
public:
	// System.Int32 LevelFader::_level
	int32_t ____level_5;

public:
	inline static int32_t get_offset_of__level_5() { return static_cast<int32_t>(offsetof(LevelFader_t80A930EAED182822655C69B819748C49C59A31A5_StaticFields, ____level_5)); }
	inline int32_t get__level_5() const { return ____level_5; }
	inline int32_t* get_address_of__level_5() { return &____level_5; }
	inline void set__level_5(int32_t value)
	{
		____level_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELFADER_T80A930EAED182822655C69B819748C49C59A31A5_H
#ifndef LIFETOKKEN_T4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98_H
#define LIFETOKKEN_T4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LifeTokken
struct  LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 LifeTokken::lives
	int32_t ___lives_4;
	// UnityEngine.Rigidbody2D LifeTokken::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_5;
	// UnityEngine.Vector3 LifeTokken::move
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move_6;
	// System.Boolean LifeTokken::needsToMove
	bool ___needsToMove_7;
	// System.Single LifeTokken::screenLeft
	float ___screenLeft_8;
	// System.Single LifeTokken::screenRight
	float ___screenRight_9;
	// System.Single LifeTokken::yValue
	float ___yValue_10;

public:
	inline static int32_t get_offset_of_lives_4() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___lives_4)); }
	inline int32_t get_lives_4() const { return ___lives_4; }
	inline int32_t* get_address_of_lives_4() { return &___lives_4; }
	inline void set_lives_4(int32_t value)
	{
		___lives_4 = value;
	}

	inline static int32_t get_offset_of_rb_5() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___rb_5)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_5() const { return ___rb_5; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_5() { return &___rb_5; }
	inline void set_rb_5(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_5 = value;
		Il2CppCodeGenWriteBarrier((&___rb_5), value);
	}

	inline static int32_t get_offset_of_move_6() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___move_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_move_6() const { return ___move_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_move_6() { return &___move_6; }
	inline void set_move_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___move_6 = value;
	}

	inline static int32_t get_offset_of_needsToMove_7() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___needsToMove_7)); }
	inline bool get_needsToMove_7() const { return ___needsToMove_7; }
	inline bool* get_address_of_needsToMove_7() { return &___needsToMove_7; }
	inline void set_needsToMove_7(bool value)
	{
		___needsToMove_7 = value;
	}

	inline static int32_t get_offset_of_screenLeft_8() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___screenLeft_8)); }
	inline float get_screenLeft_8() const { return ___screenLeft_8; }
	inline float* get_address_of_screenLeft_8() { return &___screenLeft_8; }
	inline void set_screenLeft_8(float value)
	{
		___screenLeft_8 = value;
	}

	inline static int32_t get_offset_of_screenRight_9() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___screenRight_9)); }
	inline float get_screenRight_9() const { return ___screenRight_9; }
	inline float* get_address_of_screenRight_9() { return &___screenRight_9; }
	inline void set_screenRight_9(float value)
	{
		___screenRight_9 = value;
	}

	inline static int32_t get_offset_of_yValue_10() { return static_cast<int32_t>(offsetof(LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98, ___yValue_10)); }
	inline float get_yValue_10() const { return ___yValue_10; }
	inline float* get_address_of_yValue_10() { return &___yValue_10; }
	inline void set_yValue_10(float value)
	{
		___yValue_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFETOKKEN_T4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98_H
#ifndef ONSCREENNUMBERS_TACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE_H
#define ONSCREENNUMBERS_TACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnScreenNumbers
struct  OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 OnScreenNumbers::lives
	int32_t ___lives_4;
	// UnityEngine.UI.Text OnScreenNumbers::LivesText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___LivesText_5;
	// System.Int32 OnScreenNumbers::score
	int32_t ___score_6;
	// UnityEngine.UI.Text OnScreenNumbers::ScoreText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___ScoreText_7;
	// System.Int32 OnScreenNumbers::hiscore
	int32_t ___hiscore_8;
	// Player OnScreenNumbers::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_9;

public:
	inline static int32_t get_offset_of_lives_4() { return static_cast<int32_t>(offsetof(OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE, ___lives_4)); }
	inline int32_t get_lives_4() const { return ___lives_4; }
	inline int32_t* get_address_of_lives_4() { return &___lives_4; }
	inline void set_lives_4(int32_t value)
	{
		___lives_4 = value;
	}

	inline static int32_t get_offset_of_LivesText_5() { return static_cast<int32_t>(offsetof(OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE, ___LivesText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_LivesText_5() const { return ___LivesText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_LivesText_5() { return &___LivesText_5; }
	inline void set_LivesText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___LivesText_5 = value;
		Il2CppCodeGenWriteBarrier((&___LivesText_5), value);
	}

	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE, ___score_6)); }
	inline int32_t get_score_6() const { return ___score_6; }
	inline int32_t* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(int32_t value)
	{
		___score_6 = value;
	}

	inline static int32_t get_offset_of_ScoreText_7() { return static_cast<int32_t>(offsetof(OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE, ___ScoreText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_ScoreText_7() const { return ___ScoreText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_ScoreText_7() { return &___ScoreText_7; }
	inline void set_ScoreText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___ScoreText_7 = value;
		Il2CppCodeGenWriteBarrier((&___ScoreText_7), value);
	}

	inline static int32_t get_offset_of_hiscore_8() { return static_cast<int32_t>(offsetof(OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE, ___hiscore_8)); }
	inline int32_t get_hiscore_8() const { return ___hiscore_8; }
	inline int32_t* get_address_of_hiscore_8() { return &___hiscore_8; }
	inline void set_hiscore_8(int32_t value)
	{
		___hiscore_8 = value;
	}

	inline static int32_t get_offset_of_player_9() { return static_cast<int32_t>(offsetof(OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE, ___player_9)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_9() const { return ___player_9; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_9() { return &___player_9; }
	inline void set_player_9(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_9 = value;
		Il2CppCodeGenWriteBarrier((&___player_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCREENNUMBERS_TACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE_H
#ifndef PAUSEMENU_TE6D6728ABA83B6BB731E1D06DC22A5A54656D791_H
#define PAUSEMENU_TE6D6728ABA83B6BB731E1D06DC22A5A54656D791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PauseMenu::pauseMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pauseMenu_4;
	// UnityEngine.UI.Button PauseMenu::pauseButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___pauseButton_6;

public:
	inline static int32_t get_offset_of_pauseMenu_4() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___pauseMenu_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pauseMenu_4() const { return ___pauseMenu_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pauseMenu_4() { return &___pauseMenu_4; }
	inline void set_pauseMenu_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pauseMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___pauseMenu_4), value);
	}

	inline static int32_t get_offset_of_pauseButton_6() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___pauseButton_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_pauseButton_6() const { return ___pauseButton_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_pauseButton_6() { return &___pauseButton_6; }
	inline void set_pauseButton_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___pauseButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButton_6), value);
	}
};

struct PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields
{
public:
	// System.Boolean PauseMenu::_isPaused
	bool ____isPaused_5;

public:
	inline static int32_t get_offset_of__isPaused_5() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields, ____isPaused_5)); }
	inline bool get__isPaused_5() const { return ____isPaused_5; }
	inline bool* get_address_of__isPaused_5() { return &____isPaused_5; }
	inline void set__isPaused_5(bool value)
	{
		____isPaused_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_TE6D6728ABA83B6BB731E1D06DC22A5A54656D791_H
#ifndef PLAYER_T8321F4671F549F5A7793BB8BA33D32CCCD538873_H
#define PLAYER_T8321F4671F549F5A7793BB8BA33D32CCCD538873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody2D Player::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_4;
	// UnityEngine.Vector3 Player::normalSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normalSize_5;
	// UnityEngine.Vector3 Player::smallSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___smallSize_6;
	// System.Int32 Player::score
	int32_t ___score_7;
	// System.Int32 Player::_health
	int32_t ____health_10;
	// System.Boolean Player::hasBeenHit
	bool ___hasBeenHit_12;
	// UnityEngine.Sprite[] Player::playerSprite
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___playerSprite_13;
	// UnityEngine.Sprite Player::activePlayer
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___activePlayer_14;
	// UnityEngine.SpriteRenderer Player::playerRenderer
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___playerRenderer_15;
	// UnityEngine.Color Player::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_17;
	// UnityEngine.GameObject Player::touchCourser
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___touchCourser_18;
	// System.Single Player::speed
	float ___speed_19;
	// System.Single Player::moveHorizontal
	float ___moveHorizontal_20;
	// System.Single Player::moveVertical
	float ___moveVertical_21;
	// System.Single Player::rotation
	float ___rotation_22;
	// UnityEngine.Animator Player::animate
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animate_24;
	// System.Single Player::screenLeft
	float ___screenLeft_25;
	// System.Single Player::screenRight
	float ___screenRight_26;
	// System.Boolean Player::canBeHit
	bool ___canBeHit_27;
	// System.Boolean Player::isShrunk
	bool ___isShrunk_28;
	// System.Boolean Player::isShrunkActivate
	bool ___isShrunkActivate_29;
	// UnityEngine.GameObject Player::pauseButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pauseButton_30;

public:
	inline static int32_t get_offset_of_rb_4() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___rb_4)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_4() const { return ___rb_4; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_4() { return &___rb_4; }
	inline void set_rb_4(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_4 = value;
		Il2CppCodeGenWriteBarrier((&___rb_4), value);
	}

	inline static int32_t get_offset_of_normalSize_5() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___normalSize_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_normalSize_5() const { return ___normalSize_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_normalSize_5() { return &___normalSize_5; }
	inline void set_normalSize_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___normalSize_5 = value;
	}

	inline static int32_t get_offset_of_smallSize_6() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___smallSize_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_smallSize_6() const { return ___smallSize_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_smallSize_6() { return &___smallSize_6; }
	inline void set_smallSize_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___smallSize_6 = value;
	}

	inline static int32_t get_offset_of_score_7() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___score_7)); }
	inline int32_t get_score_7() const { return ___score_7; }
	inline int32_t* get_address_of_score_7() { return &___score_7; }
	inline void set_score_7(int32_t value)
	{
		___score_7 = value;
	}

	inline static int32_t get_offset_of__health_10() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ____health_10)); }
	inline int32_t get__health_10() const { return ____health_10; }
	inline int32_t* get_address_of__health_10() { return &____health_10; }
	inline void set__health_10(int32_t value)
	{
		____health_10 = value;
	}

	inline static int32_t get_offset_of_hasBeenHit_12() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___hasBeenHit_12)); }
	inline bool get_hasBeenHit_12() const { return ___hasBeenHit_12; }
	inline bool* get_address_of_hasBeenHit_12() { return &___hasBeenHit_12; }
	inline void set_hasBeenHit_12(bool value)
	{
		___hasBeenHit_12 = value;
	}

	inline static int32_t get_offset_of_playerSprite_13() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___playerSprite_13)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_playerSprite_13() const { return ___playerSprite_13; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_playerSprite_13() { return &___playerSprite_13; }
	inline void set_playerSprite_13(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___playerSprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___playerSprite_13), value);
	}

	inline static int32_t get_offset_of_activePlayer_14() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___activePlayer_14)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_activePlayer_14() const { return ___activePlayer_14; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_activePlayer_14() { return &___activePlayer_14; }
	inline void set_activePlayer_14(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___activePlayer_14 = value;
		Il2CppCodeGenWriteBarrier((&___activePlayer_14), value);
	}

	inline static int32_t get_offset_of_playerRenderer_15() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___playerRenderer_15)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_playerRenderer_15() const { return ___playerRenderer_15; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_playerRenderer_15() { return &___playerRenderer_15; }
	inline void set_playerRenderer_15(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___playerRenderer_15 = value;
		Il2CppCodeGenWriteBarrier((&___playerRenderer_15), value);
	}

	inline static int32_t get_offset_of_color_17() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___color_17)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_17() const { return ___color_17; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_17() { return &___color_17; }
	inline void set_color_17(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_17 = value;
	}

	inline static int32_t get_offset_of_touchCourser_18() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___touchCourser_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_touchCourser_18() const { return ___touchCourser_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_touchCourser_18() { return &___touchCourser_18; }
	inline void set_touchCourser_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___touchCourser_18 = value;
		Il2CppCodeGenWriteBarrier((&___touchCourser_18), value);
	}

	inline static int32_t get_offset_of_speed_19() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___speed_19)); }
	inline float get_speed_19() const { return ___speed_19; }
	inline float* get_address_of_speed_19() { return &___speed_19; }
	inline void set_speed_19(float value)
	{
		___speed_19 = value;
	}

	inline static int32_t get_offset_of_moveHorizontal_20() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___moveHorizontal_20)); }
	inline float get_moveHorizontal_20() const { return ___moveHorizontal_20; }
	inline float* get_address_of_moveHorizontal_20() { return &___moveHorizontal_20; }
	inline void set_moveHorizontal_20(float value)
	{
		___moveHorizontal_20 = value;
	}

	inline static int32_t get_offset_of_moveVertical_21() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___moveVertical_21)); }
	inline float get_moveVertical_21() const { return ___moveVertical_21; }
	inline float* get_address_of_moveVertical_21() { return &___moveVertical_21; }
	inline void set_moveVertical_21(float value)
	{
		___moveVertical_21 = value;
	}

	inline static int32_t get_offset_of_rotation_22() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___rotation_22)); }
	inline float get_rotation_22() const { return ___rotation_22; }
	inline float* get_address_of_rotation_22() { return &___rotation_22; }
	inline void set_rotation_22(float value)
	{
		___rotation_22 = value;
	}

	inline static int32_t get_offset_of_animate_24() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___animate_24)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animate_24() const { return ___animate_24; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animate_24() { return &___animate_24; }
	inline void set_animate_24(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animate_24 = value;
		Il2CppCodeGenWriteBarrier((&___animate_24), value);
	}

	inline static int32_t get_offset_of_screenLeft_25() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___screenLeft_25)); }
	inline float get_screenLeft_25() const { return ___screenLeft_25; }
	inline float* get_address_of_screenLeft_25() { return &___screenLeft_25; }
	inline void set_screenLeft_25(float value)
	{
		___screenLeft_25 = value;
	}

	inline static int32_t get_offset_of_screenRight_26() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___screenRight_26)); }
	inline float get_screenRight_26() const { return ___screenRight_26; }
	inline float* get_address_of_screenRight_26() { return &___screenRight_26; }
	inline void set_screenRight_26(float value)
	{
		___screenRight_26 = value;
	}

	inline static int32_t get_offset_of_canBeHit_27() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___canBeHit_27)); }
	inline bool get_canBeHit_27() const { return ___canBeHit_27; }
	inline bool* get_address_of_canBeHit_27() { return &___canBeHit_27; }
	inline void set_canBeHit_27(bool value)
	{
		___canBeHit_27 = value;
	}

	inline static int32_t get_offset_of_isShrunk_28() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___isShrunk_28)); }
	inline bool get_isShrunk_28() const { return ___isShrunk_28; }
	inline bool* get_address_of_isShrunk_28() { return &___isShrunk_28; }
	inline void set_isShrunk_28(bool value)
	{
		___isShrunk_28 = value;
	}

	inline static int32_t get_offset_of_isShrunkActivate_29() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___isShrunkActivate_29)); }
	inline bool get_isShrunkActivate_29() const { return ___isShrunkActivate_29; }
	inline bool* get_address_of_isShrunkActivate_29() { return &___isShrunkActivate_29; }
	inline void set_isShrunkActivate_29(bool value)
	{
		___isShrunkActivate_29 = value;
	}

	inline static int32_t get_offset_of_pauseButton_30() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___pauseButton_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pauseButton_30() const { return ___pauseButton_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pauseButton_30() { return &___pauseButton_30; }
	inline void set_pauseButton_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pauseButton_30 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButton_30), value);
	}
};

struct Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields
{
public:
	// System.Int32 Player::_hiScore
	int32_t ____hiScore_8;
	// System.Int32 Player::_lives
	int32_t ____lives_9;
	// System.Int32 Player::_maxHealth
	int32_t ____maxHealth_11;
	// System.Int32 Player::_playerSelection
	int32_t ____playerSelection_16;
	// System.Boolean Player::playerAlive
	bool ___playerAlive_23;

public:
	inline static int32_t get_offset_of__hiScore_8() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields, ____hiScore_8)); }
	inline int32_t get__hiScore_8() const { return ____hiScore_8; }
	inline int32_t* get_address_of__hiScore_8() { return &____hiScore_8; }
	inline void set__hiScore_8(int32_t value)
	{
		____hiScore_8 = value;
	}

	inline static int32_t get_offset_of__lives_9() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields, ____lives_9)); }
	inline int32_t get__lives_9() const { return ____lives_9; }
	inline int32_t* get_address_of__lives_9() { return &____lives_9; }
	inline void set__lives_9(int32_t value)
	{
		____lives_9 = value;
	}

	inline static int32_t get_offset_of__maxHealth_11() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields, ____maxHealth_11)); }
	inline int32_t get__maxHealth_11() const { return ____maxHealth_11; }
	inline int32_t* get_address_of__maxHealth_11() { return &____maxHealth_11; }
	inline void set__maxHealth_11(int32_t value)
	{
		____maxHealth_11 = value;
	}

	inline static int32_t get_offset_of__playerSelection_16() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields, ____playerSelection_16)); }
	inline int32_t get__playerSelection_16() const { return ____playerSelection_16; }
	inline int32_t* get_address_of__playerSelection_16() { return &____playerSelection_16; }
	inline void set__playerSelection_16(int32_t value)
	{
		____playerSelection_16 = value;
	}

	inline static int32_t get_offset_of_playerAlive_23() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields, ___playerAlive_23)); }
	inline bool get_playerAlive_23() const { return ___playerAlive_23; }
	inline bool* get_address_of_playerAlive_23() { return &___playerAlive_23; }
	inline void set_playerAlive_23(bool value)
	{
		___playerAlive_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T8321F4671F549F5A7793BB8BA33D32CCCD538873_H
#ifndef PLAYERSELECTION_T851FF522452A79A15C32CA8CD539006EBC83F998_H
#define PLAYERSELECTION_T851FF522452A79A15C32CA8CD539006EBC83F998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerSelection
struct  PlayerSelection_t851FF522452A79A15C32CA8CD539006EBC83F998  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator PlayerSelection::animate
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animate_4;

public:
	inline static int32_t get_offset_of_animate_4() { return static_cast<int32_t>(offsetof(PlayerSelection_t851FF522452A79A15C32CA8CD539006EBC83F998, ___animate_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animate_4() const { return ___animate_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animate_4() { return &___animate_4; }
	inline void set_animate_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animate_4 = value;
		Il2CppCodeGenWriteBarrier((&___animate_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSELECTION_T851FF522452A79A15C32CA8CD539006EBC83F998_H
#ifndef SHRINK_TA416FC95B479B04052D570FE05839710FD8F97D5_H
#define SHRINK_TA416FC95B479B04052D570FE05839710FD8F97D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shrink
struct  Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Player Shrink::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_4;
	// UnityEngine.Rigidbody2D Shrink::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_5;
	// UnityEngine.Vector3 Shrink::move
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___move_6;
	// System.Boolean Shrink::needsToMove
	bool ___needsToMove_7;
	// System.Single Shrink::screenLeft
	float ___screenLeft_8;
	// System.Single Shrink::screenRight
	float ___screenRight_9;
	// System.Single Shrink::yValue
	float ___yValue_10;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___player_4)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_4() const { return ___player_4; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_rb_5() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___rb_5)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_5() const { return ___rb_5; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_5() { return &___rb_5; }
	inline void set_rb_5(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_5 = value;
		Il2CppCodeGenWriteBarrier((&___rb_5), value);
	}

	inline static int32_t get_offset_of_move_6() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___move_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_move_6() const { return ___move_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_move_6() { return &___move_6; }
	inline void set_move_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___move_6 = value;
	}

	inline static int32_t get_offset_of_needsToMove_7() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___needsToMove_7)); }
	inline bool get_needsToMove_7() const { return ___needsToMove_7; }
	inline bool* get_address_of_needsToMove_7() { return &___needsToMove_7; }
	inline void set_needsToMove_7(bool value)
	{
		___needsToMove_7 = value;
	}

	inline static int32_t get_offset_of_screenLeft_8() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___screenLeft_8)); }
	inline float get_screenLeft_8() const { return ___screenLeft_8; }
	inline float* get_address_of_screenLeft_8() { return &___screenLeft_8; }
	inline void set_screenLeft_8(float value)
	{
		___screenLeft_8 = value;
	}

	inline static int32_t get_offset_of_screenRight_9() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___screenRight_9)); }
	inline float get_screenRight_9() const { return ___screenRight_9; }
	inline float* get_address_of_screenRight_9() { return &___screenRight_9; }
	inline void set_screenRight_9(float value)
	{
		___screenRight_9 = value;
	}

	inline static int32_t get_offset_of_yValue_10() { return static_cast<int32_t>(offsetof(Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5, ___yValue_10)); }
	inline float get_yValue_10() const { return ___yValue_10; }
	inline float* get_address_of_yValue_10() { return &___yValue_10; }
	inline void set_yValue_10(float value)
	{
		___yValue_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHRINK_TA416FC95B479B04052D570FE05839710FD8F97D5_H
#ifndef SINGLETON_1_T601355FD73F7144A99D8FC527049802B6920F95E_H
#define SINGLETON_1_T601355FD73F7144A99D8FC527049802B6920F95E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<BuyingLives>
struct  Singleton_1_t601355FD73F7144A99D8FC527049802B6920F95E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Singleton`1::dontDestroy
	bool ___dontDestroy_4;

public:
	inline static int32_t get_offset_of_dontDestroy_4() { return static_cast<int32_t>(offsetof(Singleton_1_t601355FD73F7144A99D8FC527049802B6920F95E, ___dontDestroy_4)); }
	inline bool get_dontDestroy_4() const { return ___dontDestroy_4; }
	inline bool* get_address_of_dontDestroy_4() { return &___dontDestroy_4; }
	inline void set_dontDestroy_4(bool value)
	{
		___dontDestroy_4 = value;
	}
};

struct Singleton_1_t601355FD73F7144A99D8FC527049802B6920F95E_StaticFields
{
public:
	// T Singleton`1::m_instance
	BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5 * ___m_instance_5;

public:
	inline static int32_t get_offset_of_m_instance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t601355FD73F7144A99D8FC527049802B6920F95E_StaticFields, ___m_instance_5)); }
	inline BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5 * get_m_instance_5() const { return ___m_instance_5; }
	inline BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5 ** get_address_of_m_instance_5() { return &___m_instance_5; }
	inline void set_m_instance_5(BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5 * value)
	{
		___m_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T601355FD73F7144A99D8FC527049802B6920F95E_H
#ifndef SINGLETON_1_T79F2C980C6E97D53B76A1F21A959510D24A8A3FB_H
#define SINGLETON_1_T79F2C980C6E97D53B76A1F21A959510D24A8A3FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<GameManager>
struct  Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Singleton`1::dontDestroy
	bool ___dontDestroy_4;

public:
	inline static int32_t get_offset_of_dontDestroy_4() { return static_cast<int32_t>(offsetof(Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB, ___dontDestroy_4)); }
	inline bool get_dontDestroy_4() const { return ___dontDestroy_4; }
	inline bool* get_address_of_dontDestroy_4() { return &___dontDestroy_4; }
	inline void set_dontDestroy_4(bool value)
	{
		___dontDestroy_4 = value;
	}
};

struct Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB_StaticFields
{
public:
	// T Singleton`1::m_instance
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___m_instance_5;

public:
	inline static int32_t get_offset_of_m_instance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB_StaticFields, ___m_instance_5)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_m_instance_5() const { return ___m_instance_5; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_m_instance_5() { return &___m_instance_5; }
	inline void set_m_instance_5(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___m_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T79F2C980C6E97D53B76A1F21A959510D24A8A3FB_H
#ifndef SINGLETON_1_T7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D_H
#define SINGLETON_1_T7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton`1<IAPManager>
struct  Singleton_1_t7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Singleton`1::dontDestroy
	bool ___dontDestroy_4;

public:
	inline static int32_t get_offset_of_dontDestroy_4() { return static_cast<int32_t>(offsetof(Singleton_1_t7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D, ___dontDestroy_4)); }
	inline bool get_dontDestroy_4() const { return ___dontDestroy_4; }
	inline bool* get_address_of_dontDestroy_4() { return &___dontDestroy_4; }
	inline void set_dontDestroy_4(bool value)
	{
		___dontDestroy_4 = value;
	}
};

struct Singleton_1_t7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D_StaticFields
{
public:
	// T Singleton`1::m_instance
	IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A * ___m_instance_5;

public:
	inline static int32_t get_offset_of_m_instance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D_StaticFields, ___m_instance_5)); }
	inline IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A * get_m_instance_5() const { return ___m_instance_5; }
	inline IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A ** get_address_of_m_instance_5() { return &___m_instance_5; }
	inline void set_m_instance_5(IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A * value)
	{
		___m_instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D_H
#ifndef SPAWNER_T6D000391A607DDA6E3C0F62284BA6E5127E2010D_H
#define SPAWNER_T6D000391A607DDA6E3C0F62284BA6E5127E2010D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawner
struct  Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Player Spawner::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_4;
	// UnityEngine.GameObject Spawner::heart
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___heart_5;
	// UnityEngine.GameObject Spawner::lifeSpawn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lifeSpawn_6;
	// UnityEngine.GameObject Spawner::shrink
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___shrink_7;
	// UnityEngine.GameObject Spawner::blockSpawn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___blockSpawn_8;
	// UnityEngine.GameObject[] Spawner::block
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___block_9;
	// UnityEngine.GameObject[] Spawner::shrinkTokken
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___shrinkTokken_10;
	// UnityEngine.GameObject[] Spawner::lifeTokken
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___lifeTokken_11;
	// System.Single Spawner::minXValue
	float ___minXValue_12;
	// System.Single Spawner::maxXValue
	float ___maxXValue_13;
	// System.Single Spawner::startSpawnWait
	float ___startSpawnWait_14;
	// System.Single Spawner::minSpawnWait
	float ___minSpawnWait_15;
	// System.Single Spawner::accelerationSpawnRate
	float ___accelerationSpawnRate_16;
	// System.Single Spawner::gravityAcceleration
	float ___gravityAcceleration_17;
	// System.Single Spawner::minGravity
	float ___minGravity_18;
	// System.Single Spawner::startWait
	float ___startWait_19;
	// System.Single Spawner::gravity
	float ___gravity_21;
	// System.Int32 Spawner::blockrestart
	int32_t ___blockrestart_22;
	// System.Int32 Spawner::powerupcountdown
	int32_t ___powerupcountdown_23;
	// System.Int32 Spawner::rand
	int32_t ___rand_24;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___player_4)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_4() const { return ___player_4; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_heart_5() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___heart_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_heart_5() const { return ___heart_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_heart_5() { return &___heart_5; }
	inline void set_heart_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___heart_5 = value;
		Il2CppCodeGenWriteBarrier((&___heart_5), value);
	}

	inline static int32_t get_offset_of_lifeSpawn_6() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___lifeSpawn_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lifeSpawn_6() const { return ___lifeSpawn_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lifeSpawn_6() { return &___lifeSpawn_6; }
	inline void set_lifeSpawn_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lifeSpawn_6 = value;
		Il2CppCodeGenWriteBarrier((&___lifeSpawn_6), value);
	}

	inline static int32_t get_offset_of_shrink_7() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___shrink_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_shrink_7() const { return ___shrink_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_shrink_7() { return &___shrink_7; }
	inline void set_shrink_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___shrink_7 = value;
		Il2CppCodeGenWriteBarrier((&___shrink_7), value);
	}

	inline static int32_t get_offset_of_blockSpawn_8() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___blockSpawn_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_blockSpawn_8() const { return ___blockSpawn_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_blockSpawn_8() { return &___blockSpawn_8; }
	inline void set_blockSpawn_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___blockSpawn_8 = value;
		Il2CppCodeGenWriteBarrier((&___blockSpawn_8), value);
	}

	inline static int32_t get_offset_of_block_9() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___block_9)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_block_9() const { return ___block_9; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_block_9() { return &___block_9; }
	inline void set_block_9(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___block_9 = value;
		Il2CppCodeGenWriteBarrier((&___block_9), value);
	}

	inline static int32_t get_offset_of_shrinkTokken_10() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___shrinkTokken_10)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_shrinkTokken_10() const { return ___shrinkTokken_10; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_shrinkTokken_10() { return &___shrinkTokken_10; }
	inline void set_shrinkTokken_10(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___shrinkTokken_10 = value;
		Il2CppCodeGenWriteBarrier((&___shrinkTokken_10), value);
	}

	inline static int32_t get_offset_of_lifeTokken_11() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___lifeTokken_11)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_lifeTokken_11() const { return ___lifeTokken_11; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_lifeTokken_11() { return &___lifeTokken_11; }
	inline void set_lifeTokken_11(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___lifeTokken_11 = value;
		Il2CppCodeGenWriteBarrier((&___lifeTokken_11), value);
	}

	inline static int32_t get_offset_of_minXValue_12() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___minXValue_12)); }
	inline float get_minXValue_12() const { return ___minXValue_12; }
	inline float* get_address_of_minXValue_12() { return &___minXValue_12; }
	inline void set_minXValue_12(float value)
	{
		___minXValue_12 = value;
	}

	inline static int32_t get_offset_of_maxXValue_13() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___maxXValue_13)); }
	inline float get_maxXValue_13() const { return ___maxXValue_13; }
	inline float* get_address_of_maxXValue_13() { return &___maxXValue_13; }
	inline void set_maxXValue_13(float value)
	{
		___maxXValue_13 = value;
	}

	inline static int32_t get_offset_of_startSpawnWait_14() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___startSpawnWait_14)); }
	inline float get_startSpawnWait_14() const { return ___startSpawnWait_14; }
	inline float* get_address_of_startSpawnWait_14() { return &___startSpawnWait_14; }
	inline void set_startSpawnWait_14(float value)
	{
		___startSpawnWait_14 = value;
	}

	inline static int32_t get_offset_of_minSpawnWait_15() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___minSpawnWait_15)); }
	inline float get_minSpawnWait_15() const { return ___minSpawnWait_15; }
	inline float* get_address_of_minSpawnWait_15() { return &___minSpawnWait_15; }
	inline void set_minSpawnWait_15(float value)
	{
		___minSpawnWait_15 = value;
	}

	inline static int32_t get_offset_of_accelerationSpawnRate_16() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___accelerationSpawnRate_16)); }
	inline float get_accelerationSpawnRate_16() const { return ___accelerationSpawnRate_16; }
	inline float* get_address_of_accelerationSpawnRate_16() { return &___accelerationSpawnRate_16; }
	inline void set_accelerationSpawnRate_16(float value)
	{
		___accelerationSpawnRate_16 = value;
	}

	inline static int32_t get_offset_of_gravityAcceleration_17() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___gravityAcceleration_17)); }
	inline float get_gravityAcceleration_17() const { return ___gravityAcceleration_17; }
	inline float* get_address_of_gravityAcceleration_17() { return &___gravityAcceleration_17; }
	inline void set_gravityAcceleration_17(float value)
	{
		___gravityAcceleration_17 = value;
	}

	inline static int32_t get_offset_of_minGravity_18() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___minGravity_18)); }
	inline float get_minGravity_18() const { return ___minGravity_18; }
	inline float* get_address_of_minGravity_18() { return &___minGravity_18; }
	inline void set_minGravity_18(float value)
	{
		___minGravity_18 = value;
	}

	inline static int32_t get_offset_of_startWait_19() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___startWait_19)); }
	inline float get_startWait_19() const { return ___startWait_19; }
	inline float* get_address_of_startWait_19() { return &___startWait_19; }
	inline void set_startWait_19(float value)
	{
		___startWait_19 = value;
	}

	inline static int32_t get_offset_of_gravity_21() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___gravity_21)); }
	inline float get_gravity_21() const { return ___gravity_21; }
	inline float* get_address_of_gravity_21() { return &___gravity_21; }
	inline void set_gravity_21(float value)
	{
		___gravity_21 = value;
	}

	inline static int32_t get_offset_of_blockrestart_22() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___blockrestart_22)); }
	inline int32_t get_blockrestart_22() const { return ___blockrestart_22; }
	inline int32_t* get_address_of_blockrestart_22() { return &___blockrestart_22; }
	inline void set_blockrestart_22(int32_t value)
	{
		___blockrestart_22 = value;
	}

	inline static int32_t get_offset_of_powerupcountdown_23() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___powerupcountdown_23)); }
	inline int32_t get_powerupcountdown_23() const { return ___powerupcountdown_23; }
	inline int32_t* get_address_of_powerupcountdown_23() { return &___powerupcountdown_23; }
	inline void set_powerupcountdown_23(int32_t value)
	{
		___powerupcountdown_23 = value;
	}

	inline static int32_t get_offset_of_rand_24() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D, ___rand_24)); }
	inline int32_t get_rand_24() const { return ___rand_24; }
	inline int32_t* get_address_of_rand_24() { return &___rand_24; }
	inline void set_rand_24(int32_t value)
	{
		___rand_24 = value;
	}
};

struct Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D_StaticFields
{
public:
	// System.Boolean Spawner::_stop
	bool ____stop_20;

public:
	inline static int32_t get_offset_of__stop_20() { return static_cast<int32_t>(offsetof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D_StaticFields, ____stop_20)); }
	inline bool get__stop_20() const { return ____stop_20; }
	inline bool* get_address_of__stop_20() { return &____stop_20; }
	inline void set__stop_20(bool value)
	{
		____stop_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNER_T6D000391A607DDA6E3C0F62284BA6E5127E2010D_H
#ifndef STARTMENU_T16A2DA0B3CBABA283134C0B7AA0403F13F231016_H
#define STARTMENU_T16A2DA0B3CBABA283134C0B7AA0403F13F231016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartMenu
struct  StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTMENU_T16A2DA0B3CBABA283134C0B7AA0403F13F231016_H
#ifndef CALLBACKEXECUTOR_TB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78_H
#define CALLBACKEXECUTOR_TB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.CallbackExecutor
struct  CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Queue`1<System.Action`1<UnityEngine.Advertisements.CallbackExecutor>> UnityEngine.Advertisements.CallbackExecutor::s_Queue
	Queue_1_tBE60FA9EC7CEAC6A2101CA6BF8E09035C8DA2F3E * ___s_Queue_4;

public:
	inline static int32_t get_offset_of_s_Queue_4() { return static_cast<int32_t>(offsetof(CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78, ___s_Queue_4)); }
	inline Queue_1_tBE60FA9EC7CEAC6A2101CA6BF8E09035C8DA2F3E * get_s_Queue_4() const { return ___s_Queue_4; }
	inline Queue_1_tBE60FA9EC7CEAC6A2101CA6BF8E09035C8DA2F3E ** get_address_of_s_Queue_4() { return &___s_Queue_4; }
	inline void set_s_Queue_4(Queue_1_tBE60FA9EC7CEAC6A2101CA6BF8E09035C8DA2F3E * value)
	{
		___s_Queue_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Queue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKEXECUTOR_TB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78_H
#ifndef PLACEHOLDER_TB58246E9925EEB8DEFBD0EC187EC59F96EF4E244_H
#define PLACEHOLDER_TB58246E9925EEB8DEFBD0EC187EC59F96EF4E244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Editor.Placeholder
struct  Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D UnityEngine.Advertisements.Editor.Placeholder::m_LandscapeTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_LandscapeTexture_4;
	// UnityEngine.Texture2D UnityEngine.Advertisements.Editor.Placeholder::m_PortraitTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_PortraitTexture_5;
	// System.Boolean UnityEngine.Advertisements.Editor.Placeholder::m_Showing
	bool ___m_Showing_6;
	// System.String UnityEngine.Advertisements.Editor.Placeholder::m_PlacementId
	String_t* ___m_PlacementId_7;
	// System.Boolean UnityEngine.Advertisements.Editor.Placeholder::m_AllowSkip
	bool ___m_AllowSkip_8;
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.Editor.Placeholder::OnFinish
	EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * ___OnFinish_9;

public:
	inline static int32_t get_offset_of_m_LandscapeTexture_4() { return static_cast<int32_t>(offsetof(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244, ___m_LandscapeTexture_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_LandscapeTexture_4() const { return ___m_LandscapeTexture_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_LandscapeTexture_4() { return &___m_LandscapeTexture_4; }
	inline void set_m_LandscapeTexture_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_LandscapeTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LandscapeTexture_4), value);
	}

	inline static int32_t get_offset_of_m_PortraitTexture_5() { return static_cast<int32_t>(offsetof(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244, ___m_PortraitTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_PortraitTexture_5() const { return ___m_PortraitTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_PortraitTexture_5() { return &___m_PortraitTexture_5; }
	inline void set_m_PortraitTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_PortraitTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PortraitTexture_5), value);
	}

	inline static int32_t get_offset_of_m_Showing_6() { return static_cast<int32_t>(offsetof(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244, ___m_Showing_6)); }
	inline bool get_m_Showing_6() const { return ___m_Showing_6; }
	inline bool* get_address_of_m_Showing_6() { return &___m_Showing_6; }
	inline void set_m_Showing_6(bool value)
	{
		___m_Showing_6 = value;
	}

	inline static int32_t get_offset_of_m_PlacementId_7() { return static_cast<int32_t>(offsetof(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244, ___m_PlacementId_7)); }
	inline String_t* get_m_PlacementId_7() const { return ___m_PlacementId_7; }
	inline String_t** get_address_of_m_PlacementId_7() { return &___m_PlacementId_7; }
	inline void set_m_PlacementId_7(String_t* value)
	{
		___m_PlacementId_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacementId_7), value);
	}

	inline static int32_t get_offset_of_m_AllowSkip_8() { return static_cast<int32_t>(offsetof(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244, ___m_AllowSkip_8)); }
	inline bool get_m_AllowSkip_8() const { return ___m_AllowSkip_8; }
	inline bool* get_address_of_m_AllowSkip_8() { return &___m_AllowSkip_8; }
	inline void set_m_AllowSkip_8(bool value)
	{
		___m_AllowSkip_8 = value;
	}

	inline static int32_t get_offset_of_OnFinish_9() { return static_cast<int32_t>(offsetof(Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244, ___OnFinish_9)); }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * get_OnFinish_9() const { return ___OnFinish_9; }
	inline EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 ** get_address_of_OnFinish_9() { return &___OnFinish_9; }
	inline void set_OnFinish_9(EventHandler_1_t03FF18A8E1006BEE3BA571550529A6AF31150226 * value)
	{
		___OnFinish_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEHOLDER_TB58246E9925EEB8DEFBD0EC187EC59F96EF4E244_H
#ifndef BUYINGLIVES_TBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5_H
#define BUYINGLIVES_TBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuyingLives
struct  BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5  : public Singleton_1_t601355FD73F7144A99D8FC527049802B6920F95E
{
public:
	// Player BuyingLives::player
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___player_6;

public:
	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5, ___player_6)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_player_6() const { return ___player_6; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUYINGLIVES_TBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5_H
#ifndef GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#define GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public Singleton_1_t79F2C980C6E97D53B76A1F21A959510D24A8A3FB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#ifndef IAPMANAGER_TF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_H
#define IAPMANAGER_TF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPManager
struct  IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A  : public Singleton_1_t7800B23F9B0A8D93D8A088BA59DF72D1D8B43C9D
{
public:
	// System.String IAPManager::extraLife
	String_t* ___extraLife_8;

public:
	inline static int32_t get_offset_of_extraLife_8() { return static_cast<int32_t>(offsetof(IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A, ___extraLife_8)); }
	inline String_t* get_extraLife_8() const { return ___extraLife_8; }
	inline String_t** get_address_of_extraLife_8() { return &___extraLife_8; }
	inline void set_extraLife_8(String_t* value)
	{
		___extraLife_8 = value;
		Il2CppCodeGenWriteBarrier((&___extraLife_8), value);
	}
};

struct IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController IAPManager::m_StoreController
	RuntimeObject* ___m_StoreController_6;
	// UnityEngine.Purchasing.IExtensionProvider IAPManager::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_7;

public:
	inline static int32_t get_offset_of_m_StoreController_6() { return static_cast<int32_t>(offsetof(IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_StaticFields, ___m_StoreController_6)); }
	inline RuntimeObject* get_m_StoreController_6() const { return ___m_StoreController_6; }
	inline RuntimeObject** get_address_of_m_StoreController_6() { return &___m_StoreController_6; }
	inline void set_m_StoreController_6(RuntimeObject* value)
	{
		___m_StoreController_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreController_6), value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_7() { return static_cast<int32_t>(offsetof(IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_StaticFields, ___m_StoreExtensionProvider_7)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_7() const { return ___m_StoreExtensionProvider_7; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_7() { return &___m_StoreExtensionProvider_7; }
	inline void set_m_StoreExtensionProvider_7(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreExtensionProvider_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPMANAGER_TF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[6] = 
{
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244::get_offset_of_m_LandscapeTexture_4(),
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244::get_offset_of_m_PortraitTexture_5(),
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244::get_offset_of_m_Showing_6(),
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244::get_offset_of_m_PlacementId_7(),
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244::get_offset_of_m_AllowSkip_8(),
	Placeholder_tB58246E9925EEB8DEFBD0EC187EC59F96EF4E244::get_offset_of_OnFinish_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740), -1, sizeof(Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3501[6] = 
{
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740_StaticFields::get_offset_of_s_BaseUrl_0(),
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740::get_offset_of_m_DebugMode_1(),
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740::get_offset_of_m_Configuration_2(),
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740::get_offset_of_m_Placeholder_3(),
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740::get_offset_of_OnStart_4(),
	Platform_t8B216EBF236DAD4583B1BB43FD792439EC1BE740::get_offset_of_OnFinish_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[3] = 
{
	U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8::get_offset_of_request_0(),
	U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8::get_offset_of_gameId_1(),
	U3CInitializeU3Ec__AnonStorey0_t01695F5A9E779590A7DB912A981E37FC10911AF8::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3503[3] = 
{
	Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B::get_offset_of_U3CenabledU3Ek__BackingField_0(),
	Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B::get_offset_of_U3CdefaultPlacementU3Ek__BackingField_1(),
	Configuration_t7CFA129695A07F478DA33F95E224C457CFE8193B::get_offset_of_U3CplacementsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D), -1, sizeof(Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3504[10] = 
{
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields::get_offset_of_s_Instance_0(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields::get_offset_of_s_CallbackExecutor_1(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D::get_offset_of_OnReady_2(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D::get_offset_of_OnStart_3(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D::get_offset_of_OnFinish_4(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D::get_offset_of_OnError_5(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_8(),
	Platform_t7BD4EB9FD349D8AD5880B5D778EDBC6A421E3E7D_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (unityAdsReady_t716CB3207A4D591AFD38E610CC8DC5EA4DE62A67), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (unityAdsDidError_t0AEE1AF310020AF7C9F6A7881F50B21996A32C61), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (unityAdsDidStart_tD78BF20784F7416774E14ADE453F6F4B4428D7FA), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (unityAdsDidFinish_t9EE8992F28B4D62991C98D7D85A9E93A6835E045), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (U3CUnityAdsReadyU3Ec__AnonStorey0_tB27C3EFC407540F32BE16EEEECF2C7B88BF43A79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3509[2] = 
{
	U3CUnityAdsReadyU3Ec__AnonStorey0_tB27C3EFC407540F32BE16EEEECF2C7B88BF43A79::get_offset_of_handler_0(),
	U3CUnityAdsReadyU3Ec__AnonStorey0_tB27C3EFC407540F32BE16EEEECF2C7B88BF43A79::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3510[3] = 
{
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA::get_offset_of_handler_0(),
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA::get_offset_of_rawError_1(),
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t6A5E888349986FCAC117F1F71F73587DAE2AFFEA::get_offset_of_message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (U3CUnityAdsDidStartU3Ec__AnonStorey2_t69450E13B9A35C35663BBFDA85A317661053E4F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3511[2] = 
{
	U3CUnityAdsDidStartU3Ec__AnonStorey2_t69450E13B9A35C35663BBFDA85A317661053E4F6::get_offset_of_handler_0(),
	U3CUnityAdsDidStartU3Ec__AnonStorey2_t69450E13B9A35C35663BBFDA85A317661053E4F6::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3512[2] = 
{
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1::get_offset_of_handler_0(),
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_tFF312E3E401419649E7A80CB5F522EFBD8C7A1E1::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (U3CUnityAdsDidFinishU3Ec__AnonStorey4_tF3998F44D42BAA24F6FC8B71E5E2443B74010AB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3513[2] = 
{
	U3CUnityAdsDidFinishU3Ec__AnonStorey4_tF3998F44D42BAA24F6FC8B71E5E2443B74010AB7::get_offset_of_showResult_0(),
	U3CUnityAdsDidFinishU3Ec__AnonStorey4_tF3998F44D42BAA24F6FC8B71E5E2443B74010AB7::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139), -1, sizeof(Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3514[7] = 
{
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_s_Initialized_0(),
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_s_Platform_1(),
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_s_EditorSupportedPlatform_2(),
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_s_Showing_3(),
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_s_DebugLevel_4(),
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	Advertisement_t40473E92F9DEE2D1A2B0A6A49CBDFA61A91B9139_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (DebugLevelInternal_t2FF63CDE387CA44C2B4C4715A40FAEC807F23E22)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3515[6] = 
{
	DebugLevelInternal_t2FF63CDE387CA44C2B4C4715A40FAEC807F23E22::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (DebugLevel_t72A7B4FC98B1A5E19A789475341FD35F5CAB0537)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3516[6] = 
{
	DebugLevel_t72A7B4FC98B1A5E19A789475341FD35F5CAB0537::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3517[1] = 
{
	U3CShowU3Ec__AnonStorey0_t3F78313FF5E87445044283B31B3DB8DF4BBCA6B4::get_offset_of_showOptions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (U3CShowU3Ec__AnonStorey1_t7C410F6A2B71DAAC4DAEB5851994E08082C63A80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3518[2] = 
{
	U3CShowU3Ec__AnonStorey1_t7C410F6A2B71DAAC4DAEB5851994E08082C63A80::get_offset_of_finishHandler_0(),
	U3CShowU3Ec__AnonStorey1_t7C410F6A2B71DAAC4DAEB5851994E08082C63A80::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3519[1] = 
{
	CallbackExecutor_tB2DFCCED2168CFD8C1F6FD0C21679DA386B8CA78::get_offset_of_s_Queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (ReadyEventArgs_tA3E33B18240D91F48C92CC62CB5515388449E250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3520[1] = 
{
	ReadyEventArgs_tA3E33B18240D91F48C92CC62CB5515388449E250::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (StartEventArgs_t44F8C8482BBCC37963CFBC1C4D2F6473040717E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3521[1] = 
{
	StartEventArgs_t44F8C8482BBCC37963CFBC1C4D2F6473040717E2::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (FinishEventArgs_tBC627E351CBF3F2563ACD7F5D765110318D7F49D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3522[2] = 
{
	FinishEventArgs_tBC627E351CBF3F2563ACD7F5D765110318D7F49D::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
	FinishEventArgs_tBC627E351CBF3F2563ACD7F5D765110318D7F49D::get_offset_of_U3CshowResultU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (ErrorEventArgs_t77C07CCB4554162961BBE00E3B7E52DDA8648409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[2] = 
{
	ErrorEventArgs_t77C07CCB4554162961BBE00E3B7E52DDA8648409::get_offset_of_U3CerrorU3Ek__BackingField_1(),
	ErrorEventArgs_t77C07CCB4554162961BBE00E3B7E52DDA8648409::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (MetaData_t5CBC93B0A4A7377BB006186A40340BA51A9D6A03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[2] = 
{
	MetaData_t5CBC93B0A4A7377BB006186A40340BA51A9D6A03::get_offset_of_m_MetaData_0(),
	MetaData_t5CBC93B0A4A7377BB006186A40340BA51A9D6A03::get_offset_of_U3CcategoryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (PlacementState_t19D760B7BB28598800BD374D7ECBC43FC9D48333)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3526[6] = 
{
	PlacementState_t19D760B7BB28598800BD374D7ECBC43FC9D48333::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (ShowOptions_t6637C67D4907C169839DA10D810B696A19941904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[2] = 
{
	ShowOptions_t6637C67D4907C169839DA10D810B696A19941904::get_offset_of_U3CresultCallbackU3Ek__BackingField_0(),
	ShowOptions_t6637C67D4907C169839DA10D810B696A19941904::get_offset_of_U3CgamerSidU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (ShowResult_tE352F124530178145C4D05ACA8F66AD11DEECDA8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3528[4] = 
{
	ShowResult_tE352F124530178145C4D05ACA8F66AD11DEECDA8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (JsonArray_tEC81164ADEC87B1B3F3D28FEA89F9AF6458479A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (JsonObject_t5430387D9A9215A629850E012C366DBE5AD5A2B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3530[1] = 
{
	JsonObject_t5430387D9A9215A629850E012C366DBE5AD5A2B5::get_offset_of__members_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D), -1, sizeof(SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3531[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_StaticFields::get_offset_of__currentJsonSerializerStrategy_13(),
	SimpleJson_tD74F35DC9B5A5D3F70146A8C72026BD47A7DFE7D_StaticFields::get_offset_of__pocoJsonSerializerStrategy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E), -1, sizeof(PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3533[6] = 
{
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E::get_offset_of_ConstructorCache_0(),
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E::get_offset_of_GetCache_1(),
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E::get_offset_of_SetCache_2(),
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields::get_offset_of_EmptyTypes_3(),
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields::get_offset_of_ArrayConstructorParameterTypes_4(),
	PocoJsonSerializerStrategy_t40E49A3237BC56F62F5DE6F9034160AD2660BB0E_StaticFields::get_offset_of_Iso8601Format_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (ReflectionUtils_t716A5426EE76AD7B91D2D6989AB62AA4BB23A534), -1, sizeof(ReflectionUtils_t716A5426EE76AD7B91D2D6989AB62AA4BB23A534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3534[1] = 
{
	ReflectionUtils_t716A5426EE76AD7B91D2D6989AB62AA4BB23A534_StaticFields::get_offset_of_EmptyObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (GetDelegate_t5384142A3E4A970422627B6C02B4EB4319934A8F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (SetDelegate_tB85E10DD9F713CAE7ED82A18744F73AED4EA6D04), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (ConstructorDelegate_t758C58A6B467BA586E2C6B57A535729A620BFCEC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3539[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey0_t4266D02561276A50905B1F44DEDE646F4845D634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3540[1] = 
{
	U3CGetConstructorByReflectionU3Ec__AnonStorey0_t4266D02561276A50905B1F44DEDE646F4845D634::get_offset_of_constructorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t5304BC457840A263778B61647A6214450139CE2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3541[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey1_t5304BC457840A263778B61647A6214450139CE2F::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[1] = 
{
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t5DD58BDB4BDF3531B0981E89EF60C3ED0C510D14::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey3_tFBA12729E943700B178CA5477723857345AB686B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey3_tFBA12729E943700B178CA5477723857345AB686B::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t43269ED7005D484DB0429A87155BB781825B5E47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3544[1] = 
{
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t43269ED7005D484DB0429A87155BB781825B5E47::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (UnsupportedPlatform_t5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3545[1] = 
{
	UnsupportedPlatform_t5FA6C225FDA5FF02CA0C0297631D99C8C4C2E5C3::get_offset_of_OnFinish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23), -1, sizeof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3547[2] = 
{
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_sounds_4(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (U3CU3Ec__DisplayClass3_0_tF54F3CFD46E387BF9588D963503C85101F5C63CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[1] = 
{
	U3CU3Ec__DisplayClass3_0_tF54F3CFD46E387BF9588D963503C85101F5C63CD::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (U3CU3Ec__DisplayClass4_0_tDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[1] = 
{
	U3CU3Ec__DisplayClass4_0_tDA8B7EC36BAAE75F7AE640EB7411FE4500741F4A::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3550[2] = 
{
	BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519::get_offset_of_speed_4(),
	BackgroundScroll_tF3B9D70F56D79BB113311F4DA718F61CAB9F2519::get_offset_of_background_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3551[3] = 
{
	U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660::get_offset_of_U3CU3E1__state_0(),
	U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660::get_offset_of_U3CU3E2__current_1(),
	U3CSpeedupU3Ed__4_tD90F6D88C9B4C7242CFA6C631475692619BA0660::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124), -1, sizeof(DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3552[8] = 
{
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124_StaticFields::get_offset_of_continued_4(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_fadeout_5(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_deathMenuUI_6(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_continueButton_7(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_continueText_8(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_pauseButton_9(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_lives_10(),
	DeathMenu_t0CB407645FA349343DECE47FD54C29699A6B3124::get_offset_of_player_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[3] = 
{
	U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5::get_offset_of_U3CU3E1__state_0(),
	U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5::get_offset_of_U3CU3E2__current_1(),
	U3CReplayU3Ed__12_t3EF73B603B18AC38C3C53C07D1D1D9291A0250A5::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (GameBegin_t534AB32FFB887AF031E737A7CAD5EB4E8C809405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3554[2] = 
{
	GameBegin_t534AB32FFB887AF031E737A7CAD5EB4E8C809405::get_offset_of_player_4(),
	GameBegin_t534AB32FFB887AF031E737A7CAD5EB4E8C809405::get_offset_of_canvas_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (GameRestart_tB1422A29C71CC4EA28057CE03A9D579DA2EFFF7C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (U3CRestartU3Ed__1_tDEEC48D65C587F41A028A3F6C7F157CC873E65FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[2] = 
{
	U3CRestartU3Ed__1_tDEEC48D65C587F41A028A3F6C7F157CC873E65FA::get_offset_of_U3CU3E1__state_0(),
	U3CRestartU3Ed__1_tDEEC48D65C587F41A028A3F6C7F157CC873E65FA::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (HiScoreManager_tECD89E9A4293A0AC2D4261C014A572E0DD26BCFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[2] = 
{
	HiScoreManager_tECD89E9A4293A0AC2D4261C014A572E0DD26BCFF::get_offset_of_highScoreText_4(),
	HiScoreManager_tECD89E9A4293A0AC2D4261C014A572E0DD26BCFF::get_offset_of_highScore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (LevelFader_t80A930EAED182822655C69B819748C49C59A31A5), -1, sizeof(LevelFader_t80A930EAED182822655C69B819748C49C59A31A5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3559[3] = 
{
	LevelFader_t80A930EAED182822655C69B819748C49C59A31A5::get_offset_of_animator_4(),
	LevelFader_t80A930EAED182822655C69B819748C49C59A31A5_StaticFields::get_offset_of__level_5(),
	LevelFader_t80A930EAED182822655C69B819748C49C59A31A5::get_offset_of_skipAd_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3560[3] = 
{
	U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929::get_offset_of_U3CU3E1__state_0(),
	U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929::get_offset_of_U3CU3E2__current_1(),
	U3CFadeoutU3Ed__13_tEAC76A815C6E244A3B46A863EC2EBC6438C53929::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[3] = 
{
	U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670::get_offset_of_U3CU3E1__state_0(),
	U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670::get_offset_of_U3CU3E2__current_1(),
	U3CAdWaitU3Ed__18_tADB6750FDAAC1A9D40A2D1C1FA0A5A42F63A5670::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3562[6] = 
{
	OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE::get_offset_of_lives_4(),
	OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE::get_offset_of_LivesText_5(),
	OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE::get_offset_of_score_6(),
	OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE::get_offset_of_ScoreText_7(),
	OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE::get_offset_of_hiscore_8(),
	OnScreenNumbers_tACC8057CA300F75FAF5F4EEBF558A4ABD876F6FE::get_offset_of_player_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791), -1, sizeof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3563[3] = 
{
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_pauseMenu_4(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields::get_offset_of__isPaused_5(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_pauseButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (Sound_tE1867C28E2BC13723E294746060427A1487DF1F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3564[7] = 
{
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_name_0(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_clip_1(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_volume_2(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_pitch_3(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_source_4(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_loop_5(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_mute_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D), -1, sizeof(Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3565[21] = 
{
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_player_4(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_heart_5(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_lifeSpawn_6(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_shrink_7(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_blockSpawn_8(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_block_9(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_shrinkTokken_10(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_lifeTokken_11(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_minXValue_12(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_maxXValue_13(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_startSpawnWait_14(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_minSpawnWait_15(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_accelerationSpawnRate_16(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_gravityAcceleration_17(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_minGravity_18(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_startWait_19(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D_StaticFields::get_offset_of__stop_20(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_gravity_21(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_blockrestart_22(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_powerupcountdown_23(),
	Spawner_t6D000391A607DDA6E3C0F62284BA6E5127E2010D::get_offset_of_rand_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3566[3] = 
{
	U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120::get_offset_of_U3CU3E2__current_1(),
	U3CSpawnBlocksU3Ed__25_tB573CF44EF21F3877E971E08C705FD52830CE120::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3567[4] = 
{
	U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102::get_offset_of_U3CU3E1__state_0(),
	U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102::get_offset_of_U3CU3E2__current_1(),
	U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102::get_offset_of_U3CU3E4__this_2(),
	U3CSpawnPowerUpsU3Ed__26_tB25473D6B5134BEBA04939363DEB8B42CF807102::get_offset_of_U3CspawnerU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (U3CPlayU3Ed__4_tF0E541E4FF96BA814812B41638B704D1B0C01204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3569[2] = 
{
	U3CPlayU3Ed__4_tF0E541E4FF96BA814812B41638B704D1B0C01204::get_offset_of_U3CU3E1__state_0(),
	U3CPlayU3Ed__4_tF0E541E4FF96BA814812B41638B704D1B0C01204::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (Block_t613E03B6725E5824DD83D0B230AC258E08338D24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3570[12] = 
{
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_cF_4(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_player_5(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_value_6(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_getPoints_7(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_rand_8(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_canBeSuperSized_9(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_damage_10(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_isSuperSized_11(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_otherComets_12(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_monsterSize_13(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_trail_14(),
	Block_t613E03B6725E5824DD83D0B230AC258E08338D24::get_offset_of_explode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3571[3] = 
{
	BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B::get_offset_of_itemType_4(),
	BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B::get_offset_of_priceText_5(),
	BuyButton_tED0F7B2CEC81E44C83395BF3C51745C21F675E2B::get_offset_of_defaultText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (ItemType_tCF5610CD6C681493E68253D10F26EFC6E5F1CC61)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3572[2] = 
{
	ItemType_tCF5610CD6C681493E68253D10F26EFC6E5F1CC61::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3573[3] = 
{
	U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF::get_offset_of_U3CU3E1__state_0(),
	U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF::get_offset_of_U3CU3E2__current_1(),
	U3CLoadPriceRoutineU3Ed__6_t3EF64648C2E9AAF995F512C9A7A913A1B30E8EBF::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3574[1] = 
{
	BuyingLives_tBE4F15CE28D3FC452772A0458AF3BBC1240FE8E5::get_offset_of_player_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (DestroyAtBottom_t5D5F03C616513070775056625ADCE00243A22C28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[2] = 
{
	DestroyAtBottom_t5D5F03C616513070775056625ADCE00243A22C28::get_offset_of_player_4(),
	DestroyAtBottom_t5D5F03C616513070775056625ADCE00243A22C28::get_offset_of_block_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (DestroyAtBottomLarge_t858E6FD2D9935E296877163DD64389352C2D74D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[7] = 
{
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_player_4(),
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_rb_5(),
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_move_6(),
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_needsToMove_7(),
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_screenLeft_8(),
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_screenRight_9(),
	HeartTokken_t170763CE258ADD2C5F33A17D44655474031CC6FC::get_offset_of_yValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A), -1, sizeof(IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3578[3] = 
{
	IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_StaticFields::get_offset_of_m_StoreController_6(),
	IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A_StaticFields::get_offset_of_m_StoreExtensionProvider_7(),
	IAPManager_tF1D84AAED44642C4728E9CC5E68C40CD6D42F85A::get_offset_of_extraLife_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[7] = 
{
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_lives_4(),
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_rb_5(),
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_move_6(),
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_needsToMove_7(),
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_screenLeft_8(),
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_screenRight_9(),
	LifeTokken_t4AD02ABCF01B88B26BA844A1D1797F47F8CD5E98::get_offset_of_yValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[7] = 
{
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_player_4(),
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_rb_5(),
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_move_6(),
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_needsToMove_7(),
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_screenLeft_8(),
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_screenRight_9(),
	Shrink_tA416FC95B479B04052D570FE05839710FD8F97D5::get_offset_of_yValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3581[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (Curser_t60015E78331BAC502803E3D3F29AEB516654948E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3582[1] = 
{
	Curser_t60015E78331BAC502803E3D3F29AEB516654948E::get_offset_of_touch_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (Engine_t62C213B195B780D1183D68751B5F0118D702848D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3583[5] = 
{
	Engine_t62C213B195B780D1183D68751B5F0118D702848D::get_offset_of_engine_4(),
	Engine_t62C213B195B780D1183D68751B5F0118D702848D::get_offset_of_player_5(),
	Engine_t62C213B195B780D1183D68751B5F0118D702848D::get_offset_of_flashtime_6(),
	Engine_t62C213B195B780D1183D68751B5F0118D702848D::get_offset_of_go_7(),
	Engine_t62C213B195B780D1183D68751B5F0118D702848D::get_offset_of_color_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3584[3] = 
{
	U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4::get_offset_of_U3CU3E1__state_0(),
	U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4::get_offset_of_U3CU3E2__current_1(),
	U3CStartUpU3Ed__7_tF5CE81402D8EA5C760A8BF0DF9C31A4CAF5531E4::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3585[1] = 
{
	Explosion_t02AD52B12A8E210C86E08E90E69809A4F2DD23AA::get_offset_of_boom_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3586[3] = 
{
	U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C::get_offset_of_U3CU3E1__state_0(),
	U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C::get_offset_of_U3CU3E2__current_1(),
	U3CBoomU3Ed__3_t4B42E446A6E29CEC63E3FE3902A053401747194C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3587[5] = 
{
	HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E::get_offset_of_health_4(),
	HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E::get_offset_of_player_5(),
	HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E::get_offset_of_healthCounter_6(),
	HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E::get_offset_of_shipsHas_7(),
	HealthCounter_tEED65A1048B0E4A59E9A9F715EED8D1BE47C2A9E::get_offset_of_shipslost_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873), -1, sizeof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3588[27] = 
{
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_rb_4(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_normalSize_5(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_smallSize_6(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_score_7(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields::get_offset_of__hiScore_8(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields::get_offset_of__lives_9(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of__health_10(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields::get_offset_of__maxHealth_11(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_hasBeenHit_12(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_playerSprite_13(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_activePlayer_14(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_playerRenderer_15(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields::get_offset_of__playerSelection_16(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_color_17(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_touchCourser_18(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_speed_19(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_moveHorizontal_20(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_moveVertical_21(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_rotation_22(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields::get_offset_of_playerAlive_23(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_animate_24(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_screenLeft_25(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_screenRight_26(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_canBeHit_27(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_isShrunk_28(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_isShrunkActivate_29(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_pauseButton_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3589[3] = 
{
	U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888::get_offset_of_U3CU3E1__state_0(),
	U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888::get_offset_of_U3CU3E2__current_1(),
	U3CShrinkEffectU3Ed__50_t4C3270177B1B02F4EBF74A27ABF3D642632FB888::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3590[3] = 
{
	U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142::get_offset_of_U3CU3E1__state_0(),
	U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142::get_offset_of_U3CU3E2__current_1(),
	U3CInvincibleU3Ed__51_t8A52115D5FFD1C2623A766358A2800F0CE98E142::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3591[2] = 
{
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_lives_0(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_hiScore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (PlayerSelection_t851FF522452A79A15C32CA8CD539006EBC83F998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[1] = 
{
	PlayerSelection_t851FF522452A79A15C32CA8CD539006EBC83F998::get_offset_of_animate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (SaveSystem_t9373337803DA692D0868900C223D80B89CF75FD6), -1, sizeof(SaveSystem_t9373337803DA692D0868900C223D80B89CF75FD6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3593[2] = 
{
	SaveSystem_t9373337803DA692D0868900C223D80B89CF75FD6_StaticFields::get_offset_of_FileLoadAttemps_0(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3595[24] = 
{
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_Controller_4(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_AppleExtensions_5(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_MoolahExtensions_6(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_SamsungExtensions_7(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_MicrosoftExtensions_8(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_UnityChannelExtensions_9(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_TransactionHistoryExtensions_10(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_GooglePlayStoreExtensions_11(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_IsGooglePlayStoreSelected_12(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_IsSamsungAppsStoreSelected_13(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_IsCloudMoolahStoreSelected_14(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_IsUnityChannelSelected_15(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_LastTransactionID_16(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_IsLoggedIn_17(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_unityChannelLoginHandler_18(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_FetchReceiptPayloadOnPurchase_19(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_PurchaseInProgress_20(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_m_ProductUIs_21(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_productUITemplate_22(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_contentRect_23(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_restoreButton_24(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_loginButton_25(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_validateButton_26(),
	IAPDemo_tF4701A448092D00074EDBAB42C7EA3D673B71C92::get_offset_of_versionText_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (UnityChannelPurchaseError_tFF027995D57BFD5C96AC83869F58682E5BF7AC7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3596[2] = 
{
	UnityChannelPurchaseError_tFF027995D57BFD5C96AC83869F58682E5BF7AC7D::get_offset_of_error_0(),
	UnityChannelPurchaseError_tFF027995D57BFD5C96AC83869F58682E5BF7AC7D::get_offset_of_purchaseInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3597[3] = 
{
	UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9::get_offset_of_productCode_0(),
	UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9::get_offset_of_gameOrderId_1(),
	UnityChannelPurchaseInfo_t166CAF917D0E17C3E53B135D8652B9DE6D02F4F9::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[4] = 
{
	UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E::get_offset_of_initializeSucceededAction_0(),
	UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E::get_offset_of_initializeFailedAction_1(),
	UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E::get_offset_of_loginSucceededAction_2(),
	UnityChannelLoginHandler_t716B2D821D678D13F93E406A33996C6F12611A1E::get_offset_of_loginFailedAction_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3599[3] = 
{
	U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A::get_offset_of_builder_1(),
	U3CU3Ec__DisplayClass30_0_t4AA197CD7EF6A8646F2CE8EB2281C4F73CC7403A::get_offset_of_initializeUnityIap_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
