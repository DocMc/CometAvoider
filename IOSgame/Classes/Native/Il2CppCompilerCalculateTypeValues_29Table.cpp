﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>>
struct Action_1_tF91368AFD1372C3CAE111C0453398402BC6DB6EB;
// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F;
// System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>
struct Action_1_tB45407A14BE89BE8E584A46F19C6CC7477AAD6AE;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.Single,System.Action>
struct Dictionary_2_t8D2697B015E3E4B5C5E2F84B4EA30062C220D509;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product>
struct Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>
struct Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Purchasing.Extension.IStoreConfiguration>
struct Dictionary_2_t64A401E38825EF1AC546A560D72B9C15E9167A58;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Purchasing.IStoreExtension>
struct Dictionary_2_tE331E9E028851C066F5036220115B7D1C3B6DFB1;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product>
struct HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863;
// System.Collections.Generic.List`1<System.Action>
struct List_1_tF4B622C1ABA386932660D23A459A2974FB56E2EE;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.PayoutDefinition>
struct List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<UnityEngine.Purchasing.Product,System.String>
struct Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E;
// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Product>
struct Func_2_t07B8477BB2171FC8FCAB9AAF85EE70B4D9C8CD05;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8;
// UnityEngine.ILogger
struct ILogger_t572B66532D8EB6E76240476A788384A26D70866F;
// UnityEngine.Purchasing.AnalyticsReporter
struct AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3;
// UnityEngine.Purchasing.Extension.ICatalogProvider
struct ICatalogProvider_tF713C0B63A4E2015E1BF50F218A4FBA36CA9470B;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t29DFEB92F0789E5E0A5F52148451C2A5F9BEA425;
// UnityEngine.Purchasing.Extension.IStore
struct IStore_t496ACD1C21137C6E16BCAA48DEBEEED2047DD14C;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t4AEE9698E20BA2DCA32EC4729DCAA60D09DA95CA;
// UnityEngine.Purchasing.IInternalStoreListener
struct IInternalStoreListener_tBD4CEF51FA154BF427FA00C38370D691F94A669D;
// UnityEngine.Purchasing.IStoreListener
struct IStoreListener_tBB9A0373D14167C7600B988F37B85E14E8D60A66;
// UnityEngine.Purchasing.IUnityAnalytics
struct IUnityAnalytics_tA4A6C0E583F495A281D4C2EC0AC17ECA255A84D1;
// UnityEngine.Purchasing.Product
struct Product_t830A133A97BEA12C3CD696853098A295D99A6DE4;
// UnityEngine.Purchasing.ProductCollection
struct ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10;
// UnityEngine.Purchasing.ProductMetadata
struct ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48;
// UnityEngine.Purchasing.Product[]
struct ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237;
// UnityEngine.Purchasing.PurchasingFactory
struct PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765;
// UnityEngine.Purchasing.PurchasingManager
struct PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt[]
struct AppleInAppPurchaseReceiptU5BU5D_tC645ADBB6722E0C4288F0DC73C2EDD92D4AADBA0;
// UnityEngine.Purchasing.StoreListenerProxy
struct StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023;
// UnityEngine.Purchasing.TransactionLog
struct TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB;
// UnityEngine.UDP.Analytics.SessionInfo
struct SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60;
// UnityEngine.UDP.IInitListener
struct IInitListener_t32D654643DBC1FCAB2167C53E79FE97950FC31F9;
// UnityEngine.UDP.IPurchaseListener
struct IPurchaseListener_t6A634C3BDC04EC511965F742C6F8DED4616662B7;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T317A9D5BF1216EFB937900A181D26A1830332F44_H
#define U3CMODULEU3E_T317A9D5BF1216EFB937900A181D26A1830332F44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t317A9D5BF1216EFB937900A181D26A1830332F44 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T317A9D5BF1216EFB937900A181D26A1830332F44_H
#ifndef U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#define U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifndef U3CMODULEU3E_TD874E3101CE4B21F74D822F7B7638994C787A99B_H
#define U3CMODULEU3E_TD874E3101CE4B21F74D822F7B7638994C787A99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD874E3101CE4B21F74D822F7B7638994C787A99B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TD874E3101CE4B21F74D822F7B7638994C787A99B_H
#ifndef U3CMODULEU3E_TE75F9FB0CF760B28F220FC101BF7C9D8DF5EAB47_H
#define U3CMODULEU3E_TE75F9FB0CF760B28F220FC101BF7C9D8DF5EAB47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tE75F9FB0CF760B28F220FC101BF7C9D8DF5EAB47 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TE75F9FB0CF760B28F220FC101BF7C9D8DF5EAB47_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASN1NODE_T837D73CC809A861E760842A0DDE7C585A1A52ECA_H
#define ASN1NODE_T837D73CC809A861E760842A0DDE7C585A1A52ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Node
struct  Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA  : public RuntimeObject
{
public:
	// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::tag
	uint8_t ___tag_0;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::dataOffset
	int64_t ___dataOffset_1;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::dataLength
	int64_t ___dataLength_2;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::lengthFieldBytes
	int64_t ___lengthFieldBytes_3;
	// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Node::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_4;
	// System.Collections.ArrayList LipingShare.LCLib.Asn1Processor.Asn1Node::childNodeList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___childNodeList_5;
	// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::unusedBits
	uint8_t ___unusedBits_6;
	// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::deepness
	int64_t ___deepness_7;
	// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::path
	String_t* ___path_8;
	// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::parentNode
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA * ___parentNode_9;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::requireRecalculatePar
	bool ___requireRecalculatePar_10;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::isIndefiniteLength
	bool ___isIndefiniteLength_11;
	// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::parseEncapsulatedData
	bool ___parseEncapsulatedData_12;

public:
	inline static int32_t get_offset_of_tag_0() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___tag_0)); }
	inline uint8_t get_tag_0() const { return ___tag_0; }
	inline uint8_t* get_address_of_tag_0() { return &___tag_0; }
	inline void set_tag_0(uint8_t value)
	{
		___tag_0 = value;
	}

	inline static int32_t get_offset_of_dataOffset_1() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___dataOffset_1)); }
	inline int64_t get_dataOffset_1() const { return ___dataOffset_1; }
	inline int64_t* get_address_of_dataOffset_1() { return &___dataOffset_1; }
	inline void set_dataOffset_1(int64_t value)
	{
		___dataOffset_1 = value;
	}

	inline static int32_t get_offset_of_dataLength_2() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___dataLength_2)); }
	inline int64_t get_dataLength_2() const { return ___dataLength_2; }
	inline int64_t* get_address_of_dataLength_2() { return &___dataLength_2; }
	inline void set_dataLength_2(int64_t value)
	{
		___dataLength_2 = value;
	}

	inline static int32_t get_offset_of_lengthFieldBytes_3() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___lengthFieldBytes_3)); }
	inline int64_t get_lengthFieldBytes_3() const { return ___lengthFieldBytes_3; }
	inline int64_t* get_address_of_lengthFieldBytes_3() { return &___lengthFieldBytes_3; }
	inline void set_lengthFieldBytes_3(int64_t value)
	{
		___lengthFieldBytes_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___data_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_4() const { return ___data_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_childNodeList_5() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___childNodeList_5)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_childNodeList_5() const { return ___childNodeList_5; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_childNodeList_5() { return &___childNodeList_5; }
	inline void set_childNodeList_5(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___childNodeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___childNodeList_5), value);
	}

	inline static int32_t get_offset_of_unusedBits_6() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___unusedBits_6)); }
	inline uint8_t get_unusedBits_6() const { return ___unusedBits_6; }
	inline uint8_t* get_address_of_unusedBits_6() { return &___unusedBits_6; }
	inline void set_unusedBits_6(uint8_t value)
	{
		___unusedBits_6 = value;
	}

	inline static int32_t get_offset_of_deepness_7() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___deepness_7)); }
	inline int64_t get_deepness_7() const { return ___deepness_7; }
	inline int64_t* get_address_of_deepness_7() { return &___deepness_7; }
	inline void set_deepness_7(int64_t value)
	{
		___deepness_7 = value;
	}

	inline static int32_t get_offset_of_path_8() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___path_8)); }
	inline String_t* get_path_8() const { return ___path_8; }
	inline String_t** get_address_of_path_8() { return &___path_8; }
	inline void set_path_8(String_t* value)
	{
		___path_8 = value;
		Il2CppCodeGenWriteBarrier((&___path_8), value);
	}

	inline static int32_t get_offset_of_parentNode_9() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___parentNode_9)); }
	inline Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA * get_parentNode_9() const { return ___parentNode_9; }
	inline Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA ** get_address_of_parentNode_9() { return &___parentNode_9; }
	inline void set_parentNode_9(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA * value)
	{
		___parentNode_9 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_9), value);
	}

	inline static int32_t get_offset_of_requireRecalculatePar_10() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___requireRecalculatePar_10)); }
	inline bool get_requireRecalculatePar_10() const { return ___requireRecalculatePar_10; }
	inline bool* get_address_of_requireRecalculatePar_10() { return &___requireRecalculatePar_10; }
	inline void set_requireRecalculatePar_10(bool value)
	{
		___requireRecalculatePar_10 = value;
	}

	inline static int32_t get_offset_of_isIndefiniteLength_11() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___isIndefiniteLength_11)); }
	inline bool get_isIndefiniteLength_11() const { return ___isIndefiniteLength_11; }
	inline bool* get_address_of_isIndefiniteLength_11() { return &___isIndefiniteLength_11; }
	inline void set_isIndefiniteLength_11(bool value)
	{
		___isIndefiniteLength_11 = value;
	}

	inline static int32_t get_offset_of_parseEncapsulatedData_12() { return static_cast<int32_t>(offsetof(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA, ___parseEncapsulatedData_12)); }
	inline bool get_parseEncapsulatedData_12() const { return ___parseEncapsulatedData_12; }
	inline bool* get_address_of_parseEncapsulatedData_12() { return &___parseEncapsulatedData_12; }
	inline void set_parseEncapsulatedData_12(bool value)
	{
		___parseEncapsulatedData_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1NODE_T837D73CC809A861E760842A0DDE7C585A1A52ECA_H
#ifndef ASN1PARSER_TA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3_H
#define ASN1PARSER_TA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Parser
struct  Asn1Parser_tA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3  : public RuntimeObject
{
public:
	// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Parser::rawData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___rawData_0;
	// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Parser::rootNode
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA * ___rootNode_1;

public:
	inline static int32_t get_offset_of_rawData_0() { return static_cast<int32_t>(offsetof(Asn1Parser_tA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3, ___rawData_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_rawData_0() const { return ___rawData_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_rawData_0() { return &___rawData_0; }
	inline void set_rawData_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___rawData_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawData_0), value);
	}

	inline static int32_t get_offset_of_rootNode_1() { return static_cast<int32_t>(offsetof(Asn1Parser_tA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3, ___rootNode_1)); }
	inline Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA * get_rootNode_1() const { return ___rootNode_1; }
	inline Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA ** get_address_of_rootNode_1() { return &___rootNode_1; }
	inline void set_rootNode_1(Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA * value)
	{
		___rootNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootNode_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1PARSER_TA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3_H
#ifndef ASN1UTIL_T63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_H
#define ASN1UTIL_T63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Asn1Util
struct  Asn1Util_t63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9  : public RuntimeObject
{
public:

public:
};

struct Asn1Util_t63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_StaticFields
{
public:
	// System.Char[] LipingShare.LCLib.Asn1Processor.Asn1Util::hexDigits
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___hexDigits_0;

public:
	inline static int32_t get_offset_of_hexDigits_0() { return static_cast<int32_t>(offsetof(Asn1Util_t63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_StaticFields, ___hexDigits_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_hexDigits_0() const { return ___hexDigits_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_hexDigits_0() { return &___hexDigits_0; }
	inline void set_hexDigits_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___hexDigits_0 = value;
		Il2CppCodeGenWriteBarrier((&___hexDigits_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1UTIL_T63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_H
#ifndef OID_T9D72792E7144ECEB69284AE6004135BE53596204_H
#define OID_T9D72792E7144ECEB69284AE6004135BE53596204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.Oid
struct  Oid_t9D72792E7144ECEB69284AE6004135BE53596204  : public RuntimeObject
{
public:

public:
};

struct Oid_t9D72792E7144ECEB69284AE6004135BE53596204_StaticFields
{
public:
	// System.Collections.Specialized.StringDictionary LipingShare.LCLib.Asn1Processor.Oid::oidDictionary
	StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF * ___oidDictionary_0;

public:
	inline static int32_t get_offset_of_oidDictionary_0() { return static_cast<int32_t>(offsetof(Oid_t9D72792E7144ECEB69284AE6004135BE53596204_StaticFields, ___oidDictionary_0)); }
	inline StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF * get_oidDictionary_0() const { return ___oidDictionary_0; }
	inline StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF ** get_address_of_oidDictionary_0() { return &___oidDictionary_0; }
	inline void set_oidDictionary_0(StringDictionary_t9B6306775C5F70981BCB8A30603B4C93C22844FF * value)
	{
		___oidDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___oidDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OID_T9D72792E7144ECEB69284AE6004135BE53596204_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#define ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#ifndef ANALYTICSREPORTER_T0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3_H
#define ANALYTICSREPORTER_T0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AnalyticsReporter
struct  AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IUnityAnalytics UnityEngine.Purchasing.AnalyticsReporter::m_Analytics
	RuntimeObject* ___m_Analytics_0;

public:
	inline static int32_t get_offset_of_m_Analytics_0() { return static_cast<int32_t>(offsetof(AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3, ___m_Analytics_0)); }
	inline RuntimeObject* get_m_Analytics_0() const { return ___m_Analytics_0; }
	inline RuntimeObject** get_address_of_m_Analytics_0() { return &___m_Analytics_0; }
	inline void set_m_Analytics_0(RuntimeObject* value)
	{
		___m_Analytics_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Analytics_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSREPORTER_T0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3_H
#ifndef CONFIGURATIONBUILDER_T18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE_H
#define CONFIGURATIONBUILDER_T18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ConfigurationBuilder
struct  ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.PurchasingFactory UnityEngine.Purchasing.ConfigurationBuilder::m_Factory
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 * ___m_Factory_0;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.ConfigurationBuilder::m_Products
	HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * ___m_Products_1;
	// System.Boolean UnityEngine.Purchasing.ConfigurationBuilder::<useCatalogProvider>k__BackingField
	bool ___U3CuseCatalogProviderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Factory_0() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE, ___m_Factory_0)); }
	inline PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 * get_m_Factory_0() const { return ___m_Factory_0; }
	inline PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 ** get_address_of_m_Factory_0() { return &___m_Factory_0; }
	inline void set_m_Factory_0(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765 * value)
	{
		___m_Factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Factory_0), value);
	}

	inline static int32_t get_offset_of_m_Products_1() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE, ___m_Products_1)); }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * get_m_Products_1() const { return ___m_Products_1; }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 ** get_address_of_m_Products_1() { return &___m_Products_1; }
	inline void set_m_Products_1(HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * value)
	{
		___m_Products_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Products_1), value);
	}

	inline static int32_t get_offset_of_U3CuseCatalogProviderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE, ___U3CuseCatalogProviderU3Ek__BackingField_2)); }
	inline bool get_U3CuseCatalogProviderU3Ek__BackingField_2() const { return ___U3CuseCatalogProviderU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CuseCatalogProviderU3Ek__BackingField_2() { return &___U3CuseCatalogProviderU3Ek__BackingField_2; }
	inline void set_U3CuseCatalogProviderU3Ek__BackingField_2(bool value)
	{
		___U3CuseCatalogProviderU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONBUILDER_T18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE_H
#ifndef ABSTRACTPURCHASINGMODULE_T73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9_H
#define ABSTRACTPURCHASINGMODULE_T73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractPurchasingModule
struct  AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.IPurchasingBinder UnityEngine.Purchasing.Extension.AbstractPurchasingModule::m_Binder
	RuntimeObject* ___m_Binder_0;

public:
	inline static int32_t get_offset_of_m_Binder_0() { return static_cast<int32_t>(offsetof(AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9, ___m_Binder_0)); }
	inline RuntimeObject* get_m_Binder_0() const { return ___m_Binder_0; }
	inline RuntimeObject** get_address_of_m_Binder_0() { return &___m_Binder_0; }
	inline void set_m_Binder_0(RuntimeObject* value)
	{
		___m_Binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTPURCHASINGMODULE_T73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9_H
#ifndef ABSTRACTSTORE_T0809CE12BFD8191F29B7B77FC730AE3E8A3C039C_H
#define ABSTRACTSTORE_T0809CE12BFD8191F29B7B77FC730AE3E8A3C039C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractStore
struct  AbstractStore_t0809CE12BFD8191F29B7B77FC730AE3E8A3C039C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTSTORE_T0809CE12BFD8191F29B7B77FC730AE3E8A3C039C_H
#ifndef IDS_T978DE239966E5906480541F9982EA9624943DA53_H
#define IDS_T978DE239966E5906480541F9982EA9624943DA53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IDs
struct  IDs_t978DE239966E5906480541F9982EA9624943DA53  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Purchasing.IDs::m_Dic
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___m_Dic_0;

public:
	inline static int32_t get_offset_of_m_Dic_0() { return static_cast<int32_t>(offsetof(IDs_t978DE239966E5906480541F9982EA9624943DA53, ___m_Dic_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_m_Dic_0() const { return ___m_Dic_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_m_Dic_0() { return &___m_Dic_0; }
	inline void set_m_Dic_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___m_Dic_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDS_T978DE239966E5906480541F9982EA9624943DA53_H
#ifndef PRODUCT_T830A133A97BEA12C3CD696853098A295D99A6DE4_H
#define PRODUCT_T830A133A97BEA12C3CD696853098A295D99A6DE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Product
struct  Product_t830A133A97BEA12C3CD696853098A295D99A6DE4  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.Product::<definition>k__BackingField
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * ___U3CdefinitionU3Ek__BackingField_0;
	// UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.Product::<metadata>k__BackingField
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * ___U3CmetadataU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Purchasing.Product::<availableToPurchase>k__BackingField
	bool ___U3CavailableToPurchaseU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Product::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Product::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CdefinitionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CdefinitionU3Ek__BackingField_0)); }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * get_U3CdefinitionU3Ek__BackingField_0() const { return ___U3CdefinitionU3Ek__BackingField_0; }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 ** get_address_of_U3CdefinitionU3Ek__BackingField_0() { return &___U3CdefinitionU3Ek__BackingField_0; }
	inline void set_U3CdefinitionU3Ek__BackingField_0(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * value)
	{
		___U3CdefinitionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdefinitionU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CmetadataU3Ek__BackingField_1)); }
	inline ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * get_U3CmetadataU3Ek__BackingField_1() const { return ___U3CmetadataU3Ek__BackingField_1; }
	inline ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 ** get_address_of_U3CmetadataU3Ek__BackingField_1() { return &___U3CmetadataU3Ek__BackingField_1; }
	inline void set_U3CmetadataU3Ek__BackingField_1(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * value)
	{
		___U3CmetadataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CavailableToPurchaseU3Ek__BackingField_2)); }
	inline bool get_U3CavailableToPurchaseU3Ek__BackingField_2() const { return ___U3CavailableToPurchaseU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CavailableToPurchaseU3Ek__BackingField_2() { return &___U3CavailableToPurchaseU3Ek__BackingField_2; }
	inline void set_U3CavailableToPurchaseU3Ek__BackingField_2(bool value)
	{
		___U3CavailableToPurchaseU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CtransactionIDU3Ek__BackingField_3)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_3() const { return ___U3CtransactionIDU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_3() { return &___U3CtransactionIDU3Ek__BackingField_3; }
	inline void set_U3CtransactionIDU3Ek__BackingField_3(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4, ___U3CreceiptU3Ek__BackingField_4)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_4() const { return ___U3CreceiptU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_4() { return &___U3CreceiptU3Ek__BackingField_4; }
	inline void set_U3CreceiptU3Ek__BackingField_4(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCT_T830A133A97BEA12C3CD696853098A295D99A6DE4_H
#ifndef PRODUCTCOLLECTION_T7BE71F2FA5BE05F5DA13EA701B513861516F4C07_H
#define PRODUCTCOLLECTION_T7BE71F2FA5BE05F5DA13EA701B513861516F4C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCollection
struct  ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_IdToProduct
	Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * ___m_IdToProduct_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_StoreSpecificIdToProduct
	Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * ___m_StoreSpecificIdToProduct_1;
	// UnityEngine.Purchasing.Product[] UnityEngine.Purchasing.ProductCollection::m_Products
	ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237* ___m_Products_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_ProductSet
	HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 * ___m_ProductSet_3;

public:
	inline static int32_t get_offset_of_m_IdToProduct_0() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_IdToProduct_0)); }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * get_m_IdToProduct_0() const { return ___m_IdToProduct_0; }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 ** get_address_of_m_IdToProduct_0() { return &___m_IdToProduct_0; }
	inline void set_m_IdToProduct_0(Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * value)
	{
		___m_IdToProduct_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_IdToProduct_0), value);
	}

	inline static int32_t get_offset_of_m_StoreSpecificIdToProduct_1() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_StoreSpecificIdToProduct_1)); }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * get_m_StoreSpecificIdToProduct_1() const { return ___m_StoreSpecificIdToProduct_1; }
	inline Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 ** get_address_of_m_StoreSpecificIdToProduct_1() { return &___m_StoreSpecificIdToProduct_1; }
	inline void set_m_StoreSpecificIdToProduct_1(Dictionary_2_tCAF0D54E59FAC220E2EF9D3ECAFF045862EC66C0 * value)
	{
		___m_StoreSpecificIdToProduct_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreSpecificIdToProduct_1), value);
	}

	inline static int32_t get_offset_of_m_Products_2() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_Products_2)); }
	inline ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237* get_m_Products_2() const { return ___m_Products_2; }
	inline ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237** get_address_of_m_Products_2() { return &___m_Products_2; }
	inline void set_m_Products_2(ProductU5BU5D_tCC1E9F34B8B3A88563CA989013308FB058864237* value)
	{
		___m_Products_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Products_2), value);
	}

	inline static int32_t get_offset_of_m_ProductSet_3() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07, ___m_ProductSet_3)); }
	inline HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 * get_m_ProductSet_3() const { return ___m_ProductSet_3; }
	inline HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 ** get_address_of_m_ProductSet_3() { return &___m_ProductSet_3; }
	inline void set_m_ProductSet_3(HashSet_1_t361D3549FC7078E1C2F7C4C344909910E39A0D92 * value)
	{
		___m_ProductSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductSet_3), value);
	}
};

struct ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields
{
public:
	// System.Func`2<UnityEngine.Purchasing.Product,System.String> UnityEngine.Purchasing.ProductCollection::<>f__amU24cache0
	Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<UnityEngine.Purchasing.Product,System.String> UnityEngine.Purchasing.ProductCollection::<>f__amU24cache1
	Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_tE3DDEF72300908E3958302E8350C5EECED6BD11E * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCOLLECTION_T7BE71F2FA5BE05F5DA13EA701B513861516F4C07_H
#ifndef PURCHASEEVENTARGS_TF6E04BFD5492F5F57309FFBB2415EB26E5B88C04_H
#define PURCHASEEVENTARGS_TF6E04BFD5492F5F57309FFBB2415EB26E5B88C04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchaseEventArgs
struct  PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Product UnityEngine.Purchasing.PurchaseEventArgs::<purchasedProduct>k__BackingField
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * ___U3CpurchasedProductU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpurchasedProductU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04, ___U3CpurchasedProductU3Ek__BackingField_0)); }
	inline Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * get_U3CpurchasedProductU3Ek__BackingField_0() const { return ___U3CpurchasedProductU3Ek__BackingField_0; }
	inline Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 ** get_address_of_U3CpurchasedProductU3Ek__BackingField_0() { return &___U3CpurchasedProductU3Ek__BackingField_0; }
	inline void set_U3CpurchasedProductU3Ek__BackingField_0(Product_t830A133A97BEA12C3CD696853098A295D99A6DE4 * value)
	{
		___U3CpurchasedProductU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpurchasedProductU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEEVENTARGS_TF6E04BFD5492F5F57309FFBB2415EB26E5B88C04_H
#ifndef PURCHASINGFACTORY_TAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765_H
#define PURCHASINGFACTORY_TAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingFactory
struct  PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Purchasing.Extension.IStoreConfiguration> UnityEngine.Purchasing.PurchasingFactory::m_ConfigMap
	Dictionary_2_t64A401E38825EF1AC546A560D72B9C15E9167A58 * ___m_ConfigMap_0;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Purchasing.IStoreExtension> UnityEngine.Purchasing.PurchasingFactory::m_ExtensionMap
	Dictionary_2_tE331E9E028851C066F5036220115B7D1C3B6DFB1 * ___m_ExtensionMap_1;
	// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.PurchasingFactory::m_Store
	RuntimeObject* ___m_Store_2;
	// UnityEngine.Purchasing.Extension.ICatalogProvider UnityEngine.Purchasing.PurchasingFactory::m_CatalogProvider
	RuntimeObject* ___m_CatalogProvider_3;
	// System.String UnityEngine.Purchasing.PurchasingFactory::<storeName>k__BackingField
	String_t* ___U3CstoreNameU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_ConfigMap_0() { return static_cast<int32_t>(offsetof(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765, ___m_ConfigMap_0)); }
	inline Dictionary_2_t64A401E38825EF1AC546A560D72B9C15E9167A58 * get_m_ConfigMap_0() const { return ___m_ConfigMap_0; }
	inline Dictionary_2_t64A401E38825EF1AC546A560D72B9C15E9167A58 ** get_address_of_m_ConfigMap_0() { return &___m_ConfigMap_0; }
	inline void set_m_ConfigMap_0(Dictionary_2_t64A401E38825EF1AC546A560D72B9C15E9167A58 * value)
	{
		___m_ConfigMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigMap_0), value);
	}

	inline static int32_t get_offset_of_m_ExtensionMap_1() { return static_cast<int32_t>(offsetof(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765, ___m_ExtensionMap_1)); }
	inline Dictionary_2_tE331E9E028851C066F5036220115B7D1C3B6DFB1 * get_m_ExtensionMap_1() const { return ___m_ExtensionMap_1; }
	inline Dictionary_2_tE331E9E028851C066F5036220115B7D1C3B6DFB1 ** get_address_of_m_ExtensionMap_1() { return &___m_ExtensionMap_1; }
	inline void set_m_ExtensionMap_1(Dictionary_2_tE331E9E028851C066F5036220115B7D1C3B6DFB1 * value)
	{
		___m_ExtensionMap_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExtensionMap_1), value);
	}

	inline static int32_t get_offset_of_m_Store_2() { return static_cast<int32_t>(offsetof(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765, ___m_Store_2)); }
	inline RuntimeObject* get_m_Store_2() const { return ___m_Store_2; }
	inline RuntimeObject** get_address_of_m_Store_2() { return &___m_Store_2; }
	inline void set_m_Store_2(RuntimeObject* value)
	{
		___m_Store_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Store_2), value);
	}

	inline static int32_t get_offset_of_m_CatalogProvider_3() { return static_cast<int32_t>(offsetof(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765, ___m_CatalogProvider_3)); }
	inline RuntimeObject* get_m_CatalogProvider_3() const { return ___m_CatalogProvider_3; }
	inline RuntimeObject** get_address_of_m_CatalogProvider_3() { return &___m_CatalogProvider_3; }
	inline void set_m_CatalogProvider_3(RuntimeObject* value)
	{
		___m_CatalogProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CatalogProvider_3), value);
	}

	inline static int32_t get_offset_of_U3CstoreNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765, ___U3CstoreNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CstoreNameU3Ek__BackingField_4() const { return ___U3CstoreNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CstoreNameU3Ek__BackingField_4() { return &___U3CstoreNameU3Ek__BackingField_4; }
	inline void set_U3CstoreNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CstoreNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreNameU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASINGFACTORY_TAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765_H
#ifndef PURCHASINGMANAGER_T1C02FCB1F9869F02786EC0598E70500E423ABDF4_H
#define PURCHASINGMANAGER_T1C02FCB1F9869F02786EC0598E70500E423ABDF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingManager
struct  PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.PurchasingManager::m_Store
	RuntimeObject* ___m_Store_0;
	// UnityEngine.Purchasing.IInternalStoreListener UnityEngine.Purchasing.PurchasingManager::m_Listener
	RuntimeObject* ___m_Listener_1;
	// UnityEngine.ILogger UnityEngine.Purchasing.PurchasingManager::m_Logger
	RuntimeObject* ___m_Logger_2;
	// UnityEngine.Purchasing.TransactionLog UnityEngine.Purchasing.PurchasingManager::m_TransactionLog
	TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB * ___m_TransactionLog_3;
	// System.String UnityEngine.Purchasing.PurchasingManager::m_StoreName
	String_t* ___m_StoreName_4;
	// System.Action UnityEngine.Purchasing.PurchasingManager::m_AdditionalProductsCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_AdditionalProductsCallback_5;
	// System.Action`1<UnityEngine.Purchasing.InitializationFailureReason> UnityEngine.Purchasing.PurchasingManager::m_AdditionalProductsFailCallback
	Action_1_tB45407A14BE89BE8E584A46F19C6CC7477AAD6AE * ___m_AdditionalProductsFailCallback_6;
	// System.Boolean UnityEngine.Purchasing.PurchasingManager::<useTransactionLog>k__BackingField
	bool ___U3CuseTransactionLogU3Ek__BackingField_7;
	// UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.PurchasingManager::<products>k__BackingField
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 * ___U3CproductsU3Ek__BackingField_8;
	// System.Boolean UnityEngine.Purchasing.PurchasingManager::initialized
	bool ___initialized_9;

public:
	inline static int32_t get_offset_of_m_Store_0() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_Store_0)); }
	inline RuntimeObject* get_m_Store_0() const { return ___m_Store_0; }
	inline RuntimeObject** get_address_of_m_Store_0() { return &___m_Store_0; }
	inline void set_m_Store_0(RuntimeObject* value)
	{
		___m_Store_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Store_0), value);
	}

	inline static int32_t get_offset_of_m_Listener_1() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_Listener_1)); }
	inline RuntimeObject* get_m_Listener_1() const { return ___m_Listener_1; }
	inline RuntimeObject** get_address_of_m_Listener_1() { return &___m_Listener_1; }
	inline void set_m_Listener_1(RuntimeObject* value)
	{
		___m_Listener_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Listener_1), value);
	}

	inline static int32_t get_offset_of_m_Logger_2() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_Logger_2)); }
	inline RuntimeObject* get_m_Logger_2() const { return ___m_Logger_2; }
	inline RuntimeObject** get_address_of_m_Logger_2() { return &___m_Logger_2; }
	inline void set_m_Logger_2(RuntimeObject* value)
	{
		___m_Logger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_2), value);
	}

	inline static int32_t get_offset_of_m_TransactionLog_3() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_TransactionLog_3)); }
	inline TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB * get_m_TransactionLog_3() const { return ___m_TransactionLog_3; }
	inline TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB ** get_address_of_m_TransactionLog_3() { return &___m_TransactionLog_3; }
	inline void set_m_TransactionLog_3(TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB * value)
	{
		___m_TransactionLog_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TransactionLog_3), value);
	}

	inline static int32_t get_offset_of_m_StoreName_4() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_StoreName_4)); }
	inline String_t* get_m_StoreName_4() const { return ___m_StoreName_4; }
	inline String_t** get_address_of_m_StoreName_4() { return &___m_StoreName_4; }
	inline void set_m_StoreName_4(String_t* value)
	{
		___m_StoreName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreName_4), value);
	}

	inline static int32_t get_offset_of_m_AdditionalProductsCallback_5() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_AdditionalProductsCallback_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_AdditionalProductsCallback_5() const { return ___m_AdditionalProductsCallback_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_AdditionalProductsCallback_5() { return &___m_AdditionalProductsCallback_5; }
	inline void set_m_AdditionalProductsCallback_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_AdditionalProductsCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AdditionalProductsCallback_5), value);
	}

	inline static int32_t get_offset_of_m_AdditionalProductsFailCallback_6() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___m_AdditionalProductsFailCallback_6)); }
	inline Action_1_tB45407A14BE89BE8E584A46F19C6CC7477AAD6AE * get_m_AdditionalProductsFailCallback_6() const { return ___m_AdditionalProductsFailCallback_6; }
	inline Action_1_tB45407A14BE89BE8E584A46F19C6CC7477AAD6AE ** get_address_of_m_AdditionalProductsFailCallback_6() { return &___m_AdditionalProductsFailCallback_6; }
	inline void set_m_AdditionalProductsFailCallback_6(Action_1_tB45407A14BE89BE8E584A46F19C6CC7477AAD6AE * value)
	{
		___m_AdditionalProductsFailCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_AdditionalProductsFailCallback_6), value);
	}

	inline static int32_t get_offset_of_U3CuseTransactionLogU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___U3CuseTransactionLogU3Ek__BackingField_7)); }
	inline bool get_U3CuseTransactionLogU3Ek__BackingField_7() const { return ___U3CuseTransactionLogU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CuseTransactionLogU3Ek__BackingField_7() { return &___U3CuseTransactionLogU3Ek__BackingField_7; }
	inline void set_U3CuseTransactionLogU3Ek__BackingField_7(bool value)
	{
		___U3CuseTransactionLogU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CproductsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___U3CproductsU3Ek__BackingField_8)); }
	inline ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 * get_U3CproductsU3Ek__BackingField_8() const { return ___U3CproductsU3Ek__BackingField_8; }
	inline ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 ** get_address_of_U3CproductsU3Ek__BackingField_8() { return &___U3CproductsU3Ek__BackingField_8; }
	inline void set_U3CproductsU3Ek__BackingField_8(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07 * value)
	{
		___U3CproductsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_initialized_9() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4, ___initialized_9)); }
	inline bool get_initialized_9() const { return ___initialized_9; }
	inline bool* get_address_of_initialized_9() { return &___initialized_9; }
	inline void set_initialized_9(bool value)
	{
		___initialized_9 = value;
	}
};

struct PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4_StaticFields
{
public:
	// System.Func`2<UnityEngine.Purchasing.ProductDefinition,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.PurchasingManager::<>f__amU24cache1
	Func_2_t07B8477BB2171FC8FCAB9AAF85EE70B4D9C8CD05 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Func_2_t07B8477BB2171FC8FCAB9AAF85EE70B4D9C8CD05 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Func_2_t07B8477BB2171FC8FCAB9AAF85EE70B4D9C8CD05 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Func_2_t07B8477BB2171FC8FCAB9AAF85EE70B4D9C8CD05 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASINGMANAGER_T1C02FCB1F9869F02786EC0598E70500E423ABDF4_H
#ifndef UNIFIEDRECEIPT_T2AEB0A4791355437DF99A8FF8A2C44D69752C9B7_H
#define UNIFIEDRECEIPT_T2AEB0A4791355437DF99A8FF8A2C44D69752C9B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingManager_UnifiedReceipt
struct  UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.PurchasingManager_UnifiedReceipt::Store
	String_t* ___Store_0;
	// System.String UnityEngine.Purchasing.PurchasingManager_UnifiedReceipt::TransactionID
	String_t* ___TransactionID_1;
	// System.String UnityEngine.Purchasing.PurchasingManager_UnifiedReceipt::Payload
	String_t* ___Payload_2;

public:
	inline static int32_t get_offset_of_Store_0() { return static_cast<int32_t>(offsetof(UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7, ___Store_0)); }
	inline String_t* get_Store_0() const { return ___Store_0; }
	inline String_t** get_address_of_Store_0() { return &___Store_0; }
	inline void set_Store_0(String_t* value)
	{
		___Store_0 = value;
		Il2CppCodeGenWriteBarrier((&___Store_0), value);
	}

	inline static int32_t get_offset_of_TransactionID_1() { return static_cast<int32_t>(offsetof(UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7, ___TransactionID_1)); }
	inline String_t* get_TransactionID_1() const { return ___TransactionID_1; }
	inline String_t** get_address_of_TransactionID_1() { return &___TransactionID_1; }
	inline void set_TransactionID_1(String_t* value)
	{
		___TransactionID_1 = value;
		Il2CppCodeGenWriteBarrier((&___TransactionID_1), value);
	}

	inline static int32_t get_offset_of_Payload_2() { return static_cast<int32_t>(offsetof(UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7, ___Payload_2)); }
	inline String_t* get_Payload_2() const { return ___Payload_2; }
	inline String_t** get_address_of_Payload_2() { return &___Payload_2; }
	inline void set_Payload_2(String_t* value)
	{
		___Payload_2 = value;
		Il2CppCodeGenWriteBarrier((&___Payload_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFIEDRECEIPT_T2AEB0A4791355437DF99A8FF8A2C44D69752C9B7_H
#ifndef SIMPLECATALOGPROVIDER_TF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC_H
#define SIMPLECATALOGPROVIDER_TF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SimpleCatalogProvider
struct  SimpleCatalogProvider_tF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC  : public RuntimeObject
{
public:
	// System.Action`1<System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>> UnityEngine.Purchasing.SimpleCatalogProvider::m_Func
	Action_1_tF91368AFD1372C3CAE111C0453398402BC6DB6EB * ___m_Func_0;

public:
	inline static int32_t get_offset_of_m_Func_0() { return static_cast<int32_t>(offsetof(SimpleCatalogProvider_tF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC, ___m_Func_0)); }
	inline Action_1_tF91368AFD1372C3CAE111C0453398402BC6DB6EB * get_m_Func_0() const { return ___m_Func_0; }
	inline Action_1_tF91368AFD1372C3CAE111C0453398402BC6DB6EB ** get_address_of_m_Func_0() { return &___m_Func_0; }
	inline void set_m_Func_0(Action_1_tF91368AFD1372C3CAE111C0453398402BC6DB6EB * value)
	{
		___m_Func_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Func_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLECATALOGPROVIDER_TF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC_H
#ifndef STORELISTENERPROXY_TAB456AE612584258695F231FD84C9E8B00B9D023_H
#define STORELISTENERPROXY_TAB456AE612584258695F231FD84C9E8B00B9D023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreListenerProxy
struct  StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.AnalyticsReporter UnityEngine.Purchasing.StoreListenerProxy::m_Analytics
	AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3 * ___m_Analytics_0;
	// UnityEngine.Purchasing.IStoreListener UnityEngine.Purchasing.StoreListenerProxy::m_ForwardTo
	RuntimeObject* ___m_ForwardTo_1;
	// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.StoreListenerProxy::m_Extensions
	RuntimeObject* ___m_Extensions_2;

public:
	inline static int32_t get_offset_of_m_Analytics_0() { return static_cast<int32_t>(offsetof(StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023, ___m_Analytics_0)); }
	inline AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3 * get_m_Analytics_0() const { return ___m_Analytics_0; }
	inline AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3 ** get_address_of_m_Analytics_0() { return &___m_Analytics_0; }
	inline void set_m_Analytics_0(AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3 * value)
	{
		___m_Analytics_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Analytics_0), value);
	}

	inline static int32_t get_offset_of_m_ForwardTo_1() { return static_cast<int32_t>(offsetof(StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023, ___m_ForwardTo_1)); }
	inline RuntimeObject* get_m_ForwardTo_1() const { return ___m_ForwardTo_1; }
	inline RuntimeObject** get_address_of_m_ForwardTo_1() { return &___m_ForwardTo_1; }
	inline void set_m_ForwardTo_1(RuntimeObject* value)
	{
		___m_ForwardTo_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ForwardTo_1), value);
	}

	inline static int32_t get_offset_of_m_Extensions_2() { return static_cast<int32_t>(offsetof(StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023, ___m_Extensions_2)); }
	inline RuntimeObject* get_m_Extensions_2() const { return ___m_Extensions_2; }
	inline RuntimeObject** get_address_of_m_Extensions_2() { return &___m_Extensions_2; }
	inline void set_m_Extensions_2(RuntimeObject* value)
	{
		___m_Extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Extensions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORELISTENERPROXY_TAB456AE612584258695F231FD84C9E8B00B9D023_H
#ifndef TIZENSTOREBINDINGS_T0CD7C87597EE0CE43FF2CF4045BEDABE0F95457B_H
#define TIZENSTOREBINDINGS_T0CD7C87597EE0CE43FF2CF4045BEDABE0F95457B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TizenStoreBindings
struct  TizenStoreBindings_t0CD7C87597EE0CE43FF2CF4045BEDABE0F95457B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIZENSTOREBINDINGS_T0CD7C87597EE0CE43FF2CF4045BEDABE0F95457B_H
#ifndef TRANSACTIONLOG_T4AC4B3B8D7B61A3653B476A06F166CBBE46607BB_H
#define TRANSACTIONLOG_T4AC4B3B8D7B61A3653B476A06F166CBBE46607BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TransactionLog
struct  TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB  : public RuntimeObject
{
public:
	// UnityEngine.ILogger UnityEngine.Purchasing.TransactionLog::logger
	RuntimeObject* ___logger_0;
	// System.String UnityEngine.Purchasing.TransactionLog::persistentDataPath
	String_t* ___persistentDataPath_1;

public:
	inline static int32_t get_offset_of_logger_0() { return static_cast<int32_t>(offsetof(TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB, ___logger_0)); }
	inline RuntimeObject* get_logger_0() const { return ___logger_0; }
	inline RuntimeObject** get_address_of_logger_0() { return &___logger_0; }
	inline void set_logger_0(RuntimeObject* value)
	{
		___logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___logger_0), value);
	}

	inline static int32_t get_offset_of_persistentDataPath_1() { return static_cast<int32_t>(offsetof(TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB, ___persistentDataPath_1)); }
	inline String_t* get_persistentDataPath_1() const { return ___persistentDataPath_1; }
	inline String_t** get_address_of_persistentDataPath_1() { return &___persistentDataPath_1; }
	inline void set_persistentDataPath_1(String_t* value)
	{
		___persistentDataPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistentDataPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSACTIONLOG_T4AC4B3B8D7B61A3653B476A06F166CBBE46607BB_H
#ifndef UNITYANALYTICS_T9CA27B9569254A8D1B5CE7660D3FD131E0BBE9DA_H
#define UNITYANALYTICS_T9CA27B9569254A8D1B5CE7660D3FD131E0BBE9DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityAnalytics
struct  UnityAnalytics_t9CA27B9569254A8D1B5CE7660D3FD131E0BBE9DA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYANALYTICS_T9CA27B9569254A8D1B5CE7660D3FD131E0BBE9DA_H
#ifndef UNITYPURCHASING_TEA41D8ED18A15D67A9D0E924062E4CC18EA7444D_H
#define UNITYPURCHASING_TEA41D8ED18A15D67A9D0E924062E4CC18EA7444D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityPurchasing
struct  UnityPurchasing_tEA41D8ED18A15D67A9D0E924062E4CC18EA7444D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPURCHASING_TEA41D8ED18A15D67A9D0E924062E4CC18EA7444D_H
#ifndef U3CFETCHANDMERGEPRODUCTSU3EC__ANONSTOREY1_TE6C371BBDB710A04E8CD8A7897D4FCE667129FE0_H
#define U3CFETCHANDMERGEPRODUCTSU3EC__ANONSTOREY1_TE6C371BBDB710A04E8CD8A7897D4FCE667129FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityPurchasing_<FetchAndMergeProducts>c__AnonStorey1
struct  U3CFetchAndMergeProductsU3Ec__AnonStorey1_tE6C371BBDB710A04E8CD8A7897D4FCE667129FE0  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.UnityPurchasing_<FetchAndMergeProducts>c__AnonStorey1::localProductSet
	HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * ___localProductSet_0;
	// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>> UnityEngine.Purchasing.UnityPurchasing_<FetchAndMergeProducts>c__AnonStorey1::callback
	Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F * ___callback_1;

public:
	inline static int32_t get_offset_of_localProductSet_0() { return static_cast<int32_t>(offsetof(U3CFetchAndMergeProductsU3Ec__AnonStorey1_tE6C371BBDB710A04E8CD8A7897D4FCE667129FE0, ___localProductSet_0)); }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * get_localProductSet_0() const { return ___localProductSet_0; }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 ** get_address_of_localProductSet_0() { return &___localProductSet_0; }
	inline void set_localProductSet_0(HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * value)
	{
		___localProductSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___localProductSet_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CFetchAndMergeProductsU3Ec__AnonStorey1_tE6C371BBDB710A04E8CD8A7897D4FCE667129FE0, ___callback_1)); }
	inline Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F * get_callback_1() const { return ___callback_1; }
	inline Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHANDMERGEPRODUCTSU3EC__ANONSTOREY1_TE6C371BBDB710A04E8CD8A7897D4FCE667129FE0_H
#ifndef U3CINITIALIZEU3EC__ANONSTOREY0_TED255462990AD3D0F15BF4CC709E2B191B6A5AA6_H
#define U3CINITIALIZEU3EC__ANONSTOREY0_TED255462990AD3D0F15BF4CC709E2B191B6A5AA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityPurchasing_<Initialize>c__AnonStorey0
struct  U3CInitializeU3Ec__AnonStorey0_tED255462990AD3D0F15BF4CC709E2B191B6A5AA6  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.PurchasingManager UnityEngine.Purchasing.UnityPurchasing_<Initialize>c__AnonStorey0::manager
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4 * ___manager_0;
	// UnityEngine.Purchasing.StoreListenerProxy UnityEngine.Purchasing.UnityPurchasing_<Initialize>c__AnonStorey0::proxy
	StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023 * ___proxy_1;

public:
	inline static int32_t get_offset_of_manager_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_tED255462990AD3D0F15BF4CC709E2B191B6A5AA6, ___manager_0)); }
	inline PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4 * get_manager_0() const { return ___manager_0; }
	inline PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4 ** get_address_of_manager_0() { return &___manager_0; }
	inline void set_manager_0(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4 * value)
	{
		___manager_0 = value;
		Il2CppCodeGenWriteBarrier((&___manager_0), value);
	}

	inline static int32_t get_offset_of_proxy_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__AnonStorey0_tED255462990AD3D0F15BF4CC709E2B191B6A5AA6, ___proxy_1)); }
	inline StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023 * get_proxy_1() const { return ___proxy_1; }
	inline StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023 ** get_address_of_proxy_1() { return &___proxy_1; }
	inline void set_proxy_1(StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023 * value)
	{
		___proxy_1 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZEU3EC__ANONSTOREY0_TED255462990AD3D0F15BF4CC709E2B191B6A5AA6_H
#ifndef COMMON_TBAF67736DBA7FC1C2F5FF9237C315DC95B1B5D10_H
#define COMMON_TBAF67736DBA7FC1C2F5FF9237C315DC95B1B5D10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Common
struct  Common_tBAF67736DBA7FC1C2F5FF9237C315DC95B1B5D10  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMON_TBAF67736DBA7FC1C2F5FF9237C315DC95B1B5D10_H
#ifndef EVENTDISPATCHER_TE126AF9D6EF1D8E36BE69582E1FA4362150D365B_H
#define EVENTDISPATCHER_TE126AF9D6EF1D8E36BE69582E1FA4362150D365B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.EventDispatcher
struct  EventDispatcher_tE126AF9D6EF1D8E36BE69582E1FA4362150D365B  : public RuntimeObject
{
public:

public:
};

struct EventDispatcher_tE126AF9D6EF1D8E36BE69582E1FA4362150D365B_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.UDP.Analytics.EventDispatcher::serviceClass
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(EventDispatcher_tE126AF9D6EF1D8E36BE69582E1FA4362150D365B_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDISPATCHER_TE126AF9D6EF1D8E36BE69582E1FA4362150D365B_H
#ifndef PLATFORMWRAPPER_T3AA3F6F8F892889ADC912F180F463E5F2151B8C6_H
#define PLATFORMWRAPPER_T3AA3F6F8F892889ADC912F180F463E5F2151B8C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.PlatformWrapper
struct  PlatformWrapper_t3AA3F6F8F892889ADC912F180F463E5F2151B8C6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMWRAPPER_T3AA3F6F8F892889ADC912F180F463E5F2151B8C6_H
#ifndef SESSIONINFO_T823C38626F78DA14C36EC4CD87849B50274DCA60_H
#define SESSIONINFO_T823C38626F78DA14C36EC4CD87849B50274DCA60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.SessionInfo
struct  SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_AppId
	String_t* ___m_AppId_0;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_SessionId
	String_t* ___m_SessionId_1;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_ClientId
	String_t* ___m_ClientId_2;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_DeviceId
	String_t* ___m_DeviceId_3;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_Platform
	String_t* ___m_Platform_4;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_TargetStore
	String_t* ___m_TargetStore_5;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_SystemInfo
	String_t* ___m_SystemInfo_6;
	// System.Boolean UnityEngine.UDP.Analytics.SessionInfo::m_Vr
	bool ___m_Vr_7;

public:
	inline static int32_t get_offset_of_m_AppId_0() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_AppId_0)); }
	inline String_t* get_m_AppId_0() const { return ___m_AppId_0; }
	inline String_t** get_address_of_m_AppId_0() { return &___m_AppId_0; }
	inline void set_m_AppId_0(String_t* value)
	{
		___m_AppId_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppId_0), value);
	}

	inline static int32_t get_offset_of_m_SessionId_1() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_SessionId_1)); }
	inline String_t* get_m_SessionId_1() const { return ___m_SessionId_1; }
	inline String_t** get_address_of_m_SessionId_1() { return &___m_SessionId_1; }
	inline void set_m_SessionId_1(String_t* value)
	{
		___m_SessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionId_1), value);
	}

	inline static int32_t get_offset_of_m_ClientId_2() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_ClientId_2)); }
	inline String_t* get_m_ClientId_2() const { return ___m_ClientId_2; }
	inline String_t** get_address_of_m_ClientId_2() { return &___m_ClientId_2; }
	inline void set_m_ClientId_2(String_t* value)
	{
		___m_ClientId_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientId_2), value);
	}

	inline static int32_t get_offset_of_m_DeviceId_3() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_DeviceId_3)); }
	inline String_t* get_m_DeviceId_3() const { return ___m_DeviceId_3; }
	inline String_t** get_address_of_m_DeviceId_3() { return &___m_DeviceId_3; }
	inline void set_m_DeviceId_3(String_t* value)
	{
		___m_DeviceId_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DeviceId_3), value);
	}

	inline static int32_t get_offset_of_m_Platform_4() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_Platform_4)); }
	inline String_t* get_m_Platform_4() const { return ___m_Platform_4; }
	inline String_t** get_address_of_m_Platform_4() { return &___m_Platform_4; }
	inline void set_m_Platform_4(String_t* value)
	{
		___m_Platform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Platform_4), value);
	}

	inline static int32_t get_offset_of_m_TargetStore_5() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_TargetStore_5)); }
	inline String_t* get_m_TargetStore_5() const { return ___m_TargetStore_5; }
	inline String_t** get_address_of_m_TargetStore_5() { return &___m_TargetStore_5; }
	inline void set_m_TargetStore_5(String_t* value)
	{
		___m_TargetStore_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetStore_5), value);
	}

	inline static int32_t get_offset_of_m_SystemInfo_6() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_SystemInfo_6)); }
	inline String_t* get_m_SystemInfo_6() const { return ___m_SystemInfo_6; }
	inline String_t** get_address_of_m_SystemInfo_6() { return &___m_SystemInfo_6; }
	inline void set_m_SystemInfo_6(String_t* value)
	{
		___m_SystemInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInfo_6), value);
	}

	inline static int32_t get_offset_of_m_Vr_7() { return static_cast<int32_t>(offsetof(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60, ___m_Vr_7)); }
	inline bool get_m_Vr_7() const { return ___m_Vr_7; }
	inline bool* get_address_of_m_Vr_7() { return &___m_Vr_7; }
	inline void set_m_Vr_7(bool value)
	{
		___m_Vr_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONINFO_T823C38626F78DA14C36EC4CD87849B50274DCA60_H
#ifndef UDPANALYTICS_TD781C87CE44CE97480AAD0E99BCA8493FF7E53FC_H
#define UDPANALYTICS_TD781C87CE44CE97480AAD0E99BCA8493FF7E53FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.UdpAnalytics
struct  UdpAnalytics_tD781C87CE44CE97480AAD0E99BCA8493FF7E53FC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPANALYTICS_TD781C87CE44CE97480AAD0E99BCA8493FF7E53FC_H
#ifndef APPINFO_TCA4BF037393743799729827487AFC4C1502E37B8_H
#define APPINFO_TCA4BF037393743799729827487AFC4C1502E37B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.AppInfo
struct  AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.AppInfo::<ClientId>k__BackingField
	String_t* ___U3CClientIdU3Ek__BackingField_0;
	// System.String UnityEngine.UDP.AppInfo::<AppSlug>k__BackingField
	String_t* ___U3CAppSlugU3Ek__BackingField_1;
	// System.String UnityEngine.UDP.AppInfo::<ClientKey>k__BackingField
	String_t* ___U3CClientKeyU3Ek__BackingField_2;
	// System.String UnityEngine.UDP.AppInfo::<RSAPublicKey>k__BackingField
	String_t* ___U3CRSAPublicKeyU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CClientIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8, ___U3CClientIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CClientIdU3Ek__BackingField_0() const { return ___U3CClientIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CClientIdU3Ek__BackingField_0() { return &___U3CClientIdU3Ek__BackingField_0; }
	inline void set_U3CClientIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CClientIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAppSlugU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8, ___U3CAppSlugU3Ek__BackingField_1)); }
	inline String_t* get_U3CAppSlugU3Ek__BackingField_1() const { return ___U3CAppSlugU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAppSlugU3Ek__BackingField_1() { return &___U3CAppSlugU3Ek__BackingField_1; }
	inline void set_U3CAppSlugU3Ek__BackingField_1(String_t* value)
	{
		___U3CAppSlugU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppSlugU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CClientKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8, ___U3CClientKeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CClientKeyU3Ek__BackingField_2() const { return ___U3CClientKeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CClientKeyU3Ek__BackingField_2() { return &___U3CClientKeyU3Ek__BackingField_2; }
	inline void set_U3CClientKeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CClientKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientKeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRSAPublicKeyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8, ___U3CRSAPublicKeyU3Ek__BackingField_3)); }
	inline String_t* get_U3CRSAPublicKeyU3Ek__BackingField_3() const { return ___U3CRSAPublicKeyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CRSAPublicKeyU3Ek__BackingField_3() { return &___U3CRSAPublicKeyU3Ek__BackingField_3; }
	inline void set_U3CRSAPublicKeyU3Ek__BackingField_3(String_t* value)
	{
		___U3CRSAPublicKeyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRSAPublicKeyU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINFO_TCA4BF037393743799729827487AFC4C1502E37B8_H
#ifndef INVENTORY_T2179CC5D4C0584A26917CE8F5248C3A60B4C42A3_H
#define INVENTORY_T2179CC5D4C0584A26917CE8F5248C3A60B4C42A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Inventory
struct  Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo> UnityEngine.UDP.Inventory::_purchaseDictionary
	Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 * ____purchaseDictionary_0;

public:
	inline static int32_t get_offset_of__purchaseDictionary_0() { return static_cast<int32_t>(offsetof(Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3, ____purchaseDictionary_0)); }
	inline Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 * get__purchaseDictionary_0() const { return ____purchaseDictionary_0; }
	inline Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 ** get_address_of__purchaseDictionary_0() { return &____purchaseDictionary_0; }
	inline void set__purchaseDictionary_0(Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 * value)
	{
		____purchaseDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&____purchaseDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORY_T2179CC5D4C0584A26917CE8F5248C3A60B4C42A3_H
#ifndef U3CWAITANDDOU3ED__6_T086CF389D5516CE744E35BCA880152764DB7EE18_H
#define U3CWAITANDDOU3ED__6_T086CF389D5516CE744E35BCA880152764DB7EE18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.MainThreadDispatcher_<WaitAndDo>d__6
struct  U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UDP.MainThreadDispatcher_<WaitAndDo>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.WaitForSeconds UnityEngine.UDP.MainThreadDispatcher_<WaitAndDo>d__6::<>2__current
	WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * ___U3CU3E2__current_1;
	// System.Single UnityEngine.UDP.MainThreadDispatcher_<WaitAndDo>d__6::waitTime
	float ___waitTime_2;
	// System.Action UnityEngine.UDP.MainThreadDispatcher_<WaitAndDo>d__6::runnable
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___runnable_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18, ___U3CU3E2__current_1)); }
	inline WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_runnable_3() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18, ___runnable_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_runnable_3() const { return ___runnable_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_runnable_3() { return &___runnable_3; }
	inline void set_runnable_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___runnable_3 = value;
		Il2CppCodeGenWriteBarrier((&___runnable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITANDDOU3ED__6_T086CF389D5516CE744E35BCA880152764DB7EE18_H
#ifndef PURCHASEINFO_T878118AC0EE1BC529145064FE010557D74EDCB27_H
#define PURCHASEINFO_T878118AC0EE1BC529145064FE010557D74EDCB27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.PurchaseInfo
struct  PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.PurchaseInfo::<ItemType>k__BackingField
	String_t* ___U3CItemTypeU3Ek__BackingField_0;
	// System.String UnityEngine.UDP.PurchaseInfo::<ProductId>k__BackingField
	String_t* ___U3CProductIdU3Ek__BackingField_1;
	// System.String UnityEngine.UDP.PurchaseInfo::<GameOrderId>k__BackingField
	String_t* ___U3CGameOrderIdU3Ek__BackingField_2;
	// System.String UnityEngine.UDP.PurchaseInfo::<OrderQueryToken>k__BackingField
	String_t* ___U3COrderQueryTokenU3Ek__BackingField_3;
	// System.String UnityEngine.UDP.PurchaseInfo::<DeveloperPayload>k__BackingField
	String_t* ___U3CDeveloperPayloadU3Ek__BackingField_4;
	// System.String UnityEngine.UDP.PurchaseInfo::<StorePurchaseJsonString>k__BackingField
	String_t* ___U3CStorePurchaseJsonStringU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CItemTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27, ___U3CItemTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemTypeU3Ek__BackingField_0() const { return ___U3CItemTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemTypeU3Ek__BackingField_0() { return &___U3CItemTypeU3Ek__BackingField_0; }
	inline void set_U3CItemTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProductIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27, ___U3CProductIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CProductIdU3Ek__BackingField_1() const { return ___U3CProductIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CProductIdU3Ek__BackingField_1() { return &___U3CProductIdU3Ek__BackingField_1; }
	inline void set_U3CProductIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CProductIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProductIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CGameOrderIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27, ___U3CGameOrderIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CGameOrderIdU3Ek__BackingField_2() const { return ___U3CGameOrderIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CGameOrderIdU3Ek__BackingField_2() { return &___U3CGameOrderIdU3Ek__BackingField_2; }
	inline void set_U3CGameOrderIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CGameOrderIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameOrderIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COrderQueryTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27, ___U3COrderQueryTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3COrderQueryTokenU3Ek__BackingField_3() const { return ___U3COrderQueryTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3COrderQueryTokenU3Ek__BackingField_3() { return &___U3COrderQueryTokenU3Ek__BackingField_3; }
	inline void set_U3COrderQueryTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3COrderQueryTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COrderQueryTokenU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDeveloperPayloadU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27, ___U3CDeveloperPayloadU3Ek__BackingField_4)); }
	inline String_t* get_U3CDeveloperPayloadU3Ek__BackingField_4() const { return ___U3CDeveloperPayloadU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDeveloperPayloadU3Ek__BackingField_4() { return &___U3CDeveloperPayloadU3Ek__BackingField_4; }
	inline void set_U3CDeveloperPayloadU3Ek__BackingField_4(String_t* value)
	{
		___U3CDeveloperPayloadU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeveloperPayloadU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStorePurchaseJsonStringU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27, ___U3CStorePurchaseJsonStringU3Ek__BackingField_5)); }
	inline String_t* get_U3CStorePurchaseJsonStringU3Ek__BackingField_5() const { return ___U3CStorePurchaseJsonStringU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CStorePurchaseJsonStringU3Ek__BackingField_5() { return &___U3CStorePurchaseJsonStringU3Ek__BackingField_5; }
	inline void set_U3CStorePurchaseJsonStringU3Ek__BackingField_5(String_t* value)
	{
		___U3CStorePurchaseJsonStringU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStorePurchaseJsonStringU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEINFO_T878118AC0EE1BC529145064FE010557D74EDCB27_H
#ifndef STORESERVICE_TE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_H
#define STORESERVICE_TE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.StoreService
struct  StoreService_tE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D  : public RuntimeObject
{
public:

public:
};

struct StoreService_tE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.UDP.StoreService::serviceClass
	AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(StoreService_tE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_tFC9C1064BF4849691FEDC988743C0C271C62FDC8 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORESERVICE_TE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_H
#ifndef USERINFO_TEA6CDD375F18424AD96E23D04B0242E1D84D22DD_H
#define USERINFO_TEA6CDD375F18424AD96E23D04B0242E1D84D22DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.UserInfo
struct  UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.UserInfo::<Channel>k__BackingField
	String_t* ___U3CChannelU3Ek__BackingField_0;
	// System.String UnityEngine.UDP.UserInfo::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_1;
	// System.String UnityEngine.UDP.UserInfo::<UserLoginToken>k__BackingField
	String_t* ___U3CUserLoginTokenU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CChannelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD, ___U3CChannelU3Ek__BackingField_0)); }
	inline String_t* get_U3CChannelU3Ek__BackingField_0() const { return ___U3CChannelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CChannelU3Ek__BackingField_0() { return &___U3CChannelU3Ek__BackingField_0; }
	inline void set_U3CChannelU3Ek__BackingField_0(String_t* value)
	{
		___U3CChannelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CChannelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD, ___U3CUserIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_1() const { return ___U3CUserIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_1() { return &___U3CUserIdU3Ek__BackingField_1; }
	inline void set_U3CUserIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CUserLoginTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD, ___U3CUserLoginTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CUserLoginTokenU3Ek__BackingField_2() const { return ___U3CUserLoginTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUserLoginTokenU3Ek__BackingField_2() { return &___U3CUserLoginTokenU3Ek__BackingField_2; }
	inline void set_U3CUserLoginTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CUserLoginTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserLoginTokenU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERINFO_TEA6CDD375F18424AD96E23D04B0242E1D84D22DD_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_TD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9_H
#define __STATICARRAYINITTYPESIZEU3D32_TD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32
struct  __StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_TD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9_H
#ifndef RELATIVEOID_T83A5F80DE61FABABEBA9D2AB31B9116B678A35C5_H
#define RELATIVEOID_T83A5F80DE61FABABEBA9D2AB31B9116B678A35C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LipingShare.LCLib.Asn1Processor.RelativeOid
struct  RelativeOid_t83A5F80DE61FABABEBA9D2AB31B9116B678A35C5  : public Oid_t9D72792E7144ECEB69284AE6004135BE53596204
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIVEOID_T83A5F80DE61FABABEBA9D2AB31B9116B678A35C5_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef APPINSTALLEVENT_TAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133_H
#define APPINSTALLEVENT_TAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppInstallEvent
struct  AppInstallEvent_tAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppInstallEvent::_params
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppInstallEvent_tAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133, ____params_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINSTALLEVENT_TAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133_H
#ifndef APPRUNNINGEVENT_T6D647BEEDDB531FCB39D319EF14381FA0D9944CA_H
#define APPRUNNINGEVENT_T6D647BEEDDB531FCB39D319EF14381FA0D9944CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppRunningEvent
struct  AppRunningEvent_t6D647BEEDDB531FCB39D319EF14381FA0D9944CA  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppRunningEvent::_params
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppRunningEvent_t6D647BEEDDB531FCB39D319EF14381FA0D9944CA, ____params_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPRUNNINGEVENT_T6D647BEEDDB531FCB39D319EF14381FA0D9944CA_H
#ifndef APPSTARTEVENT_TDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47_H
#define APPSTARTEVENT_TDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppStartEvent
struct  AppStartEvent_tDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppStartEvent::_params
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppStartEvent_tDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47, ____params_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTARTEVENT_TDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47_H
#ifndef APPSTOPEVENT_T6ECD1B16F2EC9DA2BF894E7010FB11F056C08734_H
#define APPSTOPEVENT_T6ECD1B16F2EC9DA2BF894E7010FB11F056C08734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppStopEvent
struct  AppStopEvent_t6ECD1B16F2EC9DA2BF894E7010FB11F056C08734  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppStopEvent::_params
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppStopEvent_t6ECD1B16F2EC9DA2BF894E7010FB11F056C08734, ____params_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTOPEVENT_T6ECD1B16F2EC9DA2BF894E7010FB11F056C08734_H
#ifndef PURCHASEATTEMPTEVENT_T3164C3AEF1C541A228CD5925C106B5F31A9B096B_H
#define PURCHASEATTEMPTEVENT_T3164C3AEF1C541A228CD5925C106B5F31A9B096B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent
struct  PurchaseAttemptEvent_t3164C3AEF1C541A228CD5925C106B5F31A9B096B  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent::_params
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(PurchaseAttemptEvent_t3164C3AEF1C541A228CD5925C106B5F31A9B096B, ____params_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEATTEMPTEVENT_T3164C3AEF1C541A228CD5925C106B5F31A9B096B_H
#ifndef INITLOGINFORWARDCALLBACK_T3EC3816608AC4849FFF867551A844CD36DFAA219_H
#define INITLOGINFORWARDCALLBACK_T3EC3816608AC4849FFF867551A844CD36DFAA219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.InitLoginForwardCallback
struct  InitLoginForwardCallback_t3EC3816608AC4849FFF867551A844CD36DFAA219  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// UnityEngine.UDP.IInitListener UnityEngine.UDP.InitLoginForwardCallback::_initListener
	RuntimeObject* ____initListener_0;

public:
	inline static int32_t get_offset_of__initListener_0() { return static_cast<int32_t>(offsetof(InitLoginForwardCallback_t3EC3816608AC4849FFF867551A844CD36DFAA219, ____initListener_0)); }
	inline RuntimeObject* get__initListener_0() const { return ____initListener_0; }
	inline RuntimeObject** get_address_of__initListener_0() { return &____initListener_0; }
	inline void set__initListener_0(RuntimeObject* value)
	{
		____initListener_0 = value;
		Il2CppCodeGenWriteBarrier((&____initListener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITLOGINFORWARDCALLBACK_T3EC3816608AC4849FFF867551A844CD36DFAA219_H
#ifndef PURCHASEFORWARDCALLBACK_T0973275517F96FC0F0C28B1181FF0B7E73C32FD9_H
#define PURCHASEFORWARDCALLBACK_T0973275517F96FC0F0C28B1181FF0B7E73C32FD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.PurchaseForwardCallback
struct  PurchaseForwardCallback_t0973275517F96FC0F0C28B1181FF0B7E73C32FD9  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// UnityEngine.UDP.IPurchaseListener UnityEngine.UDP.PurchaseForwardCallback::purchaseListener
	RuntimeObject* ___purchaseListener_0;

public:
	inline static int32_t get_offset_of_purchaseListener_0() { return static_cast<int32_t>(offsetof(PurchaseForwardCallback_t0973275517F96FC0F0C28B1181FF0B7E73C32FD9, ___purchaseListener_0)); }
	inline RuntimeObject* get_purchaseListener_0() const { return ___purchaseListener_0; }
	inline RuntimeObject** get_address_of_purchaseListener_0() { return &___purchaseListener_0; }
	inline void set_purchaseListener_0(RuntimeObject* value)
	{
		___purchaseListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseListener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEFORWARDCALLBACK_T0973275517F96FC0F0C28B1181FF0B7E73C32FD9_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TEAE252E46D83DBFA30B524295EAF9F8217169FCE_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TEAE252E46D83DBFA30B524295EAF9F8217169FCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tEAE252E46D83DBFA30B524295EAF9F8217169FCE  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tEAE252E46D83DBFA30B524295EAF9F8217169FCE_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::59F5BD34B6C013DEACC784F69C67E95150033A84
	__StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9  ___59F5BD34B6C013DEACC784F69C67E95150033A84_0;

public:
	inline static int32_t get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tEAE252E46D83DBFA30B524295EAF9F8217169FCE_StaticFields, ___59F5BD34B6C013DEACC784F69C67E95150033A84_0)); }
	inline __StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9  get_U359F5BD34B6C013DEACC784F69C67E95150033A84_0() const { return ___59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline __StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9 * get_address_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0() { return &___59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline void set_U359F5BD34B6C013DEACC784F69C67E95150033A84_0(__StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9  value)
	{
		___59F5BD34B6C013DEACC784F69C67E95150033A84_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TEAE252E46D83DBFA30B524295EAF9F8217169FCE_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#define EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef INITIALIZATIONFAILUREREASON_T5A6284D67FA09D301793E67D8B7003299F4D3062_H
#define INITIALIZATIONFAILUREREASON_T5A6284D67FA09D301793E67D8B7003299F4D3062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.InitializationFailureReason
struct  InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062 
{
public:
	// System.Int32 UnityEngine.Purchasing.InitializationFailureReason::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZATIONFAILUREREASON_T5A6284D67FA09D301793E67D8B7003299F4D3062_H
#ifndef PAYOUTTYPE_T8339B08FE90A51DFE9311B97B42AD56DE7C8636B_H
#define PAYOUTTYPE_T8339B08FE90A51DFE9311B97B42AD56DE7C8636B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PayoutType
struct  PayoutType_t8339B08FE90A51DFE9311B97B42AD56DE7C8636B 
{
public:
	// System.Int32 UnityEngine.Purchasing.PayoutType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PayoutType_t8339B08FE90A51DFE9311B97B42AD56DE7C8636B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYOUTTYPE_T8339B08FE90A51DFE9311B97B42AD56DE7C8636B_H
#ifndef PRODUCTMETADATA_T1CAA912963F8B24B34F53CCF29A6E0BD89B33B48_H
#define PRODUCTMETADATA_T1CAA912963F8B24B34F53CCF29A6E0BD89B33B48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductMetadata
struct  ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductMetadata::<localizedPriceString>k__BackingField
	String_t* ___U3ClocalizedPriceStringU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.ProductMetadata::<localizedTitle>k__BackingField
	String_t* ___U3ClocalizedTitleU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.ProductMetadata::<localizedDescription>k__BackingField
	String_t* ___U3ClocalizedDescriptionU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.ProductMetadata::<isoCurrencyCode>k__BackingField
	String_t* ___U3CisoCurrencyCodeU3Ek__BackingField_3;
	// System.Decimal UnityEngine.Purchasing.ProductMetadata::<localizedPrice>k__BackingField
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___U3ClocalizedPriceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48, ___U3ClocalizedPriceStringU3Ek__BackingField_0)); }
	inline String_t* get_U3ClocalizedPriceStringU3Ek__BackingField_0() const { return ___U3ClocalizedPriceStringU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClocalizedPriceStringU3Ek__BackingField_0() { return &___U3ClocalizedPriceStringU3Ek__BackingField_0; }
	inline void set_U3ClocalizedPriceStringU3Ek__BackingField_0(String_t* value)
	{
		___U3ClocalizedPriceStringU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalizedPriceStringU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClocalizedTitleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48, ___U3ClocalizedTitleU3Ek__BackingField_1)); }
	inline String_t* get_U3ClocalizedTitleU3Ek__BackingField_1() const { return ___U3ClocalizedTitleU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClocalizedTitleU3Ek__BackingField_1() { return &___U3ClocalizedTitleU3Ek__BackingField_1; }
	inline void set_U3ClocalizedTitleU3Ek__BackingField_1(String_t* value)
	{
		___U3ClocalizedTitleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalizedTitleU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48, ___U3ClocalizedDescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3ClocalizedDescriptionU3Ek__BackingField_2() const { return ___U3ClocalizedDescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClocalizedDescriptionU3Ek__BackingField_2() { return &___U3ClocalizedDescriptionU3Ek__BackingField_2; }
	inline void set_U3ClocalizedDescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3ClocalizedDescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalizedDescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CisoCurrencyCodeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48, ___U3CisoCurrencyCodeU3Ek__BackingField_3)); }
	inline String_t* get_U3CisoCurrencyCodeU3Ek__BackingField_3() const { return ___U3CisoCurrencyCodeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CisoCurrencyCodeU3Ek__BackingField_3() { return &___U3CisoCurrencyCodeU3Ek__BackingField_3; }
	inline void set_U3CisoCurrencyCodeU3Ek__BackingField_3(String_t* value)
	{
		___U3CisoCurrencyCodeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CisoCurrencyCodeU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3ClocalizedPriceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48, ___U3ClocalizedPriceU3Ek__BackingField_4)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_U3ClocalizedPriceU3Ek__BackingField_4() const { return ___U3ClocalizedPriceU3Ek__BackingField_4; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_U3ClocalizedPriceU3Ek__BackingField_4() { return &___U3ClocalizedPriceU3Ek__BackingField_4; }
	inline void set_U3ClocalizedPriceU3Ek__BackingField_4(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___U3ClocalizedPriceU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTMETADATA_T1CAA912963F8B24B34F53CCF29A6E0BD89B33B48_H
#ifndef PRODUCTTYPE_TC52C3BA25156195ACF6AE97650D056434BD51075_H
#define PRODUCTTYPE_TC52C3BA25156195ACF6AE97650D056434BD51075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductType
struct  ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTTYPE_TC52C3BA25156195ACF6AE97650D056434BD51075_H
#ifndef PURCHASEFAILUREREASON_T42EC65C1103B27F82198DC42C8D96C1A14ED7432_H
#define PURCHASEFAILUREREASON_T42EC65C1103B27F82198DC42C8D96C1A14ED7432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchaseFailureReason
struct  PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432 
{
public:
	// System.Int32 UnityEngine.Purchasing.PurchaseFailureReason::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEFAILUREREASON_T42EC65C1103B27F82198DC42C8D96C1A14ED7432_H
#ifndef PURCHASEPROCESSINGRESULT_TD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3_H
#define PURCHASEPROCESSINGRESULT_TD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchaseProcessingResult
struct  PurchaseProcessingResult_tD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3 
{
public:
	// System.Int32 UnityEngine.Purchasing.PurchaseProcessingResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PurchaseProcessingResult_tD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEPROCESSINGRESULT_TD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3_H
#ifndef APPLEINAPPPURCHASERECEIPT_T7E4D7F751E1ACA7B8988C2773E0877C39711C1FC_H
#define APPLEINAPPPURCHASERECEIPT_T7E4D7F751E1ACA7B8988C2773E0877C39711C1FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct  AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<quantity>k__BackingField
	int32_t ___U3CquantityU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<productID>k__BackingField
	String_t* ___U3CproductIDU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<originalTransactionIdentifier>k__BackingField
	String_t* ___U3CoriginalTransactionIdentifierU3Ek__BackingField_3;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<purchaseDate>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CpurchaseDateU3Ek__BackingField_4;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<originalPurchaseDate>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CoriginalPurchaseDateU3Ek__BackingField_5;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<subscriptionExpirationDate>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CsubscriptionExpirationDateU3Ek__BackingField_6;
	// System.DateTime UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<cancellationDate>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CcancellationDateU3Ek__BackingField_7;
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<isFreeTrial>k__BackingField
	int32_t ___U3CisFreeTrialU3Ek__BackingField_8;
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<productType>k__BackingField
	int32_t ___U3CproductTypeU3Ek__BackingField_9;
	// System.Int32 UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt::<isIntroductoryPricePeriod>k__BackingField
	int32_t ___U3CisIntroductoryPricePeriodU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CquantityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CquantityU3Ek__BackingField_0)); }
	inline int32_t get_U3CquantityU3Ek__BackingField_0() const { return ___U3CquantityU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CquantityU3Ek__BackingField_0() { return &___U3CquantityU3Ek__BackingField_0; }
	inline void set_U3CquantityU3Ek__BackingField_0(int32_t value)
	{
		___U3CquantityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CproductIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CproductIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CproductIDU3Ek__BackingField_1() const { return ___U3CproductIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CproductIDU3Ek__BackingField_1() { return &___U3CproductIDU3Ek__BackingField_1; }
	inline void set_U3CproductIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CproductIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIDU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CtransactionIDU3Ek__BackingField_2)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_2() const { return ___U3CtransactionIDU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_2() { return &___U3CtransactionIDU3Ek__BackingField_2; }
	inline void set_U3CtransactionIDU3Ek__BackingField_2(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CoriginalTransactionIdentifierU3Ek__BackingField_3)); }
	inline String_t* get_U3CoriginalTransactionIdentifierU3Ek__BackingField_3() const { return ___U3CoriginalTransactionIdentifierU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3() { return &___U3CoriginalTransactionIdentifierU3Ek__BackingField_3; }
	inline void set_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(String_t* value)
	{
		___U3CoriginalTransactionIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalTransactionIdentifierU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CpurchaseDateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CpurchaseDateU3Ek__BackingField_4)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CpurchaseDateU3Ek__BackingField_4() const { return ___U3CpurchaseDateU3Ek__BackingField_4; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CpurchaseDateU3Ek__BackingField_4() { return &___U3CpurchaseDateU3Ek__BackingField_4; }
	inline void set_U3CpurchaseDateU3Ek__BackingField_4(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CpurchaseDateU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CoriginalPurchaseDateU3Ek__BackingField_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CoriginalPurchaseDateU3Ek__BackingField_5() const { return ___U3CoriginalPurchaseDateU3Ek__BackingField_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CoriginalPurchaseDateU3Ek__BackingField_5() { return &___U3CoriginalPurchaseDateU3Ek__BackingField_5; }
	inline void set_U3CoriginalPurchaseDateU3Ek__BackingField_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CoriginalPurchaseDateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CsubscriptionExpirationDateU3Ek__BackingField_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CsubscriptionExpirationDateU3Ek__BackingField_6() const { return ___U3CsubscriptionExpirationDateU3Ek__BackingField_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6() { return &___U3CsubscriptionExpirationDateU3Ek__BackingField_6; }
	inline void set_U3CsubscriptionExpirationDateU3Ek__BackingField_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CsubscriptionExpirationDateU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CcancellationDateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CcancellationDateU3Ek__BackingField_7)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CcancellationDateU3Ek__BackingField_7() const { return ___U3CcancellationDateU3Ek__BackingField_7; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CcancellationDateU3Ek__BackingField_7() { return &___U3CcancellationDateU3Ek__BackingField_7; }
	inline void set_U3CcancellationDateU3Ek__BackingField_7(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CcancellationDateU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CisFreeTrialU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CisFreeTrialU3Ek__BackingField_8)); }
	inline int32_t get_U3CisFreeTrialU3Ek__BackingField_8() const { return ___U3CisFreeTrialU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CisFreeTrialU3Ek__BackingField_8() { return &___U3CisFreeTrialU3Ek__BackingField_8; }
	inline void set_U3CisFreeTrialU3Ek__BackingField_8(int32_t value)
	{
		___U3CisFreeTrialU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CproductTypeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CproductTypeU3Ek__BackingField_9)); }
	inline int32_t get_U3CproductTypeU3Ek__BackingField_9() const { return ___U3CproductTypeU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CproductTypeU3Ek__BackingField_9() { return &___U3CproductTypeU3Ek__BackingField_9; }
	inline void set_U3CproductTypeU3Ek__BackingField_9(int32_t value)
	{
		___U3CproductTypeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CisIntroductoryPricePeriodU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC, ___U3CisIntroductoryPricePeriodU3Ek__BackingField_10)); }
	inline int32_t get_U3CisIntroductoryPricePeriodU3Ek__BackingField_10() const { return ___U3CisIntroductoryPricePeriodU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CisIntroductoryPricePeriodU3Ek__BackingField_10() { return &___U3CisIntroductoryPricePeriodU3Ek__BackingField_10; }
	inline void set_U3CisIntroductoryPricePeriodU3Ek__BackingField_10(int32_t value)
	{
		___U3CisIntroductoryPricePeriodU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLEINAPPPURCHASERECEIPT_T7E4D7F751E1ACA7B8988C2773E0877C39711C1FC_H
#ifndef APPLERECEIPT_T3D153BEC3207414B3C169700C93A7A6213DF693A_H
#define APPLERECEIPT_T3D153BEC3207414B3C169700C93A7A6213DF693A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.AppleReceipt
struct  AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Security.AppleReceipt::<bundleID>k__BackingField
	String_t* ___U3CbundleIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Security.AppleReceipt::<appVersion>k__BackingField
	String_t* ___U3CappVersionU3Ek__BackingField_1;
	// System.Byte[] UnityEngine.Purchasing.Security.AppleReceipt::<opaque>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CopaqueU3Ek__BackingField_2;
	// System.Byte[] UnityEngine.Purchasing.Security.AppleReceipt::<hash>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3ChashU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Security.AppleReceipt::<originalApplicationVersion>k__BackingField
	String_t* ___U3CoriginalApplicationVersionU3Ek__BackingField_4;
	// System.DateTime UnityEngine.Purchasing.Security.AppleReceipt::<receiptCreationDate>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CreceiptCreationDateU3Ek__BackingField_5;
	// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt[] UnityEngine.Purchasing.Security.AppleReceipt::inAppPurchaseReceipts
	AppleInAppPurchaseReceiptU5BU5D_tC645ADBB6722E0C4288F0DC73C2EDD92D4AADBA0* ___inAppPurchaseReceipts_6;

public:
	inline static int32_t get_offset_of_U3CbundleIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___U3CbundleIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CbundleIDU3Ek__BackingField_0() const { return ___U3CbundleIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CbundleIDU3Ek__BackingField_0() { return &___U3CbundleIDU3Ek__BackingField_0; }
	inline void set_U3CbundleIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CbundleIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbundleIDU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CappVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___U3CappVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CappVersionU3Ek__BackingField_1() const { return ___U3CappVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CappVersionU3Ek__BackingField_1() { return &___U3CappVersionU3Ek__BackingField_1; }
	inline void set_U3CappVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CappVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappVersionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CopaqueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___U3CopaqueU3Ek__BackingField_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CopaqueU3Ek__BackingField_2() const { return ___U3CopaqueU3Ek__BackingField_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CopaqueU3Ek__BackingField_2() { return &___U3CopaqueU3Ek__BackingField_2; }
	inline void set_U3CopaqueU3Ek__BackingField_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CopaqueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CopaqueU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3ChashU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___U3ChashU3Ek__BackingField_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3ChashU3Ek__BackingField_3() const { return ___U3ChashU3Ek__BackingField_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3ChashU3Ek__BackingField_3() { return &___U3ChashU3Ek__BackingField_3; }
	inline void set_U3ChashU3Ek__BackingField_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3ChashU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChashU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___U3CoriginalApplicationVersionU3Ek__BackingField_4)); }
	inline String_t* get_U3CoriginalApplicationVersionU3Ek__BackingField_4() const { return ___U3CoriginalApplicationVersionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CoriginalApplicationVersionU3Ek__BackingField_4() { return &___U3CoriginalApplicationVersionU3Ek__BackingField_4; }
	inline void set_U3CoriginalApplicationVersionU3Ek__BackingField_4(String_t* value)
	{
		___U3CoriginalApplicationVersionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoriginalApplicationVersionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___U3CreceiptCreationDateU3Ek__BackingField_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CreceiptCreationDateU3Ek__BackingField_5() const { return ___U3CreceiptCreationDateU3Ek__BackingField_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CreceiptCreationDateU3Ek__BackingField_5() { return &___U3CreceiptCreationDateU3Ek__BackingField_5; }
	inline void set_U3CreceiptCreationDateU3Ek__BackingField_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CreceiptCreationDateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_inAppPurchaseReceipts_6() { return static_cast<int32_t>(offsetof(AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A, ___inAppPurchaseReceipts_6)); }
	inline AppleInAppPurchaseReceiptU5BU5D_tC645ADBB6722E0C4288F0DC73C2EDD92D4AADBA0* get_inAppPurchaseReceipts_6() const { return ___inAppPurchaseReceipts_6; }
	inline AppleInAppPurchaseReceiptU5BU5D_tC645ADBB6722E0C4288F0DC73C2EDD92D4AADBA0** get_address_of_inAppPurchaseReceipts_6() { return &___inAppPurchaseReceipts_6; }
	inline void set_inAppPurchaseReceipts_6(AppleInAppPurchaseReceiptU5BU5D_tC645ADBB6722E0C4288F0DC73C2EDD92D4AADBA0* value)
	{
		___inAppPurchaseReceipts_6 = value;
		Il2CppCodeGenWriteBarrier((&___inAppPurchaseReceipts_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLERECEIPT_T3D153BEC3207414B3C169700C93A7A6213DF693A_H
#ifndef STATE_T537433C14200A0AA6270A7E3C329ECD923E514E0_H
#define STATE_T537433C14200A0AA6270A7E3C329ECD923E514E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsClient_State
struct  State_t537433C14200A0AA6270A7E3C329ECD923E514E0 
{
public:
	// System.Int32 UnityEngine.UDP.Analytics.AnalyticsClient_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t537433C14200A0AA6270A7E3C329ECD923E514E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T537433C14200A0AA6270A7E3C329ECD923E514E0_H
#ifndef ANALYTICSRESULT_T7C9373CC5B355392C3A7068EAAE03F4EED385B04_H
#define ANALYTICSRESULT_T7C9373CC5B355392C3A7068EAAE03F4EED385B04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsResult
struct  AnalyticsResult_t7C9373CC5B355392C3A7068EAAE03F4EED385B04 
{
public:
	// System.Int32 UnityEngine.UDP.Analytics.AnalyticsResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnalyticsResult_t7C9373CC5B355392C3A7068EAAE03F4EED385B04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSRESULT_T7C9373CC5B355392C3A7068EAAE03F4EED385B04_H
#ifndef SESSIONSTATE_T84696351CD5FE8CF6E1272D850E9F284D0288C8F_H
#define SESSIONSTATE_T84696351CD5FE8CF6E1272D850E9F284D0288C8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsService_SessionState
struct  SessionState_t84696351CD5FE8CF6E1272D850E9F284D0288C8F 
{
public:
	// System.Int32 UnityEngine.UDP.Analytics.AnalyticsService_SessionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SessionState_t84696351CD5FE8CF6E1272D850E9F284D0288C8F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATE_T84696351CD5FE8CF6E1272D850E9F284D0288C8F_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef PRODUCTDESCRIPTION_T28F70353B3E87B193B6E0A509070EC5976871582_H
#define PRODUCTDESCRIPTION_T28F70353B3E87B193B6E0A509070EC5976871582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.ProductDescription
struct  ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Extension.ProductDescription::<storeSpecificId>k__BackingField
	String_t* ___U3CstoreSpecificIdU3Ek__BackingField_0;
	// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.Extension.ProductDescription::type
	int32_t ___type_1;
	// UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.Extension.ProductDescription::<metadata>k__BackingField
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * ___U3CmetadataU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Extension.ProductDescription::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Extension.ProductDescription::<transactionId>k__BackingField
	String_t* ___U3CtransactionIdU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582, ___U3CstoreSpecificIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CstoreSpecificIdU3Ek__BackingField_0() const { return ___U3CstoreSpecificIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstoreSpecificIdU3Ek__BackingField_0() { return &___U3CstoreSpecificIdU3Ek__BackingField_0; }
	inline void set_U3CstoreSpecificIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CstoreSpecificIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreSpecificIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582, ___U3CmetadataU3Ek__BackingField_2)); }
	inline ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * get_U3CmetadataU3Ek__BackingField_2() const { return ___U3CmetadataU3Ek__BackingField_2; }
	inline ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 ** get_address_of_U3CmetadataU3Ek__BackingField_2() { return &___U3CmetadataU3Ek__BackingField_2; }
	inline void set_U3CmetadataU3Ek__BackingField_2(ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48 * value)
	{
		___U3CmetadataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582, ___U3CreceiptU3Ek__BackingField_3)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_3() const { return ___U3CreceiptU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_3() { return &___U3CreceiptU3Ek__BackingField_3; }
	inline void set_U3CreceiptU3Ek__BackingField_3(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtransactionIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582, ___U3CtransactionIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CtransactionIdU3Ek__BackingField_4() const { return ___U3CtransactionIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CtransactionIdU3Ek__BackingField_4() { return &___U3CtransactionIdU3Ek__BackingField_4; }
	inline void set_U3CtransactionIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CtransactionIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIdU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTDESCRIPTION_T28F70353B3E87B193B6E0A509070EC5976871582_H
#ifndef PURCHASEFAILUREDESCRIPTION_T54501CD2482DF31C114490488941916F8CD9B92C_H
#define PURCHASEFAILUREDESCRIPTION_T54501CD2482DF31C114490488941916F8CD9B92C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.PurchaseFailureDescription
struct  PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Extension.PurchaseFailureDescription::<productId>k__BackingField
	String_t* ___U3CproductIdU3Ek__BackingField_0;
	// UnityEngine.Purchasing.PurchaseFailureReason UnityEngine.Purchasing.Extension.PurchaseFailureDescription::<reason>k__BackingField
	int32_t ___U3CreasonU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Extension.PurchaseFailureDescription::<message>k__BackingField
	String_t* ___U3CmessageU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CproductIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C, ___U3CproductIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CproductIdU3Ek__BackingField_0() const { return ___U3CproductIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CproductIdU3Ek__BackingField_0() { return &___U3CproductIdU3Ek__BackingField_0; }
	inline void set_U3CproductIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CproductIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CreasonU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C, ___U3CreasonU3Ek__BackingField_1)); }
	inline int32_t get_U3CreasonU3Ek__BackingField_1() const { return ___U3CreasonU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CreasonU3Ek__BackingField_1() { return &___U3CreasonU3Ek__BackingField_1; }
	inline void set_U3CreasonU3Ek__BackingField_1(int32_t value)
	{
		___U3CreasonU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmessageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C, ___U3CmessageU3Ek__BackingField_2)); }
	inline String_t* get_U3CmessageU3Ek__BackingField_2() const { return ___U3CmessageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CmessageU3Ek__BackingField_2() { return &___U3CmessageU3Ek__BackingField_2; }
	inline void set_U3CmessageU3Ek__BackingField_2(String_t* value)
	{
		___U3CmessageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmessageU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEFAILUREDESCRIPTION_T54501CD2482DF31C114490488941916F8CD9B92C_H
#ifndef PAYOUTDEFINITION_TE787916C6F389E12B8C2F10F3A87CD79B6C2A273_H
#define PAYOUTDEFINITION_TE787916C6F389E12B8C2F10F3A87CD79B6C2A273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PayoutDefinition
struct  PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.PayoutType UnityEngine.Purchasing.PayoutDefinition::m_Type
	int32_t ___m_Type_0;
	// System.String UnityEngine.Purchasing.PayoutDefinition::m_Subtype
	String_t* ___m_Subtype_1;
	// System.Double UnityEngine.Purchasing.PayoutDefinition::m_Quantity
	double ___m_Quantity_2;
	// System.String UnityEngine.Purchasing.PayoutDefinition::m_Data
	String_t* ___m_Data_3;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_Subtype_1() { return static_cast<int32_t>(offsetof(PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273, ___m_Subtype_1)); }
	inline String_t* get_m_Subtype_1() const { return ___m_Subtype_1; }
	inline String_t** get_address_of_m_Subtype_1() { return &___m_Subtype_1; }
	inline void set_m_Subtype_1(String_t* value)
	{
		___m_Subtype_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Subtype_1), value);
	}

	inline static int32_t get_offset_of_m_Quantity_2() { return static_cast<int32_t>(offsetof(PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273, ___m_Quantity_2)); }
	inline double get_m_Quantity_2() const { return ___m_Quantity_2; }
	inline double* get_address_of_m_Quantity_2() { return &___m_Quantity_2; }
	inline void set_m_Quantity_2(double value)
	{
		___m_Quantity_2 = value;
	}

	inline static int32_t get_offset_of_m_Data_3() { return static_cast<int32_t>(offsetof(PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273, ___m_Data_3)); }
	inline String_t* get_m_Data_3() const { return ___m_Data_3; }
	inline String_t** get_address_of_m_Data_3() { return &___m_Data_3; }
	inline void set_m_Data_3(String_t* value)
	{
		___m_Data_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYOUTDEFINITION_TE787916C6F389E12B8C2F10F3A87CD79B6C2A273_H
#ifndef PRODUCTDEFINITION_T020888B51F9B79E1474119DBE9DEDBDEF7766C10_H
#define PRODUCTDEFINITION_T020888B51F9B79E1474119DBE9DEDBDEF7766C10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductDefinition
struct  ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductDefinition::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.ProductDefinition::<storeSpecificId>k__BackingField
	String_t* ___U3CstoreSpecificIdU3Ek__BackingField_1;
	// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.ProductDefinition::<type>k__BackingField
	int32_t ___U3CtypeU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Purchasing.ProductDefinition::<enabled>k__BackingField
	bool ___U3CenabledU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.PayoutDefinition> UnityEngine.Purchasing.ProductDefinition::m_Payouts
	List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A * ___m_Payouts_4;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CstoreSpecificIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CstoreSpecificIdU3Ek__BackingField_1() const { return ___U3CstoreSpecificIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstoreSpecificIdU3Ek__BackingField_1() { return &___U3CstoreSpecificIdU3Ek__BackingField_1; }
	inline void set_U3CstoreSpecificIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CstoreSpecificIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreSpecificIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CtypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CtypeU3Ek__BackingField_2() const { return ___U3CtypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CtypeU3Ek__BackingField_2() { return &___U3CtypeU3Ek__BackingField_2; }
	inline void set_U3CtypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CtypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CenabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___U3CenabledU3Ek__BackingField_3)); }
	inline bool get_U3CenabledU3Ek__BackingField_3() const { return ___U3CenabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CenabledU3Ek__BackingField_3() { return &___U3CenabledU3Ek__BackingField_3; }
	inline void set_U3CenabledU3Ek__BackingField_3(bool value)
	{
		___U3CenabledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_Payouts_4() { return static_cast<int32_t>(offsetof(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10, ___m_Payouts_4)); }
	inline List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A * get_m_Payouts_4() const { return ___m_Payouts_4; }
	inline List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A ** get_address_of_m_Payouts_4() { return &___m_Payouts_4; }
	inline void set_m_Payouts_4(List_1_t3BB189095B85DE5B71713AA7779513A84EB7EB9A * value)
	{
		___m_Payouts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Payouts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTDEFINITION_T020888B51F9B79E1474119DBE9DEDBDEF7766C10_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ANALYTICSCLIENT_TE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_H
#define ANALYTICSCLIENT_TE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsClient
struct  AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC  : public RuntimeObject
{
public:

public:
};

struct AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields
{
public:
	// UnityEngine.UDP.Analytics.SessionInfo UnityEngine.UDP.Analytics.AnalyticsClient::m_sessionInfo
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60 * ___m_sessionInfo_0;
	// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::m_IsNewSession
	bool ___m_IsNewSession_1;
	// UnityEngine.UDP.Analytics.AnalyticsClient_State UnityEngine.UDP.Analytics.AnalyticsClient::m_State
	int32_t ___m_State_2;
	// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::m_InStateTransition
	bool ___m_InStateTransition_3;
	// System.String UnityEngine.UDP.Analytics.AnalyticsClient::m_AppId
	String_t* ___m_AppId_4;
	// System.String UnityEngine.UDP.Analytics.AnalyticsClient::m_ClientId
	String_t* ___m_ClientId_5;
	// System.String UnityEngine.UDP.Analytics.AnalyticsClient::m_TargetStore
	String_t* ___m_TargetStore_6;
	// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::m_AppInstalled
	bool ___m_AppInstalled_7;

public:
	inline static int32_t get_offset_of_m_sessionInfo_0() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_sessionInfo_0)); }
	inline SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60 * get_m_sessionInfo_0() const { return ___m_sessionInfo_0; }
	inline SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60 ** get_address_of_m_sessionInfo_0() { return &___m_sessionInfo_0; }
	inline void set_m_sessionInfo_0(SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60 * value)
	{
		___m_sessionInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_sessionInfo_0), value);
	}

	inline static int32_t get_offset_of_m_IsNewSession_1() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_IsNewSession_1)); }
	inline bool get_m_IsNewSession_1() const { return ___m_IsNewSession_1; }
	inline bool* get_address_of_m_IsNewSession_1() { return &___m_IsNewSession_1; }
	inline void set_m_IsNewSession_1(bool value)
	{
		___m_IsNewSession_1 = value;
	}

	inline static int32_t get_offset_of_m_State_2() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_State_2)); }
	inline int32_t get_m_State_2() const { return ___m_State_2; }
	inline int32_t* get_address_of_m_State_2() { return &___m_State_2; }
	inline void set_m_State_2(int32_t value)
	{
		___m_State_2 = value;
	}

	inline static int32_t get_offset_of_m_InStateTransition_3() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_InStateTransition_3)); }
	inline bool get_m_InStateTransition_3() const { return ___m_InStateTransition_3; }
	inline bool* get_address_of_m_InStateTransition_3() { return &___m_InStateTransition_3; }
	inline void set_m_InStateTransition_3(bool value)
	{
		___m_InStateTransition_3 = value;
	}

	inline static int32_t get_offset_of_m_AppId_4() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_AppId_4)); }
	inline String_t* get_m_AppId_4() const { return ___m_AppId_4; }
	inline String_t** get_address_of_m_AppId_4() { return &___m_AppId_4; }
	inline void set_m_AppId_4(String_t* value)
	{
		___m_AppId_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppId_4), value);
	}

	inline static int32_t get_offset_of_m_ClientId_5() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_ClientId_5)); }
	inline String_t* get_m_ClientId_5() const { return ___m_ClientId_5; }
	inline String_t** get_address_of_m_ClientId_5() { return &___m_ClientId_5; }
	inline void set_m_ClientId_5(String_t* value)
	{
		___m_ClientId_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientId_5), value);
	}

	inline static int32_t get_offset_of_m_TargetStore_6() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_TargetStore_6)); }
	inline String_t* get_m_TargetStore_6() const { return ___m_TargetStore_6; }
	inline String_t** get_address_of_m_TargetStore_6() { return &___m_TargetStore_6; }
	inline void set_m_TargetStore_6(String_t* value)
	{
		___m_TargetStore_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetStore_6), value);
	}

	inline static int32_t get_offset_of_m_AppInstalled_7() { return static_cast<int32_t>(offsetof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields, ___m_AppInstalled_7)); }
	inline bool get_m_AppInstalled_7() const { return ___m_AppInstalled_7; }
	inline bool* get_address_of_m_AppInstalled_7() { return &___m_AppInstalled_7; }
	inline void set_m_AppInstalled_7(bool value)
	{
		___m_AppInstalled_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSCLIENT_TE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_H
#ifndef ANALYTICSSERVICE_TAE445B46D51F15273DE75130F630D4DAA9BE9752_H
#define ANALYTICSSERVICE_TAE445B46D51F15273DE75130F630D4DAA9BE9752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsService
struct  AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752  : public RuntimeObject
{
public:

public:
};

struct AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields
{
public:
	// UnityEngine.UDP.Analytics.AnalyticsService_SessionState UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionState
	int32_t ___m_PlayerSessionState_0;
	// System.String UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionId
	String_t* ___m_PlayerSessionId_1;
	// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionElapsedTime
	uint64_t ___m_PlayerSessionElapsedTime_2;
	// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionForegroundTime
	uint64_t ___m_PlayerSessionForegroundTime_3;
	// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionBackgroundTime
	uint64_t ___m_PlayerSessionBackgroundTime_4;

public:
	inline static int32_t get_offset_of_m_PlayerSessionState_0() { return static_cast<int32_t>(offsetof(AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields, ___m_PlayerSessionState_0)); }
	inline int32_t get_m_PlayerSessionState_0() const { return ___m_PlayerSessionState_0; }
	inline int32_t* get_address_of_m_PlayerSessionState_0() { return &___m_PlayerSessionState_0; }
	inline void set_m_PlayerSessionState_0(int32_t value)
	{
		___m_PlayerSessionState_0 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSessionId_1() { return static_cast<int32_t>(offsetof(AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields, ___m_PlayerSessionId_1)); }
	inline String_t* get_m_PlayerSessionId_1() const { return ___m_PlayerSessionId_1; }
	inline String_t** get_address_of_m_PlayerSessionId_1() { return &___m_PlayerSessionId_1; }
	inline void set_m_PlayerSessionId_1(String_t* value)
	{
		___m_PlayerSessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerSessionId_1), value);
	}

	inline static int32_t get_offset_of_m_PlayerSessionElapsedTime_2() { return static_cast<int32_t>(offsetof(AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields, ___m_PlayerSessionElapsedTime_2)); }
	inline uint64_t get_m_PlayerSessionElapsedTime_2() const { return ___m_PlayerSessionElapsedTime_2; }
	inline uint64_t* get_address_of_m_PlayerSessionElapsedTime_2() { return &___m_PlayerSessionElapsedTime_2; }
	inline void set_m_PlayerSessionElapsedTime_2(uint64_t value)
	{
		___m_PlayerSessionElapsedTime_2 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSessionForegroundTime_3() { return static_cast<int32_t>(offsetof(AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields, ___m_PlayerSessionForegroundTime_3)); }
	inline uint64_t get_m_PlayerSessionForegroundTime_3() const { return ___m_PlayerSessionForegroundTime_3; }
	inline uint64_t* get_address_of_m_PlayerSessionForegroundTime_3() { return &___m_PlayerSessionForegroundTime_3; }
	inline void set_m_PlayerSessionForegroundTime_3(uint64_t value)
	{
		___m_PlayerSessionForegroundTime_3 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSessionBackgroundTime_4() { return static_cast<int32_t>(offsetof(AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields, ___m_PlayerSessionBackgroundTime_4)); }
	inline uint64_t get_m_PlayerSessionBackgroundTime_4() const { return ___m_PlayerSessionBackgroundTime_4; }
	inline uint64_t* get_address_of_m_PlayerSessionBackgroundTime_4() { return &___m_PlayerSessionBackgroundTime_4; }
	inline void set_m_PlayerSessionBackgroundTime_4(uint64_t value)
	{
		___m_PlayerSessionBackgroundTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSERVICE_TAE445B46D51F15273DE75130F630D4DAA9BE9752_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef UNITYNATIVEPURCHASINGCALLBACK_T58A016E25D4C5E8E9C7709A2EE87766ED4D02136_H
#define UNITYNATIVEPURCHASINGCALLBACK_T58A016E25D4C5E8E9C7709A2EE87766ED4D02136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityNativePurchasingCallback
struct  UnityNativePurchasingCallback_t58A016E25D4C5E8E9C7709A2EE87766ED4D02136  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYNATIVEPURCHASINGCALLBACK_T58A016E25D4C5E8E9C7709A2EE87766ED4D02136_H
#ifndef APPSTORESETTINGS_T83D71DDA233B51A1B173F3108C3BD7013CFB78C9_H
#define APPSTORESETTINGS_T83D71DDA233B51A1B173F3108C3BD7013CFB78C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.AppStoreSettings
struct  AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String UnityEngine.UDP.AppStoreSettings::UnityProjectID
	String_t* ___UnityProjectID_4;
	// System.String UnityEngine.UDP.AppStoreSettings::UnityClientID
	String_t* ___UnityClientID_5;
	// System.String UnityEngine.UDP.AppStoreSettings::UnityClientKey
	String_t* ___UnityClientKey_6;
	// System.String UnityEngine.UDP.AppStoreSettings::UnityClientRSAPublicKey
	String_t* ___UnityClientRSAPublicKey_7;
	// System.String UnityEngine.UDP.AppStoreSettings::AppName
	String_t* ___AppName_8;
	// System.String UnityEngine.UDP.AppStoreSettings::AppSlug
	String_t* ___AppSlug_9;
	// System.String UnityEngine.UDP.AppStoreSettings::AppItemId
	String_t* ___AppItemId_10;
	// System.String UnityEngine.UDP.AppStoreSettings::Permission
	String_t* ___Permission_11;

public:
	inline static int32_t get_offset_of_UnityProjectID_4() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___UnityProjectID_4)); }
	inline String_t* get_UnityProjectID_4() const { return ___UnityProjectID_4; }
	inline String_t** get_address_of_UnityProjectID_4() { return &___UnityProjectID_4; }
	inline void set_UnityProjectID_4(String_t* value)
	{
		___UnityProjectID_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityProjectID_4), value);
	}

	inline static int32_t get_offset_of_UnityClientID_5() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___UnityClientID_5)); }
	inline String_t* get_UnityClientID_5() const { return ___UnityClientID_5; }
	inline String_t** get_address_of_UnityClientID_5() { return &___UnityClientID_5; }
	inline void set_UnityClientID_5(String_t* value)
	{
		___UnityClientID_5 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientID_5), value);
	}

	inline static int32_t get_offset_of_UnityClientKey_6() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___UnityClientKey_6)); }
	inline String_t* get_UnityClientKey_6() const { return ___UnityClientKey_6; }
	inline String_t** get_address_of_UnityClientKey_6() { return &___UnityClientKey_6; }
	inline void set_UnityClientKey_6(String_t* value)
	{
		___UnityClientKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientKey_6), value);
	}

	inline static int32_t get_offset_of_UnityClientRSAPublicKey_7() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___UnityClientRSAPublicKey_7)); }
	inline String_t* get_UnityClientRSAPublicKey_7() const { return ___UnityClientRSAPublicKey_7; }
	inline String_t** get_address_of_UnityClientRSAPublicKey_7() { return &___UnityClientRSAPublicKey_7; }
	inline void set_UnityClientRSAPublicKey_7(String_t* value)
	{
		___UnityClientRSAPublicKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientRSAPublicKey_7), value);
	}

	inline static int32_t get_offset_of_AppName_8() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___AppName_8)); }
	inline String_t* get_AppName_8() const { return ___AppName_8; }
	inline String_t** get_address_of_AppName_8() { return &___AppName_8; }
	inline void set_AppName_8(String_t* value)
	{
		___AppName_8 = value;
		Il2CppCodeGenWriteBarrier((&___AppName_8), value);
	}

	inline static int32_t get_offset_of_AppSlug_9() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___AppSlug_9)); }
	inline String_t* get_AppSlug_9() const { return ___AppSlug_9; }
	inline String_t** get_address_of_AppSlug_9() { return &___AppSlug_9; }
	inline void set_AppSlug_9(String_t* value)
	{
		___AppSlug_9 = value;
		Il2CppCodeGenWriteBarrier((&___AppSlug_9), value);
	}

	inline static int32_t get_offset_of_AppItemId_10() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___AppItemId_10)); }
	inline String_t* get_AppItemId_10() const { return ___AppItemId_10; }
	inline String_t** get_address_of_AppItemId_10() { return &___AppItemId_10; }
	inline void set_AppItemId_10(String_t* value)
	{
		___AppItemId_10 = value;
		Il2CppCodeGenWriteBarrier((&___AppItemId_10), value);
	}

	inline static int32_t get_offset_of_Permission_11() { return static_cast<int32_t>(offsetof(AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9, ___Permission_11)); }
	inline String_t* get_Permission_11() const { return ___Permission_11; }
	inline String_t** get_address_of_Permission_11() { return &___Permission_11; }
	inline void set_Permission_11(String_t* value)
	{
		___Permission_11 = value;
		Il2CppCodeGenWriteBarrier((&___Permission_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTINGS_T83D71DDA233B51A1B173F3108C3BD7013CFB78C9_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef MAINTHREADDISPATCHER_T33166B64A5A0123609283B3BBD54A31BC066E6C9_H
#define MAINTHREADDISPATCHER_T33166B64A5A0123609283B3BBD54A31BC066E6C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.MainThreadDispatcher
struct  MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields
{
public:
	// System.String UnityEngine.UDP.MainThreadDispatcher::OBJECT_NAME
	String_t* ___OBJECT_NAME_4;
	// System.Collections.Generic.List`1<System.Action> UnityEngine.UDP.MainThreadDispatcher::s_Callbacks
	List_1_tF4B622C1ABA386932660D23A459A2974FB56E2EE * ___s_Callbacks_5;
	// System.Collections.Generic.Dictionary`2<System.Single,System.Action> UnityEngine.UDP.MainThreadDispatcher::delayAction
	Dictionary_2_t8D2697B015E3E4B5C5E2F84B4EA30062C220D509 * ___delayAction_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.UDP.MainThreadDispatcher::s_CallbacksPending
	bool ___s_CallbacksPending_7;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields, ___OBJECT_NAME_4)); }
	inline String_t* get_OBJECT_NAME_4() const { return ___OBJECT_NAME_4; }
	inline String_t** get_address_of_OBJECT_NAME_4() { return &___OBJECT_NAME_4; }
	inline void set_OBJECT_NAME_4(String_t* value)
	{
		___OBJECT_NAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___OBJECT_NAME_4), value);
	}

	inline static int32_t get_offset_of_s_Callbacks_5() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields, ___s_Callbacks_5)); }
	inline List_1_tF4B622C1ABA386932660D23A459A2974FB56E2EE * get_s_Callbacks_5() const { return ___s_Callbacks_5; }
	inline List_1_tF4B622C1ABA386932660D23A459A2974FB56E2EE ** get_address_of_s_Callbacks_5() { return &___s_Callbacks_5; }
	inline void set_s_Callbacks_5(List_1_tF4B622C1ABA386932660D23A459A2974FB56E2EE * value)
	{
		___s_Callbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Callbacks_5), value);
	}

	inline static int32_t get_offset_of_delayAction_6() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields, ___delayAction_6)); }
	inline Dictionary_2_t8D2697B015E3E4B5C5E2F84B4EA30062C220D509 * get_delayAction_6() const { return ___delayAction_6; }
	inline Dictionary_2_t8D2697B015E3E4B5C5E2F84B4EA30062C220D509 ** get_address_of_delayAction_6() { return &___delayAction_6; }
	inline void set_delayAction_6(Dictionary_2_t8D2697B015E3E4B5C5E2F84B4EA30062C220D509 * value)
	{
		___delayAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___delayAction_6), value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_7() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields, ___s_CallbacksPending_7)); }
	inline bool get_s_CallbacksPending_7() const { return ___s_CallbacksPending_7; }
	inline bool* get_address_of_s_CallbacksPending_7() { return &___s_CallbacksPending_7; }
	inline void set_s_CallbacksPending_7(bool value)
	{
		___s_CallbacksPending_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADDISPATCHER_T33166B64A5A0123609283B3BBD54A31BC066E6C9_H
#ifndef UDPGAMEMANAGER_T9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_H
#define UDPGAMEMANAGER_T9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.UdpGameManager
struct  UdpGameManager_t9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct UdpGameManager_t9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_StaticFields
{
public:
	// System.String UnityEngine.UDP.UdpGameManager::OBJECT_NAME
	String_t* ___OBJECT_NAME_4;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_4() { return static_cast<int32_t>(offsetof(UdpGameManager_t9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_StaticFields, ___OBJECT_NAME_4)); }
	inline String_t* get_OBJECT_NAME_4() const { return ___OBJECT_NAME_4; }
	inline String_t** get_address_of_OBJECT_NAME_4() { return &___OBJECT_NAME_4; }
	inline void set_OBJECT_NAME_4(String_t* value)
	{
		___OBJECT_NAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___OBJECT_NAME_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPGAMEMANAGER_T9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[7] = 
{
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_U3CbundleIDU3Ek__BackingField_0(),
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_U3CappVersionU3Ek__BackingField_1(),
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_U3CopaqueU3Ek__BackingField_2(),
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_U3ChashU3Ek__BackingField_3(),
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4(),
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5(),
	AppleReceipt_t3D153BEC3207414B3C169700C93A7A6213DF693A::get_offset_of_inAppPurchaseReceipts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[11] = 
{
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CquantityU3Ek__BackingField_0(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CproductIDU3Ek__BackingField_1(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CtransactionIDU3Ek__BackingField_2(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CcancellationDateU3Ek__BackingField_7(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CisFreeTrialU3Ek__BackingField_8(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CproductTypeU3Ek__BackingField_9(),
	AppleInAppPurchaseReceipt_t7E4D7F751E1ACA7B8988C2773E0877C39711C1FC::get_offset_of_U3CisIntroductoryPricePeriodU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[13] = 
{
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_tag_0(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_dataOffset_1(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_dataLength_2(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_lengthFieldBytes_3(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_data_4(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_childNodeList_5(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_unusedBits_6(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_deepness_7(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_path_8(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_parentNode_9(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_requireRecalculatePar_10(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_isIndefiniteLength_11(),
	Asn1Node_t837D73CC809A861E760842A0DDE7C585A1A52ECA::get_offset_of_parseEncapsulatedData_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (Asn1Parser_tA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[2] = 
{
	Asn1Parser_tA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3::get_offset_of_rawData_0(),
	Asn1Parser_tA8BFCDC40EB6D32BC46874569BDD1DA02DC573B3::get_offset_of_rootNode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (Asn1Util_t63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9), -1, sizeof(Asn1Util_t63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2906[1] = 
{
	Asn1Util_t63BA3DF6E2317DA6552B8C2C71CF1E77C5007EF9_StaticFields::get_offset_of_hexDigits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (Oid_t9D72792E7144ECEB69284AE6004135BE53596204), -1, sizeof(Oid_t9D72792E7144ECEB69284AE6004135BE53596204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2907[1] = 
{
	Oid_t9D72792E7144ECEB69284AE6004135BE53596204_StaticFields::get_offset_of_oidDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (RelativeOid_t83A5F80DE61FABABEBA9D2AB31B9116B678A35C5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (U3CPrivateImplementationDetailsU3E_tEAE252E46D83DBFA30B524295EAF9F8217169FCE), -1, sizeof(U3CPrivateImplementationDetailsU3E_tEAE252E46D83DBFA30B524295EAF9F8217169FCE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2909[1] = 
{
	U3CPrivateImplementationDetailsU3E_tEAE252E46D83DBFA30B524295EAF9F8217169FCE_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (__StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_tD3ED12F05CC2D20C6DFD875F302AF03D6E690FA9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (U3CModuleU3E_tD874E3101CE4B21F74D822F7B7638994C787A99B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (UnityNativePurchasingCallback_t58A016E25D4C5E8E9C7709A2EE87766ED4D02136), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (TizenStoreBindings_t0CD7C87597EE0CE43FF2CF4045BEDABE0F95457B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (U3CModuleU3E_t317A9D5BF1216EFB937900A181D26A1830332F44), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[13] = 
{
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_UnityProjectID_4(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_UnityClientID_5(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_UnityClientKey_6(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_UnityClientRSAPublicKey_7(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_AppName_8(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_AppSlug_9(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_AppItemId_10(),
	AppStoreSettings_t83D71DDA233B51A1B173F3108C3BD7013CFB78C9::get_offset_of_Permission_11(),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (UdpGameManager_t9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8), -1, sizeof(UdpGameManager_t9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2917[1] = 
{
	UdpGameManager_t9FFE23DED8DFBBB3CAC156B075A4002CB4CE6BF8_StaticFields::get_offset_of_OBJECT_NAME_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9), -1, sizeof(MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2918[4] = 
{
	MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields::get_offset_of_OBJECT_NAME_4(),
	MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields::get_offset_of_s_Callbacks_5(),
	MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields::get_offset_of_delayAction_6(),
	MainThreadDispatcher_t33166B64A5A0123609283B3BBD54A31BC066E6C9_StaticFields::get_offset_of_s_CallbacksPending_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[4] = 
{
	U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18::get_offset_of_U3CU3E1__state_0(),
	U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18::get_offset_of_U3CU3E2__current_1(),
	U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18::get_offset_of_waitTime_2(),
	U3CWaitAndDoU3Ed__6_t086CF389D5516CE744E35BCA880152764DB7EE18::get_offset_of_runnable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[1] = 
{
	Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3::get_offset_of__purchaseDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[4] = 
{
	AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8::get_offset_of_U3CClientIdU3Ek__BackingField_0(),
	AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8::get_offset_of_U3CAppSlugU3Ek__BackingField_1(),
	AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8::get_offset_of_U3CClientKeyU3Ek__BackingField_2(),
	AppInfo_tCA4BF037393743799729827487AFC4C1502E37B8::get_offset_of_U3CRSAPublicKeyU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (PurchaseForwardCallback_t0973275517F96FC0F0C28B1181FF0B7E73C32FD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[1] = 
{
	PurchaseForwardCallback_t0973275517F96FC0F0C28B1181FF0B7E73C32FD9::get_offset_of_purchaseListener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[6] = 
{
	PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27::get_offset_of_U3CItemTypeU3Ek__BackingField_0(),
	PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27::get_offset_of_U3CProductIdU3Ek__BackingField_1(),
	PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27::get_offset_of_U3CGameOrderIdU3Ek__BackingField_2(),
	PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27::get_offset_of_U3COrderQueryTokenU3Ek__BackingField_3(),
	PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27::get_offset_of_U3CDeveloperPayloadU3Ek__BackingField_4(),
	PurchaseInfo_t878118AC0EE1BC529145064FE010557D74EDCB27::get_offset_of_U3CStorePurchaseJsonStringU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (StoreService_tE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D), -1, sizeof(StoreService_tE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2925[1] = 
{
	StoreService_tE3F3F1623B1FC1A321E26A13374A95D9CCC7A89D_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (InitLoginForwardCallback_t3EC3816608AC4849FFF867551A844CD36DFAA219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[1] = 
{
	InitLoginForwardCallback_t3EC3816608AC4849FFF867551A844CD36DFAA219::get_offset_of__initListener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[3] = 
{
	UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD::get_offset_of_U3CChannelU3Ek__BackingField_0(),
	UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD::get_offset_of_U3CUserIdU3Ek__BackingField_1(),
	UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD::get_offset_of_U3CUserLoginTokenU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC), -1, sizeof(AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2929[8] = 
{
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_sessionInfo_0(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_IsNewSession_1(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_State_2(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_InStateTransition_3(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_AppId_4(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_ClientId_5(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_TargetStore_6(),
	AnalyticsClient_tE3BC8E90F7C1EA79AE5BBCE1158A543EF36575EC_StaticFields::get_offset_of_m_AppInstalled_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (State_t537433C14200A0AA6270A7E3C329ECD923E514E0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2930[7] = 
{
	State_t537433C14200A0AA6270A7E3C329ECD923E514E0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (EventDispatcher_tE126AF9D6EF1D8E36BE69582E1FA4362150D365B), -1, sizeof(EventDispatcher_tE126AF9D6EF1D8E36BE69582E1FA4362150D365B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2931[1] = 
{
	EventDispatcher_tE126AF9D6EF1D8E36BE69582E1FA4362150D365B_StaticFields::get_offset_of_serviceClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (Common_tBAF67736DBA7FC1C2F5FF9237C315DC95B1B5D10), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (PlatformWrapper_t3AA3F6F8F892889ADC912F180F463E5F2151B8C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752), -1, sizeof(AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2934[5] = 
{
	AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields::get_offset_of_m_PlayerSessionState_0(),
	AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields::get_offset_of_m_PlayerSessionId_1(),
	AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields::get_offset_of_m_PlayerSessionElapsedTime_2(),
	AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields::get_offset_of_m_PlayerSessionForegroundTime_3(),
	AnalyticsService_tAE445B46D51F15273DE75130F630D4DAA9BE9752_StaticFields::get_offset_of_m_PlayerSessionBackgroundTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (SessionState_t84696351CD5FE8CF6E1272D850E9F284D0288C8F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2935[5] = 
{
	SessionState_t84696351CD5FE8CF6E1272D850E9F284D0288C8F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (AnalyticsResult_t7C9373CC5B355392C3A7068EAAE03F4EED385B04)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2936[5] = 
{
	AnalyticsResult_t7C9373CC5B355392C3A7068EAAE03F4EED385B04::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (UdpAnalytics_tD781C87CE44CE97480AAD0E99BCA8493FF7E53FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[8] = 
{
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_AppId_0(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_SessionId_1(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_ClientId_2(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_DeviceId_3(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_Platform_4(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_TargetStore_5(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_SystemInfo_6(),
	SessionInfo_t823C38626F78DA14C36EC4CD87849B50274DCA60::get_offset_of_m_Vr_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (AppInstallEvent_tAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[1] = 
{
	AppInstallEvent_tAC3AF9727A13EB93DEE1BF56892B5F9DAB3FE133::get_offset_of__params_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (AppRunningEvent_t6D647BEEDDB531FCB39D319EF14381FA0D9944CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[1] = 
{
	AppRunningEvent_t6D647BEEDDB531FCB39D319EF14381FA0D9944CA::get_offset_of__params_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (AppStartEvent_tDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[1] = 
{
	AppStartEvent_tDCD7F31528515AE1A7D8ECB8A0F08FF597C12C47::get_offset_of__params_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (AppStopEvent_t6ECD1B16F2EC9DA2BF894E7010FB11F056C08734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[1] = 
{
	AppStopEvent_t6ECD1B16F2EC9DA2BF894E7010FB11F056C08734::get_offset_of__params_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (PurchaseAttemptEvent_t3164C3AEF1C541A228CD5925C106B5F31A9B096B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[1] = 
{
	PurchaseAttemptEvent_t3164C3AEF1C541A228CD5925C106B5F31A9B096B::get_offset_of__params_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (U3CModuleU3E_tE75F9FB0CF760B28F220FC101BF7C9D8DF5EAB47), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	AnalyticsReporter_t0E4ADC5ED3980D83848A13CEE068B2A9FD97EFC3::get_offset_of_m_Analytics_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (IDs_t978DE239966E5906480541F9982EA9624943DA53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[1] = 
{
	IDs_t978DE239966E5906480541F9982EA9624943DA53::get_offset_of_m_Dic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[3] = 
{
	ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE::get_offset_of_m_Factory_0(),
	ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE::get_offset_of_m_Products_1(),
	ConfigurationBuilder_t18C1B421FE4071AD4DB4A340A2A8D51AB090F3AE::get_offset_of_U3CuseCatalogProviderU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2953[4] = 
{
	InitializationFailureReason_t5A6284D67FA09D301793E67D8B7003299F4D3062::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[4] = 
{
	PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273::get_offset_of_m_Type_0(),
	PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273::get_offset_of_m_Subtype_1(),
	PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273::get_offset_of_m_Quantity_2(),
	PayoutDefinition_tE787916C6F389E12B8C2F10F3A87CD79B6C2A273::get_offset_of_m_Data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (PayoutType_t8339B08FE90A51DFE9311B97B42AD56DE7C8636B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2955[5] = 
{
	PayoutType_t8339B08FE90A51DFE9311B97B42AD56DE7C8636B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (Product_t830A133A97BEA12C3CD696853098A295D99A6DE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[5] = 
{
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4::get_offset_of_U3CdefinitionU3Ek__BackingField_0(),
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4::get_offset_of_U3CmetadataU3Ek__BackingField_1(),
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4::get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2(),
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4::get_offset_of_U3CtransactionIDU3Ek__BackingField_3(),
	Product_t830A133A97BEA12C3CD696853098A295D99A6DE4::get_offset_of_U3CreceiptU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07), -1, sizeof(ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2957[6] = 
{
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07::get_offset_of_m_IdToProduct_0(),
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07::get_offset_of_m_StoreSpecificIdToProduct_1(),
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07::get_offset_of_m_Products_2(),
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07::get_offset_of_m_ProductSet_3(),
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ProductCollection_t7BE71F2FA5BE05F5DA13EA701B513861516F4C07_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[5] = 
{
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10::get_offset_of_U3CidU3Ek__BackingField_0(),
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1(),
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10::get_offset_of_U3CtypeU3Ek__BackingField_2(),
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10::get_offset_of_U3CenabledU3Ek__BackingField_3(),
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10::get_offset_of_m_Payouts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[5] = 
{
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48::get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_0(),
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48::get_offset_of_U3ClocalizedTitleU3Ek__BackingField_1(),
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48::get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_2(),
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48::get_offset_of_U3CisoCurrencyCodeU3Ek__BackingField_3(),
	ProductMetadata_t1CAA912963F8B24B34F53CCF29A6E0BD89B33B48::get_offset_of_U3ClocalizedPriceU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2960[4] = 
{
	ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[1] = 
{
	PurchaseEventArgs_tF6E04BFD5492F5F57309FFBB2415EB26E5B88C04::get_offset_of_U3CpurchasedProductU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[3] = 
{
	PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C::get_offset_of_U3CproductIdU3Ek__BackingField_0(),
	PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C::get_offset_of_U3CreasonU3Ek__BackingField_1(),
	PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2963[9] = 
{
	PurchaseFailureReason_t42EC65C1103B27F82198DC42C8D96C1A14ED7432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (PurchaseProcessingResult_tD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2964[3] = 
{
	PurchaseProcessingResult_tD6C3A87FC6F83D8D9133EDA9D1938FC51DFC35E3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[5] = 
{
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765::get_offset_of_m_ConfigMap_0(),
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765::get_offset_of_m_ExtensionMap_1(),
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765::get_offset_of_m_Store_2(),
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765::get_offset_of_m_CatalogProvider_3(),
	PurchasingFactory_tAFBB2D3F6FD1740FCC4B64AE02E4BFFF8A2AB765::get_offset_of_U3CstoreNameU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4), -1, sizeof(PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2966[11] = 
{
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_Store_0(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_Listener_1(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_Logger_2(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_TransactionLog_3(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_StoreName_4(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_AdditionalProductsCallback_5(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_m_AdditionalProductsFailCallback_6(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_U3CuseTransactionLogU3Ek__BackingField_7(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_U3CproductsU3Ek__BackingField_8(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4::get_offset_of_initialized_9(),
	PurchasingManager_t1C02FCB1F9869F02786EC0598E70500E423ABDF4_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[3] = 
{
	UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7::get_offset_of_Store_0(),
	UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7::get_offset_of_TransactionID_1(),
	UnifiedReceipt_t2AEB0A4791355437DF99A8FF8A2C44D69752C9B7::get_offset_of_Payload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (SimpleCatalogProvider_tF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[1] = 
{
	SimpleCatalogProvider_tF221A8DCC047D5F2BC28ED9AA2F888807C3D0AAC::get_offset_of_m_Func_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[3] = 
{
	StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023::get_offset_of_m_Analytics_0(),
	StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023::get_offset_of_m_ForwardTo_1(),
	StoreListenerProxy_tAB456AE612584258695F231FD84C9E8B00B9D023::get_offset_of_m_Extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2970[2] = 
{
	TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB::get_offset_of_logger_0(),
	TransactionLog_t4AC4B3B8D7B61A3653B476A06F166CBBE46607BB::get_offset_of_persistentDataPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (UnityAnalytics_t9CA27B9569254A8D1B5CE7660D3FD131E0BBE9DA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (UnityPurchasing_tEA41D8ED18A15D67A9D0E924062E4CC18EA7444D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (U3CInitializeU3Ec__AnonStorey0_tED255462990AD3D0F15BF4CC709E2B191B6A5AA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[2] = 
{
	U3CInitializeU3Ec__AnonStorey0_tED255462990AD3D0F15BF4CC709E2B191B6A5AA6::get_offset_of_manager_0(),
	U3CInitializeU3Ec__AnonStorey0_tED255462990AD3D0F15BF4CC709E2B191B6A5AA6::get_offset_of_proxy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (U3CFetchAndMergeProductsU3Ec__AnonStorey1_tE6C371BBDB710A04E8CD8A7897D4FCE667129FE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[2] = 
{
	U3CFetchAndMergeProductsU3Ec__AnonStorey1_tE6C371BBDB710A04E8CD8A7897D4FCE667129FE0::get_offset_of_localProductSet_0(),
	U3CFetchAndMergeProductsU3Ec__AnonStorey1_tE6C371BBDB710A04E8CD8A7897D4FCE667129FE0::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2975[1] = 
{
	AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9::get_offset_of_m_Binder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (AbstractStore_t0809CE12BFD8191F29B7B77FC730AE3E8A3C039C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[5] = 
{
	ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582::get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_0(),
	ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582::get_offset_of_type_1(),
	ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582::get_offset_of_U3CmetadataU3Ek__BackingField_2(),
	ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582::get_offset_of_U3CreceiptU3Ek__BackingField_3(),
	ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582::get_offset_of_U3CtransactionIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2986[3] = 
{
	EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
