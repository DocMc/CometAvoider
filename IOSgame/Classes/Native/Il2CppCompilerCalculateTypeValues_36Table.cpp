﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AppStoresSupport.AppStoreSetting
struct AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton>
struct List_1_t57418DFB68FE5ABAC7768F08FAE0A04CDB87F68B;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPListener>
struct List_1_t80F884374626A3822C5265FE372239EF7E50660C;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Purchasing.IAPButton/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5;
// UnityEngine.Purchasing.IAPButton/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9;
// UnityEngine.Purchasing.IAPListener/OnPurchaseCompletedEvent
struct OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4;
// UnityEngine.Purchasing.IAPListener/OnPurchaseFailedEvent
struct OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t4AEE9698E20BA2DCA32EC4729DCAA60D09DA95CA;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t6A30FF7F81804FCC0082AB9DF2CE86BCFB91DF67;
// UnityEngine.Purchasing.ProductCatalog
struct ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef APPSTORESETTING_T5E544ADFE0A87667FC723C47991A84D4774DC1FB_H
#define APPSTORESETTING_T5E544ADFE0A87667FC723C47991A84D4774DC1FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSetting
struct  AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB  : public RuntimeObject
{
public:
	// System.String AppStoresSupport.AppStoreSetting::AppID
	String_t* ___AppID_0;
	// System.String AppStoresSupport.AppStoreSetting::AppKey
	String_t* ___AppKey_1;
	// System.Boolean AppStoresSupport.AppStoreSetting::IsTestMode
	bool ___IsTestMode_2;

public:
	inline static int32_t get_offset_of_AppID_0() { return static_cast<int32_t>(offsetof(AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB, ___AppID_0)); }
	inline String_t* get_AppID_0() const { return ___AppID_0; }
	inline String_t** get_address_of_AppID_0() { return &___AppID_0; }
	inline void set_AppID_0(String_t* value)
	{
		___AppID_0 = value;
		Il2CppCodeGenWriteBarrier((&___AppID_0), value);
	}

	inline static int32_t get_offset_of_AppKey_1() { return static_cast<int32_t>(offsetof(AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB, ___AppKey_1)); }
	inline String_t* get_AppKey_1() const { return ___AppKey_1; }
	inline String_t** get_address_of_AppKey_1() { return &___AppKey_1; }
	inline void set_AppKey_1(String_t* value)
	{
		___AppKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___AppKey_1), value);
	}

	inline static int32_t get_offset_of_IsTestMode_2() { return static_cast<int32_t>(offsetof(AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB, ___IsTestMode_2)); }
	inline bool get_IsTestMode_2() const { return ___IsTestMode_2; }
	inline bool* get_address_of_IsTestMode_2() { return &___IsTestMode_2; }
	inline void set_IsTestMode_2(bool value)
	{
		___IsTestMode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTING_T5E544ADFE0A87667FC723C47991A84D4774DC1FB_H
#ifndef U3CU3EC_TE77B1BD41B1AE48E19EBA2195628040C22739AB4_H
#define U3CU3EC_TE77B1BD41B1AE48E19EBA2195628040C22739AB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo_<>c
struct  U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4_StaticFields
{
public:
	// IAPDemo_<>c IAPDemo_<>c::<>9
	U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4 * ___U3CU3E9_0;
	// System.Action`1<System.String> IAPDemo_<>c::<>9__30_1
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__30_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4_StaticFields, ___U3CU3E9__30_1_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__30_1_1() const { return ___U3CU3E9__30_1_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__30_1_1() { return &___U3CU3E9__30_1_1; }
	inline void set_U3CU3E9__30_1_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__30_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE77B1BD41B1AE48E19EBA2195628040C22739AB4_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T34D61E384C530FFD880E7F71AD887AF67E4E1C9F_H
#define U3CU3EC__DISPLAYCLASS38_0_T34D61E384C530FFD880E7F71AD887AF67E4E1C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemo_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t34D61E384C530FFD880E7F71AD887AF67E4E1C9F  : public RuntimeObject
{
public:
	// System.String IAPDemo_<>c__DisplayClass38_0::txId
	String_t* ___txId_0;

public:
	inline static int32_t get_offset_of_txId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t34D61E384C530FFD880E7F71AD887AF67E4E1C9F, ___txId_0)); }
	inline String_t* get_txId_0() const { return ___txId_0; }
	inline String_t** get_address_of_txId_0() { return &___txId_0; }
	inline void set_txId_0(String_t* value)
	{
		___txId_0 = value;
		Il2CppCodeGenWriteBarrier((&___txId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T34D61E384C530FFD880E7F71AD887AF67E4E1C9F_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef CODELESSIAPSTORELISTENER_TC0C269A13BAE3B9F9AA24455FAB72340347F7303_H
#define CODELESSIAPSTORELISTENER_TC0C269A13BAE3B9F9AA24455FAB72340347F7303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CodelessIAPStoreListener
struct  CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPButton> UnityEngine.Purchasing.CodelessIAPStoreListener::activeButtons
	List_1_t57418DFB68FE5ABAC7768F08FAE0A04CDB87F68B * ___activeButtons_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.IAPListener> UnityEngine.Purchasing.CodelessIAPStoreListener::activeListeners
	List_1_t80F884374626A3822C5265FE372239EF7E50660C * ___activeListeners_2;
	// UnityEngine.Purchasing.IStoreController UnityEngine.Purchasing.CodelessIAPStoreListener::controller
	RuntimeObject* ___controller_4;
	// UnityEngine.Purchasing.IExtensionProvider UnityEngine.Purchasing.CodelessIAPStoreListener::extensions
	RuntimeObject* ___extensions_5;
	// UnityEngine.Purchasing.ProductCatalog UnityEngine.Purchasing.CodelessIAPStoreListener::catalog
	ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6 * ___catalog_6;

public:
	inline static int32_t get_offset_of_activeButtons_1() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303, ___activeButtons_1)); }
	inline List_1_t57418DFB68FE5ABAC7768F08FAE0A04CDB87F68B * get_activeButtons_1() const { return ___activeButtons_1; }
	inline List_1_t57418DFB68FE5ABAC7768F08FAE0A04CDB87F68B ** get_address_of_activeButtons_1() { return &___activeButtons_1; }
	inline void set_activeButtons_1(List_1_t57418DFB68FE5ABAC7768F08FAE0A04CDB87F68B * value)
	{
		___activeButtons_1 = value;
		Il2CppCodeGenWriteBarrier((&___activeButtons_1), value);
	}

	inline static int32_t get_offset_of_activeListeners_2() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303, ___activeListeners_2)); }
	inline List_1_t80F884374626A3822C5265FE372239EF7E50660C * get_activeListeners_2() const { return ___activeListeners_2; }
	inline List_1_t80F884374626A3822C5265FE372239EF7E50660C ** get_address_of_activeListeners_2() { return &___activeListeners_2; }
	inline void set_activeListeners_2(List_1_t80F884374626A3822C5265FE372239EF7E50660C * value)
	{
		___activeListeners_2 = value;
		Il2CppCodeGenWriteBarrier((&___activeListeners_2), value);
	}

	inline static int32_t get_offset_of_controller_4() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303, ___controller_4)); }
	inline RuntimeObject* get_controller_4() const { return ___controller_4; }
	inline RuntimeObject** get_address_of_controller_4() { return &___controller_4; }
	inline void set_controller_4(RuntimeObject* value)
	{
		___controller_4 = value;
		Il2CppCodeGenWriteBarrier((&___controller_4), value);
	}

	inline static int32_t get_offset_of_extensions_5() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303, ___extensions_5)); }
	inline RuntimeObject* get_extensions_5() const { return ___extensions_5; }
	inline RuntimeObject** get_address_of_extensions_5() { return &___extensions_5; }
	inline void set_extensions_5(RuntimeObject* value)
	{
		___extensions_5 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_5), value);
	}

	inline static int32_t get_offset_of_catalog_6() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303, ___catalog_6)); }
	inline ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6 * get_catalog_6() const { return ___catalog_6; }
	inline ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6 ** get_address_of_catalog_6() { return &___catalog_6; }
	inline void set_catalog_6(ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6 * value)
	{
		___catalog_6 = value;
		Il2CppCodeGenWriteBarrier((&___catalog_6), value);
	}
};

struct CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields
{
public:
	// UnityEngine.Purchasing.CodelessIAPStoreListener UnityEngine.Purchasing.CodelessIAPStoreListener::instance
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303 * ___instance_0;
	// System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::unityPurchasingInitialized
	bool ___unityPurchasingInitialized_3;
	// System.Boolean UnityEngine.Purchasing.CodelessIAPStoreListener::initializationComplete
	bool ___initializationComplete_7;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields, ___instance_0)); }
	inline CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303 * get_instance_0() const { return ___instance_0; }
	inline CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}

	inline static int32_t get_offset_of_unityPurchasingInitialized_3() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields, ___unityPurchasingInitialized_3)); }
	inline bool get_unityPurchasingInitialized_3() const { return ___unityPurchasingInitialized_3; }
	inline bool* get_address_of_unityPurchasingInitialized_3() { return &___unityPurchasingInitialized_3; }
	inline void set_unityPurchasingInitialized_3(bool value)
	{
		___unityPurchasingInitialized_3 = value;
	}

	inline static int32_t get_offset_of_initializationComplete_7() { return static_cast<int32_t>(offsetof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields, ___initializationComplete_7)); }
	inline bool get_initializationComplete_7() const { return ___initializationComplete_7; }
	inline bool* get_address_of_initializationComplete_7() { return &___initializationComplete_7; }
	inline void set_initializationComplete_7(bool value)
	{
		___initializationComplete_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODELESSIAPSTORELISTENER_TC0C269A13BAE3B9F9AA24455FAB72340347F7303_H
#ifndef IAPCONFIGURATIONHELPER_T046F47F8CD6EAAA58C30C2DE39762DE00DC221C4_H
#define IAPCONFIGURATIONHELPER_T046F47F8CD6EAAA58C30C2DE39762DE00DC221C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPConfigurationHelper
struct  IAPConfigurationHelper_t046F47F8CD6EAAA58C30C2DE39762DE00DC221C4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPCONFIGURATIONHELPER_T046F47F8CD6EAAA58C30C2DE39762DE00DC221C4_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_TEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B_H
#define UNITYEVENT_1_TEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Purchasing.Product>
struct  UnityEvent_1_tEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_tEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B_H
#ifndef UNITYEVENT_2_T303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5_H
#define UNITYEVENT_2_T303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason>
struct  UnityEvent_2_t303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef BUTTONTYPE_T5C1BEBDE03F52E77BCC3C4F66279087D81E12663_H
#define BUTTONTYPE_T5C1BEBDE03F52E77BCC3C4F66279087D81E12663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton_ButtonType
struct  ButtonType_t5C1BEBDE03F52E77BCC3C4F66279087D81E12663 
{
public:
	// System.Int32 UnityEngine.Purchasing.IAPButton_ButtonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonType_t5C1BEBDE03F52E77BCC3C4F66279087D81E12663, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTYPE_T5C1BEBDE03F52E77BCC3C4F66279087D81E12663_H
#ifndef ONPURCHASECOMPLETEDEVENT_TE1F6B944B4FED7F7744C50608F2A02E711C82BB5_H
#define ONPURCHASECOMPLETEDEVENT_TE1F6B944B4FED7F7744C50608F2A02E711C82BB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton_OnPurchaseCompletedEvent
struct  OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5  : public UnityEvent_1_tEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASECOMPLETEDEVENT_TE1F6B944B4FED7F7744C50608F2A02E711C82BB5_H
#ifndef ONPURCHASEFAILEDEVENT_T12785B755627F44DD9D18B1141669474E76083E9_H
#define ONPURCHASEFAILEDEVENT_T12785B755627F44DD9D18B1141669474E76083E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton_OnPurchaseFailedEvent
struct  OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9  : public UnityEvent_2_t303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASEFAILEDEVENT_T12785B755627F44DD9D18B1141669474E76083E9_H
#ifndef ONPURCHASECOMPLETEDEVENT_T69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4_H
#define ONPURCHASECOMPLETEDEVENT_T69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener_OnPurchaseCompletedEvent
struct  OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4  : public UnityEvent_1_tEE1A2CFF07DB130E94E8EDC3B16BD99927EFCB0B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASECOMPLETEDEVENT_T69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4_H
#ifndef ONPURCHASEFAILEDEVENT_TBE41AD846CDA9B343E1C212C291326AF0B98B026_H
#define ONPURCHASEFAILEDEVENT_TBE41AD846CDA9B343E1C212C291326AF0B98B026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener_OnPurchaseFailedEvent
struct  OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026  : public UnityEvent_2_t303E7F876F93A36ED8E5DCA060E96AA6E2C8FAD5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPURCHASEFAILEDEVENT_TBE41AD846CDA9B343E1C212C291326AF0B98B026_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef APPSTORESETTINGS_T671C74CB5C6B969F884CEFCB81112BE08F1985E7_H
#define APPSTORESETTINGS_T671C74CB5C6B969F884CEFCB81112BE08F1985E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppStoresSupport.AppStoreSettings
struct  AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String AppStoresSupport.AppStoreSettings::UnityClientID
	String_t* ___UnityClientID_4;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientKey
	String_t* ___UnityClientKey_5;
	// System.String AppStoresSupport.AppStoreSettings::UnityClientRSAPublicKey
	String_t* ___UnityClientRSAPublicKey_6;
	// AppStoresSupport.AppStoreSetting AppStoresSupport.AppStoreSettings::XiaomiAppStoreSetting
	AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB * ___XiaomiAppStoreSetting_7;

public:
	inline static int32_t get_offset_of_UnityClientID_4() { return static_cast<int32_t>(offsetof(AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7, ___UnityClientID_4)); }
	inline String_t* get_UnityClientID_4() const { return ___UnityClientID_4; }
	inline String_t** get_address_of_UnityClientID_4() { return &___UnityClientID_4; }
	inline void set_UnityClientID_4(String_t* value)
	{
		___UnityClientID_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientID_4), value);
	}

	inline static int32_t get_offset_of_UnityClientKey_5() { return static_cast<int32_t>(offsetof(AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7, ___UnityClientKey_5)); }
	inline String_t* get_UnityClientKey_5() const { return ___UnityClientKey_5; }
	inline String_t** get_address_of_UnityClientKey_5() { return &___UnityClientKey_5; }
	inline void set_UnityClientKey_5(String_t* value)
	{
		___UnityClientKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientKey_5), value);
	}

	inline static int32_t get_offset_of_UnityClientRSAPublicKey_6() { return static_cast<int32_t>(offsetof(AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7, ___UnityClientRSAPublicKey_6)); }
	inline String_t* get_UnityClientRSAPublicKey_6() const { return ___UnityClientRSAPublicKey_6; }
	inline String_t** get_address_of_UnityClientRSAPublicKey_6() { return &___UnityClientRSAPublicKey_6; }
	inline void set_UnityClientRSAPublicKey_6(String_t* value)
	{
		___UnityClientRSAPublicKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientRSAPublicKey_6), value);
	}

	inline static int32_t get_offset_of_XiaomiAppStoreSetting_7() { return static_cast<int32_t>(offsetof(AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7, ___XiaomiAppStoreSetting_7)); }
	inline AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB * get_XiaomiAppStoreSetting_7() const { return ___XiaomiAppStoreSetting_7; }
	inline AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB ** get_address_of_XiaomiAppStoreSetting_7() { return &___XiaomiAppStoreSetting_7; }
	inline void set_XiaomiAppStoreSetting_7(AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB * value)
	{
		___XiaomiAppStoreSetting_7 = value;
		Il2CppCodeGenWriteBarrier((&___XiaomiAppStoreSetting_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTINGS_T671C74CB5C6B969F884CEFCB81112BE08F1985E7_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef IAPDEMOPRODUCTUI_T8DBFAD3179124DEECE0AD4EAB86EBF7779823C84_H
#define IAPDEMOPRODUCTUI_T8DBFAD3179124DEECE0AD4EAB86EBF7779823C84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IAPDemoProductUI
struct  IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button IAPDemoProductUI::purchaseButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___purchaseButton_4;
	// UnityEngine.UI.Button IAPDemoProductUI::receiptButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___receiptButton_5;
	// UnityEngine.UI.Text IAPDemoProductUI::titleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___titleText_6;
	// UnityEngine.UI.Text IAPDemoProductUI::descriptionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___descriptionText_7;
	// UnityEngine.UI.Text IAPDemoProductUI::priceText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___priceText_8;
	// UnityEngine.UI.Text IAPDemoProductUI::statusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___statusText_9;
	// System.String IAPDemoProductUI::m_ProductID
	String_t* ___m_ProductID_10;
	// System.Action`1<System.String> IAPDemoProductUI::m_PurchaseCallback
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___m_PurchaseCallback_11;
	// System.String IAPDemoProductUI::m_Receipt
	String_t* ___m_Receipt_12;

public:
	inline static int32_t get_offset_of_purchaseButton_4() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___purchaseButton_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_purchaseButton_4() const { return ___purchaseButton_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_purchaseButton_4() { return &___purchaseButton_4; }
	inline void set_purchaseButton_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___purchaseButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseButton_4), value);
	}

	inline static int32_t get_offset_of_receiptButton_5() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___receiptButton_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_receiptButton_5() const { return ___receiptButton_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_receiptButton_5() { return &___receiptButton_5; }
	inline void set_receiptButton_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___receiptButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___receiptButton_5), value);
	}

	inline static int32_t get_offset_of_titleText_6() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___titleText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_titleText_6() const { return ___titleText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_titleText_6() { return &___titleText_6; }
	inline void set_titleText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___titleText_6 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_6), value);
	}

	inline static int32_t get_offset_of_descriptionText_7() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___descriptionText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_descriptionText_7() const { return ___descriptionText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_descriptionText_7() { return &___descriptionText_7; }
	inline void set_descriptionText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___descriptionText_7 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_7), value);
	}

	inline static int32_t get_offset_of_priceText_8() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___priceText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_priceText_8() const { return ___priceText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_priceText_8() { return &___priceText_8; }
	inline void set_priceText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___priceText_8 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_8), value);
	}

	inline static int32_t get_offset_of_statusText_9() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___statusText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_statusText_9() const { return ___statusText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_statusText_9() { return &___statusText_9; }
	inline void set_statusText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___statusText_9 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_9), value);
	}

	inline static int32_t get_offset_of_m_ProductID_10() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___m_ProductID_10)); }
	inline String_t* get_m_ProductID_10() const { return ___m_ProductID_10; }
	inline String_t** get_address_of_m_ProductID_10() { return &___m_ProductID_10; }
	inline void set_m_ProductID_10(String_t* value)
	{
		___m_ProductID_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductID_10), value);
	}

	inline static int32_t get_offset_of_m_PurchaseCallback_11() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___m_PurchaseCallback_11)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_m_PurchaseCallback_11() const { return ___m_PurchaseCallback_11; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_m_PurchaseCallback_11() { return &___m_PurchaseCallback_11; }
	inline void set_m_PurchaseCallback_11(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___m_PurchaseCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseCallback_11), value);
	}

	inline static int32_t get_offset_of_m_Receipt_12() { return static_cast<int32_t>(offsetof(IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84, ___m_Receipt_12)); }
	inline String_t* get_m_Receipt_12() const { return ___m_Receipt_12; }
	inline String_t** get_address_of_m_Receipt_12() { return &___m_Receipt_12; }
	inline void set_m_Receipt_12(String_t* value)
	{
		___m_Receipt_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Receipt_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPDEMOPRODUCTUI_T8DBFAD3179124DEECE0AD4EAB86EBF7779823C84_H
#ifndef DEMOINVENTORY_TEE67406267DE534F798D930E84602CC4F0584EE7_H
#define DEMOINVENTORY_TEE67406267DE534F798D930E84602CC4F0584EE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.DemoInventory
struct  DemoInventory_tEE67406267DE534F798D930E84602CC4F0584EE7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOINVENTORY_TEE67406267DE534F798D930E84602CC4F0584EE7_H
#ifndef IAPBUTTON_T1BDE7DF230194C47716F1AD10806094D8DE39CD9_H
#define IAPBUTTON_T1BDE7DF230194C47716F1AD10806094D8DE39CD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPButton
struct  IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityEngine.Purchasing.IAPButton::productId
	String_t* ___productId_4;
	// UnityEngine.Purchasing.IAPButton_ButtonType UnityEngine.Purchasing.IAPButton::buttonType
	int32_t ___buttonType_5;
	// System.Boolean UnityEngine.Purchasing.IAPButton::consumePurchase
	bool ___consumePurchase_6;
	// UnityEngine.Purchasing.IAPButton_OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPButton::onPurchaseComplete
	OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5 * ___onPurchaseComplete_7;
	// UnityEngine.Purchasing.IAPButton_OnPurchaseFailedEvent UnityEngine.Purchasing.IAPButton::onPurchaseFailed
	OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9 * ___onPurchaseFailed_8;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::titleText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___titleText_9;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::descriptionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___descriptionText_10;
	// UnityEngine.UI.Text UnityEngine.Purchasing.IAPButton::priceText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___priceText_11;

public:
	inline static int32_t get_offset_of_productId_4() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___productId_4)); }
	inline String_t* get_productId_4() const { return ___productId_4; }
	inline String_t** get_address_of_productId_4() { return &___productId_4; }
	inline void set_productId_4(String_t* value)
	{
		___productId_4 = value;
		Il2CppCodeGenWriteBarrier((&___productId_4), value);
	}

	inline static int32_t get_offset_of_buttonType_5() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___buttonType_5)); }
	inline int32_t get_buttonType_5() const { return ___buttonType_5; }
	inline int32_t* get_address_of_buttonType_5() { return &___buttonType_5; }
	inline void set_buttonType_5(int32_t value)
	{
		___buttonType_5 = value;
	}

	inline static int32_t get_offset_of_consumePurchase_6() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___consumePurchase_6)); }
	inline bool get_consumePurchase_6() const { return ___consumePurchase_6; }
	inline bool* get_address_of_consumePurchase_6() { return &___consumePurchase_6; }
	inline void set_consumePurchase_6(bool value)
	{
		___consumePurchase_6 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_7() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___onPurchaseComplete_7)); }
	inline OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5 * get_onPurchaseComplete_7() const { return ___onPurchaseComplete_7; }
	inline OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5 ** get_address_of_onPurchaseComplete_7() { return &___onPurchaseComplete_7; }
	inline void set_onPurchaseComplete_7(OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5 * value)
	{
		___onPurchaseComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseComplete_7), value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_8() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___onPurchaseFailed_8)); }
	inline OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9 * get_onPurchaseFailed_8() const { return ___onPurchaseFailed_8; }
	inline OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9 ** get_address_of_onPurchaseFailed_8() { return &___onPurchaseFailed_8; }
	inline void set_onPurchaseFailed_8(OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9 * value)
	{
		___onPurchaseFailed_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseFailed_8), value);
	}

	inline static int32_t get_offset_of_titleText_9() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___titleText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_titleText_9() const { return ___titleText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_titleText_9() { return &___titleText_9; }
	inline void set_titleText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___titleText_9 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_9), value);
	}

	inline static int32_t get_offset_of_descriptionText_10() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___descriptionText_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_descriptionText_10() const { return ___descriptionText_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_descriptionText_10() { return &___descriptionText_10; }
	inline void set_descriptionText_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___descriptionText_10 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_10), value);
	}

	inline static int32_t get_offset_of_priceText_11() { return static_cast<int32_t>(offsetof(IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9, ___priceText_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_priceText_11() const { return ___priceText_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_priceText_11() { return &___priceText_11; }
	inline void set_priceText_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___priceText_11 = value;
		Il2CppCodeGenWriteBarrier((&___priceText_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPBUTTON_T1BDE7DF230194C47716F1AD10806094D8DE39CD9_H
#ifndef IAPLISTENER_TA8E5B3850C466201C221D95CF74A9BEA7DC74E61_H
#define IAPLISTENER_TA8E5B3850C466201C221D95CF74A9BEA7DC74E61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.IAPListener
struct  IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UnityEngine.Purchasing.IAPListener::consumePurchase
	bool ___consumePurchase_4;
	// System.Boolean UnityEngine.Purchasing.IAPListener::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_5;
	// UnityEngine.Purchasing.IAPListener_OnPurchaseCompletedEvent UnityEngine.Purchasing.IAPListener::onPurchaseComplete
	OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4 * ___onPurchaseComplete_6;
	// UnityEngine.Purchasing.IAPListener_OnPurchaseFailedEvent UnityEngine.Purchasing.IAPListener::onPurchaseFailed
	OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026 * ___onPurchaseFailed_7;

public:
	inline static int32_t get_offset_of_consumePurchase_4() { return static_cast<int32_t>(offsetof(IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61, ___consumePurchase_4)); }
	inline bool get_consumePurchase_4() const { return ___consumePurchase_4; }
	inline bool* get_address_of_consumePurchase_4() { return &___consumePurchase_4; }
	inline void set_consumePurchase_4(bool value)
	{
		___consumePurchase_4 = value;
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_5() { return static_cast<int32_t>(offsetof(IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61, ___dontDestroyOnLoad_5)); }
	inline bool get_dontDestroyOnLoad_5() const { return ___dontDestroyOnLoad_5; }
	inline bool* get_address_of_dontDestroyOnLoad_5() { return &___dontDestroyOnLoad_5; }
	inline void set_dontDestroyOnLoad_5(bool value)
	{
		___dontDestroyOnLoad_5 = value;
	}

	inline static int32_t get_offset_of_onPurchaseComplete_6() { return static_cast<int32_t>(offsetof(IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61, ___onPurchaseComplete_6)); }
	inline OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4 * get_onPurchaseComplete_6() const { return ___onPurchaseComplete_6; }
	inline OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4 ** get_address_of_onPurchaseComplete_6() { return &___onPurchaseComplete_6; }
	inline void set_onPurchaseComplete_6(OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4 * value)
	{
		___onPurchaseComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseComplete_6), value);
	}

	inline static int32_t get_offset_of_onPurchaseFailed_7() { return static_cast<int32_t>(offsetof(IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61, ___onPurchaseFailed_7)); }
	inline OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026 * get_onPurchaseFailed_7() const { return ___onPurchaseFailed_7; }
	inline OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026 ** get_address_of_onPurchaseFailed_7() { return &___onPurchaseFailed_7; }
	inline void set_onPurchaseFailed_7(OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026 * value)
	{
		___onPurchaseFailed_7 = value;
		Il2CppCodeGenWriteBarrier((&___onPurchaseFailed_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAPLISTENER_TA8E5B3850C466201C221D95CF74A9BEA7DC74E61_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4), -1, sizeof(U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3600[2] = 
{
	U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE77B1BD41B1AE48E19EBA2195628040C22739AB4_StaticFields::get_offset_of_U3CU3E9__30_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (U3CU3Ec__DisplayClass38_0_t34D61E384C530FFD880E7F71AD887AF67E4E1C9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[1] = 
{
	U3CU3Ec__DisplayClass38_0_t34D61E384C530FFD880E7F71AD887AF67E4E1C9F::get_offset_of_txId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[9] = 
{
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_purchaseButton_4(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_receiptButton_5(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_titleText_6(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_descriptionText_7(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_priceText_8(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_statusText_9(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_m_ProductID_10(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_m_PurchaseCallback_11(),
	IAPDemoProductUI_t8DBFAD3179124DEECE0AD4EAB86EBF7779823C84::get_offset_of_m_Receipt_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303), -1, sizeof(CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3603[8] = 
{
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields::get_offset_of_instance_0(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303::get_offset_of_activeButtons_1(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303::get_offset_of_activeListeners_2(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields::get_offset_of_unityPurchasingInitialized_3(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303::get_offset_of_controller_4(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303::get_offset_of_extensions_5(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303::get_offset_of_catalog_6(),
	CodelessIAPStoreListener_tC0C269A13BAE3B9F9AA24455FAB72340347F7303_StaticFields::get_offset_of_initializationComplete_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (DemoInventory_tEE67406267DE534F798D930E84602CC4F0584EE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[8] = 
{
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_productId_4(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_buttonType_5(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_consumePurchase_6(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_onPurchaseComplete_7(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_onPurchaseFailed_8(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_titleText_9(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_descriptionText_10(),
	IAPButton_t1BDE7DF230194C47716F1AD10806094D8DE39CD9::get_offset_of_priceText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (ButtonType_t5C1BEBDE03F52E77BCC3C4F66279087D81E12663)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3606[3] = 
{
	ButtonType_t5C1BEBDE03F52E77BCC3C4F66279087D81E12663::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (OnPurchaseCompletedEvent_tE1F6B944B4FED7F7744C50608F2A02E711C82BB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (OnPurchaseFailedEvent_t12785B755627F44DD9D18B1141669474E76083E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (IAPConfigurationHelper_t046F47F8CD6EAAA58C30C2DE39762DE00DC221C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[4] = 
{
	IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61::get_offset_of_consumePurchase_4(),
	IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61::get_offset_of_dontDestroyOnLoad_5(),
	IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61::get_offset_of_onPurchaseComplete_6(),
	IAPListener_tA8E5B3850C466201C221D95CF74A9BEA7DC74E61::get_offset_of_onPurchaseFailed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (OnPurchaseCompletedEvent_t69F2AA7C2E6305881C5030EEE41E4EBCD72B08C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (OnPurchaseFailedEvent_tBE41AD846CDA9B343E1C212C291326AF0B98B026), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3613[3] = 
{
	AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB::get_offset_of_AppID_0(),
	AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB::get_offset_of_AppKey_1(),
	AppStoreSetting_t5E544ADFE0A87667FC723C47991A84D4774DC1FB::get_offset_of_IsTestMode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[4] = 
{
	AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7::get_offset_of_UnityClientID_4(),
	AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7::get_offset_of_UnityClientKey_5(),
	AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7::get_offset_of_UnityClientRSAPublicKey_6(),
	AppStoreSettings_t671C74CB5C6B969F884CEFCB81112BE08F1985E7::get_offset_of_XiaomiAppStoreSetting_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
