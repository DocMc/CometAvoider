﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F;
// System.Action`1<System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>>
struct Action_1_tA725AD78C98168AF2201E02E5913A66DB5D3064F;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<UnityEngine.Purchasing.Product>
struct Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30;
// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>
struct Action_1_t16C7D3167F43E9D3FA6F506C90EF45189E2C0522;
// System.Action`2<System.Boolean,System.String>
struct Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2;
// System.Action`2<System.String,System.String>
struct Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274;
// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String>
struct Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39;
// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>
struct Action_3_t3BBBEEA0BD365F8EB85C8F88F570DF3FA425F072;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>>
struct Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>
struct Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String>
struct Dictionary_2_t4F339596DF041CC12B899559EAACD15AA5D2AF4F;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.LocalizedProductDescription>
struct List_1_t4A5E4DB145DD9798C5FEB91EE0F1A7C77E1F43EB;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogItem>
struct List_1_tA1C6463853DC035280694FB65B564ECD8A3453EC;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogPayout>
struct List_1_t6EBBFBE956641E9855F6E08AA6671EDB7E9F0B2B;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>
struct List_1_tFE611315D844DCEE9F38C09B0EADEFEBCCC51DBB;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.StoreID>
struct List_1_tE827FDB9C085BDBF2BD397CEC9BC2DAE20C9AB01;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition>
struct ReadOnlyCollection_1_tF0BB6018B708B7F0FCF910A9B008A3E81C00B29D;
// System.Comparison`1<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>
struct Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Object>
struct Func_2_tAFF26EB45C16E2D388535746D3CE9A030DD1BE9A;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String>
struct Func_2_t1A871D9F8DC387DE1A9B3E47BD1A9771E4CB411E;
// System.Func`2<UnityEngine.Purchasing.ProductCatalogItem,System.Boolean>
struct Func_2_tC03308396998D98527564CECC440648536E95DAC;
// System.Func`3<System.Char,System.Int32,System.String>
struct Func_3_tDBAE4B3898928FD3EB3796F1103233248C8E8B44;
// System.Func`3<System.String,System.String,System.String>
struct Func_3_t2AADDAD205C862FFE7FFD4E9756DA09107BD8A1A;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.RegularExpressions.MatchEvaluator
struct MatchEvaluator_tB0F0EC2FE9D878BB2D0F90CCAE27AA57E8107BE2;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Uniject.IUtil
struct IUtil_tB75866153468EC27B475231CBF717C98B349ED25;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E;
// UnityEngine.ILogger
struct ILogger_t572B66532D8EB6E76240476A788384A26D70866F;
// UnityEngine.Purchasing.AsyncWebUtil
struct AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00;
// UnityEngine.Purchasing.CloudCatalogImpl
struct CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711;
// UnityEngine.Purchasing.EventQueue
struct EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t29DFEB92F0789E5E0A5F52148451C2A5F9BEA425;
// UnityEngine.Purchasing.Extension.IStore
struct IStore_t496ACD1C21137C6E16BCAA48DEBEEED2047DD14C;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t4823C53C7713E1376F628AF0D95AEEC85B192C60;
// UnityEngine.Purchasing.Extension.ProductDescription
struct ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582;
// UnityEngine.Purchasing.Extension.PurchaseFailureDescription
struct PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C;
// UnityEngine.Purchasing.FileReference
struct FileReference_t82C0F6EB16F825D6A9F1786DB607F00B9CCDFF48;
// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t205A001AC4D5C8E8FE86F3BF3435FCCC35D4494E;
// UnityEngine.Purchasing.INativeAppleStore
struct INativeAppleStore_t2ED67C988FA4224C62161F7D16E8E2E3AFE2B466;
// UnityEngine.Purchasing.INativeStore
struct INativeStore_t633ED2639CF4215EEC1E8529554BBF02771FDDB0;
// UnityEngine.Purchasing.INativeStoreProvider
struct INativeStoreProvider_t8458BEB0C094BDC935B6EFBDAB9FFFD0A7439DF9;
// UnityEngine.Purchasing.INativeUDPStore
struct INativeUDPStore_tBE7EDA1F2F8E7E0A51D58063951C6B21F19C96DB;
// UnityEngine.Purchasing.INativeUnityChannelStore
struct INativeUnityChannelStore_t18D6F9F734AC2D2B0CCAED9A071D89456B57B15D;
// UnityEngine.Purchasing.IProductCatalogImpl
struct IProductCatalogImpl_t4F57C22DC2B60BC60B64D91ED7DC678CA262264D;
// UnityEngine.Purchasing.JSONStore
struct JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3;
// UnityEngine.Purchasing.LocalizedProductDescription
struct LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482;
// UnityEngine.Purchasing.Price
struct Price_t5D55521A645803976E0809F8941E8317C80E129C;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10;
// UnityEngine.Purchasing.ProfileData
struct ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A;
// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance
struct StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57;
// UnityEngine.Purchasing.StoreCatalogImpl
struct StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB;
// UnityEngine.Purchasing.UDPImpl
struct UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76;
// UnityEngine.Purchasing.UnityChannelBindings
struct UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6;
// UnityEngine.Purchasing.UnityChannelImpl
struct UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616;
// UnityEngine.Purchasing.UnityChannelImpl/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0;
// UnityEngine.Purchasing.WinRTStore
struct WinRTStore_tE10DCCB8F603A2D08C6EC23F9FA3EF0DD05B7E5F;
// UnityEngine.UDP.Inventory
struct Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3;
// UnityEngine.UDP.UserInfo
struct UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// UnityEngine.WWWForm
struct WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#define ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_TBF3E21C3639CF1A14BDC9173530DC13D45540795_H
#ifndef ADSIPC_T40A6B57B6815A25E9500D3966E4146937971FD85_H
#define ADSIPC_T40A6B57B6815A25E9500D3966E4146937971FD85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AdsIPC
struct  AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85  : public RuntimeObject
{
public:

public:
};

struct AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields
{
public:
	// System.String UnityEngine.Purchasing.AdsIPC::adsAdvertisementClassName
	String_t* ___adsAdvertisementClassName_0;
	// System.String UnityEngine.Purchasing.AdsIPC::adsMessageSendName
	String_t* ___adsMessageSendName_1;
	// System.Type UnityEngine.Purchasing.AdsIPC::adsAdvertisementType
	Type_t * ___adsAdvertisementType_2;
	// System.Reflection.MethodInfo UnityEngine.Purchasing.AdsIPC::adsMessageSend
	MethodInfo_t * ___adsMessageSend_3;

public:
	inline static int32_t get_offset_of_adsAdvertisementClassName_0() { return static_cast<int32_t>(offsetof(AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields, ___adsAdvertisementClassName_0)); }
	inline String_t* get_adsAdvertisementClassName_0() const { return ___adsAdvertisementClassName_0; }
	inline String_t** get_address_of_adsAdvertisementClassName_0() { return &___adsAdvertisementClassName_0; }
	inline void set_adsAdvertisementClassName_0(String_t* value)
	{
		___adsAdvertisementClassName_0 = value;
		Il2CppCodeGenWriteBarrier((&___adsAdvertisementClassName_0), value);
	}

	inline static int32_t get_offset_of_adsMessageSendName_1() { return static_cast<int32_t>(offsetof(AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields, ___adsMessageSendName_1)); }
	inline String_t* get_adsMessageSendName_1() const { return ___adsMessageSendName_1; }
	inline String_t** get_address_of_adsMessageSendName_1() { return &___adsMessageSendName_1; }
	inline void set_adsMessageSendName_1(String_t* value)
	{
		___adsMessageSendName_1 = value;
		Il2CppCodeGenWriteBarrier((&___adsMessageSendName_1), value);
	}

	inline static int32_t get_offset_of_adsAdvertisementType_2() { return static_cast<int32_t>(offsetof(AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields, ___adsAdvertisementType_2)); }
	inline Type_t * get_adsAdvertisementType_2() const { return ___adsAdvertisementType_2; }
	inline Type_t ** get_address_of_adsAdvertisementType_2() { return &___adsAdvertisementType_2; }
	inline void set_adsAdvertisementType_2(Type_t * value)
	{
		___adsAdvertisementType_2 = value;
		Il2CppCodeGenWriteBarrier((&___adsAdvertisementType_2), value);
	}

	inline static int32_t get_offset_of_adsMessageSend_3() { return static_cast<int32_t>(offsetof(AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields, ___adsMessageSend_3)); }
	inline MethodInfo_t * get_adsMessageSend_3() const { return ___adsMessageSend_3; }
	inline MethodInfo_t ** get_address_of_adsMessageSend_3() { return &___adsMessageSend_3; }
	inline void set_adsMessageSend_3(MethodInfo_t * value)
	{
		___adsMessageSend_3 = value;
		Il2CppCodeGenWriteBarrier((&___adsMessageSend_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSIPC_T40A6B57B6815A25E9500D3966E4146937971FD85_H
#ifndef ANDROIDJAVASTORE_T47B0807D750DE25AD6ED63F99B05E3DE942BC47C_H
#define ANDROIDJAVASTORE_T47B0807D750DE25AD6ED63F99B05E3DE942BC47C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AndroidJavaStore
struct  AndroidJavaStore_t47B0807D750DE25AD6ED63F99B05E3DE942BC47C  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AndroidJavaStore::m_Store
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___m_Store_0;

public:
	inline static int32_t get_offset_of_m_Store_0() { return static_cast<int32_t>(offsetof(AndroidJavaStore_t47B0807D750DE25AD6ED63F99B05E3DE942BC47C, ___m_Store_0)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_m_Store_0() const { return ___m_Store_0; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_m_Store_0() { return &___m_Store_0; }
	inline void set_m_Store_0(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___m_Store_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Store_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVASTORE_T47B0807D750DE25AD6ED63F99B05E3DE942BC47C_H
#ifndef U3CU3EC_T26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_H
#define U3CU3EC_T26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl_<>c
struct  U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields
{
public:
	// UnityEngine.Purchasing.AppleStoreImpl_<>c UnityEngine.Purchasing.AppleStoreImpl_<>c::<>9
	U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A * ___U3CU3E9_0;
	// System.Comparison`1<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt> UnityEngine.Purchasing.AppleStoreImpl_<>c::<>9__23_1
	Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 * ___U3CU3E9__23_1_1;
	// System.Comparison`1<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt> UnityEngine.Purchasing.AppleStoreImpl_<>c::<>9__40_1
	Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 * ___U3CU3E9__40_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields, ___U3CU3E9__23_1_1)); }
	inline Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 * get_U3CU3E9__23_1_1() const { return ___U3CU3E9__23_1_1; }
	inline Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 ** get_address_of_U3CU3E9__23_1_1() { return &___U3CU3E9__23_1_1; }
	inline void set_U3CU3E9__23_1_1(Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 * value)
	{
		___U3CU3E9__23_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields, ___U3CU3E9__40_1_2)); }
	inline Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 * get_U3CU3E9__40_1_2() const { return ___U3CU3E9__40_1_2; }
	inline Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 ** get_address_of_U3CU3E9__40_1_2() { return &___U3CU3E9__40_1_2; }
	inline void set_U3CU3E9__40_1_2(Comparison_1_t8BDF56C4A474C688A9C99B5D31D8A9E3259606A1 * value)
	{
		___U3CU3E9__40_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_TFDA2BE9F4812295D8320C3419EB89D9891744A71_H
#define U3CU3EC__DISPLAYCLASS23_0_TFDA2BE9F4812295D8320C3419EB89D9891744A71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_tFDA2BE9F4812295D8320C3419EB89D9891744A71  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.ProductDescription UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass23_0::productDescription
	ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582 * ___productDescription_0;

public:
	inline static int32_t get_offset_of_productDescription_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_tFDA2BE9F4812295D8320C3419EB89D9891744A71, ___productDescription_0)); }
	inline ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582 * get_productDescription_0() const { return ___productDescription_0; }
	inline ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582 ** get_address_of_productDescription_0() { return &___productDescription_0; }
	inline void set_productDescription_0(ProductDescription_t28F70353B3E87B193B6E0A509070EC5976871582 * value)
	{
		___productDescription_0 = value;
		Il2CppCodeGenWriteBarrier((&___productDescription_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_TFDA2BE9F4812295D8320C3419EB89D9891744A71_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T1644DE8B3C377DAB1B327FC0CEA987F63D678E2D_H
#define U3CU3EC__DISPLAYCLASS36_0_T1644DE8B3C377DAB1B327FC0CEA987F63D678E2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::subject
	String_t* ___subject_0;
	// System.String UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::payload
	String_t* ___payload_1;
	// System.String UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::receipt
	String_t* ___receipt_2;
	// System.String UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass36_0::transactionId
	String_t* ___transactionId_3;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D, ___subject_0)); }
	inline String_t* get_subject_0() const { return ___subject_0; }
	inline String_t** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(String_t* value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier((&___subject_0), value);
	}

	inline static int32_t get_offset_of_payload_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D, ___payload_1)); }
	inline String_t* get_payload_1() const { return ___payload_1; }
	inline String_t** get_address_of_payload_1() { return &___payload_1; }
	inline void set_payload_1(String_t* value)
	{
		___payload_1 = value;
		Il2CppCodeGenWriteBarrier((&___payload_1), value);
	}

	inline static int32_t get_offset_of_receipt_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D, ___receipt_2)); }
	inline String_t* get_receipt_2() const { return ___receipt_2; }
	inline String_t** get_address_of_receipt_2() { return &___receipt_2; }
	inline void set_receipt_2(String_t* value)
	{
		___receipt_2 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_2), value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T1644DE8B3C377DAB1B327FC0CEA987F63D678E2D_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_TA262F57AA743AF89BF0463BEBF334BCCB53C35EC_H
#define U3CU3EC__DISPLAYCLASS40_0_TA262F57AA743AF89BF0463BEBF334BCCB53C35EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_tA262F57AA743AF89BF0463BEBF334BCCB53C35EC  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.AppleStoreImpl_<>c__DisplayClass40_0::id
	String_t* ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_tA262F57AA743AF89BF0463BEBF334BCCB53C35EC, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_TA262F57AA743AF89BF0463BEBF334BCCB53C35EC_H
#ifndef U3CDOINVOKEU3ED__3_T0DE1C36FE99539931C000CF809F9C388389442D4_H
#define U3CDOINVOKEU3ED__3_T0DE1C36FE99539931C000CF809F9C388389442D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3
struct  U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::a
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___a_2;
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::delayInSeconds
	int32_t ___delayInSeconds_3;
	// UnityEngine.Purchasing.AsyncWebUtil UnityEngine.Purchasing.AsyncWebUtil_<DoInvoke>d__3::<>4__this
	AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_a_2() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4, ___a_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_a_2() const { return ___a_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_a_2() { return &___a_2; }
	inline void set_a_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___a_2 = value;
		Il2CppCodeGenWriteBarrier((&___a_2), value);
	}

	inline static int32_t get_offset_of_delayInSeconds_3() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4, ___delayInSeconds_3)); }
	inline int32_t get_delayInSeconds_3() const { return ___delayInSeconds_3; }
	inline int32_t* get_address_of_delayInSeconds_3() { return &___delayInSeconds_3; }
	inline void set_delayInSeconds_3(int32_t value)
	{
		___delayInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4, ___U3CU3E4__this_4)); }
	inline AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOINVOKEU3ED__3_T0DE1C36FE99539931C000CF809F9C388389442D4_H
#ifndef U3CPROCESSU3ED__4_T1152D7AD22CCD38A061E06047FDF8166A79AFD8B_H
#define U3CPROCESSU3ED__4_T1152D7AD22CCD38A061E06047FDF8166A79AFD8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4
struct  U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.WWW UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::request
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___request_2;
	// System.Action`1<System.String> UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::responseHandler
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___responseHandler_3;
	// System.Action`1<System.String> UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::errorHandler
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___errorHandler_4;
	// System.Int32 UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::maxTimeoutInSeconds
	int32_t ___maxTimeoutInSeconds_5;
	// UnityEngine.Purchasing.AsyncWebUtil UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::<>4__this
	AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 * ___U3CU3E4__this_6;
	// System.Single UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::<timer>5__1
	float ___U3CtimerU3E5__1_7;
	// System.Boolean UnityEngine.Purchasing.AsyncWebUtil_<Process>d__4::<hasTimedOut>5__2
	bool ___U3ChasTimedOutU3E5__2_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___request_2)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_request_2() const { return ___request_2; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier((&___request_2), value);
	}

	inline static int32_t get_offset_of_responseHandler_3() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___responseHandler_3)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_responseHandler_3() const { return ___responseHandler_3; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_responseHandler_3() { return &___responseHandler_3; }
	inline void set_responseHandler_3(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___responseHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___responseHandler_3), value);
	}

	inline static int32_t get_offset_of_errorHandler_4() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___errorHandler_4)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_errorHandler_4() const { return ___errorHandler_4; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_errorHandler_4() { return &___errorHandler_4; }
	inline void set_errorHandler_4(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___errorHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___errorHandler_4), value);
	}

	inline static int32_t get_offset_of_maxTimeoutInSeconds_5() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___maxTimeoutInSeconds_5)); }
	inline int32_t get_maxTimeoutInSeconds_5() const { return ___maxTimeoutInSeconds_5; }
	inline int32_t* get_address_of_maxTimeoutInSeconds_5() { return &___maxTimeoutInSeconds_5; }
	inline void set_maxTimeoutInSeconds_5(int32_t value)
	{
		___maxTimeoutInSeconds_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___U3CU3E4__this_6)); }
	inline AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_6), value);
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___U3CtimerU3E5__1_7)); }
	inline float get_U3CtimerU3E5__1_7() const { return ___U3CtimerU3E5__1_7; }
	inline float* get_address_of_U3CtimerU3E5__1_7() { return &___U3CtimerU3E5__1_7; }
	inline void set_U3CtimerU3E5__1_7(float value)
	{
		___U3CtimerU3E5__1_7 = value;
	}

	inline static int32_t get_offset_of_U3ChasTimedOutU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B, ___U3ChasTimedOutU3E5__2_8)); }
	inline bool get_U3ChasTimedOutU3E5__2_8() const { return ___U3ChasTimedOutU3E5__2_8; }
	inline bool* get_address_of_U3ChasTimedOutU3E5__2_8() { return &___U3ChasTimedOutU3E5__2_8; }
	inline void set_U3ChasTimedOutU3E5__2_8(bool value)
	{
		___U3ChasTimedOutU3E5__2_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROCESSU3ED__4_T1152D7AD22CCD38A061E06047FDF8166A79AFD8B_H
#ifndef CLOUDCATALOGIMPL_T855B4F24B3BEBA0F2C54F36337FD060A3BF5D711_H
#define CLOUDCATALOGIMPL_T855B4F24B3BEBA0F2C54F36337FD060A3BF5D711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl
struct  CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.CloudCatalogImpl::m_AsyncUtil
	RuntimeObject* ___m_AsyncUtil_0;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_CacheFileName
	String_t* ___m_CacheFileName_1;
	// UnityEngine.ILogger UnityEngine.Purchasing.CloudCatalogImpl::m_Logger
	RuntimeObject* ___m_Logger_2;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_CatalogURL
	String_t* ___m_CatalogURL_3;
	// System.String UnityEngine.Purchasing.CloudCatalogImpl::m_StoreName
	String_t* ___m_StoreName_4;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711, ___m_AsyncUtil_0)); }
	inline RuntimeObject* get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline RuntimeObject** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(RuntimeObject* value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncUtil_0), value);
	}

	inline static int32_t get_offset_of_m_CacheFileName_1() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711, ___m_CacheFileName_1)); }
	inline String_t* get_m_CacheFileName_1() const { return ___m_CacheFileName_1; }
	inline String_t** get_address_of_m_CacheFileName_1() { return &___m_CacheFileName_1; }
	inline void set_m_CacheFileName_1(String_t* value)
	{
		___m_CacheFileName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CacheFileName_1), value);
	}

	inline static int32_t get_offset_of_m_Logger_2() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711, ___m_Logger_2)); }
	inline RuntimeObject* get_m_Logger_2() const { return ___m_Logger_2; }
	inline RuntimeObject** get_address_of_m_Logger_2() { return &___m_Logger_2; }
	inline void set_m_Logger_2(RuntimeObject* value)
	{
		___m_Logger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_2), value);
	}

	inline static int32_t get_offset_of_m_CatalogURL_3() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711, ___m_CatalogURL_3)); }
	inline String_t* get_m_CatalogURL_3() const { return ___m_CatalogURL_3; }
	inline String_t** get_address_of_m_CatalogURL_3() { return &___m_CatalogURL_3; }
	inline void set_m_CatalogURL_3(String_t* value)
	{
		___m_CatalogURL_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CatalogURL_3), value);
	}

	inline static int32_t get_offset_of_m_StoreName_4() { return static_cast<int32_t>(offsetof(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711, ___m_StoreName_4)); }
	inline String_t* get_m_StoreName_4() const { return ___m_StoreName_4; }
	inline String_t** get_address_of_m_StoreName_4() { return &___m_StoreName_4; }
	inline void set_m_StoreName_4(String_t* value)
	{
		___m_StoreName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDCATALOGIMPL_T855B4F24B3BEBA0F2C54F36337FD060A3BF5D711_H
#ifndef U3CU3EC_T4C74A8971DD4C672DDED78F8E833A48A3538AAF5_H
#define U3CU3EC_T4C74A8971DD4C672DDED78F8E833A48A3538AAF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl_<>c
struct  U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields
{
public:
	// UnityEngine.Purchasing.CloudCatalogImpl_<>c UnityEngine.Purchasing.CloudCatalogImpl_<>c::<>9
	U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5 * ___U3CU3E9_0;
	// System.Func`3<System.Char,System.Int32,System.String> UnityEngine.Purchasing.CloudCatalogImpl_<>c::<>9__12_0
	Func_3_tDBAE4B3898928FD3EB3796F1103233248C8E8B44 * ___U3CU3E9__12_0_1;
	// System.Func`3<System.String,System.String,System.String> UnityEngine.Purchasing.CloudCatalogImpl_<>c::<>9__12_1
	Func_3_t2AADDAD205C862FFE7FFD4E9756DA09107BD8A1A * ___U3CU3E9__12_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_3_tDBAE4B3898928FD3EB3796F1103233248C8E8B44 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_3_tDBAE4B3898928FD3EB3796F1103233248C8E8B44 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_3_tDBAE4B3898928FD3EB3796F1103233248C8E8B44 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields, ___U3CU3E9__12_1_2)); }
	inline Func_3_t2AADDAD205C862FFE7FFD4E9756DA09107BD8A1A * get_U3CU3E9__12_1_2() const { return ___U3CU3E9__12_1_2; }
	inline Func_3_t2AADDAD205C862FFE7FFD4E9756DA09107BD8A1A ** get_address_of_U3CU3E9__12_1_2() { return &___U3CU3E9__12_1_2; }
	inline void set_U3CU3E9__12_1_2(Func_3_t2AADDAD205C862FFE7FFD4E9756DA09107BD8A1A * value)
	{
		___U3CU3E9__12_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4C74A8971DD4C672DDED78F8E833A48A3538AAF5_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7_H
#define U3CU3EC__DISPLAYCLASS10_0_T7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7  : public RuntimeObject
{
public:
	// System.Action`1<System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>> UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::callback
	Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F * ___callback_0;
	// System.Int32 UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::delayInSeconds
	int32_t ___delayInSeconds_1;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<>4__this
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 * ___U3CU3E4__this_2;
	// System.Action UnityEngine.Purchasing.CloudCatalogImpl_<>c__DisplayClass10_0::<>9__2
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__2_3;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7, ___callback_0)); }
	inline Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F * get_callback_0() const { return ___callback_0; }
	inline Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_tE5D773015F59F7E1BD7F007A3462D2361227C43F * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_delayInSeconds_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7, ___delayInSeconds_1)); }
	inline int32_t get_delayInSeconds_1() const { return ___delayInSeconds_1; }
	inline int32_t* get_address_of_delayInSeconds_1() { return &___delayInSeconds_1; }
	inline void set_delayInSeconds_1(int32_t value)
	{
		___delayInSeconds_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7, ___U3CU3E4__this_2)); }
	inline CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7, ___U3CU3E9__2_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__2_3() const { return ___U3CU3E9__2_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__2_3() { return &___U3CU3E9__2_3; }
	inline void set_U3CU3E9__2_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7_H
#ifndef EVENTQUEUE_TD991D20FAD4203922A13A31871F6046F925548FA_H
#define EVENTQUEUE_TD991D20FAD4203922A13A31871F6046F925548FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventQueue
struct  EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.EventQueue::m_AsyncUtil
	RuntimeObject* ___m_AsyncUtil_0;
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.EventQueue::Profile
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * ___Profile_2;
	// System.String UnityEngine.Purchasing.EventQueue::TrackingUrl
	String_t* ___TrackingUrl_3;
	// System.String UnityEngine.Purchasing.EventQueue::EventUrl
	String_t* ___EventUrl_4;
	// System.Object UnityEngine.Purchasing.EventQueue::ProfileDict
	RuntimeObject * ___ProfileDict_5;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA, ___m_AsyncUtil_0)); }
	inline RuntimeObject* get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline RuntimeObject** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(RuntimeObject* value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncUtil_0), value);
	}

	inline static int32_t get_offset_of_Profile_2() { return static_cast<int32_t>(offsetof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA, ___Profile_2)); }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * get_Profile_2() const { return ___Profile_2; }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B ** get_address_of_Profile_2() { return &___Profile_2; }
	inline void set_Profile_2(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * value)
	{
		___Profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_2), value);
	}

	inline static int32_t get_offset_of_TrackingUrl_3() { return static_cast<int32_t>(offsetof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA, ___TrackingUrl_3)); }
	inline String_t* get_TrackingUrl_3() const { return ___TrackingUrl_3; }
	inline String_t** get_address_of_TrackingUrl_3() { return &___TrackingUrl_3; }
	inline void set_TrackingUrl_3(String_t* value)
	{
		___TrackingUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___TrackingUrl_3), value);
	}

	inline static int32_t get_offset_of_EventUrl_4() { return static_cast<int32_t>(offsetof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA, ___EventUrl_4)); }
	inline String_t* get_EventUrl_4() const { return ___EventUrl_4; }
	inline String_t** get_address_of_EventUrl_4() { return &___EventUrl_4; }
	inline void set_EventUrl_4(String_t* value)
	{
		___EventUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___EventUrl_4), value);
	}

	inline static int32_t get_offset_of_ProfileDict_5() { return static_cast<int32_t>(offsetof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA, ___ProfileDict_5)); }
	inline RuntimeObject * get_ProfileDict_5() const { return ___ProfileDict_5; }
	inline RuntimeObject ** get_address_of_ProfileDict_5() { return &___ProfileDict_5; }
	inline void set_ProfileDict_5(RuntimeObject * value)
	{
		___ProfileDict_5 = value;
		Il2CppCodeGenWriteBarrier((&___ProfileDict_5), value);
	}
};

struct EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA_StaticFields
{
public:
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.EventQueue::QueueInstance
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * ___QueueInstance_1;

public:
	inline static int32_t get_offset_of_QueueInstance_1() { return static_cast<int32_t>(offsetof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA_StaticFields, ___QueueInstance_1)); }
	inline EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * get_QueueInstance_1() const { return ___QueueInstance_1; }
	inline EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA ** get_address_of_QueueInstance_1() { return &___QueueInstance_1; }
	inline void set_QueueInstance_1(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * value)
	{
		___QueueInstance_1 = value;
		Il2CppCodeGenWriteBarrier((&___QueueInstance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTQUEUE_TD991D20FAD4203922A13A31871F6046F925548FA_H
#ifndef U3CU3EC_TFE92D889B6366334C816C2D16974499284C0F302_H
#define U3CU3EC_TFE92D889B6366334C816C2D16974499284C0F302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventQueue_<>c
struct  U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields
{
public:
	// UnityEngine.Purchasing.EventQueue_<>c UnityEngine.Purchasing.EventQueue_<>c::<>9
	U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302 * ___U3CU3E9_0;
	// System.Action`1<System.String> UnityEngine.Purchasing.EventQueue_<>c::<>9__11_0
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__11_0_1;
	// System.Action`1<System.String> UnityEngine.Purchasing.EventQueue_<>c::<>9__11_3
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__11_3_2;
	// System.Action`1<System.String> UnityEngine.Purchasing.EventQueue_<>c::<>9__11_4
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__11_4_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_3_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields, ___U3CU3E9__11_3_2)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__11_3_2() const { return ___U3CU3E9__11_3_2; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__11_3_2() { return &___U3CU3E9__11_3_2; }
	inline void set_U3CU3E9__11_3_2(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__11_3_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_3_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_4_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields, ___U3CU3E9__11_4_3)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__11_4_3() const { return ___U3CU3E9__11_4_3; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__11_4_3() { return &___U3CU3E9__11_4_3; }
	inline void set_U3CU3E9__11_4_3(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__11_4_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_4_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFE92D889B6366334C816C2D16974499284C0F302_H
#ifndef ABSTRACTPURCHASINGMODULE_T73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9_H
#define ABSTRACTPURCHASINGMODULE_T73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractPurchasingModule
struct  AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.IPurchasingBinder UnityEngine.Purchasing.Extension.AbstractPurchasingModule::m_Binder
	RuntimeObject* ___m_Binder_0;

public:
	inline static int32_t get_offset_of_m_Binder_0() { return static_cast<int32_t>(offsetof(AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9, ___m_Binder_0)); }
	inline RuntimeObject* get_m_Binder_0() const { return ___m_Binder_0; }
	inline RuntimeObject** get_address_of_m_Binder_0() { return &___m_Binder_0; }
	inline void set_m_Binder_0(RuntimeObject* value)
	{
		___m_Binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTPURCHASINGMODULE_T73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9_H
#ifndef ABSTRACTSTORE_T0809CE12BFD8191F29B7B77FC730AE3E8A3C039C_H
#define ABSTRACTSTORE_T0809CE12BFD8191F29B7B77FC730AE3E8A3C039C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractStore
struct  AbstractStore_t0809CE12BFD8191F29B7B77FC730AE3E8A3C039C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTSTORE_T0809CE12BFD8191F29B7B77FC730AE3E8A3C039C_H
#ifndef FAKEAPPLECONFIGUATION_T7AF8F624290C9E5621BDAAC0E0E55ED674AA397C_H
#define FAKEAPPLECONFIGUATION_T7AF8F624290C9E5621BDAAC0E0E55ED674AA397C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAppleConfiguation
struct  FakeAppleConfiguation_t7AF8F624290C9E5621BDAAC0E0E55ED674AA397C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEAPPLECONFIGUATION_T7AF8F624290C9E5621BDAAC0E0E55ED674AA397C_H
#ifndef FAKEAPPLEEXTENSIONS_T782746A593A86D53022520A8E9D6ACDD12851339_H
#define FAKEAPPLEEXTENSIONS_T782746A593A86D53022520A8E9D6ACDD12851339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAppleExtensions
struct  FakeAppleExtensions_t782746A593A86D53022520A8E9D6ACDD12851339  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEAPPLEEXTENSIONS_T782746A593A86D53022520A8E9D6ACDD12851339_H
#ifndef FAKEGOOGLEPLAYCONFIGURATION_T21A6F3FDC321BD44D61A5E2A889F9AE12CA89C1D_H
#define FAKEGOOGLEPLAYCONFIGURATION_T21A6F3FDC321BD44D61A5E2A889F9AE12CA89C1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeGooglePlayConfiguration
struct  FakeGooglePlayConfiguration_t21A6F3FDC321BD44D61A5E2A889F9AE12CA89C1D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGOOGLEPLAYCONFIGURATION_T21A6F3FDC321BD44D61A5E2A889F9AE12CA89C1D_H
#ifndef FAKEGOOGLEPLAYSTOREEXTENSIONS_T5A3CEF4FC2B96358C4A701E3619B0E81DE5C25DC_H
#define FAKEGOOGLEPLAYSTOREEXTENSIONS_T5A3CEF4FC2B96358C4A701E3619B0E81DE5C25DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeGooglePlayStoreExtensions
struct  FakeGooglePlayStoreExtensions_t5A3CEF4FC2B96358C4A701E3619B0E81DE5C25DC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGOOGLEPLAYSTOREEXTENSIONS_T5A3CEF4FC2B96358C4A701E3619B0E81DE5C25DC_H
#ifndef FAKEMANAGEDSTOREEXTENSIONS_TD84A7039DFB9134F25B2BAE1F03F0437185FEEBA_H
#define FAKEMANAGEDSTOREEXTENSIONS_TD84A7039DFB9134F25B2BAE1F03F0437185FEEBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeManagedStoreExtensions
struct  FakeManagedStoreExtensions_tD84A7039DFB9134F25B2BAE1F03F0437185FEEBA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMANAGEDSTOREEXTENSIONS_TD84A7039DFB9134F25B2BAE1F03F0437185FEEBA_H
#ifndef FAKESAMSUNGAPPSEXTENSIONS_T619D148D530ACFAB81F62401C4381C3808E52C7A_H
#define FAKESAMSUNGAPPSEXTENSIONS_T619D148D530ACFAB81F62401C4381C3808E52C7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeSamsungAppsExtensions
struct  FakeSamsungAppsExtensions_t619D148D530ACFAB81F62401C4381C3808E52C7A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESAMSUNGAPPSEXTENSIONS_T619D148D530ACFAB81F62401C4381C3808E52C7A_H
#ifndef FAKEUDPEXTENSION_TAC5FB9DBD92A0F3F6F5C19899EC8774E98C78E4F_H
#define FAKEUDPEXTENSION_TAC5FB9DBD92A0F3F6F5C19899EC8774E98C78E4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUDPExtension
struct  FakeUDPExtension_tAC5FB9DBD92A0F3F6F5C19899EC8774E98C78E4F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUDPEXTENSION_TAC5FB9DBD92A0F3F6F5C19899EC8774E98C78E4F_H
#ifndef FAKEUNITYCHANNELCONFIGURATION_TB56C2BBD4B0E46D13B12DD6614588B970C32E492_H
#define FAKEUNITYCHANNELCONFIGURATION_TB56C2BBD4B0E46D13B12DD6614588B970C32E492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUnityChannelConfiguration
struct  FakeUnityChannelConfiguration_tB56C2BBD4B0E46D13B12DD6614588B970C32E492  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.FakeUnityChannelConfiguration::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FakeUnityChannelConfiguration_tB56C2BBD4B0E46D13B12DD6614588B970C32E492, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUNITYCHANNELCONFIGURATION_TB56C2BBD4B0E46D13B12DD6614588B970C32E492_H
#ifndef FAKEUNITYCHANNELEXTENSIONS_T4FCE1F11459746A7CCE88B2324584A96455E695D_H
#define FAKEUNITYCHANNELEXTENSIONS_T4FCE1F11459746A7CCE88B2324584A96455E695D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUnityChannelExtensions
struct  FakeUnityChannelExtensions_t4FCE1F11459746A7CCE88B2324584A96455E695D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUNITYCHANNELEXTENSIONS_T4FCE1F11459746A7CCE88B2324584A96455E695D_H
#ifndef U3CU3EC_T476CCA496FD17E1BD2E214B94F59290219BFF57B_H
#define U3CU3EC_T476CCA496FD17E1BD2E214B94F59290219BFF57B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.LocalizedProductDescription_<>c
struct  U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B_StaticFields
{
public:
	// UnityEngine.Purchasing.LocalizedProductDescription_<>c UnityEngine.Purchasing.LocalizedProductDescription_<>c::<>9
	U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B * ___U3CU3E9_0;
	// System.Text.RegularExpressions.MatchEvaluator UnityEngine.Purchasing.LocalizedProductDescription_<>c::<>9__11_0
	MatchEvaluator_tB0F0EC2FE9D878BB2D0F90CCAE27AA57E8107BE2 * ___U3CU3E9__11_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B_StaticFields, ___U3CU3E9__11_0_1)); }
	inline MatchEvaluator_tB0F0EC2FE9D878BB2D0F90CCAE27AA57E8107BE2 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline MatchEvaluator_tB0F0EC2FE9D878BB2D0F90CCAE27AA57E8107BE2 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(MatchEvaluator_tB0F0EC2FE9D878BB2D0F90CCAE27AA57E8107BE2 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T476CCA496FD17E1BD2E214B94F59290219BFF57B_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T8F3D6EE637D15A39F5356E317F46604DF0D2C948_H
#define U3CU3EC__DISPLAYCLASS18_0_T8F3D6EE637D15A39F5356E317F46604DF0D2C948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948  : public RuntimeObject
{
public:
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::purchaseSucceed
	Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * ___purchaseSucceed_0;
	// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::purchaseFailed
	Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 * ___purchaseFailed_1;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl_<>c__DisplayClass18_0::<>4__this
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_purchaseSucceed_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948, ___purchaseSucceed_0)); }
	inline Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * get_purchaseSucceed_0() const { return ___purchaseSucceed_0; }
	inline Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 ** get_address_of_purchaseSucceed_0() { return &___purchaseSucceed_0; }
	inline void set_purchaseSucceed_0(Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * value)
	{
		___purchaseSucceed_0 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseSucceed_0), value);
	}

	inline static int32_t get_offset_of_purchaseFailed_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948, ___purchaseFailed_1)); }
	inline Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 * get_purchaseFailed_1() const { return ___purchaseFailed_1; }
	inline Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 ** get_address_of_purchaseFailed_1() { return &___purchaseFailed_1; }
	inline void set_purchaseFailed_1(Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 * value)
	{
		___purchaseFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseFailed_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948, ___U3CU3E4__this_2)); }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T8F3D6EE637D15A39F5356E317F46604DF0D2C948_H
#ifndef U3CREQUESTAUTHCODEU3ED__22_T68AAA3F001C3B2A1E8290F19D5BB8481601D1F52_H
#define U3CREQUESTAUTHCODEU3ED__22_T68AAA3F001C3B2A1E8290F19D5BB8481601D1F52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22
struct  U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::wf
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___wf_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::productID
	String_t* ___productID_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::transactionId
	String_t* ___transactionId_4;
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::succeed
	Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * ___succeed_5;
	// System.Action`2<System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::failed
	Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * ___failed_6;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<>4__this
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * ___U3CU3E4__this_7;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<w>5__1
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwU3E5__1_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<authCodeResult>5__2
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CauthCodeResultU3E5__2_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<authCodeValues>5__3
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CauthCodeValuesU3E5__3_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<authCode>5__4
	String_t* ___U3CauthCodeU3E5__4_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RequestAuthCode>d__22::<paymentURL>5__5
	String_t* ___U3CpaymentURLU3E5__5_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_wf_2() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___wf_2)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_wf_2() const { return ___wf_2; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_wf_2() { return &___wf_2; }
	inline void set_wf_2(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___wf_2 = value;
		Il2CppCodeGenWriteBarrier((&___wf_2), value);
	}

	inline static int32_t get_offset_of_productID_3() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___productID_3)); }
	inline String_t* get_productID_3() const { return ___productID_3; }
	inline String_t** get_address_of_productID_3() { return &___productID_3; }
	inline void set_productID_3(String_t* value)
	{
		___productID_3 = value;
		Il2CppCodeGenWriteBarrier((&___productID_3), value);
	}

	inline static int32_t get_offset_of_transactionId_4() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___transactionId_4)); }
	inline String_t* get_transactionId_4() const { return ___transactionId_4; }
	inline String_t** get_address_of_transactionId_4() { return &___transactionId_4; }
	inline void set_transactionId_4(String_t* value)
	{
		___transactionId_4 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_4), value);
	}

	inline static int32_t get_offset_of_succeed_5() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___succeed_5)); }
	inline Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * get_succeed_5() const { return ___succeed_5; }
	inline Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 ** get_address_of_succeed_5() { return &___succeed_5; }
	inline void set_succeed_5(Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * value)
	{
		___succeed_5 = value;
		Il2CppCodeGenWriteBarrier((&___succeed_5), value);
	}

	inline static int32_t get_offset_of_failed_6() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___failed_6)); }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * get_failed_6() const { return ___failed_6; }
	inline Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 ** get_address_of_failed_6() { return &___failed_6; }
	inline void set_failed_6(Action_2_t65389ADBA2215CECAB0D4657DD37369A19FCD757 * value)
	{
		___failed_6 = value;
		Il2CppCodeGenWriteBarrier((&___failed_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CU3E4__this_7)); }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CwU3E5__1_8)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwU3E5__1_8() const { return ___U3CwU3E5__1_8; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwU3E5__1_8() { return &___U3CwU3E5__1_8; }
	inline void set_U3CwU3E5__1_8(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CauthCodeResultU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CauthCodeResultU3E5__2_9)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CauthCodeResultU3E5__2_9() const { return ___U3CauthCodeResultU3E5__2_9; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CauthCodeResultU3E5__2_9() { return &___U3CauthCodeResultU3E5__2_9; }
	inline void set_U3CauthCodeResultU3E5__2_9(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CauthCodeResultU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthCodeResultU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CauthCodeValuesU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CauthCodeValuesU3E5__3_10)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CauthCodeValuesU3E5__3_10() const { return ___U3CauthCodeValuesU3E5__3_10; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CauthCodeValuesU3E5__3_10() { return &___U3CauthCodeValuesU3E5__3_10; }
	inline void set_U3CauthCodeValuesU3E5__3_10(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CauthCodeValuesU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthCodeValuesU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CauthCodeU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CauthCodeU3E5__4_11)); }
	inline String_t* get_U3CauthCodeU3E5__4_11() const { return ___U3CauthCodeU3E5__4_11; }
	inline String_t** get_address_of_U3CauthCodeU3E5__4_11() { return &___U3CauthCodeU3E5__4_11; }
	inline void set_U3CauthCodeU3E5__4_11(String_t* value)
	{
		___U3CauthCodeU3E5__4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthCodeU3E5__4_11), value);
	}

	inline static int32_t get_offset_of_U3CpaymentURLU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52, ___U3CpaymentURLU3E5__5_12)); }
	inline String_t* get_U3CpaymentURLU3E5__5_12() const { return ___U3CpaymentURLU3E5__5_12; }
	inline String_t** get_address_of_U3CpaymentURLU3E5__5_12() { return &___U3CpaymentURLU3E5__5_12; }
	inline void set_U3CpaymentURLU3E5__5_12(String_t* value)
	{
		___U3CpaymentURLU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpaymentURLU3E5__5_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREQUESTAUTHCODEU3ED__22_T68AAA3F001C3B2A1E8290F19D5BB8481601D1F52_H
#ifndef U3CSTARTPURCHASEPOLLINGU3ED__23_TD74DB4FBB97235040F28631B87389D3F24AFD93E_H
#define U3CSTARTPURCHASEPOLLINGU3ED__23_TD74DB4FBB97235040F28631B87389D3F24AFD93E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23
struct  U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::authGlobal
	String_t* ___authGlobal_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::transactionId
	String_t* ___transactionId_3;
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::purchaseSucceed
	Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * ___purchaseSucceed_4;
	// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::purchaseFailed
	Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 * ___purchaseFailed_5;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<>4__this
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * ___U3CU3E4__this_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<orderSuccess>5__1
	String_t* ___U3CorderSuccessU3E5__1_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<signstr>5__2
	String_t* ___U3CsignstrU3E5__2_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<sign>5__3
	String_t* ___U3CsignU3E5__3_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<param>5__4
	String_t* ___U3CparamU3E5__4_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<url>5__5
	String_t* ___U3CurlU3E5__5_11;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<pollingstr>5__6
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CpollingstrU3E5__6_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<jsonPollingObjects>5__7
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CjsonPollingObjectsU3E5__7_13;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<code>5__8
	String_t* ___U3CcodeU3E5__8_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<pollingValues>5__9
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CpollingValuesU3E5__9_15;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<tradeSeq>5__10
	String_t* ___U3CtradeSeqU3E5__10_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<tradeState>5__11
	String_t* ___U3CtradeStateU3E5__11_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<productId>5__12
	String_t* ___U3CproductIdU3E5__12_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<Msg>5__13
	String_t* ___U3CMsgU3E5__13_19;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<StartPurchasePolling>d__23::<receipt>5__14
	String_t* ___U3CreceiptU3E5__14_20;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_authGlobal_2() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___authGlobal_2)); }
	inline String_t* get_authGlobal_2() const { return ___authGlobal_2; }
	inline String_t** get_address_of_authGlobal_2() { return &___authGlobal_2; }
	inline void set_authGlobal_2(String_t* value)
	{
		___authGlobal_2 = value;
		Il2CppCodeGenWriteBarrier((&___authGlobal_2), value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_3), value);
	}

	inline static int32_t get_offset_of_purchaseSucceed_4() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___purchaseSucceed_4)); }
	inline Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * get_purchaseSucceed_4() const { return ___purchaseSucceed_4; }
	inline Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 ** get_address_of_purchaseSucceed_4() { return &___purchaseSucceed_4; }
	inline void set_purchaseSucceed_4(Action_3_t11931CAC7A1866BF35E7316C0DFBAC801800D274 * value)
	{
		___purchaseSucceed_4 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseSucceed_4), value);
	}

	inline static int32_t get_offset_of_purchaseFailed_5() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___purchaseFailed_5)); }
	inline Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 * get_purchaseFailed_5() const { return ___purchaseFailed_5; }
	inline Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 ** get_address_of_purchaseFailed_5() { return &___purchaseFailed_5; }
	inline void set_purchaseFailed_5(Action_3_t3B461E68F5C0BCFB512BAA8D2CAA7F05A8F07C39 * value)
	{
		___purchaseFailed_5 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseFailed_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CU3E4__this_6)); }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_6), value);
	}

	inline static int32_t get_offset_of_U3CorderSuccessU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CorderSuccessU3E5__1_7)); }
	inline String_t* get_U3CorderSuccessU3E5__1_7() const { return ___U3CorderSuccessU3E5__1_7; }
	inline String_t** get_address_of_U3CorderSuccessU3E5__1_7() { return &___U3CorderSuccessU3E5__1_7; }
	inline void set_U3CorderSuccessU3E5__1_7(String_t* value)
	{
		___U3CorderSuccessU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CorderSuccessU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CsignstrU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CsignstrU3E5__2_8)); }
	inline String_t* get_U3CsignstrU3E5__2_8() const { return ___U3CsignstrU3E5__2_8; }
	inline String_t** get_address_of_U3CsignstrU3E5__2_8() { return &___U3CsignstrU3E5__2_8; }
	inline void set_U3CsignstrU3E5__2_8(String_t* value)
	{
		___U3CsignstrU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignstrU3E5__2_8), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_9() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CsignU3E5__3_9)); }
	inline String_t* get_U3CsignU3E5__3_9() const { return ___U3CsignU3E5__3_9; }
	inline String_t** get_address_of_U3CsignU3E5__3_9() { return &___U3CsignU3E5__3_9; }
	inline void set_U3CsignU3E5__3_9(String_t* value)
	{
		___U3CsignU3E5__3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__3_9), value);
	}

	inline static int32_t get_offset_of_U3CparamU3E5__4_10() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CparamU3E5__4_10)); }
	inline String_t* get_U3CparamU3E5__4_10() const { return ___U3CparamU3E5__4_10; }
	inline String_t** get_address_of_U3CparamU3E5__4_10() { return &___U3CparamU3E5__4_10; }
	inline void set_U3CparamU3E5__4_10(String_t* value)
	{
		___U3CparamU3E5__4_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparamU3E5__4_10), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E5__5_11() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CurlU3E5__5_11)); }
	inline String_t* get_U3CurlU3E5__5_11() const { return ___U3CurlU3E5__5_11; }
	inline String_t** get_address_of_U3CurlU3E5__5_11() { return &___U3CurlU3E5__5_11; }
	inline void set_U3CurlU3E5__5_11(String_t* value)
	{
		___U3CurlU3E5__5_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E5__5_11), value);
	}

	inline static int32_t get_offset_of_U3CpollingstrU3E5__6_12() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CpollingstrU3E5__6_12)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CpollingstrU3E5__6_12() const { return ___U3CpollingstrU3E5__6_12; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CpollingstrU3E5__6_12() { return &___U3CpollingstrU3E5__6_12; }
	inline void set_U3CpollingstrU3E5__6_12(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CpollingstrU3E5__6_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpollingstrU3E5__6_12), value);
	}

	inline static int32_t get_offset_of_U3CjsonPollingObjectsU3E5__7_13() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CjsonPollingObjectsU3E5__7_13)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CjsonPollingObjectsU3E5__7_13() const { return ___U3CjsonPollingObjectsU3E5__7_13; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CjsonPollingObjectsU3E5__7_13() { return &___U3CjsonPollingObjectsU3E5__7_13; }
	inline void set_U3CjsonPollingObjectsU3E5__7_13(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CjsonPollingObjectsU3E5__7_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonPollingObjectsU3E5__7_13), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__8_14() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CcodeU3E5__8_14)); }
	inline String_t* get_U3CcodeU3E5__8_14() const { return ___U3CcodeU3E5__8_14; }
	inline String_t** get_address_of_U3CcodeU3E5__8_14() { return &___U3CcodeU3E5__8_14; }
	inline void set_U3CcodeU3E5__8_14(String_t* value)
	{
		___U3CcodeU3E5__8_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__8_14), value);
	}

	inline static int32_t get_offset_of_U3CpollingValuesU3E5__9_15() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CpollingValuesU3E5__9_15)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CpollingValuesU3E5__9_15() const { return ___U3CpollingValuesU3E5__9_15; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CpollingValuesU3E5__9_15() { return &___U3CpollingValuesU3E5__9_15; }
	inline void set_U3CpollingValuesU3E5__9_15(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CpollingValuesU3E5__9_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpollingValuesU3E5__9_15), value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__10_16() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CtradeSeqU3E5__10_16)); }
	inline String_t* get_U3CtradeSeqU3E5__10_16() const { return ___U3CtradeSeqU3E5__10_16; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__10_16() { return &___U3CtradeSeqU3E5__10_16; }
	inline void set_U3CtradeSeqU3E5__10_16(String_t* value)
	{
		___U3CtradeSeqU3E5__10_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeSeqU3E5__10_16), value);
	}

	inline static int32_t get_offset_of_U3CtradeStateU3E5__11_17() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CtradeStateU3E5__11_17)); }
	inline String_t* get_U3CtradeStateU3E5__11_17() const { return ___U3CtradeStateU3E5__11_17; }
	inline String_t** get_address_of_U3CtradeStateU3E5__11_17() { return &___U3CtradeStateU3E5__11_17; }
	inline void set_U3CtradeStateU3E5__11_17(String_t* value)
	{
		___U3CtradeStateU3E5__11_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeStateU3E5__11_17), value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__12_18() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CproductIdU3E5__12_18)); }
	inline String_t* get_U3CproductIdU3E5__12_18() const { return ___U3CproductIdU3E5__12_18; }
	inline String_t** get_address_of_U3CproductIdU3E5__12_18() { return &___U3CproductIdU3E5__12_18; }
	inline void set_U3CproductIdU3E5__12_18(String_t* value)
	{
		___U3CproductIdU3E5__12_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIdU3E5__12_18), value);
	}

	inline static int32_t get_offset_of_U3CMsgU3E5__13_19() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CMsgU3E5__13_19)); }
	inline String_t* get_U3CMsgU3E5__13_19() const { return ___U3CMsgU3E5__13_19; }
	inline String_t** get_address_of_U3CMsgU3E5__13_19() { return &___U3CMsgU3E5__13_19; }
	inline void set_U3CMsgU3E5__13_19(String_t* value)
	{
		___U3CMsgU3E5__13_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMsgU3E5__13_19), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__14_20() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E, ___U3CreceiptU3E5__14_20)); }
	inline String_t* get_U3CreceiptU3E5__14_20() const { return ___U3CreceiptU3E5__14_20; }
	inline String_t** get_address_of_U3CreceiptU3E5__14_20() { return &___U3CreceiptU3E5__14_20; }
	inline void set_U3CreceiptU3E5__14_20(String_t* value)
	{
		___U3CreceiptU3E5__14_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3E5__14_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTPURCHASEPOLLINGU3ED__23_TD74DB4FBB97235040F28631B87389D3F24AFD93E_H
#ifndef U3CVAILDATEPRODUCTU3ED__13_TB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680_H
#define U3CVAILDATEPRODUCTU3ED__13_TB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13
struct  U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::appkey
	String_t* ___appkey_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::productInfo
	String_t* ___productInfo_3;
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::result
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::<>4__this
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * ___U3CU3E4__this_5;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::<sign>5__1
	String_t* ___U3CsignU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::<wf>5__2
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CwfU3E5__2_7;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl_<VaildateProduct>d__13::<w>5__3
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwU3E5__3_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_appkey_2() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___appkey_2)); }
	inline String_t* get_appkey_2() const { return ___appkey_2; }
	inline String_t** get_address_of_appkey_2() { return &___appkey_2; }
	inline void set_appkey_2(String_t* value)
	{
		___appkey_2 = value;
		Il2CppCodeGenWriteBarrier((&___appkey_2), value);
	}

	inline static int32_t get_offset_of_productInfo_3() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___productInfo_3)); }
	inline String_t* get_productInfo_3() const { return ___productInfo_3; }
	inline String_t** get_address_of_productInfo_3() { return &___productInfo_3; }
	inline void set_productInfo_3(String_t* value)
	{
		___productInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___productInfo_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___result_4)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_result_4() const { return ___result_4; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___U3CsignU3E5__1_6)); }
	inline String_t* get_U3CsignU3E5__1_6() const { return ___U3CsignU3E5__1_6; }
	inline String_t** get_address_of_U3CsignU3E5__1_6() { return &___U3CsignU3E5__1_6; }
	inline void set_U3CsignU3E5__1_6(String_t* value)
	{
		___U3CsignU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680, ___U3CwU3E5__3_8)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwU3E5__3_8() const { return ___U3CwU3E5__3_8; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwU3E5__3_8() { return &___U3CwU3E5__3_8; }
	inline void set_U3CwU3E5__3_8(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVAILDATEPRODUCTU3ED__13_TB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680_H
#ifndef U3CVALIDATERECEIPTPROCESSU3ED__47_T49A8523713B44DEACC6BC525029665BCC9AED057_H
#define U3CVALIDATERECEIPTPROCESSU3ED__47_T49A8523713B44DEACC6BC525029665BCC9AED057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47
struct  U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::transactionId
	String_t* ___transactionId_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::receipt
	String_t* ___receipt_3;
	// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String> UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::result
	Action_3_t3BBBEEA0BD365F8EB85C8F88F570DF3FA425F072 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<>4__this
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * ___U3CU3E4__this_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<tempJson>5__1
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CtempJsonU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<wf>5__2
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CwfU3E5__2_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<sign>5__3
	String_t* ___U3CsignU3E5__3_8;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<w>5__4
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwU3E5__4_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<jsonObjects>5__5
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CjsonObjectsU3E5__5_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<code>5__6
	String_t* ___U3CcodeU3E5__6_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<ValidateReceiptProcess>d__47::<msg>5__7
	String_t* ___U3CmsgU3E5__7_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_transactionId_2() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___transactionId_2)); }
	inline String_t* get_transactionId_2() const { return ___transactionId_2; }
	inline String_t** get_address_of_transactionId_2() { return &___transactionId_2; }
	inline void set_transactionId_2(String_t* value)
	{
		___transactionId_2 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_2), value);
	}

	inline static int32_t get_offset_of_receipt_3() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___receipt_3)); }
	inline String_t* get_receipt_3() const { return ___receipt_3; }
	inline String_t** get_address_of_receipt_3() { return &___receipt_3; }
	inline void set_receipt_3(String_t* value)
	{
		___receipt_3 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___result_4)); }
	inline Action_3_t3BBBEEA0BD365F8EB85C8F88F570DF3FA425F072 * get_result_4() const { return ___result_4; }
	inline Action_3_t3BBBEEA0BD365F8EB85C8F88F570DF3FA425F072 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_3_t3BBBEEA0BD365F8EB85C8F88F570DF3FA425F072 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CtempJsonU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CtempJsonU3E5__1_6)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CtempJsonU3E5__1_6() const { return ___U3CtempJsonU3E5__1_6; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CtempJsonU3E5__1_6() { return &___U3CtempJsonU3E5__1_6; }
	inline void set_U3CtempJsonU3E5__1_6(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CtempJsonU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempJsonU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CsignU3E5__3_8)); }
	inline String_t* get_U3CsignU3E5__3_8() const { return ___U3CsignU3E5__3_8; }
	inline String_t** get_address_of_U3CsignU3E5__3_8() { return &___U3CsignU3E5__3_8; }
	inline void set_U3CsignU3E5__3_8(String_t* value)
	{
		___U3CsignU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CwU3E5__4_9)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwU3E5__4_9() const { return ___U3CwU3E5__4_9; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwU3E5__4_9() { return &___U3CwU3E5__4_9; }
	inline void set_U3CwU3E5__4_9(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__4_9), value);
	}

	inline static int32_t get_offset_of_U3CjsonObjectsU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CjsonObjectsU3E5__5_10)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CjsonObjectsU3E5__5_10() const { return ___U3CjsonObjectsU3E5__5_10; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CjsonObjectsU3E5__5_10() { return &___U3CjsonObjectsU3E5__5_10; }
	inline void set_U3CjsonObjectsU3E5__5_10(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CjsonObjectsU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonObjectsU3E5__5_10), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CcodeU3E5__6_11)); }
	inline String_t* get_U3CcodeU3E5__6_11() const { return ___U3CcodeU3E5__6_11; }
	inline String_t** get_address_of_U3CcodeU3E5__6_11() { return &___U3CcodeU3E5__6_11; }
	inline void set_U3CcodeU3E5__6_11(String_t* value)
	{
		___U3CcodeU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__6_11), value);
	}

	inline static int32_t get_offset_of_U3CmsgU3E5__7_12() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057, ___U3CmsgU3E5__7_12)); }
	inline String_t* get_U3CmsgU3E5__7_12() const { return ___U3CmsgU3E5__7_12; }
	inline String_t** get_address_of_U3CmsgU3E5__7_12() { return &___U3CmsgU3E5__7_12; }
	inline void set_U3CmsgU3E5__7_12(String_t* value)
	{
		___U3CmsgU3E5__7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmsgU3E5__7_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATERECEIPTPROCESSU3ED__47_T49A8523713B44DEACC6BC525029665BCC9AED057_H
#ifndef NATIVESTOREPROVIDER_TD96BC473A252F39A262C28AB8A48FD5D98363A6A_H
#define NATIVESTOREPROVIDER_TD96BC473A252F39A262C28AB8A48FD5D98363A6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.NativeStoreProvider
struct  NativeStoreProvider_tD96BC473A252F39A262C28AB8A48FD5D98363A6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVESTOREPROVIDER_TD96BC473A252F39A262C28AB8A48FD5D98363A6A_H
#ifndef PAYMETHOD_T24C4E316D622714D9DF2F0F872B4DC44A35CB703_H
#define PAYMETHOD_T24C4E316D622714D9DF2F0F872B4DC44A35CB703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PayMethod
struct  PayMethod_t24C4E316D622714D9DF2F0F872B4DC44A35CB703  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYMETHOD_T24C4E316D622714D9DF2F0F872B4DC44A35CB703_H
#ifndef PRODUCTCATALOG_T7643D269828196E196CDF8B0ACC4AE00EA1A15F6_H
#define PRODUCTCATALOG_T7643D269828196E196CDF8B0ACC4AE00EA1A15F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalog
struct  ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.ProductCatalog::enableCodelessAutoInitialization
	bool ___enableCodelessAutoInitialization_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogItem> UnityEngine.Purchasing.ProductCatalog::products
	List_1_tA1C6463853DC035280694FB65B564ECD8A3453EC * ___products_2;

public:
	inline static int32_t get_offset_of_enableCodelessAutoInitialization_1() { return static_cast<int32_t>(offsetof(ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6, ___enableCodelessAutoInitialization_1)); }
	inline bool get_enableCodelessAutoInitialization_1() const { return ___enableCodelessAutoInitialization_1; }
	inline bool* get_address_of_enableCodelessAutoInitialization_1() { return &___enableCodelessAutoInitialization_1; }
	inline void set_enableCodelessAutoInitialization_1(bool value)
	{
		___enableCodelessAutoInitialization_1 = value;
	}

	inline static int32_t get_offset_of_products_2() { return static_cast<int32_t>(offsetof(ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6, ___products_2)); }
	inline List_1_tA1C6463853DC035280694FB65B564ECD8A3453EC * get_products_2() const { return ___products_2; }
	inline List_1_tA1C6463853DC035280694FB65B564ECD8A3453EC ** get_address_of_products_2() { return &___products_2; }
	inline void set_products_2(List_1_tA1C6463853DC035280694FB65B564ECD8A3453EC * value)
	{
		___products_2 = value;
		Il2CppCodeGenWriteBarrier((&___products_2), value);
	}
};

struct ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6_StaticFields
{
public:
	// UnityEngine.Purchasing.IProductCatalogImpl UnityEngine.Purchasing.ProductCatalog::instance
	RuntimeObject* ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6_StaticFields, ___instance_0)); }
	inline RuntimeObject* get_instance_0() const { return ___instance_0; }
	inline RuntimeObject** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject* value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOG_T7643D269828196E196CDF8B0ACC4AE00EA1A15F6_H
#ifndef U3CU3EC_T74AF868FF34CE9F33880DA547DEEA475FDA0D035_H
#define U3CU3EC_T74AF868FF34CE9F33880DA547DEEA475FDA0D035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalog_<>c
struct  U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035_StaticFields
{
public:
	// UnityEngine.Purchasing.ProductCatalog_<>c UnityEngine.Purchasing.ProductCatalog_<>c::<>9
	U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Purchasing.ProductCatalogItem,System.Boolean> UnityEngine.Purchasing.ProductCatalog_<>c::<>9__8_0
	Func_2_tC03308396998D98527564CECC440648536E95DAC * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_tC03308396998D98527564CECC440648536E95DAC * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_tC03308396998D98527564CECC440648536E95DAC ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_tC03308396998D98527564CECC440648536E95DAC * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T74AF868FF34CE9F33880DA547DEEA475FDA0D035_H
#ifndef PRODUCTCATALOGIMPL_T09FB0B201A0F9B871EB95F93432697B259EC1214_H
#define PRODUCTCATALOGIMPL_T09FB0B201A0F9B871EB95F93432697B259EC1214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogImpl
struct  ProductCatalogImpl_t09FB0B201A0F9B871EB95F93432697B259EC1214  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGIMPL_T09FB0B201A0F9B871EB95F93432697B259EC1214_H
#ifndef PRODUCTCATALOGPAYOUT_T79BFC38C9C47E0CF033E69D860277EA71E617476_H
#define PRODUCTCATALOGPAYOUT_T79BFC38C9C47E0CF033E69D860277EA71E617476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogPayout
struct  ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::t
	String_t* ___t_0;
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::st
	String_t* ___st_1;
	// System.Double UnityEngine.Purchasing.ProductCatalogPayout::q
	double ___q_2;
	// System.String UnityEngine.Purchasing.ProductCatalogPayout::d
	String_t* ___d_3;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476, ___t_0)); }
	inline String_t* get_t_0() const { return ___t_0; }
	inline String_t** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(String_t* value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((&___t_0), value);
	}

	inline static int32_t get_offset_of_st_1() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476, ___st_1)); }
	inline String_t* get_st_1() const { return ___st_1; }
	inline String_t** get_address_of_st_1() { return &___st_1; }
	inline void set_st_1(String_t* value)
	{
		___st_1 = value;
		Il2CppCodeGenWriteBarrier((&___st_1), value);
	}

	inline static int32_t get_offset_of_q_2() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476, ___q_2)); }
	inline double get_q_2() const { return ___q_2; }
	inline double* get_address_of_q_2() { return &___q_2; }
	inline void set_q_2(double value)
	{
		___q_2 = value;
	}

	inline static int32_t get_offset_of_d_3() { return static_cast<int32_t>(offsetof(ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476, ___d_3)); }
	inline String_t* get_d_3() const { return ___d_3; }
	inline String_t** get_address_of_d_3() { return &___d_3; }
	inline void set_d_3(String_t* value)
	{
		___d_3 = value;
		Il2CppCodeGenWriteBarrier((&___d_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGPAYOUT_T79BFC38C9C47E0CF033E69D860277EA71E617476_H
#ifndef PURCHASINGEVENT_TF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7_H
#define PURCHASINGEVENT_TF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingEvent
struct  PurchasingEvent_tF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.PurchasingEvent::EventDict
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___EventDict_0;

public:
	inline static int32_t get_offset_of_EventDict_0() { return static_cast<int32_t>(offsetof(PurchasingEvent_tF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7, ___EventDict_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_EventDict_0() const { return ___EventDict_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_EventDict_0() { return &___EventDict_0; }
	inline void set_EventDict_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___EventDict_0 = value;
		Il2CppCodeGenWriteBarrier((&___EventDict_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASINGEVENT_TF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7_H
#ifndef U3CU3EC_T930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_H
#define U3CU3EC_T930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchasingEvent_<>c
struct  U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields
{
public:
	// UnityEngine.Purchasing.PurchasingEvent_<>c UnityEngine.Purchasing.PurchasingEvent_<>c::<>9
	U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.String> UnityEngine.Purchasing.PurchasingEvent_<>c::<>9__2_0
	Func_2_t1A871D9F8DC387DE1A9B3E47BD1A9771E4CB411E * ___U3CU3E9__2_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Object> UnityEngine.Purchasing.PurchasingEvent_<>c::<>9__2_1
	Func_2_tAFF26EB45C16E2D388535746D3CE9A030DD1BE9A * ___U3CU3E9__2_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_2_t1A871D9F8DC387DE1A9B3E47BD1A9771E4CB411E * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_2_t1A871D9F8DC387DE1A9B3E47BD1A9771E4CB411E ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_2_t1A871D9F8DC387DE1A9B3E47BD1A9771E4CB411E * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields, ___U3CU3E9__2_1_2)); }
	inline Func_2_tAFF26EB45C16E2D388535746D3CE9A030DD1BE9A * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline Func_2_tAFF26EB45C16E2D388535746D3CE9A030DD1BE9A ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(Func_2_tAFF26EB45C16E2D388535746D3CE9A030DD1BE9A * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_H
#ifndef QUERYHELPER_T806E01B87ADF5BB73686FC647841A955D0EC117E_H
#define QUERYHELPER_T806E01B87ADF5BB73686FC647841A955D0EC117E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.QueryHelper
struct  QueryHelper_t806E01B87ADF5BB73686FC647841A955D0EC117E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYHELPER_T806E01B87ADF5BB73686FC647841A955D0EC117E_H
#ifndef MICROSOFTCONFIGURATION_T66D1CD39C4D988FECCD476BA14A9B30943C96127_H
#define MICROSOFTCONFIGURATION_T66D1CD39C4D988FECCD476BA14A9B30943C96127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration
struct  MicrosoftConfiguration_t66D1CD39C4D988FECCD476BA14A9B30943C96127  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration::useMock
	bool ___useMock_0;
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule_MicrosoftConfiguration::module
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * ___module_1;

public:
	inline static int32_t get_offset_of_useMock_0() { return static_cast<int32_t>(offsetof(MicrosoftConfiguration_t66D1CD39C4D988FECCD476BA14A9B30943C96127, ___useMock_0)); }
	inline bool get_useMock_0() const { return ___useMock_0; }
	inline bool* get_address_of_useMock_0() { return &___useMock_0; }
	inline void set_useMock_0(bool value)
	{
		___useMock_0 = value;
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(MicrosoftConfiguration_t66D1CD39C4D988FECCD476BA14A9B30943C96127, ___module_1)); }
	inline StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * get_module_1() const { return ___module_1; }
	inline StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROSOFTCONFIGURATION_T66D1CD39C4D988FECCD476BA14A9B30943C96127_H
#ifndef STOREINSTANCE_T3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57_H
#define STOREINSTANCE_T3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance
struct  StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::<storeName>k__BackingField
	String_t* ___U3CstoreNameU3Ek__BackingField_0;
	// UnityEngine.Purchasing.Extension.IStore UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance::<instance>k__BackingField
	RuntimeObject* ___U3CinstanceU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CstoreNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57, ___U3CstoreNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CstoreNameU3Ek__BackingField_0() const { return ___U3CstoreNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CstoreNameU3Ek__BackingField_0() { return &___U3CstoreNameU3Ek__BackingField_0; }
	inline void set_U3CstoreNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CstoreNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57, ___U3CinstanceU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CinstanceU3Ek__BackingField_1() const { return ___U3CinstanceU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CinstanceU3Ek__BackingField_1() { return &___U3CinstanceU3Ek__BackingField_1; }
	inline void set_U3CinstanceU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CinstanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOREINSTANCE_T3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57_H
#ifndef STORECATALOGIMPL_TF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_H
#define STORECATALOGIMPL_TF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreCatalogImpl
struct  StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StoreCatalogImpl::m_AsyncUtil
	RuntimeObject* ___m_AsyncUtil_0;
	// UnityEngine.ILogger UnityEngine.Purchasing.StoreCatalogImpl::m_Logger
	RuntimeObject* ___m_Logger_1;
	// System.String UnityEngine.Purchasing.StoreCatalogImpl::m_CatalogURL
	String_t* ___m_CatalogURL_2;
	// System.String UnityEngine.Purchasing.StoreCatalogImpl::m_StoreName
	String_t* ___m_StoreName_3;
	// UnityEngine.Purchasing.FileReference UnityEngine.Purchasing.StoreCatalogImpl::m_cachedStoreCatalogReference
	FileReference_t82C0F6EB16F825D6A9F1786DB607F00B9CCDFF48 * ___m_cachedStoreCatalogReference_4;

public:
	inline static int32_t get_offset_of_m_AsyncUtil_0() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB, ___m_AsyncUtil_0)); }
	inline RuntimeObject* get_m_AsyncUtil_0() const { return ___m_AsyncUtil_0; }
	inline RuntimeObject** get_address_of_m_AsyncUtil_0() { return &___m_AsyncUtil_0; }
	inline void set_m_AsyncUtil_0(RuntimeObject* value)
	{
		___m_AsyncUtil_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncUtil_0), value);
	}

	inline static int32_t get_offset_of_m_Logger_1() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB, ___m_Logger_1)); }
	inline RuntimeObject* get_m_Logger_1() const { return ___m_Logger_1; }
	inline RuntimeObject** get_address_of_m_Logger_1() { return &___m_Logger_1; }
	inline void set_m_Logger_1(RuntimeObject* value)
	{
		___m_Logger_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_1), value);
	}

	inline static int32_t get_offset_of_m_CatalogURL_2() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB, ___m_CatalogURL_2)); }
	inline String_t* get_m_CatalogURL_2() const { return ___m_CatalogURL_2; }
	inline String_t** get_address_of_m_CatalogURL_2() { return &___m_CatalogURL_2; }
	inline void set_m_CatalogURL_2(String_t* value)
	{
		___m_CatalogURL_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CatalogURL_2), value);
	}

	inline static int32_t get_offset_of_m_StoreName_3() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB, ___m_StoreName_3)); }
	inline String_t* get_m_StoreName_3() const { return ___m_StoreName_3; }
	inline String_t** get_address_of_m_StoreName_3() { return &___m_StoreName_3; }
	inline void set_m_StoreName_3(String_t* value)
	{
		___m_StoreName_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreName_3), value);
	}

	inline static int32_t get_offset_of_m_cachedStoreCatalogReference_4() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB, ___m_cachedStoreCatalogReference_4)); }
	inline FileReference_t82C0F6EB16F825D6A9F1786DB607F00B9CCDFF48 * get_m_cachedStoreCatalogReference_4() const { return ___m_cachedStoreCatalogReference_4; }
	inline FileReference_t82C0F6EB16F825D6A9F1786DB607F00B9CCDFF48 ** get_address_of_m_cachedStoreCatalogReference_4() { return &___m_cachedStoreCatalogReference_4; }
	inline void set_m_cachedStoreCatalogReference_4(FileReference_t82C0F6EB16F825D6A9F1786DB607F00B9CCDFF48 * value)
	{
		___m_cachedStoreCatalogReference_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedStoreCatalogReference_4), value);
	}
};

struct StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_StaticFields
{
public:
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.StoreCatalogImpl::profile
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * ___profile_5;

public:
	inline static int32_t get_offset_of_profile_5() { return static_cast<int32_t>(offsetof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_StaticFields, ___profile_5)); }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * get_profile_5() const { return ___profile_5; }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B ** get_address_of_profile_5() { return &___profile_5; }
	inline void set_profile_5(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * value)
	{
		___profile_5 = value;
		Il2CppCodeGenWriteBarrier((&___profile_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORECATALOGIMPL_TF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49_H
#define U3CU3EC__DISPLAYCLASS10_0_T606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreCatalogImpl_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49  : public RuntimeObject
{
public:
	// System.Action`1<System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition>> UnityEngine.Purchasing.StoreCatalogImpl_<>c__DisplayClass10_0::callback
	Action_1_tA725AD78C98168AF2201E02E5913A66DB5D3064F * ___callback_0;
	// UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.StoreCatalogImpl_<>c__DisplayClass10_0::<>4__this
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49, ___callback_0)); }
	inline Action_1_tA725AD78C98168AF2201E02E5913A66DB5D3064F * get_callback_0() const { return ___callback_0; }
	inline Action_1_tA725AD78C98168AF2201E02E5913A66DB5D3064F ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_tA725AD78C98168AF2201E02E5913A66DB5D3064F * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49, ___U3CU3E4__this_1)); }
	inline StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49_H
#ifndef STOREID_T4C8817F5B62D22D6E04A2B82069F15C7420A9605_H
#define STOREID_T4C8817F5B62D22D6E04A2B82069F15C7420A9605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreID
struct  StoreID_t4C8817F5B62D22D6E04A2B82069F15C7420A9605  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.StoreID::store
	String_t* ___store_0;
	// System.String UnityEngine.Purchasing.StoreID::id
	String_t* ___id_1;

public:
	inline static int32_t get_offset_of_store_0() { return static_cast<int32_t>(offsetof(StoreID_t4C8817F5B62D22D6E04A2B82069F15C7420A9605, ___store_0)); }
	inline String_t* get_store_0() const { return ___store_0; }
	inline String_t** get_address_of_store_0() { return &___store_0; }
	inline void set_store_0(String_t* value)
	{
		___store_0 = value;
		Il2CppCodeGenWriteBarrier((&___store_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(StoreID_t4C8817F5B62D22D6E04A2B82069F15C7420A9605, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOREID_T4C8817F5B62D22D6E04A2B82069F15C7420A9605_H
#ifndef UDP_T90A3ABD1AA681B1CA1FC9EC6B4A3B14200CCEFB3_H
#define UDP_T90A3ABD1AA681B1CA1FC9EC6B4A3B14200CCEFB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UDP
struct  UDP_t90A3ABD1AA681B1CA1FC9EC6B4A3B14200CCEFB3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDP_T90A3ABD1AA681B1CA1FC9EC6B4A3B14200CCEFB3_H
#ifndef UDPBINDINGS_T97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE_H
#define UDPBINDINGS_T97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UDPBindings
struct  UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE  : public RuntimeObject
{
public:
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UDPBindings::m_PurchaseCallback
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___m_PurchaseCallback_0;
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UDPBindings::m_InitCallback
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___m_InitCallback_1;
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UDPBindings::m_RetrieveProductsCallback
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___m_RetrieveProductsCallback_2;
	// UnityEngine.UDP.Inventory UnityEngine.Purchasing.UDPBindings::m_Inventory
	Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3 * ___m_Inventory_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo> UnityEngine.Purchasing.UDPBindings::m_LocalPurchasesCache
	Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 * ___m_LocalPurchasesCache_4;

public:
	inline static int32_t get_offset_of_m_PurchaseCallback_0() { return static_cast<int32_t>(offsetof(UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE, ___m_PurchaseCallback_0)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_m_PurchaseCallback_0() const { return ___m_PurchaseCallback_0; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_m_PurchaseCallback_0() { return &___m_PurchaseCallback_0; }
	inline void set_m_PurchaseCallback_0(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___m_PurchaseCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseCallback_0), value);
	}

	inline static int32_t get_offset_of_m_InitCallback_1() { return static_cast<int32_t>(offsetof(UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE, ___m_InitCallback_1)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_m_InitCallback_1() const { return ___m_InitCallback_1; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_m_InitCallback_1() { return &___m_InitCallback_1; }
	inline void set_m_InitCallback_1(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___m_InitCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_InitCallback_1), value);
	}

	inline static int32_t get_offset_of_m_RetrieveProductsCallback_2() { return static_cast<int32_t>(offsetof(UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE, ___m_RetrieveProductsCallback_2)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_m_RetrieveProductsCallback_2() const { return ___m_RetrieveProductsCallback_2; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_m_RetrieveProductsCallback_2() { return &___m_RetrieveProductsCallback_2; }
	inline void set_m_RetrieveProductsCallback_2(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___m_RetrieveProductsCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RetrieveProductsCallback_2), value);
	}

	inline static int32_t get_offset_of_m_Inventory_3() { return static_cast<int32_t>(offsetof(UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE, ___m_Inventory_3)); }
	inline Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3 * get_m_Inventory_3() const { return ___m_Inventory_3; }
	inline Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3 ** get_address_of_m_Inventory_3() { return &___m_Inventory_3; }
	inline void set_m_Inventory_3(Inventory_t2179CC5D4C0584A26917CE8F5248C3A60B4C42A3 * value)
	{
		___m_Inventory_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Inventory_3), value);
	}

	inline static int32_t get_offset_of_m_LocalPurchasesCache_4() { return static_cast<int32_t>(offsetof(UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE, ___m_LocalPurchasesCache_4)); }
	inline Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 * get_m_LocalPurchasesCache_4() const { return ___m_LocalPurchasesCache_4; }
	inline Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 ** get_address_of_m_LocalPurchasesCache_4() { return &___m_LocalPurchasesCache_4; }
	inline void set_m_LocalPurchasesCache_4(Dictionary_2_t553EE73D8A332BFC1C6419ACB5246039EDE990A9 * value)
	{
		___m_LocalPurchasesCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalPurchasesCache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPBINDINGS_T97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T53603FF6A50DA53F3E78B7D52C63574639DC7154_H
#define U3CU3EC__DISPLAYCLASS7_0_T53603FF6A50DA53F3E78B7D52C63574639DC7154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154  : public RuntimeObject
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::products
	ReadOnlyCollection_1_tF0BB6018B708B7F0FCF910A9B008A3E81C00B29D * ___products_0;
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::retrieveCallback
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___retrieveCallback_1;
	// UnityEngine.Purchasing.UDPImpl UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass7_0::<>4__this
	UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_products_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154, ___products_0)); }
	inline ReadOnlyCollection_1_tF0BB6018B708B7F0FCF910A9B008A3E81C00B29D * get_products_0() const { return ___products_0; }
	inline ReadOnlyCollection_1_tF0BB6018B708B7F0FCF910A9B008A3E81C00B29D ** get_address_of_products_0() { return &___products_0; }
	inline void set_products_0(ReadOnlyCollection_1_tF0BB6018B708B7F0FCF910A9B008A3E81C00B29D * value)
	{
		___products_0 = value;
		Il2CppCodeGenWriteBarrier((&___products_0), value);
	}

	inline static int32_t get_offset_of_retrieveCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154, ___retrieveCallback_1)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_retrieveCallback_1() const { return ___retrieveCallback_1; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_retrieveCallback_1() { return &___retrieveCallback_1; }
	inline void set_retrieveCallback_1(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___retrieveCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___retrieveCallback_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154, ___U3CU3E4__this_2)); }
	inline UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T53603FF6A50DA53F3E78B7D52C63574639DC7154_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TABAF20373A271E8D799FE1C91217AE25EC57E359_H
#define U3CU3EC__DISPLAYCLASS8_0_TABAF20373A271E8D799FE1C91217AE25EC57E359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tABAF20373A271E8D799FE1C91217AE25EC57E359  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0::product
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * ___product_0;
	// UnityEngine.Purchasing.UDPImpl UnityEngine.Purchasing.UDPImpl_<>c__DisplayClass8_0::<>4__this
	UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_product_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tABAF20373A271E8D799FE1C91217AE25EC57E359, ___product_0)); }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * get_product_0() const { return ___product_0; }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 ** get_address_of_product_0() { return &___product_0; }
	inline void set_product_0(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * value)
	{
		___product_0 = value;
		Il2CppCodeGenWriteBarrier((&___product_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tABAF20373A271E8D799FE1C91217AE25EC57E359, ___U3CU3E4__this_1)); }
	inline UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TABAF20373A271E8D799FE1C91217AE25EC57E359_H
#ifndef UNITYCHANNELBINDINGS_T1F55436FB3D015BA0B293C37951600A73A93E0E6_H
#define UNITYCHANNELBINDINGS_T1F55436FB3D015BA0B293C37951600A73A93E0E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelBindings
struct  UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6  : public RuntimeObject
{
public:
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseCallback
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___m_PurchaseCallback_0;
	// System.String UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseGuid
	String_t* ___m_PurchaseGuid_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>> UnityEngine.Purchasing.UnityChannelBindings::m_ValidateCallbacks
	Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 * ___m_ValidateCallbacks_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`3<System.Boolean,System.String,System.String>>> UnityEngine.Purchasing.UnityChannelBindings::m_PurchaseConfirmCallbacks
	Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 * ___m_PurchaseConfirmCallbacks_3;

public:
	inline static int32_t get_offset_of_m_PurchaseCallback_0() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6, ___m_PurchaseCallback_0)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_m_PurchaseCallback_0() const { return ___m_PurchaseCallback_0; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_m_PurchaseCallback_0() { return &___m_PurchaseCallback_0; }
	inline void set_m_PurchaseCallback_0(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___m_PurchaseCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseCallback_0), value);
	}

	inline static int32_t get_offset_of_m_PurchaseGuid_1() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6, ___m_PurchaseGuid_1)); }
	inline String_t* get_m_PurchaseGuid_1() const { return ___m_PurchaseGuid_1; }
	inline String_t** get_address_of_m_PurchaseGuid_1() { return &___m_PurchaseGuid_1; }
	inline void set_m_PurchaseGuid_1(String_t* value)
	{
		___m_PurchaseGuid_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseGuid_1), value);
	}

	inline static int32_t get_offset_of_m_ValidateCallbacks_2() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6, ___m_ValidateCallbacks_2)); }
	inline Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 * get_m_ValidateCallbacks_2() const { return ___m_ValidateCallbacks_2; }
	inline Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 ** get_address_of_m_ValidateCallbacks_2() { return &___m_ValidateCallbacks_2; }
	inline void set_m_ValidateCallbacks_2(Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 * value)
	{
		___m_ValidateCallbacks_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidateCallbacks_2), value);
	}

	inline static int32_t get_offset_of_m_PurchaseConfirmCallbacks_3() { return static_cast<int32_t>(offsetof(UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6, ___m_PurchaseConfirmCallbacks_3)); }
	inline Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 * get_m_PurchaseConfirmCallbacks_3() const { return ___m_PurchaseConfirmCallbacks_3; }
	inline Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 ** get_address_of_m_PurchaseConfirmCallbacks_3() { return &___m_PurchaseConfirmCallbacks_3; }
	inline void set_m_PurchaseConfirmCallbacks_3(Dictionary_2_t7A4635A3B21B52DE7104114E8C18AD138B19F958 * value)
	{
		___m_PurchaseConfirmCallbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PurchaseConfirmCallbacks_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELBINDINGS_T1F55436FB3D015BA0B293C37951600A73A93E0E6_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_TB91567274B7906CE554758FDB7180DBA22B1A9A5_H
#define U3CU3EC__DISPLAYCLASS16_0_TB91567274B7906CE554758FDB7180DBA22B1A9A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelBindings_<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_tB91567274B7906CE554758FDB7180DBA22B1A9A5  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UnityChannelBindings_<>c__DisplayClass16_0::transactionId
	String_t* ___transactionId_0;
	// UnityEngine.Purchasing.UnityChannelBindings UnityEngine.Purchasing.UnityChannelBindings_<>c__DisplayClass16_0::<>4__this
	UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_transactionId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tB91567274B7906CE554758FDB7180DBA22B1A9A5, ___transactionId_0)); }
	inline String_t* get_transactionId_0() const { return ___transactionId_0; }
	inline String_t** get_address_of_transactionId_0() { return &___transactionId_0; }
	inline void set_transactionId_0(String_t* value)
	{
		___transactionId_0 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tB91567274B7906CE554758FDB7180DBA22B1A9A5, ___U3CU3E4__this_1)); }
	inline UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_TB91567274B7906CE554758FDB7180DBA22B1A9A5_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T49FACE65C2487483820DF485344FA9E4DC6351A0_H
#define U3CU3EC__DISPLAYCLASS7_0_T49FACE65C2487483820DF485344FA9E4DC6351A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_0::product
	ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * ___product_0;
	// UnityEngine.Purchasing.UnityChannelImpl UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_0::<>4__this
	UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_product_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0, ___product_0)); }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * get_product_0() const { return ___product_0; }
	inline ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 ** get_address_of_product_0() { return &___product_0; }
	inline void set_product_0(ProductDefinition_t020888B51F9B79E1474119DBE9DEDBDEF7766C10 * value)
	{
		___product_0 = value;
		Il2CppCodeGenWriteBarrier((&___product_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0, ___U3CU3E4__this_1)); }
	inline UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T49FACE65C2487483820DF485344FA9E4DC6351A0_H
#ifndef U3CU3EC__DISPLAYCLASS7_1_T38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE_H
#define U3CU3EC__DISPLAYCLASS7_1_T38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_1
struct  U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_1::dic
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___dic_0;
	// System.String UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_1::transactionId
	String_t* ___transactionId_1;
	// UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_0 UnityEngine.Purchasing.UnityChannelImpl_<>c__DisplayClass7_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0 * ___CSU24U3CU3E8__locals1_2;

public:
	inline static int32_t get_offset_of_dic_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE, ___dic_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_dic_0() const { return ___dic_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_dic_0() { return &___dic_0; }
	inline void set_dic_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___dic_0 = value;
		Il2CppCodeGenWriteBarrier((&___dic_0), value);
	}

	inline static int32_t get_offset_of_transactionId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE, ___transactionId_1)); }
	inline String_t* get_transactionId_1() const { return ___transactionId_1; }
	inline String_t** get_address_of_transactionId_1() { return &___transactionId_1; }
	inline void set_transactionId_1(String_t* value)
	{
		___transactionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_1), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE, ___CSU24U3CU3E8__locals1_2)); }
	inline U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0 * get_CSU24U3CU3E8__locals1_2() const { return ___CSU24U3CU3E8__locals1_2; }
	inline U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0 ** get_address_of_CSU24U3CU3E8__locals1_2() { return &___CSU24U3CU3E8__locals1_2; }
	inline void set_CSU24U3CU3E8__locals1_2(U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0 * value)
	{
		___CSU24U3CU3E8__locals1_2 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_1_T38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE_H
#ifndef UNITYCHANNELPURCHASERECEIPT_T22BA5D3915A198D116BB4813CF1D5A9C66DC6D69_H
#define UNITYCHANNELPURCHASERECEIPT_T22BA5D3915A198D116BB4813CF1D5A9C66DC6D69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelPurchaseReceipt
struct  UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.UnityChannelPurchaseReceipt::storeSpecificId
	String_t* ___storeSpecificId_0;
	// System.String UnityEngine.Purchasing.UnityChannelPurchaseReceipt::transactionId
	String_t* ___transactionId_1;
	// System.String UnityEngine.Purchasing.UnityChannelPurchaseReceipt::orderQueryToken
	String_t* ___orderQueryToken_2;

public:
	inline static int32_t get_offset_of_storeSpecificId_0() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69, ___storeSpecificId_0)); }
	inline String_t* get_storeSpecificId_0() const { return ___storeSpecificId_0; }
	inline String_t** get_address_of_storeSpecificId_0() { return &___storeSpecificId_0; }
	inline void set_storeSpecificId_0(String_t* value)
	{
		___storeSpecificId_0 = value;
		Il2CppCodeGenWriteBarrier((&___storeSpecificId_0), value);
	}

	inline static int32_t get_offset_of_transactionId_1() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69, ___transactionId_1)); }
	inline String_t* get_transactionId_1() const { return ___transactionId_1; }
	inline String_t** get_address_of_transactionId_1() { return &___transactionId_1; }
	inline void set_transactionId_1(String_t* value)
	{
		___transactionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_1), value);
	}

	inline static int32_t get_offset_of_orderQueryToken_2() { return static_cast<int32_t>(offsetof(UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69, ___orderQueryToken_2)); }
	inline String_t* get_orderQueryToken_2() const { return ___orderQueryToken_2; }
	inline String_t** get_address_of_orderQueryToken_2() { return &___orderQueryToken_2; }
	inline void set_orderQueryToken_2(String_t* value)
	{
		___orderQueryToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___orderQueryToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELPURCHASERECEIPT_T22BA5D3915A198D116BB4813CF1D5A9C66DC6D69_H
#ifndef XIAOMIPRICETIERS_T277154F32924391EF362A479856C47A2A43A6E5B_H
#define XIAOMIPRICETIERS_T277154F32924391EF362A479856C47A2A43A6E5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.XiaomiPriceTiers
struct  XiaomiPriceTiers_t277154F32924391EF362A479856C47A2A43A6E5B  : public RuntimeObject
{
public:

public:
};

struct XiaomiPriceTiers_t277154F32924391EF362A479856C47A2A43A6E5B_StaticFields
{
public:
	// System.Int32[] UnityEngine.Purchasing.XiaomiPriceTiers::XiaomiPriceTierPrices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___XiaomiPriceTierPrices_0;

public:
	inline static int32_t get_offset_of_XiaomiPriceTierPrices_0() { return static_cast<int32_t>(offsetof(XiaomiPriceTiers_t277154F32924391EF362A479856C47A2A43A6E5B_StaticFields, ___XiaomiPriceTierPrices_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_XiaomiPriceTierPrices_0() const { return ___XiaomiPriceTierPrices_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_XiaomiPriceTierPrices_0() { return &___XiaomiPriceTierPrices_0; }
	inline void set_XiaomiPriceTierPrices_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___XiaomiPriceTierPrices_0 = value;
		Il2CppCodeGenWriteBarrier((&___XiaomiPriceTierPrices_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XIAOMIPRICETIERS_T277154F32924391EF362A479856C47A2A43A6E5B_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#define ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TE0C99528D3DCE5863566CE37BD94162A4C0431CD_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef GOOGLEPLAYANDROIDJAVASTORE_T73E3ED5DB8900A395E0CA494ACEC02E158FBBA44_H
#define GOOGLEPLAYANDROIDJAVASTORE_T73E3ED5DB8900A395E0CA494ACEC02E158FBBA44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayAndroidJavaStore
struct  GooglePlayAndroidJavaStore_t73E3ED5DB8900A395E0CA494ACEC02E158FBBA44  : public AndroidJavaStore_t47B0807D750DE25AD6ED63F99B05E3DE942BC47C
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.GooglePlayAndroidJavaStore::m_Util
	RuntimeObject* ___m_Util_1;

public:
	inline static int32_t get_offset_of_m_Util_1() { return static_cast<int32_t>(offsetof(GooglePlayAndroidJavaStore_t73E3ED5DB8900A395E0CA494ACEC02E158FBBA44, ___m_Util_1)); }
	inline RuntimeObject* get_m_Util_1() const { return ___m_Util_1; }
	inline RuntimeObject** get_address_of_m_Util_1() { return &___m_Util_1; }
	inline void set_m_Util_1(RuntimeObject* value)
	{
		___m_Util_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Util_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYANDROIDJAVASTORE_T73E3ED5DB8900A395E0CA494ACEC02E158FBBA44_H
#ifndef GOOGLEPLAYSTORECALLBACK_TC9B4E8F8744E8223CA416D9DD39E7879C529A58C_H
#define GOOGLEPLAYSTORECALLBACK_TC9B4E8F8744E8223CA416D9DD39E7879C529A58C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayStoreCallback
struct  GooglePlayStoreCallback_tC9B4E8F8744E8223CA416D9DD39E7879C529A58C  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.GooglePlayStoreCallback::callback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(GooglePlayStoreCallback_tC9B4E8F8744E8223CA416D9DD39E7879C529A58C, ___callback_0)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_callback_0() const { return ___callback_0; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYSTORECALLBACK_TC9B4E8F8744E8223CA416D9DD39E7879C529A58C_H
#ifndef GOOGLEPLAYSTOREEXTENSIONS_T0FA3D71679F20931E482A918A283E495D174524E_H
#define GOOGLEPLAYSTOREEXTENSIONS_T0FA3D71679F20931E482A918A283E495D174524E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayStoreExtensions
struct  GooglePlayStoreExtensions_t0FA3D71679F20931E482A918A283E495D174524E  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.GooglePlayStoreExtensions::m_Java
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___m_Java_0;

public:
	inline static int32_t get_offset_of_m_Java_0() { return static_cast<int32_t>(offsetof(GooglePlayStoreExtensions_t0FA3D71679F20931E482A918A283E495D174524E, ___m_Java_0)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_m_Java_0() const { return ___m_Java_0; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_m_Java_0() { return &___m_Java_0; }
	inline void set_m_Java_0(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___m_Java_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Java_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYSTOREEXTENSIONS_T0FA3D71679F20931E482A918A283E495D174524E_H
#ifndef SAMSUNGAPPSSTOREEXTENSIONS_T7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2_H
#define SAMSUNGAPPSSTOREEXTENSIONS_T7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SamsungAppsStoreExtensions
struct  SamsungAppsStoreExtensions_t7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2  : public AndroidJavaProxy_tBF3E21C3639CF1A14BDC9173530DC13D45540795
{
public:
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.SamsungAppsStoreExtensions::m_RestoreCallback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___m_RestoreCallback_0;
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.SamsungAppsStoreExtensions::m_Java
	AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * ___m_Java_1;

public:
	inline static int32_t get_offset_of_m_RestoreCallback_0() { return static_cast<int32_t>(offsetof(SamsungAppsStoreExtensions_t7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2, ___m_RestoreCallback_0)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_m_RestoreCallback_0() const { return ___m_RestoreCallback_0; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_m_RestoreCallback_0() { return &___m_RestoreCallback_0; }
	inline void set_m_RestoreCallback_0(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___m_RestoreCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_RestoreCallback_0), value);
	}

	inline static int32_t get_offset_of_m_Java_1() { return static_cast<int32_t>(offsetof(SamsungAppsStoreExtensions_t7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2, ___m_Java_1)); }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * get_m_Java_1() const { return ___m_Java_1; }
	inline AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E ** get_address_of_m_Java_1() { return &___m_Java_1; }
	inline void set_m_Java_1(AndroidJavaObject_t5B3829FB6E1DBC020F5BA17846F1351EAA982F8E * value)
	{
		___m_Java_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Java_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMSUNGAPPSSTOREEXTENSIONS_T7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef APPSTORE_T27444A4FF359217774CC32493678590052ECD360_H
#define APPSTORE_T27444A4FF359217774CC32493678590052ECD360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppStore
struct  AppStore_t27444A4FF359217774CC32493678590052ECD360 
{
public:
	// System.Int32 UnityEngine.Purchasing.AppStore::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppStore_t27444A4FF359217774CC32493678590052ECD360, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORE_T27444A4FF359217774CC32493678590052ECD360_H
#ifndef APPLESTOREPRODUCTTYPE_TADAB43B081D09C4D7F08145CED6B7EB52716076A_H
#define APPLESTOREPRODUCTTYPE_TADAB43B081D09C4D7F08145CED6B7EB52716076A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreProductType
struct  AppleStoreProductType_tADAB43B081D09C4D7F08145CED6B7EB52716076A 
{
public:
	// System.Int32 UnityEngine.Purchasing.AppleStoreProductType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppleStoreProductType_tADAB43B081D09C4D7F08145CED6B7EB52716076A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLESTOREPRODUCTTYPE_TADAB43B081D09C4D7F08145CED6B7EB52716076A_H
#ifndef CLOUDMOOLAHMODE_TB7D8A32780C5327C2996C62E49DF45B98100E322_H
#define CLOUDMOOLAHMODE_TB7D8A32780C5327C2996C62E49DF45B98100E322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudMoolahMode
struct  CloudMoolahMode_tB7D8A32780C5327C2996C62E49DF45B98100E322 
{
public:
	// System.Int32 UnityEngine.Purchasing.CloudMoolahMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloudMoolahMode_tB7D8A32780C5327C2996C62E49DF45B98100E322, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDMOOLAHMODE_TB7D8A32780C5327C2996C62E49DF45B98100E322_H
#ifndef EVENTDESTTYPE_T2A641E5C0B5276778DE6D86C34D4EBFEA6466785_H
#define EVENTDESTTYPE_T2A641E5C0B5276778DE6D86C34D4EBFEA6466785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventDestType
struct  EventDestType_t2A641E5C0B5276778DE6D86C34D4EBFEA6466785 
{
public:
	// System.Int32 UnityEngine.Purchasing.EventDestType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventDestType_t2A641E5C0B5276778DE6D86C34D4EBFEA6466785, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDESTTYPE_T2A641E5C0B5276778DE6D86C34D4EBFEA6466785_H
#ifndef FAKEMANAGEDSTORECONFIG_T15D56215F0228DEA7C5B21C2E4FD32939D8D7C07_H
#define FAKEMANAGEDSTORECONFIG_T15D56215F0228DEA7C5B21C2E4FD32939D8D7C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeManagedStoreConfig
struct  FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.FakeManagedStoreConfig::catalogDisabled
	bool ___catalogDisabled_0;
	// System.Boolean UnityEngine.Purchasing.FakeManagedStoreConfig::testStore
	bool ___testStore_1;
	// System.String UnityEngine.Purchasing.FakeManagedStoreConfig::iapBaseUrl
	String_t* ___iapBaseUrl_2;
	// System.String UnityEngine.Purchasing.FakeManagedStoreConfig::eventBaseUrl
	String_t* ___eventBaseUrl_3;
	// System.Nullable`1<System.Boolean> UnityEngine.Purchasing.FakeManagedStoreConfig::trackingOptedOut
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___trackingOptedOut_4;

public:
	inline static int32_t get_offset_of_catalogDisabled_0() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07, ___catalogDisabled_0)); }
	inline bool get_catalogDisabled_0() const { return ___catalogDisabled_0; }
	inline bool* get_address_of_catalogDisabled_0() { return &___catalogDisabled_0; }
	inline void set_catalogDisabled_0(bool value)
	{
		___catalogDisabled_0 = value;
	}

	inline static int32_t get_offset_of_testStore_1() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07, ___testStore_1)); }
	inline bool get_testStore_1() const { return ___testStore_1; }
	inline bool* get_address_of_testStore_1() { return &___testStore_1; }
	inline void set_testStore_1(bool value)
	{
		___testStore_1 = value;
	}

	inline static int32_t get_offset_of_iapBaseUrl_2() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07, ___iapBaseUrl_2)); }
	inline String_t* get_iapBaseUrl_2() const { return ___iapBaseUrl_2; }
	inline String_t** get_address_of_iapBaseUrl_2() { return &___iapBaseUrl_2; }
	inline void set_iapBaseUrl_2(String_t* value)
	{
		___iapBaseUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___iapBaseUrl_2), value);
	}

	inline static int32_t get_offset_of_eventBaseUrl_3() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07, ___eventBaseUrl_3)); }
	inline String_t* get_eventBaseUrl_3() const { return ___eventBaseUrl_3; }
	inline String_t** get_address_of_eventBaseUrl_3() { return &___eventBaseUrl_3; }
	inline void set_eventBaseUrl_3(String_t* value)
	{
		___eventBaseUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___eventBaseUrl_3), value);
	}

	inline static int32_t get_offset_of_trackingOptedOut_4() { return static_cast<int32_t>(offsetof(FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07, ___trackingOptedOut_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_trackingOptedOut_4() const { return ___trackingOptedOut_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_trackingOptedOut_4() { return &___trackingOptedOut_4; }
	inline void set_trackingOptedOut_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___trackingOptedOut_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMANAGEDSTORECONFIG_T15D56215F0228DEA7C5B21C2E4FD32939D8D7C07_H
#ifndef FAKESTOREUIMODE_TC9023D94845BDC28E71412A4D078CAB33C129834_H
#define FAKESTOREUIMODE_TC9023D94845BDC28E71412A4D078CAB33C129834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStoreUIMode
struct  FakeStoreUIMode_tC9023D94845BDC28E71412A4D078CAB33C129834 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStoreUIMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FakeStoreUIMode_tC9023D94845BDC28E71412A4D078CAB33C129834, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESTOREUIMODE_TC9023D94845BDC28E71412A4D078CAB33C129834_H
#ifndef U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T07929999CA5328D0D010DA7DC8CA56C82D832A10_H
#define U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T07929999CA5328D0D010DA7DC8CA56C82D832A10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45
struct  U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState> UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::result
	Action_1_t16C7D3167F43E9D3FA6F506C90EF45189E2C0522 * ___result_2;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<>4__this
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * ___U3CU3E4__this_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<customID>5__1
	String_t* ___U3CcustomIDU3E5__1_4;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<wf>5__2
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CwfU3E5__2_5;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<now>5__3
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CnowU3E5__3_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<endDate>5__4
	String_t* ___U3CendDateU3E5__4_7;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<upperWeek>5__5
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CupperWeekU3E5__5_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<startDate>5__6
	String_t* ___U3CstartDateU3E5__6_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<sign>5__7
	String_t* ___U3CsignU3E5__7_10;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<w>5__8
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwU3E5__8_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<restoreObjects>5__9
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CrestoreObjectsU3E5__9_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<code>5__10
	String_t* ___U3CcodeU3E5__10_13;
	// System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<restoreValues>5__11
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CrestoreValuesU3E5__11_14;
	// System.Collections.Generic.List`1_Enumerator<System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<>s__12
	Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  ___U3CU3Es__12_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<restoreObjectElem>5__13
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___U3CrestoreObjectElemU3E5__13_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<productId>5__14
	String_t* ___U3CproductIdU3E5__14_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<tradeSeq>5__15
	String_t* ___U3CtradeSeqU3E5__15_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl_<RestoreTransactionIDProcess>d__45::<receipt>5__16
	String_t* ___U3CreceiptU3E5__16_19;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___result_2)); }
	inline Action_1_t16C7D3167F43E9D3FA6F506C90EF45189E2C0522 * get_result_2() const { return ___result_2; }
	inline Action_1_t16C7D3167F43E9D3FA6F506C90EF45189E2C0522 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(Action_1_t16C7D3167F43E9D3FA6F506C90EF45189E2C0522 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CU3E4__this_3)); }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcustomIDU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CcustomIDU3E5__1_4)); }
	inline String_t* get_U3CcustomIDU3E5__1_4() const { return ___U3CcustomIDU3E5__1_4; }
	inline String_t** get_address_of_U3CcustomIDU3E5__1_4() { return &___U3CcustomIDU3E5__1_4; }
	inline void set_U3CcustomIDU3E5__1_4(String_t* value)
	{
		___U3CcustomIDU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomIDU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CwfU3E5__2_5)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CwfU3E5__2_5() const { return ___U3CwfU3E5__2_5; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CwfU3E5__2_5() { return &___U3CwfU3E5__2_5; }
	inline void set_U3CwfU3E5__2_5(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CwfU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CnowU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CnowU3E5__3_6)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CnowU3E5__3_6() const { return ___U3CnowU3E5__3_6; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CnowU3E5__3_6() { return &___U3CnowU3E5__3_6; }
	inline void set_U3CnowU3E5__3_6(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CnowU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CendDateU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CendDateU3E5__4_7)); }
	inline String_t* get_U3CendDateU3E5__4_7() const { return ___U3CendDateU3E5__4_7; }
	inline String_t** get_address_of_U3CendDateU3E5__4_7() { return &___U3CendDateU3E5__4_7; }
	inline void set_U3CendDateU3E5__4_7(String_t* value)
	{
		___U3CendDateU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CendDateU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CupperWeekU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CupperWeekU3E5__5_8)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CupperWeekU3E5__5_8() const { return ___U3CupperWeekU3E5__5_8; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CupperWeekU3E5__5_8() { return &___U3CupperWeekU3E5__5_8; }
	inline void set_U3CupperWeekU3E5__5_8(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CupperWeekU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CstartDateU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CstartDateU3E5__6_9)); }
	inline String_t* get_U3CstartDateU3E5__6_9() const { return ___U3CstartDateU3E5__6_9; }
	inline String_t** get_address_of_U3CstartDateU3E5__6_9() { return &___U3CstartDateU3E5__6_9; }
	inline void set_U3CstartDateU3E5__6_9(String_t* value)
	{
		___U3CstartDateU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstartDateU3E5__6_9), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CsignU3E5__7_10)); }
	inline String_t* get_U3CsignU3E5__7_10() const { return ___U3CsignU3E5__7_10; }
	inline String_t** get_address_of_U3CsignU3E5__7_10() { return &___U3CsignU3E5__7_10; }
	inline void set_U3CsignU3E5__7_10(String_t* value)
	{
		___U3CsignU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__7_10), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CwU3E5__8_11)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwU3E5__8_11() const { return ___U3CwU3E5__8_11; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwU3E5__8_11() { return &___U3CwU3E5__8_11; }
	inline void set_U3CwU3E5__8_11(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwU3E5__8_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__8_11), value);
	}

	inline static int32_t get_offset_of_U3CrestoreObjectsU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CrestoreObjectsU3E5__9_12)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CrestoreObjectsU3E5__9_12() const { return ___U3CrestoreObjectsU3E5__9_12; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CrestoreObjectsU3E5__9_12() { return &___U3CrestoreObjectsU3E5__9_12; }
	inline void set_U3CrestoreObjectsU3E5__9_12(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CrestoreObjectsU3E5__9_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreObjectsU3E5__9_12), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CcodeU3E5__10_13)); }
	inline String_t* get_U3CcodeU3E5__10_13() const { return ___U3CcodeU3E5__10_13; }
	inline String_t** get_address_of_U3CcodeU3E5__10_13() { return &___U3CcodeU3E5__10_13; }
	inline void set_U3CcodeU3E5__10_13(String_t* value)
	{
		___U3CcodeU3E5__10_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__10_13), value);
	}

	inline static int32_t get_offset_of_U3CrestoreValuesU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CrestoreValuesU3E5__11_14)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CrestoreValuesU3E5__11_14() const { return ___U3CrestoreValuesU3E5__11_14; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CrestoreValuesU3E5__11_14() { return &___U3CrestoreValuesU3E5__11_14; }
	inline void set_U3CrestoreValuesU3E5__11_14(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CrestoreValuesU3E5__11_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreValuesU3E5__11_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__12_15() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CU3Es__12_15)); }
	inline Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  get_U3CU3Es__12_15() const { return ___U3CU3Es__12_15; }
	inline Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * get_address_of_U3CU3Es__12_15() { return &___U3CU3Es__12_15; }
	inline void set_U3CU3Es__12_15(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  value)
	{
		___U3CU3Es__12_15 = value;
	}

	inline static int32_t get_offset_of_U3CrestoreObjectElemU3E5__13_16() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CrestoreObjectElemU3E5__13_16)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_U3CrestoreObjectElemU3E5__13_16() const { return ___U3CrestoreObjectElemU3E5__13_16; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_U3CrestoreObjectElemU3E5__13_16() { return &___U3CrestoreObjectElemU3E5__13_16; }
	inline void set_U3CrestoreObjectElemU3E5__13_16(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___U3CrestoreObjectElemU3E5__13_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreObjectElemU3E5__13_16), value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__14_17() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CproductIdU3E5__14_17)); }
	inline String_t* get_U3CproductIdU3E5__14_17() const { return ___U3CproductIdU3E5__14_17; }
	inline String_t** get_address_of_U3CproductIdU3E5__14_17() { return &___U3CproductIdU3E5__14_17; }
	inline void set_U3CproductIdU3E5__14_17(String_t* value)
	{
		___U3CproductIdU3E5__14_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIdU3E5__14_17), value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__15_18() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CtradeSeqU3E5__15_18)); }
	inline String_t* get_U3CtradeSeqU3E5__15_18() const { return ___U3CtradeSeqU3E5__15_18; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__15_18() { return &___U3CtradeSeqU3E5__15_18; }
	inline void set_U3CtradeSeqU3E5__15_18(String_t* value)
	{
		___U3CtradeSeqU3E5__15_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeSeqU3E5__15_18), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__16_19() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10, ___U3CreceiptU3E5__16_19)); }
	inline String_t* get_U3CreceiptU3E5__16_19() const { return ___U3CreceiptU3E5__16_19; }
	inline String_t** get_address_of_U3CreceiptU3E5__16_19() { return &___U3CreceiptU3E5__16_19; }
	inline void set_U3CreceiptU3E5__16_19(String_t* value)
	{
		___U3CreceiptU3E5__16_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3E5__16_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T07929999CA5328D0D010DA7DC8CA56C82D832A10_H
#ifndef PRICE_T5D55521A645803976E0809F8941E8317C80E129C_H
#define PRICE_T5D55521A645803976E0809F8941E8317C80E129C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Price
struct  Price_t5D55521A645803976E0809F8941E8317C80E129C  : public RuntimeObject
{
public:
	// System.Decimal UnityEngine.Purchasing.Price::value
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___value_0;
	// System.Int32[] UnityEngine.Purchasing.Price::data
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___data_1;
	// System.Double UnityEngine.Purchasing.Price::num
	double ___num_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Price_t5D55521A645803976E0809F8941E8317C80E129C, ___value_0)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_value_0() const { return ___value_0; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Price_t5D55521A645803976E0809F8941E8317C80E129C, ___data_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_data_1() const { return ___data_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_num_2() { return static_cast<int32_t>(offsetof(Price_t5D55521A645803976E0809F8941E8317C80E129C, ___num_2)); }
	inline double get_num_2() const { return ___num_2; }
	inline double* get_address_of_num_2() { return &___num_2; }
	inline void set_num_2(double value)
	{
		___num_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRICE_T5D55521A645803976E0809F8941E8317C80E129C_H
#ifndef PRODUCTCATALOGPAYOUTTYPE_T0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5_H
#define PRODUCTCATALOGPAYOUTTYPE_T0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogPayout_ProductCatalogPayoutType
struct  ProductCatalogPayoutType_t0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductCatalogPayout_ProductCatalogPayoutType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProductCatalogPayoutType_t0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGPAYOUTTYPE_T0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5_H
#ifndef PRODUCTTYPE_TC52C3BA25156195ACF6AE97650D056434BD51075_H
#define PRODUCTTYPE_TC52C3BA25156195ACF6AE97650D056434BD51075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductType
struct  ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProductType_tC52C3BA25156195ACF6AE97650D056434BD51075, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTTYPE_TC52C3BA25156195ACF6AE97650D056434BD51075_H
#ifndef PROFILEDATA_T59F197E40BC007C791FD64C4E05735F692CEB96B_H
#define PROFILEDATA_T59F197E40BC007C791FD64C4E05735F692CEB96B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProfileData
struct  ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B  : public RuntimeObject
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.ProfileData::m_Util
	RuntimeObject* ___m_Util_0;
	// System.String UnityEngine.Purchasing.ProfileData::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.ProfileData::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_3;
	// System.UInt64 UnityEngine.Purchasing.ProfileData::<SessionId>k__BackingField
	uint64_t ___U3CSessionIdU3Ek__BackingField_4;
	// System.String UnityEngine.Purchasing.ProfileData::<Platform>k__BackingField
	String_t* ___U3CPlatformU3Ek__BackingField_5;
	// System.Int32 UnityEngine.Purchasing.ProfileData::<PlatformId>k__BackingField
	int32_t ___U3CPlatformIdU3Ek__BackingField_6;
	// System.String UnityEngine.Purchasing.ProfileData::<SdkVer>k__BackingField
	String_t* ___U3CSdkVerU3Ek__BackingField_7;
	// System.String UnityEngine.Purchasing.ProfileData::<OsVer>k__BackingField
	String_t* ___U3COsVerU3Ek__BackingField_8;
	// System.Int32 UnityEngine.Purchasing.ProfileData::<ScreenWidth>k__BackingField
	int32_t ___U3CScreenWidthU3Ek__BackingField_9;
	// System.Int32 UnityEngine.Purchasing.ProfileData::<ScreenHeight>k__BackingField
	int32_t ___U3CScreenHeightU3Ek__BackingField_10;
	// System.Single UnityEngine.Purchasing.ProfileData::<ScreenDpi>k__BackingField
	float ___U3CScreenDpiU3Ek__BackingField_11;
	// System.String UnityEngine.Purchasing.ProfileData::<ScreenOrientation>k__BackingField
	String_t* ___U3CScreenOrientationU3Ek__BackingField_12;
	// System.String UnityEngine.Purchasing.ProfileData::<DeviceId>k__BackingField
	String_t* ___U3CDeviceIdU3Ek__BackingField_13;
	// System.String UnityEngine.Purchasing.ProfileData::<BuildGUID>k__BackingField
	String_t* ___U3CBuildGUIDU3Ek__BackingField_14;
	// System.String UnityEngine.Purchasing.ProfileData::<IapVer>k__BackingField
	String_t* ___U3CIapVerU3Ek__BackingField_15;
	// System.String UnityEngine.Purchasing.ProfileData::<AdsGamerToken>k__BackingField
	String_t* ___U3CAdsGamerTokenU3Ek__BackingField_16;
	// System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::<TrackingOptOut>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CTrackingOptOutU3Ek__BackingField_17;
	// System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::<AdsABGroup>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CAdsABGroupU3Ek__BackingField_18;
	// System.String UnityEngine.Purchasing.ProfileData::<AdsGameId>k__BackingField
	String_t* ___U3CAdsGameIdU3Ek__BackingField_19;
	// System.Nullable`1<System.Int32> UnityEngine.Purchasing.ProfileData::<StoreABGroup>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CStoreABGroupU3Ek__BackingField_20;
	// System.String UnityEngine.Purchasing.ProfileData::<CatalogId>k__BackingField
	String_t* ___U3CCatalogIdU3Ek__BackingField_21;
	// System.String UnityEngine.Purchasing.ProfileData::<MonetizationId>k__BackingField
	String_t* ___U3CMonetizationIdU3Ek__BackingField_22;
	// System.String UnityEngine.Purchasing.ProfileData::<StoreName>k__BackingField
	String_t* ___U3CStoreNameU3Ek__BackingField_23;
	// System.String UnityEngine.Purchasing.ProfileData::<GameVersion>k__BackingField
	String_t* ___U3CGameVersionU3Ek__BackingField_24;
	// System.Nullable`1<System.Boolean> UnityEngine.Purchasing.ProfileData::<StoreTestEnabled>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CStoreTestEnabledU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Util_0() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___m_Util_0)); }
	inline RuntimeObject* get_m_Util_0() const { return ___m_Util_0; }
	inline RuntimeObject** get_address_of_m_Util_0() { return &___m_Util_0; }
	inline void set_m_Util_0(RuntimeObject* value)
	{
		___m_Util_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Util_0), value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CAppIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_2() const { return ___U3CAppIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_2() { return &___U3CAppIdU3Ek__BackingField_2; }
	inline void set_U3CAppIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CUserIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_3() const { return ___U3CUserIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_3() { return &___U3CUserIdU3Ek__BackingField_3; }
	inline void set_U3CUserIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CSessionIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CSessionIdU3Ek__BackingField_4)); }
	inline uint64_t get_U3CSessionIdU3Ek__BackingField_4() const { return ___U3CSessionIdU3Ek__BackingField_4; }
	inline uint64_t* get_address_of_U3CSessionIdU3Ek__BackingField_4() { return &___U3CSessionIdU3Ek__BackingField_4; }
	inline void set_U3CSessionIdU3Ek__BackingField_4(uint64_t value)
	{
		___U3CSessionIdU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CPlatformU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CPlatformU3Ek__BackingField_5)); }
	inline String_t* get_U3CPlatformU3Ek__BackingField_5() const { return ___U3CPlatformU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPlatformU3Ek__BackingField_5() { return &___U3CPlatformU3Ek__BackingField_5; }
	inline void set_U3CPlatformU3Ek__BackingField_5(String_t* value)
	{
		___U3CPlatformU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPlatformIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CPlatformIdU3Ek__BackingField_6)); }
	inline int32_t get_U3CPlatformIdU3Ek__BackingField_6() const { return ___U3CPlatformIdU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CPlatformIdU3Ek__BackingField_6() { return &___U3CPlatformIdU3Ek__BackingField_6; }
	inline void set_U3CPlatformIdU3Ek__BackingField_6(int32_t value)
	{
		___U3CPlatformIdU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSdkVerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CSdkVerU3Ek__BackingField_7)); }
	inline String_t* get_U3CSdkVerU3Ek__BackingField_7() const { return ___U3CSdkVerU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CSdkVerU3Ek__BackingField_7() { return &___U3CSdkVerU3Ek__BackingField_7; }
	inline void set_U3CSdkVerU3Ek__BackingField_7(String_t* value)
	{
		___U3CSdkVerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSdkVerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COsVerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3COsVerU3Ek__BackingField_8)); }
	inline String_t* get_U3COsVerU3Ek__BackingField_8() const { return ___U3COsVerU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3COsVerU3Ek__BackingField_8() { return &___U3COsVerU3Ek__BackingField_8; }
	inline void set_U3COsVerU3Ek__BackingField_8(String_t* value)
	{
		___U3COsVerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COsVerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CScreenWidthU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CScreenWidthU3Ek__BackingField_9)); }
	inline int32_t get_U3CScreenWidthU3Ek__BackingField_9() const { return ___U3CScreenWidthU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CScreenWidthU3Ek__BackingField_9() { return &___U3CScreenWidthU3Ek__BackingField_9; }
	inline void set_U3CScreenWidthU3Ek__BackingField_9(int32_t value)
	{
		___U3CScreenWidthU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CScreenHeightU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CScreenHeightU3Ek__BackingField_10)); }
	inline int32_t get_U3CScreenHeightU3Ek__BackingField_10() const { return ___U3CScreenHeightU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CScreenHeightU3Ek__BackingField_10() { return &___U3CScreenHeightU3Ek__BackingField_10; }
	inline void set_U3CScreenHeightU3Ek__BackingField_10(int32_t value)
	{
		___U3CScreenHeightU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CScreenDpiU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CScreenDpiU3Ek__BackingField_11)); }
	inline float get_U3CScreenDpiU3Ek__BackingField_11() const { return ___U3CScreenDpiU3Ek__BackingField_11; }
	inline float* get_address_of_U3CScreenDpiU3Ek__BackingField_11() { return &___U3CScreenDpiU3Ek__BackingField_11; }
	inline void set_U3CScreenDpiU3Ek__BackingField_11(float value)
	{
		___U3CScreenDpiU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CScreenOrientationU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CScreenOrientationU3Ek__BackingField_12)); }
	inline String_t* get_U3CScreenOrientationU3Ek__BackingField_12() const { return ___U3CScreenOrientationU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CScreenOrientationU3Ek__BackingField_12() { return &___U3CScreenOrientationU3Ek__BackingField_12; }
	inline void set_U3CScreenOrientationU3Ek__BackingField_12(String_t* value)
	{
		___U3CScreenOrientationU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScreenOrientationU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDeviceIdU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CDeviceIdU3Ek__BackingField_13)); }
	inline String_t* get_U3CDeviceIdU3Ek__BackingField_13() const { return ___U3CDeviceIdU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDeviceIdU3Ek__BackingField_13() { return &___U3CDeviceIdU3Ek__BackingField_13; }
	inline void set_U3CDeviceIdU3Ek__BackingField_13(String_t* value)
	{
		___U3CDeviceIdU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceIdU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CBuildGUIDU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CBuildGUIDU3Ek__BackingField_14)); }
	inline String_t* get_U3CBuildGUIDU3Ek__BackingField_14() const { return ___U3CBuildGUIDU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CBuildGUIDU3Ek__BackingField_14() { return &___U3CBuildGUIDU3Ek__BackingField_14; }
	inline void set_U3CBuildGUIDU3Ek__BackingField_14(String_t* value)
	{
		___U3CBuildGUIDU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBuildGUIDU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CIapVerU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CIapVerU3Ek__BackingField_15)); }
	inline String_t* get_U3CIapVerU3Ek__BackingField_15() const { return ___U3CIapVerU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CIapVerU3Ek__BackingField_15() { return &___U3CIapVerU3Ek__BackingField_15; }
	inline void set_U3CIapVerU3Ek__BackingField_15(String_t* value)
	{
		___U3CIapVerU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIapVerU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CAdsGamerTokenU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CAdsGamerTokenU3Ek__BackingField_16)); }
	inline String_t* get_U3CAdsGamerTokenU3Ek__BackingField_16() const { return ___U3CAdsGamerTokenU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CAdsGamerTokenU3Ek__BackingField_16() { return &___U3CAdsGamerTokenU3Ek__BackingField_16; }
	inline void set_U3CAdsGamerTokenU3Ek__BackingField_16(String_t* value)
	{
		___U3CAdsGamerTokenU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdsGamerTokenU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CTrackingOptOutU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CTrackingOptOutU3Ek__BackingField_17)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CTrackingOptOutU3Ek__BackingField_17() const { return ___U3CTrackingOptOutU3Ek__BackingField_17; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CTrackingOptOutU3Ek__BackingField_17() { return &___U3CTrackingOptOutU3Ek__BackingField_17; }
	inline void set_U3CTrackingOptOutU3Ek__BackingField_17(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CTrackingOptOutU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CAdsABGroupU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CAdsABGroupU3Ek__BackingField_18)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CAdsABGroupU3Ek__BackingField_18() const { return ___U3CAdsABGroupU3Ek__BackingField_18; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CAdsABGroupU3Ek__BackingField_18() { return &___U3CAdsABGroupU3Ek__BackingField_18; }
	inline void set_U3CAdsABGroupU3Ek__BackingField_18(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CAdsABGroupU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CAdsGameIdU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CAdsGameIdU3Ek__BackingField_19)); }
	inline String_t* get_U3CAdsGameIdU3Ek__BackingField_19() const { return ___U3CAdsGameIdU3Ek__BackingField_19; }
	inline String_t** get_address_of_U3CAdsGameIdU3Ek__BackingField_19() { return &___U3CAdsGameIdU3Ek__BackingField_19; }
	inline void set_U3CAdsGameIdU3Ek__BackingField_19(String_t* value)
	{
		___U3CAdsGameIdU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdsGameIdU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CStoreABGroupU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CStoreABGroupU3Ek__BackingField_20)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CStoreABGroupU3Ek__BackingField_20() const { return ___U3CStoreABGroupU3Ek__BackingField_20; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CStoreABGroupU3Ek__BackingField_20() { return &___U3CStoreABGroupU3Ek__BackingField_20; }
	inline void set_U3CStoreABGroupU3Ek__BackingField_20(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CStoreABGroupU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CCatalogIdU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CCatalogIdU3Ek__BackingField_21)); }
	inline String_t* get_U3CCatalogIdU3Ek__BackingField_21() const { return ___U3CCatalogIdU3Ek__BackingField_21; }
	inline String_t** get_address_of_U3CCatalogIdU3Ek__BackingField_21() { return &___U3CCatalogIdU3Ek__BackingField_21; }
	inline void set_U3CCatalogIdU3Ek__BackingField_21(String_t* value)
	{
		___U3CCatalogIdU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCatalogIdU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CMonetizationIdU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CMonetizationIdU3Ek__BackingField_22)); }
	inline String_t* get_U3CMonetizationIdU3Ek__BackingField_22() const { return ___U3CMonetizationIdU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CMonetizationIdU3Ek__BackingField_22() { return &___U3CMonetizationIdU3Ek__BackingField_22; }
	inline void set_U3CMonetizationIdU3Ek__BackingField_22(String_t* value)
	{
		___U3CMonetizationIdU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMonetizationIdU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CStoreNameU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CStoreNameU3Ek__BackingField_23)); }
	inline String_t* get_U3CStoreNameU3Ek__BackingField_23() const { return ___U3CStoreNameU3Ek__BackingField_23; }
	inline String_t** get_address_of_U3CStoreNameU3Ek__BackingField_23() { return &___U3CStoreNameU3Ek__BackingField_23; }
	inline void set_U3CStoreNameU3Ek__BackingField_23(String_t* value)
	{
		___U3CStoreNameU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStoreNameU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CGameVersionU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CGameVersionU3Ek__BackingField_24)); }
	inline String_t* get_U3CGameVersionU3Ek__BackingField_24() const { return ___U3CGameVersionU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CGameVersionU3Ek__BackingField_24() { return &___U3CGameVersionU3Ek__BackingField_24; }
	inline void set_U3CGameVersionU3Ek__BackingField_24(String_t* value)
	{
		___U3CGameVersionU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameVersionU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CStoreTestEnabledU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B, ___U3CStoreTestEnabledU3Ek__BackingField_25)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CStoreTestEnabledU3Ek__BackingField_25() const { return ___U3CStoreTestEnabledU3Ek__BackingField_25; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CStoreTestEnabledU3Ek__BackingField_25() { return &___U3CStoreTestEnabledU3Ek__BackingField_25; }
	inline void set_U3CStoreTestEnabledU3Ek__BackingField_25(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CStoreTestEnabledU3Ek__BackingField_25 = value;
	}
};

struct ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B_StaticFields
{
public:
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.ProfileData::ProfileInstance
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * ___ProfileInstance_1;

public:
	inline static int32_t get_offset_of_ProfileInstance_1() { return static_cast<int32_t>(offsetof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B_StaticFields, ___ProfileInstance_1)); }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * get_ProfileInstance_1() const { return ___ProfileInstance_1; }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B ** get_address_of_ProfileInstance_1() { return &___ProfileInstance_1; }
	inline void set_ProfileInstance_1(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * value)
	{
		___ProfileInstance_1 = value;
		Il2CppCodeGenWriteBarrier((&___ProfileInstance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEDATA_T59F197E40BC007C791FD64C4E05735F692CEB96B_H
#ifndef RESULT_T5191579D8310B3B0258ADC727B5BA9BDE174C572_H
#define RESULT_T5191579D8310B3B0258ADC727B5BA9BDE174C572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Result
struct  Result_t5191579D8310B3B0258ADC727B5BA9BDE174C572 
{
public:
	// System.Int32 UnityEngine.Purchasing.Result::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Result_t5191579D8310B3B0258ADC727B5BA9BDE174C572, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T5191579D8310B3B0258ADC727B5BA9BDE174C572_H
#ifndef SAMSUNGAPPSMODE_T1342C05EF24DD9123D3B5F24575A33E763B84EF1_H
#define SAMSUNGAPPSMODE_T1342C05EF24DD9123D3B5F24575A33E763B84EF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SamsungAppsMode
struct  SamsungAppsMode_t1342C05EF24DD9123D3B5F24575A33E763B84EF1 
{
public:
	// System.Int32 UnityEngine.Purchasing.SamsungAppsMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SamsungAppsMode_t1342C05EF24DD9123D3B5F24575A33E763B84EF1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMSUNGAPPSMODE_T1342C05EF24DD9123D3B5F24575A33E763B84EF1_H
#ifndef STORESPECIFICPURCHASEERRORCODE_T98BECD468210A3936A568F742D016DB19BB6B976_H
#define STORESPECIFICPURCHASEERRORCODE_T98BECD468210A3936A568F742D016DB19BB6B976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode
struct  StoreSpecificPurchaseErrorCode_t98BECD468210A3936A568F742D016DB19BB6B976 
{
public:
	// System.Int32 UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StoreSpecificPurchaseErrorCode_t98BECD468210A3936A568F742D016DB19BB6B976, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORESPECIFICPURCHASEERRORCODE_T98BECD468210A3936A568F742D016DB19BB6B976_H
#ifndef SUBSCRIPTIONPERIODUNIT_TBA4BCE498384075C6C808971B029C03CE0CA9673_H
#define SUBSCRIPTIONPERIODUNIT_TBA4BCE498384075C6C808971B029C03CE0CA9673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SubscriptionPeriodUnit
struct  SubscriptionPeriodUnit_tBA4BCE498384075C6C808971B029C03CE0CA9673 
{
public:
	// System.Int32 UnityEngine.Purchasing.SubscriptionPeriodUnit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SubscriptionPeriodUnit_tBA4BCE498384075C6C808971B029C03CE0CA9673, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIPTIONPERIODUNIT_TBA4BCE498384075C6C808971B029C03CE0CA9673_H
#ifndef TRANSLATIONLOCALE_TA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905_H
#define TRANSLATIONLOCALE_TA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TranslationLocale
struct  TranslationLocale_tA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905 
{
public:
	// System.Int32 UnityEngine.Purchasing.TranslationLocale::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TranslationLocale_tA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATIONLOCALE_TA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905_H
#ifndef RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#define RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimePlatform_tD5F5737C1BBBCBB115EB104DF2B7876387E80132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_TD5F5737C1BBBCBB115EB104DF2B7876387E80132_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T9ABDE042059E82B5E7F952F894F48526BC6EB749_H
#define U3CU3EC__DISPLAYCLASS11_0_T9ABDE042059E82B5E7F952F894F48526BC6EB749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int32> UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::delayInSeconds
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___delayInSeconds_0;
	// UnityEngine.Purchasing.EventDestType UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::dest
	int32_t ___dest_1;
	// System.String UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::json
	String_t* ___json_2;
	// System.String UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::target
	String_t* ___target_3;
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::<>4__this
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * ___U3CU3E4__this_4;
	// System.Action UnityEngine.Purchasing.EventQueue_<>c__DisplayClass11_0::<>9__2
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__2_5;

public:
	inline static int32_t get_offset_of_delayInSeconds_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749, ___delayInSeconds_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_delayInSeconds_0() const { return ___delayInSeconds_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_delayInSeconds_0() { return &___delayInSeconds_0; }
	inline void set_delayInSeconds_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___delayInSeconds_0 = value;
	}

	inline static int32_t get_offset_of_dest_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749, ___dest_1)); }
	inline int32_t get_dest_1() const { return ___dest_1; }
	inline int32_t* get_address_of_dest_1() { return &___dest_1; }
	inline void set_dest_1(int32_t value)
	{
		___dest_1 = value;
	}

	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749, ___json_2)); }
	inline String_t* get_json_2() const { return ___json_2; }
	inline String_t** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(String_t* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier((&___json_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749, ___target_3)); }
	inline String_t* get_target_3() const { return ___target_3; }
	inline String_t** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(String_t* value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749, ___U3CU3E4__this_4)); }
	inline EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749, ___U3CU3E9__2_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__2_5() const { return ___U3CU3E9__2_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__2_5() { return &___U3CU3E9__2_5; }
	inline void set_U3CU3E9__2_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T9ABDE042059E82B5E7F952F894F48526BC6EB749_H
#ifndef JSONSTORE_TA1476E0077C2071520663F35D28A5E7AF830CEE3_H
#define JSONSTORE_TA1476E0077C2071520663F35D28A5E7AF830CEE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JSONStore
struct  JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3  : public AbstractStore_t0809CE12BFD8191F29B7B77FC730AE3E8A3C039C
{
public:
	// UnityEngine.Purchasing.StoreCatalogImpl UnityEngine.Purchasing.JSONStore::m_managedStore
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB * ___m_managedStore_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.JSONStore::unity
	RuntimeObject* ___unity_1;
	// UnityEngine.Purchasing.INativeStore UnityEngine.Purchasing.JSONStore::store
	RuntimeObject* ___store_2;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.JSONStore::m_storeCatalog
	List_1_tFE611315D844DCEE9F38C09B0EADEFEBCCC51DBB * ___m_storeCatalog_3;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isManagedStoreEnabled
	bool ___isManagedStoreEnabled_4;
	// UnityEngine.Purchasing.ProfileData UnityEngine.Purchasing.JSONStore::m_profileData
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * ___m_profileData_5;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isRefreshing
	bool ___isRefreshing_6;
	// System.Boolean UnityEngine.Purchasing.JSONStore::isFirstTimeRetrievingProducts
	bool ___isFirstTimeRetrievingProducts_7;
	// System.Action UnityEngine.Purchasing.JSONStore::refreshCallback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___refreshCallback_8;
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.JSONStore::m_Module
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * ___m_Module_9;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.JSONStore::m_BuilderProducts
	HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * ___m_BuilderProducts_10;
	// UnityEngine.ILogger UnityEngine.Purchasing.JSONStore::m_Logger
	RuntimeObject* ___m_Logger_11;
	// UnityEngine.Purchasing.EventQueue UnityEngine.Purchasing.JSONStore::m_EventQueue
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * ___m_EventQueue_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.JSONStore::promoPayload
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ___promoPayload_13;
	// System.Boolean UnityEngine.Purchasing.JSONStore::catalogDisabled
	bool ___catalogDisabled_14;
	// System.Boolean UnityEngine.Purchasing.JSONStore::testStore
	bool ___testStore_15;
	// System.String UnityEngine.Purchasing.JSONStore::iapBaseUrl
	String_t* ___iapBaseUrl_16;
	// System.String UnityEngine.Purchasing.JSONStore::eventBaseUrl
	String_t* ___eventBaseUrl_17;
	// UnityEngine.Purchasing.Extension.PurchaseFailureDescription UnityEngine.Purchasing.JSONStore::lastPurchaseFailureDescription
	PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C * ___lastPurchaseFailureDescription_18;
	// UnityEngine.Purchasing.StoreSpecificPurchaseErrorCode UnityEngine.Purchasing.JSONStore::_lastPurchaseErrorCode
	int32_t ____lastPurchaseErrorCode_19;
	// System.String UnityEngine.Purchasing.JSONStore::kStoreSpecificErrorCodeKey
	String_t* ___kStoreSpecificErrorCodeKey_20;

public:
	inline static int32_t get_offset_of_m_managedStore_0() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_managedStore_0)); }
	inline StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB * get_m_managedStore_0() const { return ___m_managedStore_0; }
	inline StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB ** get_address_of_m_managedStore_0() { return &___m_managedStore_0; }
	inline void set_m_managedStore_0(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB * value)
	{
		___m_managedStore_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_managedStore_0), value);
	}

	inline static int32_t get_offset_of_unity_1() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___unity_1)); }
	inline RuntimeObject* get_unity_1() const { return ___unity_1; }
	inline RuntimeObject** get_address_of_unity_1() { return &___unity_1; }
	inline void set_unity_1(RuntimeObject* value)
	{
		___unity_1 = value;
		Il2CppCodeGenWriteBarrier((&___unity_1), value);
	}

	inline static int32_t get_offset_of_store_2() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___store_2)); }
	inline RuntimeObject* get_store_2() const { return ___store_2; }
	inline RuntimeObject** get_address_of_store_2() { return &___store_2; }
	inline void set_store_2(RuntimeObject* value)
	{
		___store_2 = value;
		Il2CppCodeGenWriteBarrier((&___store_2), value);
	}

	inline static int32_t get_offset_of_m_storeCatalog_3() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_storeCatalog_3)); }
	inline List_1_tFE611315D844DCEE9F38C09B0EADEFEBCCC51DBB * get_m_storeCatalog_3() const { return ___m_storeCatalog_3; }
	inline List_1_tFE611315D844DCEE9F38C09B0EADEFEBCCC51DBB ** get_address_of_m_storeCatalog_3() { return &___m_storeCatalog_3; }
	inline void set_m_storeCatalog_3(List_1_tFE611315D844DCEE9F38C09B0EADEFEBCCC51DBB * value)
	{
		___m_storeCatalog_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_storeCatalog_3), value);
	}

	inline static int32_t get_offset_of_isManagedStoreEnabled_4() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___isManagedStoreEnabled_4)); }
	inline bool get_isManagedStoreEnabled_4() const { return ___isManagedStoreEnabled_4; }
	inline bool* get_address_of_isManagedStoreEnabled_4() { return &___isManagedStoreEnabled_4; }
	inline void set_isManagedStoreEnabled_4(bool value)
	{
		___isManagedStoreEnabled_4 = value;
	}

	inline static int32_t get_offset_of_m_profileData_5() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_profileData_5)); }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * get_m_profileData_5() const { return ___m_profileData_5; }
	inline ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B ** get_address_of_m_profileData_5() { return &___m_profileData_5; }
	inline void set_m_profileData_5(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B * value)
	{
		___m_profileData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_profileData_5), value);
	}

	inline static int32_t get_offset_of_isRefreshing_6() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___isRefreshing_6)); }
	inline bool get_isRefreshing_6() const { return ___isRefreshing_6; }
	inline bool* get_address_of_isRefreshing_6() { return &___isRefreshing_6; }
	inline void set_isRefreshing_6(bool value)
	{
		___isRefreshing_6 = value;
	}

	inline static int32_t get_offset_of_isFirstTimeRetrievingProducts_7() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___isFirstTimeRetrievingProducts_7)); }
	inline bool get_isFirstTimeRetrievingProducts_7() const { return ___isFirstTimeRetrievingProducts_7; }
	inline bool* get_address_of_isFirstTimeRetrievingProducts_7() { return &___isFirstTimeRetrievingProducts_7; }
	inline void set_isFirstTimeRetrievingProducts_7(bool value)
	{
		___isFirstTimeRetrievingProducts_7 = value;
	}

	inline static int32_t get_offset_of_refreshCallback_8() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___refreshCallback_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_refreshCallback_8() const { return ___refreshCallback_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_refreshCallback_8() { return &___refreshCallback_8; }
	inline void set_refreshCallback_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___refreshCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___refreshCallback_8), value);
	}

	inline static int32_t get_offset_of_m_Module_9() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_Module_9)); }
	inline StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * get_m_Module_9() const { return ___m_Module_9; }
	inline StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A ** get_address_of_m_Module_9() { return &___m_Module_9; }
	inline void set_m_Module_9(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * value)
	{
		___m_Module_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Module_9), value);
	}

	inline static int32_t get_offset_of_m_BuilderProducts_10() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_BuilderProducts_10)); }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * get_m_BuilderProducts_10() const { return ___m_BuilderProducts_10; }
	inline HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 ** get_address_of_m_BuilderProducts_10() { return &___m_BuilderProducts_10; }
	inline void set_m_BuilderProducts_10(HashSet_1_tE69581B2FBB736F3DE03D470AE10601F835DF863 * value)
	{
		___m_BuilderProducts_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_BuilderProducts_10), value);
	}

	inline static int32_t get_offset_of_m_Logger_11() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_Logger_11)); }
	inline RuntimeObject* get_m_Logger_11() const { return ___m_Logger_11; }
	inline RuntimeObject** get_address_of_m_Logger_11() { return &___m_Logger_11; }
	inline void set_m_Logger_11(RuntimeObject* value)
	{
		___m_Logger_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Logger_11), value);
	}

	inline static int32_t get_offset_of_m_EventQueue_12() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___m_EventQueue_12)); }
	inline EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * get_m_EventQueue_12() const { return ___m_EventQueue_12; }
	inline EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA ** get_address_of_m_EventQueue_12() { return &___m_EventQueue_12; }
	inline void set_m_EventQueue_12(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA * value)
	{
		___m_EventQueue_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventQueue_12), value);
	}

	inline static int32_t get_offset_of_promoPayload_13() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___promoPayload_13)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get_promoPayload_13() const { return ___promoPayload_13; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of_promoPayload_13() { return &___promoPayload_13; }
	inline void set_promoPayload_13(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		___promoPayload_13 = value;
		Il2CppCodeGenWriteBarrier((&___promoPayload_13), value);
	}

	inline static int32_t get_offset_of_catalogDisabled_14() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___catalogDisabled_14)); }
	inline bool get_catalogDisabled_14() const { return ___catalogDisabled_14; }
	inline bool* get_address_of_catalogDisabled_14() { return &___catalogDisabled_14; }
	inline void set_catalogDisabled_14(bool value)
	{
		___catalogDisabled_14 = value;
	}

	inline static int32_t get_offset_of_testStore_15() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___testStore_15)); }
	inline bool get_testStore_15() const { return ___testStore_15; }
	inline bool* get_address_of_testStore_15() { return &___testStore_15; }
	inline void set_testStore_15(bool value)
	{
		___testStore_15 = value;
	}

	inline static int32_t get_offset_of_iapBaseUrl_16() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___iapBaseUrl_16)); }
	inline String_t* get_iapBaseUrl_16() const { return ___iapBaseUrl_16; }
	inline String_t** get_address_of_iapBaseUrl_16() { return &___iapBaseUrl_16; }
	inline void set_iapBaseUrl_16(String_t* value)
	{
		___iapBaseUrl_16 = value;
		Il2CppCodeGenWriteBarrier((&___iapBaseUrl_16), value);
	}

	inline static int32_t get_offset_of_eventBaseUrl_17() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___eventBaseUrl_17)); }
	inline String_t* get_eventBaseUrl_17() const { return ___eventBaseUrl_17; }
	inline String_t** get_address_of_eventBaseUrl_17() { return &___eventBaseUrl_17; }
	inline void set_eventBaseUrl_17(String_t* value)
	{
		___eventBaseUrl_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventBaseUrl_17), value);
	}

	inline static int32_t get_offset_of_lastPurchaseFailureDescription_18() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___lastPurchaseFailureDescription_18)); }
	inline PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C * get_lastPurchaseFailureDescription_18() const { return ___lastPurchaseFailureDescription_18; }
	inline PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C ** get_address_of_lastPurchaseFailureDescription_18() { return &___lastPurchaseFailureDescription_18; }
	inline void set_lastPurchaseFailureDescription_18(PurchaseFailureDescription_t54501CD2482DF31C114490488941916F8CD9B92C * value)
	{
		___lastPurchaseFailureDescription_18 = value;
		Il2CppCodeGenWriteBarrier((&___lastPurchaseFailureDescription_18), value);
	}

	inline static int32_t get_offset_of__lastPurchaseErrorCode_19() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ____lastPurchaseErrorCode_19)); }
	inline int32_t get__lastPurchaseErrorCode_19() const { return ____lastPurchaseErrorCode_19; }
	inline int32_t* get_address_of__lastPurchaseErrorCode_19() { return &____lastPurchaseErrorCode_19; }
	inline void set__lastPurchaseErrorCode_19(int32_t value)
	{
		____lastPurchaseErrorCode_19 = value;
	}

	inline static int32_t get_offset_of_kStoreSpecificErrorCodeKey_20() { return static_cast<int32_t>(offsetof(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3, ___kStoreSpecificErrorCodeKey_20)); }
	inline String_t* get_kStoreSpecificErrorCodeKey_20() const { return ___kStoreSpecificErrorCodeKey_20; }
	inline String_t** get_address_of_kStoreSpecificErrorCodeKey_20() { return &___kStoreSpecificErrorCodeKey_20; }
	inline void set_kStoreSpecificErrorCodeKey_20(String_t* value)
	{
		___kStoreSpecificErrorCodeKey_20 = value;
		Il2CppCodeGenWriteBarrier((&___kStoreSpecificErrorCodeKey_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTORE_TA1476E0077C2071520663F35D28A5E7AF830CEE3_H
#ifndef LOCALIZEDPRODUCTDESCRIPTION_T6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1_H
#define LOCALIZEDPRODUCTDESCRIPTION_T6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.LocalizedProductDescription
struct  LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.TranslationLocale UnityEngine.Purchasing.LocalizedProductDescription::googleLocale
	int32_t ___googleLocale_0;
	// System.String UnityEngine.Purchasing.LocalizedProductDescription::title
	String_t* ___title_1;
	// System.String UnityEngine.Purchasing.LocalizedProductDescription::description
	String_t* ___description_2;

public:
	inline static int32_t get_offset_of_googleLocale_0() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1, ___googleLocale_0)); }
	inline int32_t get_googleLocale_0() const { return ___googleLocale_0; }
	inline int32_t* get_address_of_googleLocale_0() { return &___googleLocale_0; }
	inline void set_googleLocale_0(int32_t value)
	{
		___googleLocale_0 = value;
	}

	inline static int32_t get_offset_of_title_1() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1, ___title_1)); }
	inline String_t* get_title_1() const { return ___title_1; }
	inline String_t** get_address_of_title_1() { return &___title_1; }
	inline void set_title_1(String_t* value)
	{
		___title_1 = value;
		Il2CppCodeGenWriteBarrier((&___title_1), value);
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((&___description_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZEDPRODUCTDESCRIPTION_T6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1_H
#ifndef PRODUCTCATALOGITEM_T532825A86166318D5A6DB6D7671F51573BD0267D_H
#define PRODUCTCATALOGITEM_T532825A86166318D5A6DB6D7671F51573BD0267D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogItem
struct  ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductCatalogItem::id
	String_t* ___id_0;
	// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.ProductCatalogItem::type
	int32_t ___type_1;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.StoreID> UnityEngine.Purchasing.ProductCatalogItem::storeIDs
	List_1_tE827FDB9C085BDBF2BD397CEC9BC2DAE20C9AB01 * ___storeIDs_2;
	// UnityEngine.Purchasing.LocalizedProductDescription UnityEngine.Purchasing.ProductCatalogItem::defaultDescription
	LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1 * ___defaultDescription_3;
	// System.Int32 UnityEngine.Purchasing.ProductCatalogItem::applePriceTier
	int32_t ___applePriceTier_4;
	// System.Int32 UnityEngine.Purchasing.ProductCatalogItem::xiaomiPriceTier
	int32_t ___xiaomiPriceTier_5;
	// UnityEngine.Purchasing.Price UnityEngine.Purchasing.ProductCatalogItem::googlePrice
	Price_t5D55521A645803976E0809F8941E8317C80E129C * ___googlePrice_6;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.LocalizedProductDescription> UnityEngine.Purchasing.ProductCatalogItem::descriptions
	List_1_t4A5E4DB145DD9798C5FEB91EE0F1A7C77E1F43EB * ___descriptions_7;
	// UnityEngine.Purchasing.Price UnityEngine.Purchasing.ProductCatalogItem::udpPrice
	Price_t5D55521A645803976E0809F8941E8317C80E129C * ___udpPrice_8;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.ProductCatalogPayout> UnityEngine.Purchasing.ProductCatalogItem::payouts
	List_1_t6EBBFBE956641E9855F6E08AA6671EDB7E9F0B2B * ___payouts_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_storeIDs_2() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___storeIDs_2)); }
	inline List_1_tE827FDB9C085BDBF2BD397CEC9BC2DAE20C9AB01 * get_storeIDs_2() const { return ___storeIDs_2; }
	inline List_1_tE827FDB9C085BDBF2BD397CEC9BC2DAE20C9AB01 ** get_address_of_storeIDs_2() { return &___storeIDs_2; }
	inline void set_storeIDs_2(List_1_tE827FDB9C085BDBF2BD397CEC9BC2DAE20C9AB01 * value)
	{
		___storeIDs_2 = value;
		Il2CppCodeGenWriteBarrier((&___storeIDs_2), value);
	}

	inline static int32_t get_offset_of_defaultDescription_3() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___defaultDescription_3)); }
	inline LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1 * get_defaultDescription_3() const { return ___defaultDescription_3; }
	inline LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1 ** get_address_of_defaultDescription_3() { return &___defaultDescription_3; }
	inline void set_defaultDescription_3(LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1 * value)
	{
		___defaultDescription_3 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDescription_3), value);
	}

	inline static int32_t get_offset_of_applePriceTier_4() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___applePriceTier_4)); }
	inline int32_t get_applePriceTier_4() const { return ___applePriceTier_4; }
	inline int32_t* get_address_of_applePriceTier_4() { return &___applePriceTier_4; }
	inline void set_applePriceTier_4(int32_t value)
	{
		___applePriceTier_4 = value;
	}

	inline static int32_t get_offset_of_xiaomiPriceTier_5() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___xiaomiPriceTier_5)); }
	inline int32_t get_xiaomiPriceTier_5() const { return ___xiaomiPriceTier_5; }
	inline int32_t* get_address_of_xiaomiPriceTier_5() { return &___xiaomiPriceTier_5; }
	inline void set_xiaomiPriceTier_5(int32_t value)
	{
		___xiaomiPriceTier_5 = value;
	}

	inline static int32_t get_offset_of_googlePrice_6() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___googlePrice_6)); }
	inline Price_t5D55521A645803976E0809F8941E8317C80E129C * get_googlePrice_6() const { return ___googlePrice_6; }
	inline Price_t5D55521A645803976E0809F8941E8317C80E129C ** get_address_of_googlePrice_6() { return &___googlePrice_6; }
	inline void set_googlePrice_6(Price_t5D55521A645803976E0809F8941E8317C80E129C * value)
	{
		___googlePrice_6 = value;
		Il2CppCodeGenWriteBarrier((&___googlePrice_6), value);
	}

	inline static int32_t get_offset_of_descriptions_7() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___descriptions_7)); }
	inline List_1_t4A5E4DB145DD9798C5FEB91EE0F1A7C77E1F43EB * get_descriptions_7() const { return ___descriptions_7; }
	inline List_1_t4A5E4DB145DD9798C5FEB91EE0F1A7C77E1F43EB ** get_address_of_descriptions_7() { return &___descriptions_7; }
	inline void set_descriptions_7(List_1_t4A5E4DB145DD9798C5FEB91EE0F1A7C77E1F43EB * value)
	{
		___descriptions_7 = value;
		Il2CppCodeGenWriteBarrier((&___descriptions_7), value);
	}

	inline static int32_t get_offset_of_udpPrice_8() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___udpPrice_8)); }
	inline Price_t5D55521A645803976E0809F8941E8317C80E129C * get_udpPrice_8() const { return ___udpPrice_8; }
	inline Price_t5D55521A645803976E0809F8941E8317C80E129C ** get_address_of_udpPrice_8() { return &___udpPrice_8; }
	inline void set_udpPrice_8(Price_t5D55521A645803976E0809F8941E8317C80E129C * value)
	{
		___udpPrice_8 = value;
		Il2CppCodeGenWriteBarrier((&___udpPrice_8), value);
	}

	inline static int32_t get_offset_of_payouts_9() { return static_cast<int32_t>(offsetof(ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D, ___payouts_9)); }
	inline List_1_t6EBBFBE956641E9855F6E08AA6671EDB7E9F0B2B * get_payouts_9() const { return ___payouts_9; }
	inline List_1_t6EBBFBE956641E9855F6E08AA6671EDB7E9F0B2B ** get_address_of_payouts_9() { return &___payouts_9; }
	inline void set_payouts_9(List_1_t6EBBFBE956641E9855F6E08AA6671EDB7E9F0B2B * value)
	{
		___payouts_9 = value;
		Il2CppCodeGenWriteBarrier((&___payouts_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCATALOGITEM_T532825A86166318D5A6DB6D7671F51573BD0267D_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T4900BD1283E21B410243ACA012CBBD7581F1843D_H
#define U3CU3EC__DISPLAYCLASS22_0_T4900BD1283E21B410243ACA012CBBD7581F1843D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCatalogItem_<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t4900BD1283E21B410243ACA012CBBD7581F1843D  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.TranslationLocale UnityEngine.Purchasing.ProductCatalogItem_<>c__DisplayClass22_0::locale
	int32_t ___locale_0;

public:
	inline static int32_t get_offset_of_locale_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t4900BD1283E21B410243ACA012CBBD7581F1843D, ___locale_0)); }
	inline int32_t get_locale_0() const { return ___locale_0; }
	inline int32_t* get_address_of_locale_0() { return &___locale_0; }
	inline void set_locale_0(int32_t value)
	{
		___locale_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T4900BD1283E21B410243ACA012CBBD7581F1843D_H
#ifndef PROMO_T15638919BA9DB952B4D59C5791FB46774C12D1A4_H
#define PROMO_T15638919BA9DB952B4D59C5791FB46774C12D1A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Promo
struct  Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4  : public RuntimeObject
{
public:

public:
};

struct Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields
{
public:
	// UnityEngine.Purchasing.JSONStore UnityEngine.Purchasing.Promo::s_PromoPurchaser
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3 * ___s_PromoPurchaser_0;
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.Promo::s_Unity
	RuntimeObject* ___s_Unity_1;
	// UnityEngine.RuntimePlatform UnityEngine.Purchasing.Promo::s_RuntimePlatform
	int32_t ___s_RuntimePlatform_2;
	// UnityEngine.ILogger UnityEngine.Purchasing.Promo::s_Logger
	RuntimeObject* ___s_Logger_3;
	// System.String UnityEngine.Purchasing.Promo::s_Version
	String_t* ___s_Version_4;
	// Uniject.IUtil UnityEngine.Purchasing.Promo::s_Util
	RuntimeObject* ___s_Util_5;
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.Promo::s_WebUtil
	RuntimeObject* ___s_WebUtil_6;
	// System.Boolean UnityEngine.Purchasing.Promo::s_IsReady
	bool ___s_IsReady_7;
	// System.String UnityEngine.Purchasing.Promo::s_ProductJSON
	String_t* ___s_ProductJSON_8;

public:
	inline static int32_t get_offset_of_s_PromoPurchaser_0() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_PromoPurchaser_0)); }
	inline JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3 * get_s_PromoPurchaser_0() const { return ___s_PromoPurchaser_0; }
	inline JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3 ** get_address_of_s_PromoPurchaser_0() { return &___s_PromoPurchaser_0; }
	inline void set_s_PromoPurchaser_0(JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3 * value)
	{
		___s_PromoPurchaser_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PromoPurchaser_0), value);
	}

	inline static int32_t get_offset_of_s_Unity_1() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_Unity_1)); }
	inline RuntimeObject* get_s_Unity_1() const { return ___s_Unity_1; }
	inline RuntimeObject** get_address_of_s_Unity_1() { return &___s_Unity_1; }
	inline void set_s_Unity_1(RuntimeObject* value)
	{
		___s_Unity_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Unity_1), value);
	}

	inline static int32_t get_offset_of_s_RuntimePlatform_2() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_RuntimePlatform_2)); }
	inline int32_t get_s_RuntimePlatform_2() const { return ___s_RuntimePlatform_2; }
	inline int32_t* get_address_of_s_RuntimePlatform_2() { return &___s_RuntimePlatform_2; }
	inline void set_s_RuntimePlatform_2(int32_t value)
	{
		___s_RuntimePlatform_2 = value;
	}

	inline static int32_t get_offset_of_s_Logger_3() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_Logger_3)); }
	inline RuntimeObject* get_s_Logger_3() const { return ___s_Logger_3; }
	inline RuntimeObject** get_address_of_s_Logger_3() { return &___s_Logger_3; }
	inline void set_s_Logger_3(RuntimeObject* value)
	{
		___s_Logger_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Logger_3), value);
	}

	inline static int32_t get_offset_of_s_Version_4() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_Version_4)); }
	inline String_t* get_s_Version_4() const { return ___s_Version_4; }
	inline String_t** get_address_of_s_Version_4() { return &___s_Version_4; }
	inline void set_s_Version_4(String_t* value)
	{
		___s_Version_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Version_4), value);
	}

	inline static int32_t get_offset_of_s_Util_5() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_Util_5)); }
	inline RuntimeObject* get_s_Util_5() const { return ___s_Util_5; }
	inline RuntimeObject** get_address_of_s_Util_5() { return &___s_Util_5; }
	inline void set_s_Util_5(RuntimeObject* value)
	{
		___s_Util_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Util_5), value);
	}

	inline static int32_t get_offset_of_s_WebUtil_6() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_WebUtil_6)); }
	inline RuntimeObject* get_s_WebUtil_6() const { return ___s_WebUtil_6; }
	inline RuntimeObject** get_address_of_s_WebUtil_6() { return &___s_WebUtil_6; }
	inline void set_s_WebUtil_6(RuntimeObject* value)
	{
		___s_WebUtil_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_WebUtil_6), value);
	}

	inline static int32_t get_offset_of_s_IsReady_7() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_IsReady_7)); }
	inline bool get_s_IsReady_7() const { return ___s_IsReady_7; }
	inline bool* get_address_of_s_IsReady_7() { return &___s_IsReady_7; }
	inline void set_s_IsReady_7(bool value)
	{
		___s_IsReady_7 = value;
	}

	inline static int32_t get_offset_of_s_ProductJSON_8() { return static_cast<int32_t>(offsetof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields, ___s_ProductJSON_8)); }
	inline String_t* get_s_ProductJSON_8() const { return ___s_ProductJSON_8; }
	inline String_t** get_address_of_s_ProductJSON_8() { return &___s_ProductJSON_8; }
	inline void set_s_ProductJSON_8(String_t* value)
	{
		___s_ProductJSON_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_ProductJSON_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROMO_T15638919BA9DB952B4D59C5791FB46774C12D1A4_H
#ifndef STANDARDPURCHASINGMODULE_TDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_H
#define STANDARDPURCHASINGMODULE_TDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule
struct  StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A  : public AbstractPurchasingModule_t73F8B7B7D6CA305D6CFAFE4692A6F0F7B42531D9
{
public:
	// UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StandardPurchasingModule::m_AppStorePlatform
	int32_t ___m_AppStorePlatform_1;
	// UnityEngine.Purchasing.INativeStoreProvider UnityEngine.Purchasing.StandardPurchasingModule::m_NativeStoreProvider
	RuntimeObject* ___m_NativeStoreProvider_2;
	// UnityEngine.RuntimePlatform UnityEngine.Purchasing.StandardPurchasingModule::m_RuntimePlatform
	int32_t ___m_RuntimePlatform_3;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::m_UseCloudCatalog
	bool ___m_UseCloudCatalog_4;
	// Uniject.IUtil UnityEngine.Purchasing.StandardPurchasingModule::<util>k__BackingField
	RuntimeObject* ___U3CutilU3Ek__BackingField_6;
	// UnityEngine.ILogger UnityEngine.Purchasing.StandardPurchasingModule::<logger>k__BackingField
	RuntimeObject* ___U3CloggerU3Ek__BackingField_7;
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StandardPurchasingModule::<webUtil>k__BackingField
	RuntimeObject* ___U3CwebUtilU3Ek__BackingField_8;
	// UnityEngine.Purchasing.StandardPurchasingModule_StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::<storeInstance>k__BackingField
	StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57 * ___U3CstoreInstanceU3Ek__BackingField_9;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.StandardPurchasingModule::m_CloudCatalog
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 * ___m_CloudCatalog_11;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreUIMode>k__BackingField
	int32_t ___U3CuseFakeStoreUIModeU3Ek__BackingField_12;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreAlways>k__BackingField
	bool ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13;
	// UnityEngine.Purchasing.WinRTStore UnityEngine.Purchasing.StandardPurchasingModule::windowsStore
	WinRTStore_tE10DCCB8F603A2D08C6EC23F9FA3EF0DD05B7E5F * ___windowsStore_14;

public:
	inline static int32_t get_offset_of_m_AppStorePlatform_1() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___m_AppStorePlatform_1)); }
	inline int32_t get_m_AppStorePlatform_1() const { return ___m_AppStorePlatform_1; }
	inline int32_t* get_address_of_m_AppStorePlatform_1() { return &___m_AppStorePlatform_1; }
	inline void set_m_AppStorePlatform_1(int32_t value)
	{
		___m_AppStorePlatform_1 = value;
	}

	inline static int32_t get_offset_of_m_NativeStoreProvider_2() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___m_NativeStoreProvider_2)); }
	inline RuntimeObject* get_m_NativeStoreProvider_2() const { return ___m_NativeStoreProvider_2; }
	inline RuntimeObject** get_address_of_m_NativeStoreProvider_2() { return &___m_NativeStoreProvider_2; }
	inline void set_m_NativeStoreProvider_2(RuntimeObject* value)
	{
		___m_NativeStoreProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeStoreProvider_2), value);
	}

	inline static int32_t get_offset_of_m_RuntimePlatform_3() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___m_RuntimePlatform_3)); }
	inline int32_t get_m_RuntimePlatform_3() const { return ___m_RuntimePlatform_3; }
	inline int32_t* get_address_of_m_RuntimePlatform_3() { return &___m_RuntimePlatform_3; }
	inline void set_m_RuntimePlatform_3(int32_t value)
	{
		___m_RuntimePlatform_3 = value;
	}

	inline static int32_t get_offset_of_m_UseCloudCatalog_4() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___m_UseCloudCatalog_4)); }
	inline bool get_m_UseCloudCatalog_4() const { return ___m_UseCloudCatalog_4; }
	inline bool* get_address_of_m_UseCloudCatalog_4() { return &___m_UseCloudCatalog_4; }
	inline void set_m_UseCloudCatalog_4(bool value)
	{
		___m_UseCloudCatalog_4 = value;
	}

	inline static int32_t get_offset_of_U3CutilU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___U3CutilU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CutilU3Ek__BackingField_6() const { return ___U3CutilU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CutilU3Ek__BackingField_6() { return &___U3CutilU3Ek__BackingField_6; }
	inline void set_U3CutilU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CutilU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CutilU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CloggerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___U3CloggerU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CloggerU3Ek__BackingField_7() const { return ___U3CloggerU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CloggerU3Ek__BackingField_7() { return &___U3CloggerU3Ek__BackingField_7; }
	inline void set_U3CloggerU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CloggerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloggerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CwebUtilU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___U3CwebUtilU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CwebUtilU3Ek__BackingField_8() const { return ___U3CwebUtilU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CwebUtilU3Ek__BackingField_8() { return &___U3CwebUtilU3Ek__BackingField_8; }
	inline void set_U3CwebUtilU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CwebUtilU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebUtilU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CstoreInstanceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___U3CstoreInstanceU3Ek__BackingField_9)); }
	inline StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57 * get_U3CstoreInstanceU3Ek__BackingField_9() const { return ___U3CstoreInstanceU3Ek__BackingField_9; }
	inline StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57 ** get_address_of_U3CstoreInstanceU3Ek__BackingField_9() { return &___U3CstoreInstanceU3Ek__BackingField_9; }
	inline void set_U3CstoreInstanceU3Ek__BackingField_9(StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57 * value)
	{
		___U3CstoreInstanceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreInstanceU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_m_CloudCatalog_11() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___m_CloudCatalog_11)); }
	inline CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 * get_m_CloudCatalog_11() const { return ___m_CloudCatalog_11; }
	inline CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 ** get_address_of_m_CloudCatalog_11() { return &___m_CloudCatalog_11; }
	inline void set_m_CloudCatalog_11(CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711 * value)
	{
		___m_CloudCatalog_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CloudCatalog_11), value);
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___U3CuseFakeStoreUIModeU3Ek__BackingField_12)); }
	inline int32_t get_U3CuseFakeStoreUIModeU3Ek__BackingField_12() const { return ___U3CuseFakeStoreUIModeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12() { return &___U3CuseFakeStoreUIModeU3Ek__BackingField_12; }
	inline void set_U3CuseFakeStoreUIModeU3Ek__BackingField_12(int32_t value)
	{
		___U3CuseFakeStoreUIModeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13)); }
	inline bool get_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() const { return ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() { return &___U3CuseFakeStoreAlwaysU3Ek__BackingField_13; }
	inline void set_U3CuseFakeStoreAlwaysU3Ek__BackingField_13(bool value)
	{
		___U3CuseFakeStoreAlwaysU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_windowsStore_14() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A, ___windowsStore_14)); }
	inline WinRTStore_tE10DCCB8F603A2D08C6EC23F9FA3EF0DD05B7E5F * get_windowsStore_14() const { return ___windowsStore_14; }
	inline WinRTStore_tE10DCCB8F603A2D08C6EC23F9FA3EF0DD05B7E5F ** get_address_of_windowsStore_14() { return &___windowsStore_14; }
	inline void set_windowsStore_14(WinRTStore_tE10DCCB8F603A2D08C6EC23F9FA3EF0DD05B7E5F * value)
	{
		___windowsStore_14 = value;
		Il2CppCodeGenWriteBarrier((&___windowsStore_14), value);
	}
};

struct StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_StaticFields
{
public:
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::ModuleInstance
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * ___ModuleInstance_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String> UnityEngine.Purchasing.StandardPurchasingModule::AndroidStoreNameMap
	Dictionary_2_t4F339596DF041CC12B899559EAACD15AA5D2AF4F * ___AndroidStoreNameMap_10;

public:
	inline static int32_t get_offset_of_ModuleInstance_5() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_StaticFields, ___ModuleInstance_5)); }
	inline StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * get_ModuleInstance_5() const { return ___ModuleInstance_5; }
	inline StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A ** get_address_of_ModuleInstance_5() { return &___ModuleInstance_5; }
	inline void set_ModuleInstance_5(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A * value)
	{
		___ModuleInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___ModuleInstance_5), value);
	}

	inline static int32_t get_offset_of_AndroidStoreNameMap_10() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_StaticFields, ___AndroidStoreNameMap_10)); }
	inline Dictionary_2_t4F339596DF041CC12B899559EAACD15AA5D2AF4F * get_AndroidStoreNameMap_10() const { return ___AndroidStoreNameMap_10; }
	inline Dictionary_2_t4F339596DF041CC12B899559EAACD15AA5D2AF4F ** get_address_of_AndroidStoreNameMap_10() { return &___AndroidStoreNameMap_10; }
	inline void set_AndroidStoreNameMap_10(Dictionary_2_t4F339596DF041CC12B899559EAACD15AA5D2AF4F * value)
	{
		___AndroidStoreNameMap_10 = value;
		Il2CppCodeGenWriteBarrier((&___AndroidStoreNameMap_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDPURCHASINGMODULE_TDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_H
#ifndef STORECONFIGURATION_T19E406F75E8D4B671D72D2A5AFE051B3605E6B86_H
#define STORECONFIGURATION_T19E406F75E8D4B671D72D2A5AFE051B3605E6B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StoreConfiguration
struct  StoreConfiguration_t19E406F75E8D4B671D72D2A5AFE051B3605E6B86  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StoreConfiguration::<androidStore>k__BackingField
	int32_t ___U3CandroidStoreU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CandroidStoreU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StoreConfiguration_t19E406F75E8D4B671D72D2A5AFE051B3605E6B86, ___U3CandroidStoreU3Ek__BackingField_0)); }
	inline int32_t get_U3CandroidStoreU3Ek__BackingField_0() const { return ___U3CandroidStoreU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CandroidStoreU3Ek__BackingField_0() { return &___U3CandroidStoreU3Ek__BackingField_0; }
	inline void set_U3CandroidStoreU3Ek__BackingField_0(int32_t value)
	{
		___U3CandroidStoreU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORECONFIGURATION_T19E406F75E8D4B671D72D2A5AFE051B3605E6B86_H
#ifndef SUBSCRIPTIONINFO_T32C4ADDD2146194CB2C6540C693A977AFA158133_H
#define SUBSCRIPTIONINFO_T32C4ADDD2146194CB2C6540C693A977AFA158133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SubscriptionInfo
struct  SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_subscribed
	int32_t ___is_subscribed_0;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_expired
	int32_t ___is_expired_1;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_cancelled
	int32_t ___is_cancelled_2;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_free_trial
	int32_t ___is_free_trial_3;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_auto_renewing
	int32_t ___is_auto_renewing_4;
	// UnityEngine.Purchasing.Result UnityEngine.Purchasing.SubscriptionInfo::is_introductory_price_period
	int32_t ___is_introductory_price_period_5;
	// System.String UnityEngine.Purchasing.SubscriptionInfo::productId
	String_t* ___productId_6;
	// System.DateTime UnityEngine.Purchasing.SubscriptionInfo::purchaseDate
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___purchaseDate_7;
	// System.DateTime UnityEngine.Purchasing.SubscriptionInfo::subscriptionExpireDate
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___subscriptionExpireDate_8;
	// System.DateTime UnityEngine.Purchasing.SubscriptionInfo::subscriptionCancelDate
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___subscriptionCancelDate_9;
	// System.TimeSpan UnityEngine.Purchasing.SubscriptionInfo::remainedTime
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___remainedTime_10;
	// System.String UnityEngine.Purchasing.SubscriptionInfo::introductory_price
	String_t* ___introductory_price_11;
	// System.TimeSpan UnityEngine.Purchasing.SubscriptionInfo::introductory_price_period
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___introductory_price_period_12;
	// System.Int64 UnityEngine.Purchasing.SubscriptionInfo::introductory_price_cycles
	int64_t ___introductory_price_cycles_13;

public:
	inline static int32_t get_offset_of_is_subscribed_0() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___is_subscribed_0)); }
	inline int32_t get_is_subscribed_0() const { return ___is_subscribed_0; }
	inline int32_t* get_address_of_is_subscribed_0() { return &___is_subscribed_0; }
	inline void set_is_subscribed_0(int32_t value)
	{
		___is_subscribed_0 = value;
	}

	inline static int32_t get_offset_of_is_expired_1() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___is_expired_1)); }
	inline int32_t get_is_expired_1() const { return ___is_expired_1; }
	inline int32_t* get_address_of_is_expired_1() { return &___is_expired_1; }
	inline void set_is_expired_1(int32_t value)
	{
		___is_expired_1 = value;
	}

	inline static int32_t get_offset_of_is_cancelled_2() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___is_cancelled_2)); }
	inline int32_t get_is_cancelled_2() const { return ___is_cancelled_2; }
	inline int32_t* get_address_of_is_cancelled_2() { return &___is_cancelled_2; }
	inline void set_is_cancelled_2(int32_t value)
	{
		___is_cancelled_2 = value;
	}

	inline static int32_t get_offset_of_is_free_trial_3() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___is_free_trial_3)); }
	inline int32_t get_is_free_trial_3() const { return ___is_free_trial_3; }
	inline int32_t* get_address_of_is_free_trial_3() { return &___is_free_trial_3; }
	inline void set_is_free_trial_3(int32_t value)
	{
		___is_free_trial_3 = value;
	}

	inline static int32_t get_offset_of_is_auto_renewing_4() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___is_auto_renewing_4)); }
	inline int32_t get_is_auto_renewing_4() const { return ___is_auto_renewing_4; }
	inline int32_t* get_address_of_is_auto_renewing_4() { return &___is_auto_renewing_4; }
	inline void set_is_auto_renewing_4(int32_t value)
	{
		___is_auto_renewing_4 = value;
	}

	inline static int32_t get_offset_of_is_introductory_price_period_5() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___is_introductory_price_period_5)); }
	inline int32_t get_is_introductory_price_period_5() const { return ___is_introductory_price_period_5; }
	inline int32_t* get_address_of_is_introductory_price_period_5() { return &___is_introductory_price_period_5; }
	inline void set_is_introductory_price_period_5(int32_t value)
	{
		___is_introductory_price_period_5 = value;
	}

	inline static int32_t get_offset_of_productId_6() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___productId_6)); }
	inline String_t* get_productId_6() const { return ___productId_6; }
	inline String_t** get_address_of_productId_6() { return &___productId_6; }
	inline void set_productId_6(String_t* value)
	{
		___productId_6 = value;
		Il2CppCodeGenWriteBarrier((&___productId_6), value);
	}

	inline static int32_t get_offset_of_purchaseDate_7() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___purchaseDate_7)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_purchaseDate_7() const { return ___purchaseDate_7; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_purchaseDate_7() { return &___purchaseDate_7; }
	inline void set_purchaseDate_7(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___purchaseDate_7 = value;
	}

	inline static int32_t get_offset_of_subscriptionExpireDate_8() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___subscriptionExpireDate_8)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_subscriptionExpireDate_8() const { return ___subscriptionExpireDate_8; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_subscriptionExpireDate_8() { return &___subscriptionExpireDate_8; }
	inline void set_subscriptionExpireDate_8(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___subscriptionExpireDate_8 = value;
	}

	inline static int32_t get_offset_of_subscriptionCancelDate_9() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___subscriptionCancelDate_9)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_subscriptionCancelDate_9() const { return ___subscriptionCancelDate_9; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_subscriptionCancelDate_9() { return &___subscriptionCancelDate_9; }
	inline void set_subscriptionCancelDate_9(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___subscriptionCancelDate_9 = value;
	}

	inline static int32_t get_offset_of_remainedTime_10() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___remainedTime_10)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_remainedTime_10() const { return ___remainedTime_10; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_remainedTime_10() { return &___remainedTime_10; }
	inline void set_remainedTime_10(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___remainedTime_10 = value;
	}

	inline static int32_t get_offset_of_introductory_price_11() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___introductory_price_11)); }
	inline String_t* get_introductory_price_11() const { return ___introductory_price_11; }
	inline String_t** get_address_of_introductory_price_11() { return &___introductory_price_11; }
	inline void set_introductory_price_11(String_t* value)
	{
		___introductory_price_11 = value;
		Il2CppCodeGenWriteBarrier((&___introductory_price_11), value);
	}

	inline static int32_t get_offset_of_introductory_price_period_12() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___introductory_price_period_12)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_introductory_price_period_12() const { return ___introductory_price_period_12; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_introductory_price_period_12() { return &___introductory_price_period_12; }
	inline void set_introductory_price_period_12(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___introductory_price_period_12 = value;
	}

	inline static int32_t get_offset_of_introductory_price_cycles_13() { return static_cast<int32_t>(offsetof(SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133, ___introductory_price_cycles_13)); }
	inline int64_t get_introductory_price_cycles_13() const { return ___introductory_price_cycles_13; }
	inline int64_t* get_address_of_introductory_price_cycles_13() { return &___introductory_price_cycles_13; }
	inline void set_introductory_price_cycles_13(int64_t value)
	{
		___introductory_price_cycles_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSCRIPTIONINFO_T32C4ADDD2146194CB2C6540C693A977AFA158133_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef APPLESTOREIMPL_T13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_H
#define APPLESTOREIMPL_T13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppleStoreImpl
struct  AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD  : public JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3
{
public:
	// System.Action`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.AppleStoreImpl::m_DeferredCallback
	Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 * ___m_DeferredCallback_21;
	// System.Action UnityEngine.Purchasing.AppleStoreImpl::m_RefreshReceiptError
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_RefreshReceiptError_22;
	// System.Action`1<System.String> UnityEngine.Purchasing.AppleStoreImpl::m_RefreshReceiptSuccess
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___m_RefreshReceiptSuccess_23;
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.AppleStoreImpl::m_RestoreCallback
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___m_RestoreCallback_24;
	// System.Action`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.AppleStoreImpl::m_PromotionalPurchaseCallback
	Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 * ___m_PromotionalPurchaseCallback_25;
	// UnityEngine.Purchasing.INativeAppleStore UnityEngine.Purchasing.AppleStoreImpl::m_Native
	RuntimeObject* ___m_Native_26;
	// System.String UnityEngine.Purchasing.AppleStoreImpl::products_json
	String_t* ___products_json_29;

public:
	inline static int32_t get_offset_of_m_DeferredCallback_21() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___m_DeferredCallback_21)); }
	inline Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 * get_m_DeferredCallback_21() const { return ___m_DeferredCallback_21; }
	inline Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 ** get_address_of_m_DeferredCallback_21() { return &___m_DeferredCallback_21; }
	inline void set_m_DeferredCallback_21(Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 * value)
	{
		___m_DeferredCallback_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_DeferredCallback_21), value);
	}

	inline static int32_t get_offset_of_m_RefreshReceiptError_22() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___m_RefreshReceiptError_22)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_RefreshReceiptError_22() const { return ___m_RefreshReceiptError_22; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_RefreshReceiptError_22() { return &___m_RefreshReceiptError_22; }
	inline void set_m_RefreshReceiptError_22(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_RefreshReceiptError_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefreshReceiptError_22), value);
	}

	inline static int32_t get_offset_of_m_RefreshReceiptSuccess_23() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___m_RefreshReceiptSuccess_23)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_m_RefreshReceiptSuccess_23() const { return ___m_RefreshReceiptSuccess_23; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_m_RefreshReceiptSuccess_23() { return &___m_RefreshReceiptSuccess_23; }
	inline void set_m_RefreshReceiptSuccess_23(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___m_RefreshReceiptSuccess_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefreshReceiptSuccess_23), value);
	}

	inline static int32_t get_offset_of_m_RestoreCallback_24() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___m_RestoreCallback_24)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_m_RestoreCallback_24() const { return ___m_RestoreCallback_24; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_m_RestoreCallback_24() { return &___m_RestoreCallback_24; }
	inline void set_m_RestoreCallback_24(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___m_RestoreCallback_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_RestoreCallback_24), value);
	}

	inline static int32_t get_offset_of_m_PromotionalPurchaseCallback_25() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___m_PromotionalPurchaseCallback_25)); }
	inline Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 * get_m_PromotionalPurchaseCallback_25() const { return ___m_PromotionalPurchaseCallback_25; }
	inline Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 ** get_address_of_m_PromotionalPurchaseCallback_25() { return &___m_PromotionalPurchaseCallback_25; }
	inline void set_m_PromotionalPurchaseCallback_25(Action_1_t2482F53569EF3B2342065C4D842BC9BBDD535D30 * value)
	{
		___m_PromotionalPurchaseCallback_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_PromotionalPurchaseCallback_25), value);
	}

	inline static int32_t get_offset_of_m_Native_26() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___m_Native_26)); }
	inline RuntimeObject* get_m_Native_26() const { return ___m_Native_26; }
	inline RuntimeObject** get_address_of_m_Native_26() { return &___m_Native_26; }
	inline void set_m_Native_26(RuntimeObject* value)
	{
		___m_Native_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Native_26), value);
	}

	inline static int32_t get_offset_of_products_json_29() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD, ___products_json_29)); }
	inline String_t* get_products_json_29() const { return ___products_json_29; }
	inline String_t** get_address_of_products_json_29() { return &___products_json_29; }
	inline void set_products_json_29(String_t* value)
	{
		___products_json_29 = value;
		Il2CppCodeGenWriteBarrier((&___products_json_29), value);
	}
};

struct AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_StaticFields
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.AppleStoreImpl::util
	RuntimeObject* ___util_27;
	// UnityEngine.Purchasing.AppleStoreImpl UnityEngine.Purchasing.AppleStoreImpl::instance
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD * ___instance_28;

public:
	inline static int32_t get_offset_of_util_27() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_StaticFields, ___util_27)); }
	inline RuntimeObject* get_util_27() const { return ___util_27; }
	inline RuntimeObject** get_address_of_util_27() { return &___util_27; }
	inline void set_util_27(RuntimeObject* value)
	{
		___util_27 = value;
		Il2CppCodeGenWriteBarrier((&___util_27), value);
	}

	inline static int32_t get_offset_of_instance_28() { return static_cast<int32_t>(offsetof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_StaticFields, ___instance_28)); }
	inline AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD * get_instance_28() const { return ___instance_28; }
	inline AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD ** get_address_of_instance_28() { return &___instance_28; }
	inline void set_instance_28(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD * value)
	{
		___instance_28 = value;
		Il2CppCodeGenWriteBarrier((&___instance_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLESTOREIMPL_T13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_H
#ifndef UDPIMPL_TE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76_H
#define UDPIMPL_TE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UDPImpl
struct  UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76  : public JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3
{
public:
	// UnityEngine.Purchasing.INativeUDPStore UnityEngine.Purchasing.UDPImpl::m_Bindings
	RuntimeObject* ___m_Bindings_21;
	// UnityEngine.UDP.UserInfo UnityEngine.Purchasing.UDPImpl::m_UserInfo
	UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD * ___m_UserInfo_22;
	// System.String UnityEngine.Purchasing.UDPImpl::m_LastInitError
	String_t* ___m_LastInitError_23;
	// System.Boolean UnityEngine.Purchasing.UDPImpl::m_Initialized
	bool ___m_Initialized_24;

public:
	inline static int32_t get_offset_of_m_Bindings_21() { return static_cast<int32_t>(offsetof(UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76, ___m_Bindings_21)); }
	inline RuntimeObject* get_m_Bindings_21() const { return ___m_Bindings_21; }
	inline RuntimeObject** get_address_of_m_Bindings_21() { return &___m_Bindings_21; }
	inline void set_m_Bindings_21(RuntimeObject* value)
	{
		___m_Bindings_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bindings_21), value);
	}

	inline static int32_t get_offset_of_m_UserInfo_22() { return static_cast<int32_t>(offsetof(UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76, ___m_UserInfo_22)); }
	inline UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD * get_m_UserInfo_22() const { return ___m_UserInfo_22; }
	inline UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD ** get_address_of_m_UserInfo_22() { return &___m_UserInfo_22; }
	inline void set_m_UserInfo_22(UserInfo_tEA6CDD375F18424AD96E23D04B0242E1D84D22DD * value)
	{
		___m_UserInfo_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_UserInfo_22), value);
	}

	inline static int32_t get_offset_of_m_LastInitError_23() { return static_cast<int32_t>(offsetof(UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76, ___m_LastInitError_23)); }
	inline String_t* get_m_LastInitError_23() const { return ___m_LastInitError_23; }
	inline String_t** get_address_of_m_LastInitError_23() { return &___m_LastInitError_23; }
	inline void set_m_LastInitError_23(String_t* value)
	{
		___m_LastInitError_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastInitError_23), value);
	}

	inline static int32_t get_offset_of_m_Initialized_24() { return static_cast<int32_t>(offsetof(UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76, ___m_Initialized_24)); }
	inline bool get_m_Initialized_24() const { return ___m_Initialized_24; }
	inline bool* get_address_of_m_Initialized_24() { return &___m_Initialized_24; }
	inline void set_m_Initialized_24(bool value)
	{
		___m_Initialized_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPIMPL_TE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76_H
#ifndef UNITYCHANNELIMPL_T014C99B115ED39FE57BFCA3BCD85B66E6BE4E616_H
#define UNITYCHANNELIMPL_T014C99B115ED39FE57BFCA3BCD85B66E6BE4E616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.UnityChannelImpl
struct  UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616  : public JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3
{
public:
	// UnityEngine.Purchasing.INativeUnityChannelStore UnityEngine.Purchasing.UnityChannelImpl::m_Bindings
	RuntimeObject* ___m_Bindings_21;
	// System.String UnityEngine.Purchasing.UnityChannelImpl::m_LastPurchaseError
	String_t* ___m_LastPurchaseError_22;
	// System.Boolean UnityEngine.Purchasing.UnityChannelImpl::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_m_Bindings_21() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616, ___m_Bindings_21)); }
	inline RuntimeObject* get_m_Bindings_21() const { return ___m_Bindings_21; }
	inline RuntimeObject** get_address_of_m_Bindings_21() { return &___m_Bindings_21; }
	inline void set_m_Bindings_21(RuntimeObject* value)
	{
		___m_Bindings_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bindings_21), value);
	}

	inline static int32_t get_offset_of_m_LastPurchaseError_22() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616, ___m_LastPurchaseError_22)); }
	inline String_t* get_m_LastPurchaseError_22() const { return ___m_LastPurchaseError_22; }
	inline String_t** get_address_of_m_LastPurchaseError_22() { return &___m_LastPurchaseError_22; }
	inline void set_m_LastPurchaseError_22(String_t* value)
	{
		___m_LastPurchaseError_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastPurchaseError_22), value);
	}

	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCHANNELIMPL_T014C99B115ED39FE57BFCA3BCD85B66E6BE4E616_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ASYNCWEBUTIL_TD5387FF5B51600EE3010878E8EB320FB74075E00_H
#define ASYNCWEBUTIL_TD5387FF5B51600EE3010878E8EB320FB74075E00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AsyncWebUtil
struct  AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCWEBUTIL_TD5387FF5B51600EE3010878E8EB320FB74075E00_H
#ifndef MOOLAHSTOREIMPL_T339168A0AC4C502257567A64AAB9E925F3A5E482_H
#define MOOLAHSTOREIMPL_T339168A0AC4C502257567A64AAB9E925F3A5E482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl
struct  MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.MoolahStoreImpl::m_callback
	RuntimeObject* ___m_callback_9;
	// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl::isNeedPolling
	bool ___isNeedPolling_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_CurrentStoreProductID
	String_t* ___m_CurrentStoreProductID_11;
	// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl::isRequestAuthCodeing
	bool ___isRequestAuthCodeing_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_appKey
	String_t* ___m_appKey_13;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_hashKey
	String_t* ___m_hashKey_14;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_notificationURL
	String_t* ___m_notificationURL_15;
	// UnityEngine.Purchasing.CloudMoolahMode UnityEngine.Purchasing.MoolahStoreImpl::m_mode
	int32_t ___m_mode_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_CustomerID
	String_t* ___m_CustomerID_17;

public:
	inline static int32_t get_offset_of_m_callback_9() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_callback_9)); }
	inline RuntimeObject* get_m_callback_9() const { return ___m_callback_9; }
	inline RuntimeObject** get_address_of_m_callback_9() { return &___m_callback_9; }
	inline void set_m_callback_9(RuntimeObject* value)
	{
		___m_callback_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_callback_9), value);
	}

	inline static int32_t get_offset_of_isNeedPolling_10() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___isNeedPolling_10)); }
	inline bool get_isNeedPolling_10() const { return ___isNeedPolling_10; }
	inline bool* get_address_of_isNeedPolling_10() { return &___isNeedPolling_10; }
	inline void set_isNeedPolling_10(bool value)
	{
		___isNeedPolling_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentStoreProductID_11() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_CurrentStoreProductID_11)); }
	inline String_t* get_m_CurrentStoreProductID_11() const { return ___m_CurrentStoreProductID_11; }
	inline String_t** get_address_of_m_CurrentStoreProductID_11() { return &___m_CurrentStoreProductID_11; }
	inline void set_m_CurrentStoreProductID_11(String_t* value)
	{
		___m_CurrentStoreProductID_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentStoreProductID_11), value);
	}

	inline static int32_t get_offset_of_isRequestAuthCodeing_12() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___isRequestAuthCodeing_12)); }
	inline bool get_isRequestAuthCodeing_12() const { return ___isRequestAuthCodeing_12; }
	inline bool* get_address_of_isRequestAuthCodeing_12() { return &___isRequestAuthCodeing_12; }
	inline void set_isRequestAuthCodeing_12(bool value)
	{
		___isRequestAuthCodeing_12 = value;
	}

	inline static int32_t get_offset_of_m_appKey_13() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_appKey_13)); }
	inline String_t* get_m_appKey_13() const { return ___m_appKey_13; }
	inline String_t** get_address_of_m_appKey_13() { return &___m_appKey_13; }
	inline void set_m_appKey_13(String_t* value)
	{
		___m_appKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_appKey_13), value);
	}

	inline static int32_t get_offset_of_m_hashKey_14() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_hashKey_14)); }
	inline String_t* get_m_hashKey_14() const { return ___m_hashKey_14; }
	inline String_t** get_address_of_m_hashKey_14() { return &___m_hashKey_14; }
	inline void set_m_hashKey_14(String_t* value)
	{
		___m_hashKey_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashKey_14), value);
	}

	inline static int32_t get_offset_of_m_notificationURL_15() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_notificationURL_15)); }
	inline String_t* get_m_notificationURL_15() const { return ___m_notificationURL_15; }
	inline String_t** get_address_of_m_notificationURL_15() { return &___m_notificationURL_15; }
	inline void set_m_notificationURL_15(String_t* value)
	{
		___m_notificationURL_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_notificationURL_15), value);
	}

	inline static int32_t get_offset_of_m_mode_16() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_mode_16)); }
	inline int32_t get_m_mode_16() const { return ___m_mode_16; }
	inline int32_t* get_address_of_m_mode_16() { return &___m_mode_16; }
	inline void set_m_mode_16(int32_t value)
	{
		___m_mode_16 = value;
	}

	inline static int32_t get_offset_of_m_CustomerID_17() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482, ___m_CustomerID_17)); }
	inline String_t* get_m_CustomerID_17() const { return ___m_CustomerID_17; }
	inline String_t** get_address_of_m_CustomerID_17() { return &___m_CustomerID_17; }
	inline void set_m_CustomerID_17(String_t* value)
	{
		___m_CustomerID_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomerID_17), value);
	}
};

struct MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields
{
public:
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::pollingPath
	String_t* ___pollingPath_4;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestAuthCodePath
	String_t* ___requestAuthCodePath_5;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestRestoreTransactionUrl
	String_t* ___requestRestoreTransactionUrl_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestValidateReceiptUrl
	String_t* ___requestValidateReceiptUrl_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestProductValidateUrl
	String_t* ___requestProductValidateUrl_8;

public:
	inline static int32_t get_offset_of_pollingPath_4() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields, ___pollingPath_4)); }
	inline String_t* get_pollingPath_4() const { return ___pollingPath_4; }
	inline String_t** get_address_of_pollingPath_4() { return &___pollingPath_4; }
	inline void set_pollingPath_4(String_t* value)
	{
		___pollingPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___pollingPath_4), value);
	}

	inline static int32_t get_offset_of_requestAuthCodePath_5() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields, ___requestAuthCodePath_5)); }
	inline String_t* get_requestAuthCodePath_5() const { return ___requestAuthCodePath_5; }
	inline String_t** get_address_of_requestAuthCodePath_5() { return &___requestAuthCodePath_5; }
	inline void set_requestAuthCodePath_5(String_t* value)
	{
		___requestAuthCodePath_5 = value;
		Il2CppCodeGenWriteBarrier((&___requestAuthCodePath_5), value);
	}

	inline static int32_t get_offset_of_requestRestoreTransactionUrl_6() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields, ___requestRestoreTransactionUrl_6)); }
	inline String_t* get_requestRestoreTransactionUrl_6() const { return ___requestRestoreTransactionUrl_6; }
	inline String_t** get_address_of_requestRestoreTransactionUrl_6() { return &___requestRestoreTransactionUrl_6; }
	inline void set_requestRestoreTransactionUrl_6(String_t* value)
	{
		___requestRestoreTransactionUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestRestoreTransactionUrl_6), value);
	}

	inline static int32_t get_offset_of_requestValidateReceiptUrl_7() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields, ___requestValidateReceiptUrl_7)); }
	inline String_t* get_requestValidateReceiptUrl_7() const { return ___requestValidateReceiptUrl_7; }
	inline String_t** get_address_of_requestValidateReceiptUrl_7() { return &___requestValidateReceiptUrl_7; }
	inline void set_requestValidateReceiptUrl_7(String_t* value)
	{
		___requestValidateReceiptUrl_7 = value;
		Il2CppCodeGenWriteBarrier((&___requestValidateReceiptUrl_7), value);
	}

	inline static int32_t get_offset_of_requestProductValidateUrl_8() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields, ___requestProductValidateUrl_8)); }
	inline String_t* get_requestProductValidateUrl_8() const { return ___requestProductValidateUrl_8; }
	inline String_t** get_address_of_requestProductValidateUrl_8() { return &___requestProductValidateUrl_8; }
	inline void set_requestProductValidateUrl_8(String_t* value)
	{
		___requestProductValidateUrl_8 = value;
		Il2CppCodeGenWriteBarrier((&___requestProductValidateUrl_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOOLAHSTOREIMPL_T339168A0AC4C502257567A64AAB9E925F3A5E482_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482), -1, sizeof(MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3200[14] = 
{
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields::get_offset_of_pollingPath_4(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields::get_offset_of_requestAuthCodePath_5(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields::get_offset_of_requestRestoreTransactionUrl_6(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields::get_offset_of_requestValidateReceiptUrl_7(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482_StaticFields::get_offset_of_requestProductValidateUrl_8(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_callback_9(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_isNeedPolling_10(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_CurrentStoreProductID_11(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_isRequestAuthCodeing_12(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_appKey_13(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_hashKey_14(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_notificationURL_15(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_mode_16(),
	MoolahStoreImpl_t339168A0AC4C502257567A64AAB9E925F3A5E482::get_offset_of_m_CustomerID_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[9] = 
{
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_U3CU3E1__state_0(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_U3CU3E2__current_1(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_appkey_2(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_productInfo_3(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_result_4(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_U3CU3E4__this_5(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_U3CsignU3E5__1_6(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_U3CwfU3E5__2_7(),
	U3CVaildateProductU3Ed__13_tB6D342BC4C0D7B0D01EC94E7A6580BCE896E6680::get_offset_of_U3CwU3E5__3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3202[3] = 
{
	U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948::get_offset_of_purchaseSucceed_0(),
	U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948::get_offset_of_purchaseFailed_1(),
	U3CU3Ec__DisplayClass18_0_t8F3D6EE637D15A39F5356E317F46604DF0D2C948::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[13] = 
{
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CU3E1__state_0(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CU3E2__current_1(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_wf_2(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_productID_3(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_transactionId_4(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_succeed_5(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_failed_6(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CU3E4__this_7(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CwU3E5__1_8(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CauthCodeResultU3E5__2_9(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CauthCodeValuesU3E5__3_10(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CauthCodeU3E5__4_11(),
	U3CRequestAuthCodeU3Ed__22_t68AAA3F001C3B2A1E8290F19D5BB8481601D1F52::get_offset_of_U3CpaymentURLU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[21] = 
{
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CU3E1__state_0(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CU3E2__current_1(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_authGlobal_2(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_transactionId_3(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_purchaseSucceed_4(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_purchaseFailed_5(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CU3E4__this_6(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CorderSuccessU3E5__1_7(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CsignstrU3E5__2_8(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CsignU3E5__3_9(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CparamU3E5__4_10(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CurlU3E5__5_11(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CpollingstrU3E5__6_12(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CjsonPollingObjectsU3E5__7_13(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CcodeU3E5__8_14(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CpollingValuesU3E5__9_15(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CtradeSeqU3E5__10_16(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CtradeStateU3E5__11_17(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CproductIdU3E5__12_18(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CMsgU3E5__13_19(),
	U3CStartPurchasePollingU3Ed__23_tD74DB4FBB97235040F28631B87389D3F24AFD93E::get_offset_of_U3CreceiptU3E5__14_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3205[20] = 
{
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CU3E1__state_0(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CU3E2__current_1(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_result_2(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CU3E4__this_3(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CcustomIDU3E5__1_4(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CwfU3E5__2_5(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CnowU3E5__3_6(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CendDateU3E5__4_7(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CupperWeekU3E5__5_8(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CstartDateU3E5__6_9(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CsignU3E5__7_10(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CwU3E5__8_11(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CrestoreObjectsU3E5__9_12(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CcodeU3E5__10_13(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CrestoreValuesU3E5__11_14(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CU3Es__12_15(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CrestoreObjectElemU3E5__13_16(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CproductIdU3E5__14_17(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CtradeSeqU3E5__15_18(),
	U3CRestoreTransactionIDProcessU3Ed__45_t07929999CA5328D0D010DA7DC8CA56C82D832A10::get_offset_of_U3CreceiptU3E5__16_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3206[13] = 
{
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CU3E1__state_0(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CU3E2__current_1(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_transactionId_2(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_receipt_3(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_result_4(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CU3E4__this_5(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CtempJsonU3E5__1_6(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CwfU3E5__2_7(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CsignU3E5__3_8(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CwU3E5__4_9(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CjsonObjectsU3E5__5_10(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CcodeU3E5__6_11(),
	U3CValidateReceiptProcessU3Ed__47_t49A8523713B44DEACC6BC525029665BCC9AED057::get_offset_of_U3CmsgU3E5__7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (PayMethod_t24C4E316D622714D9DF2F0F872B4DC44A35CB703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (FakeGooglePlayStoreExtensions_t5A3CEF4FC2B96358C4A701E3619B0E81DE5C25DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (GooglePlayAndroidJavaStore_t73E3ED5DB8900A395E0CA494ACEC02E158FBBA44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[1] = 
{
	GooglePlayAndroidJavaStore_t73E3ED5DB8900A395E0CA494ACEC02E158FBBA44::get_offset_of_m_Util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (FakeGooglePlayConfiguration_t21A6F3FDC321BD44D61A5E2A889F9AE12CA89C1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (GooglePlayStoreCallback_tC9B4E8F8744E8223CA416D9DD39E7879C529A58C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3211[1] = 
{
	GooglePlayStoreCallback_tC9B4E8F8744E8223CA416D9DD39E7879C529A58C::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (GooglePlayStoreExtensions_t0FA3D71679F20931E482A918A283E495D174524E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[1] = 
{
	GooglePlayStoreExtensions_t0FA3D71679F20931E482A918A283E495D174524E::get_offset_of_m_Java_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (FakeSamsungAppsExtensions_t619D148D530ACFAB81F62401C4381C3808E52C7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (SamsungAppsMode_t1342C05EF24DD9123D3B5F24575A33E763B84EF1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3219[4] = 
{
	SamsungAppsMode_t1342C05EF24DD9123D3B5F24575A33E763B84EF1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (SamsungAppsStoreExtensions_t7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3220[2] = 
{
	SamsungAppsStoreExtensions_t7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2::get_offset_of_m_RestoreCallback_0(),
	SamsungAppsStoreExtensions_t7E0B00B3571D4823EA698C6A0E1AC02C4E361FC2::get_offset_of_m_Java_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (FakeUnityChannelConfiguration_tB56C2BBD4B0E46D13B12DD6614588B970C32E492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3221[1] = 
{
	FakeUnityChannelConfiguration_tB56C2BBD4B0E46D13B12DD6614588B970C32E492::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (FakeUnityChannelExtensions_t4FCE1F11459746A7CCE88B2324584A96455E695D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (XiaomiPriceTiers_t277154F32924391EF362A479856C47A2A43A6E5B), -1, sizeof(XiaomiPriceTiers_t277154F32924391EF362A479856C47A2A43A6E5B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3226[1] = 
{
	XiaomiPriceTiers_t277154F32924391EF362A479856C47A2A43A6E5B_StaticFields::get_offset_of_XiaomiPriceTierPrices_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[4] = 
{
	UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6::get_offset_of_m_PurchaseCallback_0(),
	UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6::get_offset_of_m_PurchaseGuid_1(),
	UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6::get_offset_of_m_ValidateCallbacks_2(),
	UnityChannelBindings_t1F55436FB3D015BA0B293C37951600A73A93E0E6::get_offset_of_m_PurchaseConfirmCallbacks_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (U3CU3Ec__DisplayClass16_0_tB91567274B7906CE554758FDB7180DBA22B1A9A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[2] = 
{
	U3CU3Ec__DisplayClass16_0_tB91567274B7906CE554758FDB7180DBA22B1A9A5::get_offset_of_transactionId_0(),
	U3CU3Ec__DisplayClass16_0_tB91567274B7906CE554758FDB7180DBA22B1A9A5::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[3] = 
{
	UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616::get_offset_of_m_Bindings_21(),
	UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616::get_offset_of_m_LastPurchaseError_22(),
	UnityChannelImpl_t014C99B115ED39FE57BFCA3BCD85B66E6BE4E616::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[2] = 
{
	U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass7_0_t49FACE65C2487483820DF485344FA9E4DC6351A0::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[3] = 
{
	U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE::get_offset_of_dic_0(),
	U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE::get_offset_of_transactionId_1(),
	U3CU3Ec__DisplayClass7_1_t38B67CB8D5BA086EE9FD4CB02099F5C891F09FEE::get_offset_of_CSU24U3CU3E8__locals1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[3] = 
{
	UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69::get_offset_of_storeSpecificId_0(),
	UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69::get_offset_of_transactionId_1(),
	UnityChannelPurchaseReceipt_t22BA5D3915A198D116BB4813CF1D5A9C66DC6D69::get_offset_of_orderQueryToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (FakeUDPExtension_tAC5FB9DBD92A0F3F6F5C19899EC8774E98C78E4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (UDP_t90A3ABD1AA681B1CA1FC9EC6B4A3B14200CCEFB3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[5] = 
{
	UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE::get_offset_of_m_PurchaseCallback_0(),
	UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE::get_offset_of_m_InitCallback_1(),
	UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE::get_offset_of_m_RetrieveProductsCallback_2(),
	UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE::get_offset_of_m_Inventory_3(),
	UDPBindings_t97BCD3D0F5E8A64D5B8CA2D242019A76CE6B0BBE::get_offset_of_m_LocalPurchasesCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3238[4] = 
{
	UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76::get_offset_of_m_Bindings_21(),
	UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76::get_offset_of_m_UserInfo_22(),
	UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76::get_offset_of_m_LastInitError_23(),
	UDPImpl_tE87DE02672BD67B6E1B3E35FFC80F702FF1EBB76::get_offset_of_m_Initialized_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[3] = 
{
	U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154::get_offset_of_products_0(),
	U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154::get_offset_of_retrieveCallback_1(),
	U3CU3Ec__DisplayClass7_0_t53603FF6A50DA53F3E78B7D52C63574639DC7154::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (U3CU3Ec__DisplayClass8_0_tABAF20373A271E8D799FE1C91217AE25EC57E359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[2] = 
{
	U3CU3Ec__DisplayClass8_0_tABAF20373A271E8D799FE1C91217AE25EC57E359::get_offset_of_product_0(),
	U3CU3Ec__DisplayClass8_0_tABAF20373A271E8D799FE1C91217AE25EC57E359::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD), -1, sizeof(AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3241[9] = 
{
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_m_DeferredCallback_21(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_m_RefreshReceiptError_22(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_m_RefreshReceiptSuccess_23(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_m_RestoreCallback_24(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_m_PromotionalPurchaseCallback_25(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_m_Native_26(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_StaticFields::get_offset_of_util_27(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD_StaticFields::get_offset_of_instance_28(),
	AppleStoreImpl_t13CF2EB0D448CD9E422E3FFC3F5C1F0097531FAD::get_offset_of_products_json_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (U3CU3Ec__DisplayClass23_0_tFDA2BE9F4812295D8320C3419EB89D9891744A71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[1] = 
{
	U3CU3Ec__DisplayClass23_0_tFDA2BE9F4812295D8320C3419EB89D9891744A71::get_offset_of_productDescription_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A), -1, sizeof(U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3243[3] = 
{
	U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields::get_offset_of_U3CU3E9__23_1_1(),
	U3CU3Ec_t26CBD7DB2114D62D52EE18A57AE0FF640A3C282A_StaticFields::get_offset_of_U3CU3E9__40_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[4] = 
{
	U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D::get_offset_of_subject_0(),
	U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D::get_offset_of_payload_1(),
	U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D::get_offset_of_receipt_2(),
	U3CU3Ec__DisplayClass36_0_t1644DE8B3C377DAB1B327FC0CEA987F63D678E2D::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (U3CU3Ec__DisplayClass40_0_tA262F57AA743AF89BF0463BEBF334BCCB53C35EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[1] = 
{
	U3CU3Ec__DisplayClass40_0_tA262F57AA743AF89BF0463BEBF334BCCB53C35EC::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (FakeAppleConfiguation_t7AF8F624290C9E5621BDAAC0E0E55ED674AA397C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (FakeAppleExtensions_t782746A593A86D53022520A8E9D6ACDD12851339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (AppStore_t27444A4FF359217774CC32493678590052ECD360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3250[14] = 
{
	AppStore_t27444A4FF359217774CC32493678590052ECD360::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[21] = 
{
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_managedStore_0(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_unity_1(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_store_2(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_storeCatalog_3(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_isManagedStoreEnabled_4(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_profileData_5(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_isRefreshing_6(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_isFirstTimeRetrievingProducts_7(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_refreshCallback_8(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_Module_9(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_BuilderProducts_10(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_Logger_11(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_m_EventQueue_12(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_promoPayload_13(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_catalogDisabled_14(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_testStore_15(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_iapBaseUrl_16(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_eventBaseUrl_17(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_lastPurchaseFailureDescription_18(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of__lastPurchaseErrorCode_19(),
	JSONStore_tA1476E0077C2071520663F35D28A5E7AF830CEE3::get_offset_of_kStoreSpecificErrorCodeKey_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (NativeStoreProvider_tD96BC473A252F39A262C28AB8A48FD5D98363A6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[7] = 
{
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711::get_offset_of_m_AsyncUtil_0(),
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711::get_offset_of_m_CacheFileName_1(),
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711::get_offset_of_m_Logger_2(),
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711::get_offset_of_m_CatalogURL_3(),
	CloudCatalogImpl_t855B4F24B3BEBA0F2C54F36337FD060A3BF5D711::get_offset_of_m_StoreName_4(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[4] = 
{
	U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7::get_offset_of_delayInSeconds_1(),
	U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7::get_offset_of_U3CU3E4__this_2(),
	U3CU3Ec__DisplayClass10_0_t7E74E6320BE5F32DBF8CA3A3CE0AD70F3AE737A7::get_offset_of_U3CU3E9__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5), -1, sizeof(U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3257[3] = 
{
	U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_t4C74A8971DD4C672DDED78F8E833A48A3538AAF5_StaticFields::get_offset_of_U3CU3E9__12_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[5] = 
{
	FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07::get_offset_of_catalogDisabled_0(),
	FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07::get_offset_of_testStore_1(),
	FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07::get_offset_of_iapBaseUrl_2(),
	FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07::get_offset_of_eventBaseUrl_3(),
	FakeManagedStoreConfig_t15D56215F0228DEA7C5B21C2E4FD32939D8D7C07::get_offset_of_trackingOptedOut_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (FakeManagedStoreExtensions_tD84A7039DFB9134F25B2BAE1F03F0437185FEEBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB), -1, sizeof(StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3262[6] = 
{
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB::get_offset_of_m_AsyncUtil_0(),
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB::get_offset_of_m_Logger_1(),
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB::get_offset_of_m_CatalogURL_2(),
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB::get_offset_of_m_StoreName_3(),
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB::get_offset_of_m_cachedStoreCatalogReference_4(),
	StoreCatalogImpl_tF33B4AB1E4D4A5DF713AA3F94354BAE32CD2F6AB_StaticFields::get_offset_of_profile_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (U3CU3Ec__DisplayClass10_0_t606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[2] = 
{
	U3CU3Ec__DisplayClass10_0_t606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass10_0_t606E5165F2AE27DC7B1E75EA5456B3D9E22C3E49::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85), -1, sizeof(AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3264[4] = 
{
	AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields::get_offset_of_adsAdvertisementClassName_0(),
	AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields::get_offset_of_adsMessageSendName_1(),
	AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields::get_offset_of_adsAdvertisementType_2(),
	AdsIPC_t40A6B57B6815A25E9500D3966E4146937971FD85_StaticFields::get_offset_of_adsMessageSend_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (EventDestType_t2A641E5C0B5276778DE6D86C34D4EBFEA6466785)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3265[8] = 
{
	EventDestType_t2A641E5C0B5276778DE6D86C34D4EBFEA6466785::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA), -1, sizeof(EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3266[6] = 
{
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA::get_offset_of_m_AsyncUtil_0(),
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA_StaticFields::get_offset_of_QueueInstance_1(),
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA::get_offset_of_Profile_2(),
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA::get_offset_of_TrackingUrl_3(),
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA::get_offset_of_EventUrl_4(),
	EventQueue_tD991D20FAD4203922A13A31871F6046F925548FA::get_offset_of_ProfileDict_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[6] = 
{
	U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749::get_offset_of_delayInSeconds_0(),
	U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749::get_offset_of_dest_1(),
	U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749::get_offset_of_json_2(),
	U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749::get_offset_of_target_3(),
	U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749::get_offset_of_U3CU3E4__this_4(),
	U3CU3Ec__DisplayClass11_0_t9ABDE042059E82B5E7F952F894F48526BC6EB749::get_offset_of_U3CU3E9__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302), -1, sizeof(U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3268[4] = 
{
	U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields::get_offset_of_U3CU3E9__11_3_2(),
	U3CU3Ec_tFE92D889B6366334C816C2D16974499284C0F302_StaticFields::get_offset_of_U3CU3E9__11_4_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (PurchasingEvent_tF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3269[1] = 
{
	PurchasingEvent_tF6D0AA241E93387AFA3880D92C36A0FF5CD6C6E7::get_offset_of_EventDict_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3), -1, sizeof(U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3270[3] = 
{
	U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_t930F4D5FBD4FAD2B3A0F7C2DE13C2D301FE477B3_StaticFields::get_offset_of_U3CU3E9__2_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (AsyncWebUtil_tD5387FF5B51600EE3010878E8EB320FB74075E00), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[5] = 
{
	U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4::get_offset_of_U3CU3E1__state_0(),
	U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4::get_offset_of_U3CU3E2__current_1(),
	U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4::get_offset_of_a_2(),
	U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4::get_offset_of_delayInSeconds_3(),
	U3CDoInvokeU3Ed__3_t0DE1C36FE99539931C000CF809F9C388389442D4::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[9] = 
{
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_U3CU3E1__state_0(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_U3CU3E2__current_1(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_request_2(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_responseHandler_3(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_errorHandler_4(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_maxTimeoutInSeconds_5(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_U3CU3E4__this_6(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_U3CtimerU3E5__1_7(),
	U3CProcessU3Ed__4_t1152D7AD22CCD38A061E06047FDF8166A79AFD8B::get_offset_of_U3ChasTimedOutU3E5__2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (QueryHelper_t806E01B87ADF5BB73686FC647841A955D0EC117E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (Price_t5D55521A645803976E0809F8941E8317C80E129C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[3] = 
{
	Price_t5D55521A645803976E0809F8941E8317C80E129C::get_offset_of_value_0(),
	Price_t5D55521A645803976E0809F8941E8317C80E129C::get_offset_of_data_1(),
	Price_t5D55521A645803976E0809F8941E8317C80E129C::get_offset_of_num_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (StoreID_t4C8817F5B62D22D6E04A2B82069F15C7420A9605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3277[2] = 
{
	StoreID_t4C8817F5B62D22D6E04A2B82069F15C7420A9605::get_offset_of_store_0(),
	StoreID_t4C8817F5B62D22D6E04A2B82069F15C7420A9605::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (TranslationLocale_tA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3278[33] = 
{
	TranslationLocale_tA4EC10521BA9E8CAECCDC04789A2A7B5A9AF4905::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3279[3] = 
{
	LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1::get_offset_of_googleLocale_0(),
	LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1::get_offset_of_title_1(),
	LocalizedProductDescription_t6677548B0FDC9DF7B17E6DA3CBCADFB9A80286F1::get_offset_of_description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B), -1, sizeof(U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[2] = 
{
	U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t476CCA496FD17E1BD2E214B94F59290219BFF57B_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3281[4] = 
{
	ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476::get_offset_of_t_0(),
	ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476::get_offset_of_st_1(),
	ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476::get_offset_of_q_2(),
	ProductCatalogPayout_t79BFC38C9C47E0CF033E69D860277EA71E617476::get_offset_of_d_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (ProductCatalogPayoutType_t0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3282[5] = 
{
	ProductCatalogPayoutType_t0BBE3C0C9A0AE426FCCEAE049A44E9302DBE6FC5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[10] = 
{
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_id_0(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_type_1(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_storeIDs_2(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_defaultDescription_3(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_applePriceTier_4(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_xiaomiPriceTier_5(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_googlePrice_6(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_descriptions_7(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_udpPrice_8(),
	ProductCatalogItem_t532825A86166318D5A6DB6D7671F51573BD0267D::get_offset_of_payouts_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (U3CU3Ec__DisplayClass22_0_t4900BD1283E21B410243ACA012CBBD7581F1843D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[1] = 
{
	U3CU3Ec__DisplayClass22_0_t4900BD1283E21B410243ACA012CBBD7581F1843D::get_offset_of_locale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6), -1, sizeof(ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3285[3] = 
{
	ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6_StaticFields::get_offset_of_instance_0(),
	ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6::get_offset_of_enableCodelessAutoInitialization_1(),
	ProductCatalog_t7643D269828196E196CDF8B0ACC4AE00EA1A15F6::get_offset_of_products_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035), -1, sizeof(U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3286[2] = 
{
	U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t74AF868FF34CE9F33880DA547DEEA475FDA0D035_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (ProductCatalogImpl_t09FB0B201A0F9B871EB95F93432697B259EC1214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B), -1, sizeof(ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3289[26] = 
{
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_m_Util_0(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B_StaticFields::get_offset_of_ProfileInstance_1(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CAppIdU3Ek__BackingField_2(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CUserIdU3Ek__BackingField_3(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CSessionIdU3Ek__BackingField_4(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CPlatformU3Ek__BackingField_5(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CPlatformIdU3Ek__BackingField_6(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CSdkVerU3Ek__BackingField_7(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3COsVerU3Ek__BackingField_8(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CScreenWidthU3Ek__BackingField_9(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CScreenHeightU3Ek__BackingField_10(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CScreenDpiU3Ek__BackingField_11(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CScreenOrientationU3Ek__BackingField_12(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CDeviceIdU3Ek__BackingField_13(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CBuildGUIDU3Ek__BackingField_14(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CIapVerU3Ek__BackingField_15(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CAdsGamerTokenU3Ek__BackingField_16(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CTrackingOptOutU3Ek__BackingField_17(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CAdsABGroupU3Ek__BackingField_18(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CAdsGameIdU3Ek__BackingField_19(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CStoreABGroupU3Ek__BackingField_20(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CCatalogIdU3Ek__BackingField_21(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CMonetizationIdU3Ek__BackingField_22(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CStoreNameU3Ek__BackingField_23(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CGameVersionU3Ek__BackingField_24(),
	ProfileData_t59F197E40BC007C791FD64C4E05735F692CEB96B::get_offset_of_U3CStoreTestEnabledU3Ek__BackingField_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4), -1, sizeof(Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3290[9] = 
{
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_PromoPurchaser_0(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_Unity_1(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_RuntimePlatform_2(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_Logger_3(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_Version_4(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_Util_5(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_WebUtil_6(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_IsReady_7(),
	Promo_t15638919BA9DB952B4D59C5791FB46774C12D1A4_StaticFields::get_offset_of_s_ProductJSON_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A), -1, sizeof(StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3291[14] = 
{
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_m_AppStorePlatform_1(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_m_NativeStoreProvider_2(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_m_RuntimePlatform_3(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_m_UseCloudCatalog_4(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_StaticFields::get_offset_of_ModuleInstance_5(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_U3CutilU3Ek__BackingField_6(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_U3CloggerU3Ek__BackingField_7(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_U3CwebUtilU3Ek__BackingField_8(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_U3CstoreInstanceU3Ek__BackingField_9(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A_StaticFields::get_offset_of_AndroidStoreNameMap_10(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_m_CloudCatalog_11(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13(),
	StandardPurchasingModule_tDAF0BD8C9F2C251F5B2F92CA8CA91B664970422A::get_offset_of_windowsStore_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[2] = 
{
	StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57::get_offset_of_U3CstoreNameU3Ek__BackingField_0(),
	StoreInstance_t3D2543B6031A966FEA81D4F0EBAE3B4AE5F33E57::get_offset_of_U3CinstanceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (MicrosoftConfiguration_t66D1CD39C4D988FECCD476BA14A9B30943C96127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[2] = 
{
	MicrosoftConfiguration_t66D1CD39C4D988FECCD476BA14A9B30943C96127::get_offset_of_useMock_0(),
	MicrosoftConfiguration_t66D1CD39C4D988FECCD476BA14A9B30943C96127::get_offset_of_module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (StoreConfiguration_t19E406F75E8D4B671D72D2A5AFE051B3605E6B86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[1] = 
{
	StoreConfiguration_t19E406F75E8D4B671D72D2A5AFE051B3605E6B86::get_offset_of_U3CandroidStoreU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (StoreSpecificPurchaseErrorCode_t98BECD468210A3936A568F742D016DB19BB6B976)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3295[35] = 
{
	StoreSpecificPurchaseErrorCode_t98BECD468210A3936A568F742D016DB19BB6B976::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[14] = 
{
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_is_subscribed_0(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_is_expired_1(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_is_cancelled_2(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_is_free_trial_3(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_is_auto_renewing_4(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_is_introductory_price_period_5(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_productId_6(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_purchaseDate_7(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_subscriptionExpireDate_8(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_subscriptionCancelDate_9(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_remainedTime_10(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_introductory_price_11(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_introductory_price_period_12(),
	SubscriptionInfo_t32C4ADDD2146194CB2C6540C693A977AFA158133::get_offset_of_introductory_price_cycles_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (Result_t5191579D8310B3B0258ADC727B5BA9BDE174C572)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3297[4] = 
{
	Result_t5191579D8310B3B0258ADC727B5BA9BDE174C572::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (SubscriptionPeriodUnit_tBA4BCE498384075C6C808971B029C03CE0CA9673)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3298[6] = 
{
	SubscriptionPeriodUnit_tBA4BCE498384075C6C808971B029C03CE0CA9673::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (AppleStoreProductType_tADAB43B081D09C4D7F08145CED6B7EB52716076A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3299[5] = 
{
	AppleStoreProductType_tADAB43B081D09C4D7F08145CED6B7EB52716076A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
