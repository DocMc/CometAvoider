﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using UnityEngine;

public class Block : MonoBehaviour
{
    ConstantForce2D cF;
    [SerializeField]
    Player player;
    private int value = 1;
    bool getPoints = false;
    int rand;
    bool canBeSuperSized = true;
    [SerializeField]
    int damage = 1;
    public bool isSuperSized = false;
    GameObject[] otherComets;
    Vector3 monsterSize = new Vector3(7, 7, 0);
    public ParticleSystem trail;
    Animator explode;

    private void Awake()
    {
        rand = Random.Range(0, 101);
        otherComets = GameObject.FindGameObjectsWithTag("Block");

    }
    private void Start()
    {
        cF = gameObject.GetComponent<ConstantForce2D>();
        explode = gameObject.GetComponent<Animator>();
        isSuperSized = false;
        trail = GetComponentInChildren<ParticleSystem>();
        player = FindObjectOfType<Player>();
        for (int i = 0; i < otherComets.Length; i++)
        {
            Block comet = otherComets[i].GetComponent<Block>();
            if (comet.isSuperSized)
            {
                rand = rand + 10;
            }
        }
        if (rand <= 10 && canBeSuperSized)
        {
            SuperSize();
        }

    }
    private void Update()
    {
        if (isSuperSized)
        {
            damage = 2;
        }
        else
        {
            damage = 1;
        }
        if (gameObject.transform.position.y <= -4.0f)
        {
            getPoints = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if (collision.tag == "Player")
        {
            Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
            rb.velocity = Vector3.zero;
            ConstantForce2D cF = gameObject.GetComponent<ConstantForce2D>();
            cF.force = Vector2.zero;
            FindObjectOfType<AudioManager>().Play("MeteorExplode");
            trail.Stop();
            player.health = player.health - damage;
            getPoints = false;
            explode.SetBool("Explode", true);
        }
    }
    private void AnimateComplete()
    {
        Destroy(gameObject);
    }
    private void OnDestroy()
    {
        if (getPoints)
        {
            player.score = player.score + value;
        }
    }
    void SuperSize()
    {
        isSuperSized = true;
        trail.transform.localScale = monsterSize;
        gameObject.transform.localScale = monsterSize;
        value = 10;
    }
}
