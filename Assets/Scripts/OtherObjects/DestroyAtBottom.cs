﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DestroyAtBottom : MonoBehaviour
{
    Player player;
    Block block;
    private void Start()
    {
        player = FindObjectOfType<Player>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        block = collision.GetComponent<Block>();
        if (collision.tag == "Block" && block.isSuperSized == false)
        {
            Destroy(collision.gameObject);
        }
    }
}
