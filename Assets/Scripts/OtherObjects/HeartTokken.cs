﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartTokken : MonoBehaviour
{
    Player player;
    Rigidbody2D rb;
    Vector3 move = new Vector3(.1f, 0, 0);
    bool needsToMove = false;
    float screenLeft = -2.17f;
    float screenRight = 2.20f;
    float yValue;
    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        yValue = gameObject.transform.position.y;
        if (OffScreen() == true)
            Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = FindObjectOfType<Player>();
        if (collision.tag == "Player")
        {
            FindObjectOfType<AudioManager>().Play("LifePickUp");
            if (player.health < 3)
            {
                player.health++;
            }
            else
            {
                player.score++;
            }
            Destroy(gameObject);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Block")
        {
            Move();
        }
    }
    private void Move()
    {
        if (gameObject.transform.position.x <= Screen.width / 2)
            Adjust(false);
        else if (gameObject.transform.position.x >= Screen.width / 2)
            Adjust(true);
    }
    void Adjust (bool isNegative)
    {
        if (isNegative)
        {
            move = move * -1;
        }
        gameObject.transform.position += move;
    }
    private bool OffScreen()
    {
        if (gameObject.transform.position.x <= screenLeft)
        {
            return true;
        }
        else if (gameObject.transform.position.x >= screenRight)
        {
            return true;
        }
        return false;
    }
    private void UpdatePositionAndRotation(float screen)
    {
        gameObject.transform.position = new Vector3(screen, yValue, 0);
        rb.velocity = new Vector2(0, 0);
        gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
    }

}
