﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Advertisements;

public class BuyButton : Singleton<BuyButton>
{
    public bool adWatched;
    public enum ItemType
    {
        ExtraLife
    }
    public ItemType itemType;

    public TextMeshProUGUI priceText;

    private string defaultText;
    private void Start()
    {
        adWatched = false;
        defaultText = priceText.text;
        StartCoroutine(LoadPriceRoutine());
    }
    public void ClickBuy()
    {
        IAPManager.Instance.BuyExtraLife();
    }

    private IEnumerator LoadPriceRoutine()
    {
        while(!IAPManager.Instance.IsInitialized())
        {
            yield return null;
        }
        string loadedPrice = "";
        loadedPrice = IAPManager.Instance.GetProducePriceFromStore(IAPManager.Instance.extraLife);

        priceText.text = loadedPrice;
    }
    void ShowResults(ShowResult result)
    {
        Player player;
        player = FindObjectOfType<Player>();
        switch (result)
        {
            case ShowResult.Finished:
                player.lives++;
                adWatched = true;
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
    }
    public void WatchButtonPressed()
    {
        if (Advertisement.IsReady())
        {
            var options = new ShowOptions { resultCallback = ShowResults };
            Advertisement.Show("rewardedVideo", options);

        }
    }
}
