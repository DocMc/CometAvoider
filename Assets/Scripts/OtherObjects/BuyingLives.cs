﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyingLives : Singleton<BuyingLives>
{
    Player player;
    public void AddLife()
    {
        player = FindObjectOfType<Player>();
        player.lives++;
    }
}
