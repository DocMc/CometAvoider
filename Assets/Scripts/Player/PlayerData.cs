﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerData 
{
    public int lives;
    public int hiScore;

    public PlayerData (Player player)
    {
        lives = player.lives;
        hiScore = player.hiScore;
    }
}
