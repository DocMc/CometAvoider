﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curser : MonoBehaviour
{
    Touch touch;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.position = new Vector3(0, Screen.width / 2, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount <= 1)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    {
                        gameObject.transform.position = touch.position;
                        break;
                    }
                case TouchPhase.Stationary:
                    {
                        gameObject.transform.position = touch.position;
                        break;
                    }
                case TouchPhase.Moved:
                    {
                        gameObject.transform.position = touch.position;
                        break;
                    }
                case TouchPhase.Ended:
                    {
                        gameObject.transform.position = Vector3.zero;
                        break;
                    }
                case TouchPhase.Canceled:
                    {
                        gameObject.transform.position = Vector3.zero;
                        break;
                    }
            }
        }
    }
}

