﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : Singleton<Engine>
{
    public ParticleSystem engine;
    Player player;
    [SerializeField]
    float flashtime = 0.5f;
    private bool go = false;
    Color color;
    
    // Start is called before the first frame update
    void Start()
    {
        engine = gameObject.GetComponent<ParticleSystem>();
        player = FindObjectOfType<Player>();
        engine.Stop();
        StartCoroutine(StartUp());
        player = FindObjectOfType<Player>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Player.playerAlive == false || player.hasBeenHit == true)
        {
            StopCoroutine(StartUp());
            engine.Stop();
            engine.Clear();
        }
        else if (go && Player.playerAlive == true && player.hasBeenHit == false)
        {
            engine.Play();
        }
        
    }
    IEnumerator StartUp()
    {
        go = false;
        engine.Stop();
        yield return new WaitForSecondsRealtime(flashtime);
        engine.Play();
        yield return new WaitForSecondsRealtime(flashtime);
        engine.Stop();
        go = true;
        StopCoroutine("StartUp");
    }
}
