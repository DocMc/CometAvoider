﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public ParticleSystem boom;
    // makes sure the explosion doesn't play at the start
    void Start()
    {
        boom.Stop();
    }

    //checks each frame to see if player is alive before explodes.
    void Update()
    {
        if (Player.playerAlive == true)
        {
            boom.Clear();
        }
        if (Player.playerAlive == false && DeathMenu.continued == false )
        {
            StartCoroutine(Boom());
        }
    }
    IEnumerator Boom()
    {
        boom.Play();
        yield return new WaitForSecondsRealtime(.5f);
        boom.Stop();
    }
        
    
}
