﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
public class Player : Singleton<Player>
{
    // is is really the player class just haven't renamed it.
    private Rigidbody2D rb;
    Vector3 normalSize = new Vector3(3, 3, 3);
    Vector3 smallSize = new Vector3(1.5f, 1.5f, 1.5f);
    public int score;
    protected static int _hiScore;
    public int hiScore { get { return _hiScore; } set { _hiScore = value; } }

    protected static int _lives;
    [SerializeField]
    public int lives { get { return _lives; } set { _lives = value; } }
    [SerializeField]
    private int _health = 2;
    public int health { get { return _health; } set { _health = value; } }
    private static int _maxHealth = 3;
    public static int maxHealth { get { return _maxHealth; } }
    public bool hasBeenHit = false;

    public Sprite[] playerSprite;
    public Sprite activePlayer;
    SpriteRenderer playerRenderer;
    static int _playerSelection;
    public static int playerSelection { set { _playerSelection = value; } }
    Color color;

    public GameObject touchCourser;

    [SerializeField]
    private float speed = 12;
    private float moveHorizontal, moveVertical;
    [SerializeField]
    float rotation = 6;

    public static bool playerAlive = true;
    private Animator animate;

    float screenLeft = -2.17f;
    float screenRight = 2.19f;
    [SerializeField]
    protected bool canBeHit;

    public bool isShrunk = false;
    public bool isShrunkActivate = false;
    public bool watchedAd;
    GameObject pauseButton;

    //get all caracteristics before first frame
    void Start()
    {
        watchedAd = false;
        canBeHit = true;
        hasBeenHit = false;
        color = gameObject.GetComponent<SpriteRenderer>().color;
        touchCourser = GameObject.FindGameObjectWithTag("Curser");
        activePlayer = playerSprite[_playerSelection];
        playerRenderer = gameObject.GetComponent<SpriteRenderer>();
        playerRenderer.sprite = activePlayer;
        playerRenderer.enabled = true;
        rb = gameObject.GetComponent<Rigidbody2D>();
        animate = gameObject.GetComponent<Animator>();
        FindObjectOfType<AudioManager>().Play("Engine");
        health = 3;
        score = 0;
        LoadData();
        playerAlive = true;
    }
    void LoadData()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        if(data == null)
        {
            lives = 0;
            hiScore = 0;
        }
        lives = data.lives;
        hiScore = data.hiScore;
    }
    private void Update()
    {
        if (!canBeHit)
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
        }
        if(score > hiScore)
        {
            hiScore = score;
        }
    }
    void FixedUpdate()
    {
        bool cantMove = OffScreen();
        if (!playerAlive)
        {
            rb.velocity = new Vector2(0, 0);
            playerRenderer.enabled = false;
        }
        if (playerAlive)
        {
            playerRenderer.enabled = true;
            if (!OffScreen())
            {
                Move();
            }
            else if (OffScreen())
            {
                if (gameObject.transform.position.x <= screenLeft)
                {
                    UpdatePositionAndRotation(screenLeft + .01f);
                }
                else if (gameObject.transform.position.x >= screenRight)
                {
                    UpdatePositionAndRotation(screenRight - .01f);
                }
            }
        }
    }
    public void Move()
    {
        if (!OffScreen())
        {
            if (Input.GetKey(KeyCode.D))
            {
                this.IntraMovementRotation(false);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                this.IntraMovementRotation(true);
            }
            else
            {
                rb.velocity = new Vector2(0, 0);
                gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
            }
            if (Input.touchCount > 0)
            {
                touchCourser.transform.position = Input.GetTouch(0).position;
                if (touchCourser.transform.position.x > Screen.width / 2)
                {
                    this.IntraMovementRotation(false);
                }
                else if (touchCourser.transform.position.x < Screen.width / 2)
                {
                    this.IntraMovementRotation(true);
                }
            }
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
            gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
        }
    }
    private void IntraMovementRotation(bool isNegative)
    {
        float xVelocity = speed;
        float rotation = this.rotation * -1;
        if (isShrunk)
        {
            xVelocity = speed * 2;
            rotation = this.rotation * -1;
        }
        if (isNegative == true)
        {
            xVelocity = xVelocity * -1;
            rotation = rotation * -1;
        }
        rb.velocity = new Vector2(xVelocity, 0);
        gameObject.transform.eulerAngles = new Vector3(0, 0, rotation);


    }

    private void UpdatePositionAndRotation(float screen)
    {
        gameObject.transform.position = new Vector3(screen, -3.7f, 0);
        rb.velocity = new Vector2(0, 0);
        gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
    }
    private bool OffScreen()
    {
        if (gameObject.transform.position.x <= screenLeft)
        {
            return true;
        }
        else if (gameObject.transform.position.x >= screenRight)
        {
            return true;
        }
        return false;
    }
    // collision with block 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Block")
        {
            hasBeenHit = true;
            if (health <= 0)
            {
                if(this.watchedAd == false)
                {
                    PlayAd();
                    watchedAd = true;
                }
                canBeHit = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                gameObject.GetComponent<Collider2D>().enabled = false;
                Destroy(collision.gameObject);
                playerAlive = false;
                AudioManager.instance.Stop("Engine");
                SaveSystem.SavePlayer(this);
            }
            if (hasBeenHit)
            {
                FindObjectOfType<AudioManager>().Stop("Engine");
                IEnumerator invincible = Invincible();
                StartCoroutine(Invincible());
            }
        }

        if (collision.tag == "Shrink" && !isShrunkActivate)
        {
            isShrunkActivate = true;
            if (isShrunkActivate)
            {
                IEnumerator shrinkEffect = ShrinkEffect();
                isShrunkActivate = false;
                isShrunk = true;
                StartCoroutine(shrinkEffect);
            }
        }

    }
    private void StopEnterance()
    {
        animate.enabled = false;
    }
    IEnumerator ShrinkEffect()
    {
        gameObject.transform.localScale = smallSize;
        yield return new WaitForSecondsRealtime(3);
        gameObject.transform.localScale = normalSize;
        isShrunk = false;
        StopCoroutine(this.ShrinkEffect());
    }
    IEnumerator Invincible()
    {
        canBeHit = false;
        color.a = .5f;
        gameObject.GetComponent<SpriteRenderer>().color = color;
        yield return new WaitForSeconds(2);
        color.a = 1;
        gameObject.GetComponent<SpriteRenderer>().color = color;
        canBeHit = true;
        hasBeenHit = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = true;
        FindObjectOfType<AudioManager>().Play("Engine");
        StopCoroutine(this.Invincible());
    }
    void PlayAd()
    {
        Advertisement.Initialize("3170680");
        if (Advertisement.IsReady())
        {
            var options = new ShowOptions() { resultCallback = ShowResults };
            Advertisement.Show("video") ;
        }
    }
    void ShowResults(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                watchedAd = true;
                break;
            case ShowResult.Skipped:
                watchedAd = true;
                break;
            case ShowResult.Failed:
                break;
        }
    }
}

