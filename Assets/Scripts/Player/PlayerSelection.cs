﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSelection : MonoBehaviour
{
    public Animator animate;
    public void playerSelcetion(int playernumber)
    {

        Player.playerSelection = playernumber;
        LevelFader.loadlevel = 2;
        DeathMenu.continued = true;
        animate.SetTrigger("ButtonPressed");

    }
}
