﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{

    static int FileLoadAttemps = 0;

    const int MAX_ATTEMPTS = 5;

    public static void SavePlayer(Player player)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/player.fun";
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            PlayerData data = new PlayerData(player);
            formatter.Serialize(stream, data);
            stream.Close();
        }
       
    }

    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.fun";
        if (File.Exists(path) == true)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = null;

            if(stream.Length > 0) {
                data = formatter.Deserialize(stream) as PlayerData;
            } else {
                data = new PlayerData(new Player());
            }
            stream.Close();
            return data;
        }
        else
        {
            SaveSystem.FileLoadAttemps = SaveSystem.FileLoadAttemps + 1;
            Debug.Log("File doesn't Exist. Creating File. Attempts: " + SaveSystem.FileLoadAttemps);
            if(SaveSystem.FileLoadAttemps >= SaveSystem.MAX_ATTEMPTS) {
                return null;
            } else {
                using(FileStream fs = File.Create(path)){}
                return SaveSystem.LoadPlayer();
            }
        }
    }
    
}

