﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthCounter : MonoBehaviour
{
    [SerializeField]
    int health = 3;
    [SerializeField]
    Player player;
    public Image []healthCounter;

    public Sprite shipsHas;
    public Sprite shipslost;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        this.health = player.health;
    }
    private void LateUpdate()
    {
        shipsHas = player.activePlayer;
        this.health = player.health;
        for (int i = 0; i < healthCounter.Length; i++)
        {
            if (i < health)
            {
                healthCounter[i].sprite = shipsHas;
            }
            else
            {
                healthCounter[i].sprite = shipslost;
            }

        }
    }
}
