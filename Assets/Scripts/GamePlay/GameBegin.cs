﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBegin : MonoBehaviour
{
    public Animator player;
    public GameObject canvas;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
    }
    public void InstructionsPressed()
    {
        canvas.SetActive(true);
        Time.timeScale = 1;
        player.SetTrigger("Instructions");
        Destroy(gameObject);

    }
}
