﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
public class LevelFader : MonoBehaviour
{
    public Animator animator;
    private static int _level;
    public static int loadlevel { get { return _level; } set { _level = value; } }
    int skipAd;
    private void Awake()
    {
        animator.SetBool("PlayerDies", false);
    }
    private void Start()
    {
        skipAd = 0;
        Advertisement.Initialize("3170680");
        gameObject.GetComponent<Animator>();
    }
    private void Update()
    {
        if (Player.playerAlive == false && DeathMenu.continued == false)
        {
            FadeToCredits(loadlevel);
        }
    }
    public void FadeToSelection(int levelIndex)
    {
        _level = levelIndex;
    }
    public void FadeToCredits(int levelIndex)
    {
        _level = levelIndex;
        StartCoroutine(Fadeout());
    }
    public void OnFadeComplete()
    {
        SceneManager.LoadSceneAsync(_level);
    }
    public void FadeToGame(int LevelIndex)
    {
        _level = LevelIndex;
        animator.SetTrigger("ButtonPressed");
    }
    IEnumerator Fadeout()
    {
        yield return new WaitForSecondsRealtime(.5f);
        animator.SetBool("PlayerDies", true);
    }
    public void StartButtonPressed()
    {
        animator.SetTrigger("StartButtonPressed");
    }
    public void StopSound()
    {
        FindObjectOfType<AudioManager>().Stop("Theme");
    }
}
