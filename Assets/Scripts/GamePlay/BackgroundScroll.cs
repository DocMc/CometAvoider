﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    [SerializeField]
    float speed = .001f;
    public GameObject background;
    private void Start()
    {
        background.GetComponent<MeshRenderer>();
        StartCoroutine("Speedup");
    }
    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.isPaused == false)
        {
            if (Player.playerAlive == true)
            {
                Vector2 offset = new Vector2(0, speed);
                background.GetComponent<MeshRenderer>().material.mainTextureOffset -= offset;
            }
            else
            {
                background.GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0, 0);
            }
        }
        
    }
    IEnumerator Speedup()
    {
        while (Spawner.stop == false)
        {
            speed += .001f;
            if (speed >= .01f)
            {
                speed = .01f;
            }
            yield return new WaitForSeconds(4f);
        }
    }
    
}
