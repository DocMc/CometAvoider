﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    private static bool _isPaused = false;
    public static bool isPaused { get { return _isPaused; } }
    public Button pauseButton;

    private void Start()
    {
        pauseMenu.SetActive(false);
        pauseButton.enabled = false;
    }
    private void Update()
    {
        if (Player.playerAlive == true)
        {
            pauseButton.enabled = true;
        }
        else
        {
            pauseButton.enabled = false;
        }
    }
    public void Pause(bool gamePaused)
    {
        if (gamePaused)
        {
            _isPaused = true;
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
        if(!gamePaused)
        {
            _isPaused = false;
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
    }

}
