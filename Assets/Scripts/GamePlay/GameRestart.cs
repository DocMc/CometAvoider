﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameRestart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Restart());
    }
    // waits for credits to scroll threw then goes back to start game
    IEnumerator Restart ()
    {
        yield return new WaitForSecondsRealtime(4.8f);
        SceneManager.LoadScene(0);
    }
}
