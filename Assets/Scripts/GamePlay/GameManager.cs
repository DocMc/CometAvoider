﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    BuyButton buyButton;
    private void OnApplicationQuit()
    {
        Player player = FindObjectOfType<Player>();
        if (player != null)
            SaveSystem.SavePlayer(player);

        Caching.ClearCache();
    }
    private void OnApplicationPause(bool pause)
    {
        
        if (pause == true && IAPManager.Instance.IsInitialized() == false )
        {
            PauseGame();
        }
    }
    private void OnApplicationFocus(bool focus)
    {
        if (focus == false && IAPManager.Instance.IsInitialized() == false)
        {
            PauseGame();
        }
    }
    void PauseGame()
    {
        PauseMenu pauseMenu = FindObjectOfType<PauseMenu>();
        if (pauseMenu != null)
        {
            pauseMenu.Pause(true);
        }
    }
}
