﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class StartMenu : MonoBehaviour
{
    private void Start()
    {
        //SaveSystem.SavePlayer(FindObjectOfType<Player>());
        Destroy(FindObjectOfType<Player>());
        AudioManager audioManager = FindObjectOfType<AudioManager>();
        audioManager.Play("Theme");
    }
    // pressing start button
    public void PlayGame()
    {
        StartCoroutine(Play());
    }

    //going to make a quit button
    public void QuitGame()
    {
        Application.Quit();

    }
    //restart all stats i.e. hiscore and life 
    public void RestScore()
    {
    }
    //wait time to see the flash of the start button pressed
    IEnumerator Play()
    {
        yield return new WaitForSecondsRealtime(.1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
}
