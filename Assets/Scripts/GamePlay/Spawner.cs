﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Singleton<Spawner>
{
    Player player;
    public GameObject heart;
    public GameObject lifeSpawn;
    public GameObject shrink;
    public GameObject blockSpawn;
    private GameObject[] block;
    private GameObject[] shrinkTokken;
    private GameObject[] lifeTokken;
    private float minXValue = -2.17f, maxXValue = 1.54f;

    private float startSpawnWait = 1.5f;

    float minSpawnWait = .35f;
    float accelerationSpawnRate = .03f;
    float gravityAcceleration = 0.25f;

    float minGravity = -9f;
    private float startWait = .25f;

    private static bool _stop;
    public static bool stop { get { return _stop; } }
    [SerializeField]
    private float gravity = 1f;

    private int blockrestart = 0;
    private int powerupcountdown;


    int rand;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnBlocks());
        startWait = .25f;
        player = FindObjectOfType<Player>();

    }

    //checks if player is alive and if the 
    void Update()
    {
        block = GameObject.FindGameObjectsWithTag("Block");
        shrinkTokken = GameObject.FindGameObjectsWithTag("Shrink");
        lifeTokken = GameObject.FindGameObjectsWithTag("LifeTokken");
        if (Player.playerAlive == false)
        {
            _stop = true;
        }
        else
        {
            _stop = false;
        }
        if (Player.playerAlive == false && DeathMenu.continued == false)
        {
            for (int i = 0; i < block.Length; i++)
            {
                Destroy(block[i]);

            }
            for (int i = 0; i < shrinkTokken.Length; i++)
            {
                Destroy(shrinkTokken[i]);

            }
            for (int i = 0; i < lifeTokken.Length; i++)
            {
                Destroy(lifeTokken[i]);

            }
        }
        if (stop)
        {
            blockrestart = 1;
        }
        if (!stop && block.Length == 0 && blockrestart == 1)
        {
            blockrestart = 0;
            StartCoroutine(SpawnBlocks());
        }


    }
    IEnumerator SpawnBlocks()
    {
        yield return new WaitForSeconds(startWait);
        while (!stop)
        {
            startWait = 0;
            gravity = GetGravity();
            Vector3 spawnPosition = new Vector3(Random.Range(minXValue, maxXValue), 1f, 1);
            GameObject newblock = Instantiate(blockSpawn, spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            newblock.GetComponent<ConstantForce2D>().force = new Vector2(0, gravity);
            powerupcountdown++;
            /*if (gravity <= minGravity)
            {
                gravity = minGravity;
            }*/
            startSpawnWait = startSpawnWait - accelerationSpawnRate;
            if (startSpawnWait <= minSpawnWait)
            {
                startSpawnWait = minSpawnWait;
            }
            if (powerupcountdown == 5)
            {
                StartCoroutine(SpawnPowerUps());
                powerupcountdown = 0;
            }
            if (powerupcountdown == 0)
            {
                StopCoroutine(SpawnPowerUps());
            }
            yield return new WaitForSeconds(startSpawnWait);
        }

    }
    IEnumerator SpawnPowerUps()
    {
        IEnumerator spawner = SpawnPowerUps();
        yield return new WaitForSeconds(.4f);
        rand = Random.Range(0, 101);
        if (rand <= 1)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(minXValue, maxXValue), 1f, 1f);
            GameObject newextralife = Instantiate(lifeSpawn, spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            newextralife.GetComponent<ConstantForce2D>().force = new Vector2(0, gravity);
            yield return new WaitForSeconds(2);
            StopCoroutine(spawner);
        }
        else if (rand > 1 && rand < 60)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(minXValue, maxXValue), 1f, 1f);
            GameObject newShrink = Instantiate(shrink, spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            newShrink.GetComponent<ConstantForce2D>().force = new Vector2(0, gravity);
            yield return new WaitForSecondsRealtime(2);
            StopCoroutine(spawner);
        }
        else if (rand >= 60)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(minXValue, maxXValue), 1f, 1f);
            GameObject newHeart = Instantiate(heart, spawnPosition + transform.TransformPoint(0, 0, 0), gameObject.transform.rotation);
            newHeart.GetComponent<ConstantForce2D>().force = new Vector2(0, gravity);
            yield return new WaitForSeconds(2);
            StopCoroutine(spawner);
        }
        else
        {
            StopCoroutine(SpawnPowerUps());
            yield return null;
        }
    }
    float GetGravity()
    {
        float gravity = Mathf.Log((player.score + 1) * 8);
        gravity = gravity * -1;
        return gravity;
    }
}
    

    




