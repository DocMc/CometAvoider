﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiScoreManager : MonoBehaviour
{
    public Text highScoreText;
    int highScore;
    private void Start()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        highScore = data.hiScore;

        highScoreText.text = "HiScore:"+ highScore.ToString();
    }
}
