﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class DeathMenu : Singleton<DeathMenu>
{
    public static bool continued = true;

    public Animator fadeout;
    public GameObject deathMenuUI;
    public Button continueButton;
    public TextMeshProUGUI continueText;
    GameObject pauseButton;
    public Button watchAdButton;
    public TextMeshProUGUI watchAdText;
    string adText;
    private int lives;
    Player player;
    // Start is called before the first frame update
    void Start()
    {
        // making sure the deathMenu in disabled at the start
        deathMenuUI.SetActive(false);
        player = FindObjectOfType<Player>();
        pauseButton = GameObject.FindGameObjectWithTag("PauseButton");
        adText = watchAdText.text;
    }

    // Update is called once per frame
    void Update()
    {
        this.lives = player.lives; 
        if (Player.playerAlive == false && continued == true)
        {
            Time.timeScale = 0f;
            deathMenuUI.SetActive(true);
            pauseButton.SetActive(false);
        }
        if (lives < 1)
        {
            continueButton.gameObject.GetComponent<Button>().interactable = false;
            continueText.enabled = false;
        }
        else
        {
            continueButton.gameObject.GetComponent<Button>().interactable = true;
            continueText.enabled = true;
        }
        if(BuyButton.Instance.adWatched == false)
        {
            watchAdButton.interactable = true;
            watchAdText.text = adText;
        }
        else if (BuyButton.Instance.adWatched == true)
        {
            watchAdButton.interactable = false;
            watchAdText.text = "";
        }
    }
    public void Continue()
    {
        StartCoroutine(Replay());
    }
    // what pressing the No button does
    public void No()
    {
        Player.playerAlive = false;
        continued = false;
        GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().enabled = false;
        FindObjectOfType<AudioManager>().Play("PlayerDeath");
        deathMenuUI.SetActive(false);
        pauseButton.SetActive(false);
        Time.timeScale = 1f;
        LevelFader.loadlevel = 3;
    }

    IEnumerator Replay()
    {
        player.lives--;
        player.health = 3;
        yield return new WaitForSecondsRealtime(.3f);
        Player.playerAlive = true;
        continued = true;
        deathMenuUI.SetActive(false);
        FindObjectOfType<AudioManager>().Play("Engine");
        pauseButton.SetActive(true);
        Time.timeScale = 1f;
        
        StopCoroutine(Replay());
    }
}
