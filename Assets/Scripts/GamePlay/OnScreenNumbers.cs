﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OnScreenNumbers : MonoBehaviour
{
    private int lives;
    public Text LivesText;

    private int score;
    public Text ScoreText;
    private int hiscore;

    Player player;
    void Start()
    {
        player = FindObjectOfType<Player>();
        this.lives = player.lives;
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.text = "Score: " + player.score;
        LivesText.text = "Lives: " + player.lives;
    }
}
